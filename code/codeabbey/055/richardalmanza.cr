#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.3 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

words = Hash(String, Int32).new
oput = [] of String

args.each do |word|
  if words.has_key?(word)
    words[word] += 1
  else
    words[word] = 1
  end
end

words.each do |key, count|
  oput << key if count > 1
end

puts oput.sort.join " "

# $ ./richardalmanza.cr
# bec bes bip bop boq box byx daq deh dih dik diq dix dyh dyk gaf gat gex gih
# giq gix gok gut gyf gyh juk jus jut jyh jyq jys lax lif lih lok lot luh mah
# meq mes mif muq mut myq nit nuf rac raq roc rop ruc ruf vyh zep zes zyk zyx
