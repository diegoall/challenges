/**
>golint slayfer1112.go
**/
package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
)

func main() {
  dataIn, _ := os.Open("DATA.lst")
  dataScan := bufio.NewScanner(dataIn)
  for dataScan.Scan() {
    line := strings.Split(dataScan.Text(), " ")
    if len(line) > 1 {
      A, _ := strconv.Atoi(line[0])
      C, _ := strconv.Atoi(line[1])
      M, _ := strconv.Atoi(line[2])
      X0, _ := strconv.Atoi(line[3])
      N, _ := strconv.Atoi(line[4])
      var Nth int
      for j := 0; j < N; j++ {
        Nth = (A*X0 + C) % M
        X0 = Nth
      }
      fmt.Print(Nth, " ")
    }
  }
}

/*
>go run slayfer1112.go
286 0 2 401907 35 30 149439 10 57 647 38566 2129 83549 13339 6
*/
