#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun insertion (new-item tree)
  (let ((current-position)
        (parent-position)
        (aux))
    (setq tree (append tree (list new-item)))
    (setq current-position (- (length tree) 1))

    (loop while (> current-position 0) do
      (setq parent-position (floor (/ (- current-position 1) 2)))
      (when (< (elt tree current-position) (elt tree parent-position))
        (setq aux (elt tree parent-position))
        (setf (elt tree parent-position) (elt tree current-position))
        (setf (elt tree current-position) aux))
      (setq current-position parent-position)

      (if (= current-position 0)
        (return)))

    (return-from insertion tree)))

(defun deletion (tree)
  (let ((current-position)
        (aux-tree)
        (left-child-position)
        (right-child-position)
        (child-position)
        (aux))
    (setq current-position 0)
    (setf (elt tree 0) (elt tree (- (length tree) 1)))
    (setq aux-tree ())
    (loop for i from 0 to (- (length tree) 2) do
      (setq aux-tree (append aux-tree (list (elt tree i)))))
    (setq tree aux-tree)

    (loop while (< (+ (* 2 current-position) 1) (length tree)) do

      (setq left-child-position (+ (* 2 current-position) 1))
      (setq right-child-position (+ (* 2 current-position) 2))

      (when (or (> (elt tree current-position) (elt tree left-child-position))
              (> (elt tree current-position) (elt tree right-child-position)))

        (cond ((equal (numberp (nth (+ (* 2 current-position) 2) tree)) T)
                (if (< (elt tree right-child-position)
                      (elt tree left-child-position))
                    (setq child-position right-child-position)
                    (setq child-position left-child-position)))
              (T (setq child-position left-child-position)))

        (setq aux (elt tree current-position))
        (setf (elt tree current-position) (elt tree child-position))
        (setf (elt tree child-position) aux))

      (setq current-position child-position)

      (if (or (= current-position (- (length tree) 1))
            (> (+ (* 2 current-position) 1) (- (length tree) 1)))
          (return))

      (when (equal (numberp (nth (+ (* 2 current-position) 2) tree)) T)
        (if (and (> (elt tree (+ (* 2 current-position) 1))
              (elt tree current-position))
              (> (elt tree (+ (* 2 current-position) 2))
              (elt tree current-position)))
            (return)))

      (when (equal (nth (+ (* 2 current-position) 2) tree) NIL)
        (if (> (elt tree (+ (* 2 current-position) 1))
              (elt tree current-position))
            (return))))

    (return-from deletion tree)))

(defun binary-heap()
  (let ((tree)
        (data))
    (setq tree ())
    (setq data (first (cdr (read-data))))
    (dolist (item data)
      (if (/= item 0)
        (setq tree (insertion item tree))
        (setq tree (deletion tree))))
    (loop for i in tree do
      (format t "~s " i))))

(binary-heap)

#|
cat DATA.lst | clisp ludsrill.lsp
12 17 24 18 22 29 28 20 19 25 35 36 30 32 34 31 26 33 21
|#
