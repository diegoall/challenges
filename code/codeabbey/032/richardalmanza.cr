#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.74 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args.map {|x| x.to_i}

deathto = 0
soldiers = (1..args[0]).to_a

while soldiers.size > 1
  deathto = (deathto + args[1] - 1) % soldiers.size
  soldiers.delete_at(deathto)
end

puts soldiers[0]

# $ ./richardalmanza.cr
# 54
