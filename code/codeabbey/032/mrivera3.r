# $ lintr::lint('mrivera3.r')


josephus <- function() {
  data <- scan("DATA.lst")
  round_initial <-  c(1:data[1])
  counter <- 0
  prov <- round_initial
  i <- 0
  residual <- 0
  res <- c(round_initial)
  asd <- c()
  repeat {
    counter <- -residual
    prov <- res
    eliminated <- c()
    i <- 0
    prov <- c(asd, prov)
    while (counter <= length(res) - data[2]) {
      print(counter < length(res) - data[2])
      counter <- counter + data[2]
      eliminated <- c(eliminated, prov[data[2]])
      prov <- tail(prov, length(prov) - data[2])
      i <- i + 1
    }
    if (is.null(eliminated)) {
      residual2 <- data[2] - length(res) - residual
      break
    }
    else {
      residual <- length(res) - match(tail(eliminated, n = 1), res)
    }
    eliminated <- eliminated[!is.na(eliminated)]
    res <- res[!res %in% eliminated]
    if (residual != 0) {
      asd <- c(1:residual)
    }
    else {
      asd <- c()
    }
  }
  residual <- residual2
  while (1 < length(res)) {
    print(res)
    eliminated <- abs(-residual + data[2] %% length(res))
    if (eliminated == 0) {
      residual <- 0
      res <- head(res, length(res) - 1)
    } else {
      residual <- length(res) - eliminated
      res <- res[-eliminated]
    }
  }
  print(res)
}
josephus()

# $ Rscript mrivera3.r
# 54
