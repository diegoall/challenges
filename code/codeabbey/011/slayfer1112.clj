;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(defn get-vals [line]
  (let [values (str/split line #" ")
        a (read-string (values 0))
        b (read-string (values 1))
        c (read-string (values 2))]
    [a b c]))

(defn sum-digits [arr len counter rec]
  (if (< counter len)
    (let [res (+ rec (read-string (arr counter)))]
      (sum-digits arr len (inc counter) res))
    rec))

(defn solution [_ body]
  (doseq [x body]
    (let [[a b c] (get-vals x)
          res (str (+ (* a b) c))
          res-arr (str/split res #"")
          rec (sum-digits res-arr (count res-arr) 0 0)]
      (print (str rec " ")))))

(defn main []
  (let [[head body] (get-data)]
    (solution head body)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 18 20 14 27 22 34 15 16 20 30 15 11 4 14 15
