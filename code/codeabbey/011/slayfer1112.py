# $ mypy --strict slayfer1112.py
# Success: no issues found in 1 source file

from typing import List

a: int = int(input())

for i in range(a):
    b: List[str] = input().split()
    res: int = 0

    sol: int = int(b[0]) * int(b[1]) + int(b[2])

    arr: str = str(sol)

    for digit in arr:
        res += int(digit)

    print(res, end=" ")
print()

# $ cat DATA.lst | python3 slayfer1112.py
# 18 20 14 27 22 34 15 16 20 30 15 11 4 14 15
