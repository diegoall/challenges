;; $ clj-kondo --lint andresclm.clj
;; linting took 19ms, errors: 0, warnings: 0

(ns andresclm
  (:require [clojure.string :as str]))

(defn get-data-from [file]
  (let [data (slurp file)
        data' (str/split data #"\n")
        data'' (rest data')]
    (str/split (nth data'' 0) #" ")))

(defn get-range [n]
  (->> n inc (range 1) vec))

(defn primes-to [n]
  (filter (fn [number]
            (loop [end (int (Math/sqrt number)), div 2, re (rem number div)]
              (cond
                (< number 2) false
                (= number 2) true
                (= re 0) false
                (> div end) true
                :else (recur end (inc div) (rem number div)))))
          (get-range n)))

(defn -main "Entry point" []
  (let [data (get-data-from "DATA.lst")
        primes (primes-to 2750159)
        results (map (fn [position] (nth primes (- (Integer. position) 1)))
                     data)]
    (apply println results)))

;; $ clj -m andresclm
;; 2529089 2018167 2543621 1800619 1955113 2491117 2684933 1422089 2464349
;; 1370053 2224753 1761883
