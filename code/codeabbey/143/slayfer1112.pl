# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
our ($VERSION) = 1;

use Readonly;
use POSIX;

my $EMPTY = q{};
my $SPACE = q{ };

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data;
}

sub euclidean {
  my @args  = @_;
  my $x     = $args[0];
  my $y     = $args[1];
  my $sprev = 1;
  my $scur  = 0;
  my $tprev = 0;
  my $tcur  = 1;
  my $r     = $x % $Y;

  while ( $r <=> 0 ) {
    my $q = floor $x / $y;
    $x = $y;
    $y = $r;
    $r = $x % $y;
    my $snext = $sprev - $q * $scur;
    my $tnext = $tprev - $q * $tcur;
    $sprev = $scur;
    $tprev = $tcur;
    $scur  = $snext;
    $tcur  = $tnext;
  }
  my $a = $scur;
  my $b = $tcur;
  return $a, $b;
}

sub gcd_calculator {
  my @args = @_;
  my $val1 = $args[0];
  my $val2 = $args[1];
  while ( ( $val1 <=> $val2 ) ) {
    if ( $val1 < $val2 ) {
      $val2 = $val2 - $val1;
    }
    else {
      $val1 = $val1 - $val2;
    }
  }
  return $val1;
}

sub solution {
  my @args = @_;
  my @vals = split $SPACE, $args[0];
  my $val1 = $vals[0];
  my $val2 = $vals[1];
  my $gcd  = gcd_calculator( $val1, $val2 );
  my ( $a, $b ) = euclidean( $val1, $val2 );
  exit 1 if !print "$gcd $a $b ";
  return 'Sometimes I took more time with perlcritic than the code itself';
}

sub main {
  my @vals = data_in();
  for ( 1 .. $#vals ) {
    solution $vals[$_];
  }
  exit 1 if !print "\n";
  return 'I think that is curious to use for instead foreach';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 1 26521 -18561
# 1 -25313 12007
# 1 5491 -10079
# 1 12315 -12073
# 4 5621 -2311
# 1 17949 -19906
# 3 12226 -4983
# 1 -28961 29712
# 1 -15379 20530
# 1 -1855 2686
# 1 1909 -1883
# 7 -3373 2246
# 1 -13114 18355
# 1 -5903 9083
# 1 3791 -36240
# 11 -53 76
# 2 -1398 1217
# 1 -8176 35789
# 1 7028 -1159
# 2 -3003 8044
# 1 -4886 5931
# 1 -14049 14470
# 1 -5735 2156
# 5 4829 -3145
# 1 430 -1563
# 1 6983 -7955
# 1 19420 -13593
