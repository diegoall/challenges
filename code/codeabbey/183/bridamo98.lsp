#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun delimiterp (c)
  (char= c #\Space)
)

(defun split (string &key (delimiterp #'delimiterp))
  (loop
    :for beg = (position-if-not delimiterp string)
    :then (position-if-not delimiterp string :start (1+ end))
    :for end = (and beg (position-if delimiterp string :start beg))
    :when beg :collect (subseq string beg end)
    :while end
  )
)

(defun initialize-board(list-state board)
  (let ((i 1))
    (loop
      (if (not (string= (nth i list-state) "0"))
        (progn
          (loop for j from 0 to (- (length (nth i list-state)) 1)
            do(setf (aref board  (- 7 j) (- i 1))
              (subseq (nth i list-state) j (+ j 1)))
          )
        )
      )
      (setq i (+ i 1))
      (when (> i 9)(return NIL))
    )
  )
)

(defun winner(board)
  (let ((i 0)(j 3)(i2 0)(j2 0)(last-chip "-")(last-cont 0))
    (loop
      (setq i2 i)
      (setq j2 j)
      (loop
        (if (and (string= (aref board i2 j2) last-chip)
        (not (string= (aref board i2 j2) "-")))
          (progn
            (setq last-cont (+ last-cont 1))
            (if (= last-cont 4)
              (return-from winner (aref board i2 j2))
            )
          )
          (progn
            (if (not (string= (aref board i2 j2) "-"))
              (setq last-cont 1)
              (setq last-cont 0)
            )
          )
        )
        (setq last-chip (aref board i2 j2))
        (setq i2 (+ i2 1))
        (setq j2 (- j2 1))
        (when (or (< j2 0)(> i2 7))(return NIL))
      )
      (setq last-cont 0)
      (setq last-chip "-")
      (if (> (+ j 1) 8)
        (setq i (+ i 1))
        (setq j (+ j 1))
      )
      (when (or (> i 4)(> j 8))(return NIL))
    )
    (setq i 0)
    (setq j 5)
    (setq i2 0)
    (setq j2 0)
    (setq last-chip "-")
    (setq last-cont 0)
    (loop
      (setq i2 i)
      (setq j2 j)
      (loop
        (if (and (string= (aref board i2 j2) last-chip)
        (not (string= (aref board i2 j2) "-")))
          (progn
            (setq last-cont (+ last-cont 1))
            (if (= last-cont 4)
              (return-from winner (aref board i2 j2))
            )
          )
          (progn
            (if (not (string= (aref board i2 j2) "-"))
              (setq last-cont 1)
              (setq last-cont 0)
            )
          )
        )
        (setq last-chip (aref board i2 j2))
        (setq i2 (+ i2 1))
        (setq j2 (+ j2 1))
        (when (or (> j2 8)(> i2 7))(return NIL))
      )
      (setq last-cont 0)
      (setq last-chip "-")
      (if (< (- j 1) 0)
        (setq i (+ i 1))
        (setq j (- j 1))
      )
      (when (or (> i 4)(< j 0))(return NIL))
    )
    (setq i 0)
    (setq j 0)
    (setq i2 0)
    (setq j2 0)
    (setq last-chip "-")
    (setq last-cont 0)
    (loop
      (setq i2 i)
      (setq j2 j)
      (loop
        (if (and (string= (aref board i2 j2) last-chip)
        (not (string= (aref board i2 j2) "-")))
          (progn
            (setq last-cont (+ last-cont 1))
            (if (= last-cont 4)
              (return-from winner (aref board i2 j2))
            )
          )
          (progn
            (if (not (string= (aref board i2 j2) "-"))
              (setq last-cont 1)
              (setq last-cont 0)
            )
          )
        )
        (setq last-chip (aref board i2 j2))
        (setq j2 (+ j2 1))
        (when (> j2 8)(return NIL))
      )
      (setq last-cont 0)
      (setq last-chip "-")
      (setq i (+ i 1))
      (when (> i 7)(return NIL))
    )
    (setq i 0)
    (setq j 0)
    (setq i2 0)
    (setq j2 0)
    (setq last-chip "-")
    (setq last-cont 0)
    (loop
      (setq i2 i)
      (setq j2 j)
      (loop
        (if (and (string= (aref board i2 j2) last-chip)
        (not (string= (aref board i2 j2) "-")))
          (progn
            (setq last-cont (+ last-cont 1))
            (if (= last-cont 4)
              (return-from winner (aref board i2 j2))
            )
          )
          (progn
            (if (not (string= (aref board i2 j2) "-"))
              (setq last-cont 1)
              (setq last-cont 0)
            )
          )
        )
        (setq last-chip (aref board i2 j2))
        (setq i2 (+ i2 1))
        (when (> i2 7)(return NIL))
      )
      (setq last-cont 0)
      (setq last-chip "-")
      (setq j (+ j 1))
      (when (> j 8)(return NIL))
    )
  )
)

(defun utility(pos-winner)
  (let ((uti (make-array 2)))
    (if pos-winner
      (progn
        (if (string= pos-winner "1")
          (setf (aref uti 0) 2)
          (setf (aref uti 0) 0)
        )
      )
      (setf (aref uti 0) 1)
    )
    (return-from utility uti)
  )
)

(defun board-is-full(board)
  (let ((i 0) (j 0))
    (loop
      (loop
        (if (string= (aref board i j) "-")
          (return-from board-is-full 0)
        )
        (setq j (+ j 1))
        (when (> j 8)(return NIL))
      )
      (setq j 0)
      (setq i (+ i 1))
      (when (> i 7)(return-from board-is-full 1))
    )
  )
)

(defun copy-array(board)
  (let ((copy (make-array '(8 9) :initial-element "-")) (i 0)(j 0))
    (loop
      (loop
        (setf (aref copy i j) (aref board i j))
        (setq j (+ j 1))
        (when (> j 8)(return NIL))
      )
      (setq j 0)
      (setq i (+ i 1))
      (when (> i 7)(return-from copy-array copy))
    )
  )
)

(defun set-move(copy current-player position)
  (let ((i 0)(ant ""))
    (loop
      (if (and (string= ant "-") (not (string= (aref copy i position) "-")))
        (progn
          (setf (aref copy (- i 1) position) current-player)
          (return-from set-move NIL)
        )
      )
      (setq ant (aref copy i position))
      (setq i (+ i 1))
      (when (> i 7)(return NIL))
    )
    (setf (aref copy 7 position) current-player)
    (return-from set-move NIL)
  )
)

(defun min-value-action(copy depth)
  (let ((min-uti (make-array 2))(uti (make-array 2))(ite 0)
  (pos-winner (winner copy)))
    (if (or pos-winner (= (board-is-full copy) 1) (> depth max-depth))
      (return-from min-value-action (utility pos-winner))
    )
    (setf (aref min-uti 0) 100)
    (loop
      (if (string= (aref copy 0 ite) "-")
        (progn
          (setq board-copy (copy-array copy))
          (set-move board-copy "2" ite)
          (setq uti (max-value-action board-copy (+ depth 1)))
          (if (> (aref min-uti 0) (aref uti 0))
            (progn
              (setf (aref min-uti 0) (aref uti 0))
              (setf (aref min-uti 1) ite)
              (if (= (aref min-uti 0) 0)
                (return-from min-value-action min-uti)
              )
            )
          )
        )
      )
      (setq ite (+ ite 1))
      (when (> ite 8)(return-from min-value-action min-uti))
    )
  )
)

(defun max-value-action(copy depth)
  (let ((max-uti (make-array 2))(uti (make-array 2))(ite 0)
  (pos-winner (winner copy)))
    (if (or pos-winner (= (board-is-full copy) 1) (> depth max-depth))
      (return-from max-value-action (utility pos-winner))
    )
    (setf (aref max-uti 0) (- 0 100))
    (loop
      (if (string= (aref copy 0 ite) "-")
        (progn
          (setq board-copy (copy-array copy))
          (set-move board-copy "1" ite)
          (setq uti (min-value-action board-copy (+ depth 1)))
          (if (< (aref max-uti 0) (aref uti 0))
            (progn
              (setf (aref max-uti 0) (aref uti 0))
              (setf (aref max-uti 1) ite)
              (if (= (aref max-uti 0) 2)
                (return-from max-value-action max-uti)
              )
            )
          )
        )
      )
      (setq ite (+ ite 1))
      (when (> ite 8)(return-from max-value-action max-uti))
    )
  )
)

(defvar max-depth 4)
(defparameter board (make-array '(8 9) :initial-element "-"))
(defvar util (make-array 2))
(defvar move 0)
(defvar flag NIL)
(defvar param NIL)
(defvar tk (read-line))
(defvar host "http://open-abbey.appspot.com/interactive/connect4")
(defvar token (cons "token" tk))
(defvar input "")

(setq param (list token))

(defvar list-state (split input))
(initialize-board list-state board)
(setq util (max-value-action board 0))
(set-move board "1" (aref util 1))
(setq param (list token (cons "move" (write-to-string (aref util 1)))))

(loop
  (format t "~a" input)
  (setq move (parse-integer (subseq input 6 7)))
  (set-move board "2" move)
  (setq util (max-value-action board 0))
  (set-move board "1" (aref util 1))
  (setq param (list token (cons "move" (write-to-string (aref util 1)))))
  (when (string= (subseq input 0 4) "end")(return NIL))
)

(format t "~a" input)

#|
  cat DATA.lst | clisp bridamo98.lsp
  state: 0 1 0 0 0 0 2 0 0
  move: 2
  state: 1 1 2 0 0 0 2 0 0
  move: 1
  state: 11 12 2 0 0 0 2 0 0
  move: 0
  state: 1112 12 2 0 0 0 2 0 0
  move: 4
  state: 11121 12 2 0 2 0 2 0 0
  move: 7
  state: 11121 12 2 1 2 0 2 2 0
  move: 1
  state: 11121 122 2 1 2 1 2 2 0
  move: 6
  state: 111211 122 2 1 2 1 22 2 0
  move: 0
  state: 11121112 122 2 1 2 1 22 2 0
  move: 5
  state: 11121112 1221 2 1 2 12 22 2 0
  move: 1
  state: 11121112 12212 2 11 2 12 22 2 0
  move: 7
  state: 11121112 122121 2 11 2 12 22 22 0
  move: 6
  state: 11121112 122121 2 11 21 12 222 22 0
  move: 5
  state: 11121112 122121 2 11 21 122 2221 22 0
  move: 5
  state: 11121112 122121 21 11 21 1222 2221 22 0
  end: Egz6L7psy+iqKU4bnTGRGTQ1
  message: You win!
|#
