(defun read-data (&optional (read-line))
    (let (*read-eval*)
      (loop :for line = (read-line nil nil)
            :while line
            :collect (read-from-string (concatenate 'string "(" line ")")))))


(setq data (cdr (read-data)))
(dolist (item data)
  (print (second (sort item #'<))))


;;$cat DATA.lst | clisp paolagiraldo.lsp
;;728  282  1393  83  623  9  9  1465  82  20  136  5  54  19  895  491  99
;;356 74  842  98  10  71  75  44  311  495  37  631  300
