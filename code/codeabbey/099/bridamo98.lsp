#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun deg-to-rad (degrees)
  (* pi (/ degrees 180.0))
)

(defun pos-y(vi alpha ti)
  (- (* vi (* (sin (deg-to-rad alpha)) ti)) (/ (* 9.81 (expt ti 2)) 2.0))
)

(defun get-time(vi alpha pos-x)
  (/ pos-x (* vi (cos (deg-to-rad alpha))))
)

(defun get-input-data()
  (loop for x from 0 to 39
    do(setf (aref first-problem x) (read))
  )
  (loop for x from 0 to 5
    do(setf (aref first-problem-parameters x) (read))
  )

  (loop for x from 0 to 39
    do(setf (aref second-problem x) (read))
  )
  (loop for x from 0 to 5
    do(setf (aref second-problem-parameters x) (read))
  )

  (loop for x from 0 to 39
    do(setf (aref third-problem x) (read))
  )
  (loop for x from 0 to 5
    do(setf (aref third-problem-parameters x) (read))
  )
)

(defun find-solution (problem velocity angle)
  (setq delta-x 0.1)
  (setq xi 0.0)
  (setq ti 0)
  (setq pyi 0)
  (loop
    (setq ti (get-time velocity angle xi))
    (setq pyi (pos-y velocity angle ti))
    (if (< pyi (* (aref problem (nth-value 0 (floor xi 4.0))) 4))
      (progn
        (format t "~a " (nth-value 0 (floor xi 1.0)))
        (setq xi 160.0)
      )
    )
    (setq xi (+ xi delta-x))
    (when (> xi 160.0)(return NIL))
  )
)

(defun solve-problem(problem parameters)
  (find-solution problem (aref parameters 0) (aref parameters 1))
  (find-solution problem (aref parameters 2) (aref parameters 3))
  (find-solution problem (aref parameters 4) (aref parameters 5))
)

(defparameter first-problem (make-array 40))
(defparameter second-problem (make-array 40))
(defparameter third-problem (make-array 40))
(defparameter first-problem-parameters (make-array 6))
(defparameter second-problem-parameters (make-array 6))
(defparameter third-problem-parameters (make-array 6))

(get-input-data)

(solve-problem first-problem first-problem-parameters)
(solve-problem second-problem second-problem-parameters)
(solve-problem third-problem third-problem-parameters)

#|
  cat DATA.lst | clisp bridamo98.lsp
  96 68 120 60 88 120 97 68 76
|#
