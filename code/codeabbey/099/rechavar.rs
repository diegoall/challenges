/*
$ rustfmt rechavar.rs
$ rustc rechavar.rs
*/
use std::io::{self, Read};

fn get_t(x: f32, v0: f32, alpha: f32) -> f32 {
  x / (v0 * alpha.to_radians().cos())
}

fn get_y(v0: f32, alpha: f32, t: f32) -> f32 {
  (t * v0 * (alpha.to_radians().sin())) - (9.81 * (t.powf(2.0)) * 0.5)
}
fn get_x(v0: f32, alpha: f32, t: f32) -> f32 {
  t * (v0 * alpha.to_radians().cos())
}

fn get_t_from_y(a: f32, b: f32, c: f32) -> f32 {
  ((-b) + ((b.powf(2.0)) - (4.0 * a * c)).sqrt()) / (2.0 * a)
}

fn start_simulation(uphill: Vec<f32>, shoot: Vec<f32>) -> f32 {
  let mut x_dist = 0.0;
  for i in 0..uphill.len() - 1 {
    x_dist = x_dist + 4.0;
    let t = get_t(x_dist, shoot[0], shoot[1]);
    let y = get_y(shoot[0], shoot[1], t);

    if uphill[i] * 4.0 > y {
      let t = get_t_from_y(
        0.5 * 9.81,
        -(shoot[0] * (shoot[1].to_radians().sin())),
        uphill[i] * 4.0,
      );
      x_dist = get_x(shoot[0], shoot[1], t);
      break;
    } else if uphill[i + 1] * 4.0 > y {
      break;
    }
  }

  x_dist.floor()
}

fn main() -> io::Result<()> {
  let mut buffer = String::new();
  io::stdin().read_to_string(&mut buffer)?;
  let mut info: Vec<&str> = buffer.split("\n").collect();
  info.pop();
  let vec: Vec<usize> = [0, 4, 8].to_vec();
  let mut x = Vec::new();
  for i in vec {
    for j in 1..4 {
      let uphill: Vec<f32> = info[i]
        .split(" ")
        .map(|x| x.trim().parse::<f32>().unwrap())
        .collect();
      let shoot: Vec<f32> = info[j + i]
        .split(" ")
        .map(|x| x.trim().parse::<f32>().unwrap())
        .collect();
      x.push(start_simulation(uphill, shoot));
    }
  }
  println!("x:{:?}", x);
  Ok(())
}

/*
$ cat DATA.lst | ./rechavar
96.0, 68.0, 120.0, 60.0, 88.0, 120.0, 97.0, 68.0, 76.0
  */
