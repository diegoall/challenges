let inp=[["A#";"5"];["A#";"4"];["C#";"4"];["B";"4"];["E";"2"];["A#";"3"];["F";"4"];["C";"3"];["C";"2"];["A";"4"];["B";"3"];["G#";"5"];["F";"2"];["A#";"2"];["D#";"5"];["C#";"5"];["E";"4"];["F#";"5"];["D";"4"];["D#";"1"];["D";"1"];["F";"1"]];;
for i=0 to List.length inp -1 do
    let exp= ref 0.0 in
    let note=(List.nth (List.nth inp i) 0) in
    if note = "C" then     (exp:=1.0/.(2.0**(9.0/.12.0)))else if note = "C#" then (exp:=1.0/.(2.0**(8.0/.12.0))) else if note = "D" then (exp:=1.0/.(2.0**(7.0/.12.0)))else if note = "D#" then (exp:=1.0/.(2.0**(6.0/.12.0))) else if note = "E" then (exp:=1.0/.(2.0**(5.0/.12.0))) else if note = "F" then (exp:=1.0/.(2.0**(4.0/.12.0)))else if note = "F#" then (exp:=1.0/.(2.0**(3.0/.12.0)))
    else if note = "G" then (exp:=1.0/.(2.0**(2.0/.12.0))) else if note = "G#" then (exp:=1.0/.(2.0**(1.0/.12.0))) else if note = "A" then (exp:=1.0)else if note = "A#" then (exp:=2.0**(1.0/.12.0))else if note = "B" then (exp:=2.0**(2.0/.12.0));
    let res= floor((!exp*.(440.0/.(2.0**(float_of_int (4-(int_of_string (List.nth (List.nth inp i) (List.length (List.nth inp i) -1)))))) )) +. 0.5) in    
    print_int (int_of_float res);
    print_string " ";
done;
 
