# frozen_string_literal: true

# Inspecting 1 file
#                  .
#
#                      1 file inspected, no offenses detected

# frozen_string_literal: true

def load_ipdb
  ipdb_file = 'db-ip.txt'
  ipdb_data = []
  f_ipdb = File.open(ipdb_file, 'r')
  f_ipdb.each_line { |data| ipdb_data.push(data.strip.split) }
  ipdb_data
end

def load_unknown_ipdb
  unknown_ipdb_file = 'DATA.lst'
  unknown_ipdb_data = []
  f_uipdb = File.open(unknown_ipdb_file, 'r')
  f_uipdb.each_line { |data| unknown_ipdb_data.push(data.strip) }
  unknown_ipdb_data
end

def inrange?(ip_list, middle, ip2locate)
  range_start = ip_list[middle][0].to_i(36)
  range_end = range_start + ip_list[middle][1].to_i(36)
  true if ip2locate >= range_start && ip2locate <= range_end
end

def outrange?(ip_list, mid, ip2locate)
  range_start = ip_list[mid][0].to_i(36)
  range_end = range_start + ip_list[mid][1].to_i(36)
  if ip2locate > range_start && ip2locate > range_end
    true
  else
    false
  end
end

# The propoused binary search algo asumes
# there are no duplicate and is elements
# sorted in ip-db.txt

def bin_search(ip_list, ip2locate)
  left = 0
  right = ip_list.length - 1
  while left <= right
    mid = left + (right - left) / 2
    return ip_list[mid][2] if inrange?(ip_list, mid, ip2locate)

    outrange?(ip_list, mid, ip2locate) ? left = mid + 1 : right = mid - 1
  end
end

def locate_ip
  ipdb_data = load_ipdb
  unknown_ipdb_data = load_unknown_ipdb
  unknown_ipdb_data.each_with_index do |ip2locate, index|
    next if index.zero?

    print bin_search(ipdb_data, ip2locate.to_i(36))
    print "\t"
  end
end

locate_ip

# /usr/bin/ruby /root/RubymineProjects/g4cc3p-170/g4cc3p.rb
# GB PK FR AU ZA US AU CN BH ... HR US BR US RU JP DE US FR
# Process finished with exit code 0
