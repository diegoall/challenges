#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun find-gcd(fst scd)
  (let ((md (mod fst scd)))
    (if (= md 0)
      (return-from find-gcd scd)
      (return-from find-gcd (find-gcd scd md))
    )
  )
)

(defun find-gcd-and-lcm ()
  (let ((fst (read)) (scd(read)) (i-lcm 0) (i-gcd 0))
    (if (> fst scd)
      (setq i-gcd (find-gcd fst scd))
      (setq i-gcd (find-gcd scd fst))
    )
    (format t "(~a ~a) " i-gcd (/ (* fst scd) i-gcd))
  )
)

(defun solve-all(size-input)
  (loop for i from 0 to (- size-input 1)
    do(find-gcd-and-lcm)
  )
)

(defvar size-input (read))
(solve-all size-input)

#|
  cat DATA.lst | clisp bridamo98.lsp
  (1 23751) (2 47286) (1860 37200) (52 102492) (11 13728)
  (71 191913) (2 1786) (78 323544) (28 100492) (108 19656)
  (52 146224) (275 22000) (2 1012) (5 190) (1 602) (8 824)
  (62 335916) (44 11704) (4 12) (1 2577120)
|#
