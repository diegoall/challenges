% $ erlc -Werror andresclm.erl
% andresclm.beam

-module(andresclm).

-export([start/0]).

-compile(andresclm).

start() ->
  [A,B,C] = read_file("DATA.lst"),
  {P, _} = string:to_integer(A),
  {AnnualR, _} = string:to_integer(B),
  R = (AnnualR/100)/12,
  {L, _} = string:to_integer(C),
  Result = erlang:ceil(P*((R*math:pow(1+R,L))/(math:pow(1+R,L)-1))),
  io:format("~B\n", [Result]).

read_file(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:lexemes(string:trim(erlang:binary_to_list(Binary),trailing,"\n")," ").

% $ erl -noshell -s andresclm -s init stop
% 12765
