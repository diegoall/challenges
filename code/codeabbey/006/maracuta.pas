program maracutarounding; {Nombre del programa}

uses
  SysUtils;

var
  Num2, control, x, y, res: longint; {Declaro variables como enteras}

  vector: array [1..100] of longint;

begin

  {Inicializo variables para prevenir error de compilacion}
  Num2 := 0;
  res := 0;
  control := 0;

  {Solicito variable de control}
  writeLn('Introduzca la variable de control: ');
  readLn(control);



  writeLn('Escriba los pares de numeros separados por espacio:');
  for x := 1 to control do
  begin
    y := 2;
    repeat
      if(y=2) then
      begin
      Read(Num2);
      res := Num2;
      y := y - 1;
      end

      else
      begin
           Read(Num2);
           y := y - 1;
           vector[x] := Round(res/Num2);
      end;

    until y = 0;


    res := 0;

  end;

  for x := 1 to control do
  begin
    Write(vector[x]);
    Write(' ');
  end;

  readLn();
  readLn();

end.
