#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun rounding ()
  (let ((data)
        (divide)
        (round-floor))
    (setq data (cdr (read-data)))
    (dolist (item data)
      (setq divide (/ (first item) (second item)))
      (setq round-floor (floor (first item) (second item)))
      (if (>= (- divide round-floor) 0.5)
        (format t "~s " (ceiling (first item) (second item)))
        (format t "~s " round-floor)))))

(rounding)

#|
cat DATA.lst | clisp ludsrill.lsp
3 6 3 16040 -2 14 17320 13 31766 18 18 3 5 41470 19 37201 8 17622
9 17 10 -8 2 18 18 9 8 21 18 5 22
|#
