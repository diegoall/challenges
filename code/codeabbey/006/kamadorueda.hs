-- $ ghc -o kedavamaru kedavamaru.hs
--   [1 of 1] Compiling Main ( kedavamaru.hs, kedavamaru.o )
--   Linking code ...
-- $ hlint kedavamaru.hs
--   No hints

module Main where

  import System.IO
  import Control.Monad

  transform :: Int -> Int -> Int
  transform a b = round (fromIntegral a / fromIntegral b)

  processfile :: Handle -> IO ()
  processfile ifile =
    do
      iseof <- hIsEOF ifile
      Control.Monad.unless iseof $
        do
          line <- hGetLine ifile
          let vector_ints = map read $ words line :: [Int]
          let a = head vector_ints
          let b = vector_ints!!1
          let c = transform a b
          print c
          processfile ifile

  main =
    do
      ifile <- openFile "DATA.lst" ReadMode
      line <- hGetLine ifile
      processfile ifile
      hClose ifile

-- $ ./kedavamaru
--   2
--   5
--   4
--   91528
--   8
--   -4
--   18117
--   30841
--   -1
--   -19
--   6
--   -90
--   10
--   10
--   13
--   4
--   7
