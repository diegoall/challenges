#! /usr/bin/crystal
# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 1.36 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

fluid_code = true

vowels = "aeiouyAEIOUY"
oput = [] of Int32

if fluid_code
  args = File.read("DATA.lst").split("\n", remove_empty: true)
else
  args = ARGV.map { |x| x.split("\n", remove_empty: true) }.flatten
end

args[1..].map { |x| oput << x.count(vowels) }

puts oput.join(" ")

# $ ./richardalmanza.cr --
# 10 11 15 14 17 11 10 12 6 6 5 8 11 10 16 9
