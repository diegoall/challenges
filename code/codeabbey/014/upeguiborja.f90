! # Lint
! $ fortran-linter --syntax-only --linelength 80 --verbose upeguiborja.f90
! Checking upeguiborja.f90
! # Compile
! $ gfortran -Wall -pedantic-errors -Werror upeguiborja.f90 -o upeguiborja

module list_module
  implicit none
  private

  type, public :: ListNode
    type(ListNode), pointer :: next => null()
    integer :: n
    character :: op
  end type ListNode

  type, public :: List
    type(ListNode), pointer :: head => null()
    type(ListNode), pointer :: tail => null()

    contains
      procedure :: append
  end type List

contains
  subroutine append(this, n, op)
    class(List), intent(inout) :: this
    integer, intent(in) :: n
    character, intent(in) :: op

    type(ListNode), pointer :: new_node

    allocate(new_node)
    new_node % n = n
    new_node % op = op
    new_node % next => null()

    if (.not. associated(this % head)) then
      this % head => new_node
      this % tail => new_node
    else
      this % tail % next => new_node
      this % tail => new_node
    end if

  end subroutine append
end module list_module

! Did all this to avoid handling very large numbers to avoid overflow
! and optimized using the distributive property of the modulo operator
program modular_calculator
  use list_module

  implicit none

  type(List) :: operations
  type(ListNode), pointer :: element
  integer :: n, result
  character :: op = "$"

  read(*, *) n
  call operations % append(n, op)

  read(*, "(a1, i5)") op, n ! all numbers are leq than 10000

  do while (op /= "%")
    if ((op /= "+") .and. (op /= "*")) then
      call exit(1)
    end if

    call operations % append(n, op)

    read(*, "(a1, i5)") op, n
  end do

  element => operations % head
  result = modulo(element % n, n)
  element => element % next

  do while ( associated(element) )
    if (element % op == "+") then
      result = modulo(result + modulo((element % n), n), n)
    else if (element % op == "*") then
      result = modulo(result * modulo((element % n), n), n)
    end if
    element => element % next
  end do

  write(*, "(i0)") result

  stop
end program modular_calculator

! $ cat DATA.lst | ./upeguiborja
! 2412
