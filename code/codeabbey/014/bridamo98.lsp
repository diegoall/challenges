#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun solve-problem(first-number)
  (let ((result first-number) (flag 0) (value NIL)(operator NIL))
    (loop
      (setq operator (read))
      (if (or (string= operator "*")(string= operator "+"))
        (progn
          (setq value (read))
          (if (string= operator "*")
            (setq result (* result value))
            (setq result (+ result value))
          )
        )
        (progn
          (setq flag 1)
          (setq value (read))
          (setq result (mod result value))
        )
      )
      (when (= flag 1)(return-from solve-problem result))
    )
  )
)

(defvar first-number (read))
(format t "~a" (solve-problem first-number))

#|
  cat DATA.lst | clisp bridamo98.lsp
  2178
|#
