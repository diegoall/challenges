;; $ racket -I typed/racket
;; $ raco exe --gui iral.rkt

#lang typed/racket

(define data (open-output-file "DATA.lst"))

(for ([i (in-range (read (string-split (read-line data))))]))
  (printf ~A  (min (read) (read) (read))))

(close-output-port data)

;; ./iral
;; 107814 386086 -5721210 -3265497 -9006366 124682
;; -455274 -4695046 -2027624 -2726599 6289004 -2232700
;; -5564619 -9844812 -3110310 -2335335 -9253696 -9053831
;; -3981282 -2784460 -1406274 -8164135 3309434
