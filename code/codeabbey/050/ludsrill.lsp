#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(defvar *phrase*)
(defvar *reverse-phrase*)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
  (loop for line = (read-line NIL NIL)
    while line
    collect line)))

(setq *data* (cdr (read-data)))

(dolist (*phrase* *data*)
  (setq *phrase* (remove #\Space *phrase*))
  (setq *phrase* (remove #\, *phrase*))
  (setq *phrase* (remove #\- *phrase*))
  (setq *phrase* (string-downcase *phrase*))

  (setq *reverse-phrase* (reverse *phrase*))

  (if (string= *phrase* *reverse-phrase*)
    (format t "Y ")
    (format t "N ")))

#|
$ cat DATA.lst | clisp ludsrill.lsp
Y Y Y N N N N N Y Y Y N Y N Y
|#
