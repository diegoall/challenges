/*
$ cargo clippy
Checking code_124 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.10s
$ cargo build
Compiling code_124 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.30s
$ rustc sullenestd1.rs
*/

use std::cmp;
use std::io::{self, BufRead};

fn print_index(
  k: Vec<Vec<i64>>,
  values: Vec<i64>,
  weights: Vec<i64>,
  w_max: i64,
  n: i64,
) {
  let mut indexes: Vec<i64> = Vec::new();
  let mut result = k[n as usize][w_max as usize];
  let mut j = w_max;
  for i in (0..n).rev() {
    if result <= 0 {
      break;
    }
    if result != k[(i - 1) as usize][j as usize] {
      indexes.push(i - 1);
      result -= values[(i - 1) as usize];
      j -= weights[(i - 1) as usize];
    }
  }
  indexes.reverse();
  for i in indexes {
    println!("{}", i);
  }
}

fn knapsack(values: Vec<i64>, weights: Vec<i64>, w_max: i64, n: i64) {
  let mut k: Vec<Vec<i64>> = Vec::new();
  for _ in 0..n + 1 {
    k.push((0i64..w_max + 1).collect::<Vec<_>>());
  }
  for i in 0i64..n + 1 {
    for j in 0i64..w_max + 1 {
      if i == 0 || j == 0 {
        k[i as usize][j as usize] = 0
      } else if weights[(i - 1) as usize] <= j {
        k[i as usize][j as usize] = cmp::max(
          values[(i - 1) as usize]
            + k[(i - 1) as usize][(j - weights[(i - 1) as usize]) as usize],
          k[(i - 1) as usize][j as usize],
        )
      } else {
        k[i as usize][j as usize] = k[(i - 1) as usize][j as usize]
      }
    }
  }
  print_index(k, values, weights, w_max, n)
}

fn main() {
  let input = io::stdin();
  let lines = input.lock().lines();
  let mut n = 0;
  let mut w_max = 0;
  let mut values: Vec<i64> = Vec::new();
  let mut weights: Vec<i64> = Vec::new();
  for w in lines {
    let line = w.unwrap();
    let num: Vec<i64> = line
      .trim()
      .split(' ')
      .map(|w| w.parse().expect("Not a float!"))
      .collect();
    if n == 0 {
      n = num[0];
    } else if num.len() == 1 {
      w_max = num[0];
    } else {
      weights.push(num[0]);
      values.push(num[1]);
    }
    if weights.len() == n as usize {
      break;
    }
  }
  knapsack(values, weights, w_max, n);
}

/*
$ cat DATA.lst | ./sullenestd1
3
6
8
9
11
12
13
14
20
21
25
29
32
34
35
36
39
40
41
43
47
51
55
57
58
62
64
65
66
69
70
71
77
78
79
80
81
82
83
85
86
88
90
91
93
94
97
99
100
102
103
105
106
107
108
109
110
113
114
115
116
119
121
125
127
130
133
137
138
139
142
145
*/
