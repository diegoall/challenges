#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 1.81 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

require "big"

PHI = (Math.sqrt(BigRational.new(5.to_big_i)) + 1) / 2

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_big_i}


args.each do |x|
  n = Math.log(x, 10) + Math.log(5.to_big_i, 10) / 2
  n = n / Math.log(PHI, 10)
  print "#{n.round.to_big_i} "
end

puts

# $ ./richardalmanza.cr
# 977 442 846 557 800 683 943 360 630 385 702 322 549 693
