;; $ clj-kondo --lint andresclm.clj
;; linting took 88ms, errors: 0, warnings: 0

(ns andresclm
  (:require [clojure.string :as str]))

(defn get-range [n]
  (->> n inc (range 1) vec))

(defn get-data-from [file]
  (let [data (slurp file)
        data' (str/split data #"\n")
        data'' (rest data')]
    (vector (nth (str/split (first data') #" ") 1)
            (str/split (nth data'' 0) #" "))))

(defn -main "Entry point" []
  (let [[n,data] (get-data-from "DATA.lst")
        data' (frequencies data)
        sqnce (get-range (Integer. n))
        results (map (fn [number] (if (contains? data' (str number))
                                    (get data' (str number))
                                    0)) sqnce)]
    (apply println results)))

;; $ clj -m andresclm
;; 21 24 32 24 23 25 29 27 25 28 16 19 30
