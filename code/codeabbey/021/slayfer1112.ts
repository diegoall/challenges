/*
$ eslint slayfer1112.ts
$ tsc slayfer1112.ts
*/

function solution(entry: string): number {
  const dat: number[] = entry.split(' ').map((value) => Number(value));
  const values: number = Math.max(...dat);
  const arr: number[] = Array.from(Array(values).keys()).map((val) => val + 1);
  const sols: number[] = arr.map(
    (value) => dat.filter((val) => val === value).length
  );
  process.stdout.write(`${sols.join(' ')}`);
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    dat.map((value) => solution(value));
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
21 24 32 24 23 25 29 27 25 28 16 19 30
*/
