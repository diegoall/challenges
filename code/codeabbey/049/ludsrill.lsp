#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun rock-papper-scissors ()
  (let ((data)
        (first-player)
        (second-player))
  (setq data (cdr (read-data)))
  (dolist (match data)
    (setq first-player 0)
    (setq second-player 0)
    (dolist (result match)
      (cond ((or (equal result 'RS) (equal result 'PR) (equal result 'SP))
             (setq first-player (+ first-player 1)))
            ((or (equal result 'RP) (equal result 'PS) (equal result 'SR))
             (setq second-player (+ second-player 1)))))

  (if (> first-player second-player)
    (format t "~s " 1)
    (format t "~s " 2)))))

(rock-papper-scissors)

#|
cat DATA.lst | clisp ludsrill.lsp
2 1 1 1 1 2 1 1 2 1 1 2 2 2 2 2 2 2 2 1 1 2 1 2 2 2 2 2 1 1
|#
