#!/usr/bin/env python3
# $ mypy johan17.py
# Success: no issues found in 1 source file

from typing import List
from typing import Tuple
import math


def iterate(r_a: float, var_a: float, var_b: float,
            var_c: float, time: bool, exponent: int) -> Tuple[float, int]:

    r_b = r_a - r_a / (10 ** exponent * var_c)

    while (math.floor(r_b / var_a) + math.floor(r_b / var_b)
           ) < var_c and time:
        r_b += r_b / (10 ** (exponent + 1) * var_c)
    return float(r_b), exponent + 1


def compare(r_a: float, r_b: float, time: bool) -> Tuple[str, bool]:

    zero = math.floor(r_a) - math.floor(r_b) == 0
    if zero and time:
        time = not(zero) and time
        return str(math.floor(r_a)), time
    time = not(zero) and time
    return str(math.floor(r_b)), time


def data() -> List[int]:

    input_0 = input()
    inputs: List[int] = []
    count: int = 0

    while count < int(input_0):
        printer_1, printer_2, quantity = input().split()
        inputs = inputs + [int(printer_1), int(printer_2), int(quantity)]
        count += 1

    return inputs


def cycles(previous: float, p_a: int, p_b: int, copies: int,
           time: bool, exponent: int) -> None:
    var: int = 0
    while var < 10:
        after, exponent = iterate(previous, p_a,
                                  p_b, copies, time,
                                  exponent)
        add, time = compare(previous, after, time)
        previous = after
        var += 1

    last: int = int(add)
    while(math.floor(last / p_a) +
          math.floor(last / p_b)) < copies:
        last += 1
    print(str(last) + " ", end='')


def calculate(numbers: List[int]) -> None:
    input_1 = int(len(numbers) / 3)
    for x_count in range(0, input_1):
        printer_a: int = numbers[x_count]
        printer_b: int = numbers[x_count + 1]
        pages: int = numbers[x_count + 2]
        inverse_speed: float = (printer_a * printer_b /
                                (printer_a + printer_b))
        response: int = math.ceil(pages * inverse_speed)
        before: float = float(response)

        while((math.floor(before / printer_a) +
              math.floor(before / printer_b)) <
              pages):
            before += before / (10 * pages)

        time_0: bool = not math.floor(before) - response == 0
        cycles(before, printer_a, printer_b,
               pages, time_0, 0)

    print("")


if __name__ == "__main__":
    calculate(data())


# $ cat DATA.lst | python3 johan17.py
# 263758430 136094900976 758118502080 237548096 8180425 5840250 379877502
# 5572 8748 197944 167105106 1500701652 211802745 44271163200 50042551356
# 193536936 251860 182630 349935036 154222
