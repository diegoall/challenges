/*
$ rustfmt rechavar.rs
$ rustc rechavar.rs
*/
use std::io::{self, Read};

#[derive(Debug, Clone)]
struct Travellers {
  name: String,
  condition: String,
  travel: Vec<String>,
  infectious: bool,
}

fn create_traveller(
  name: String,
  condition: String,
  travel: Vec<String>,
  infectious: bool,
) -> Travellers {
  Travellers {
    name: name,
    condition: condition,
    travel: travel,
    infectious: infectious,
  }
}

fn create_list(
  object_info_array: Vec<&str>,
  mut travellers: Vec<Travellers>,
) -> Vec<Travellers> {
  if object_info_array.len() != 0 {
    let travel_info = object_info_array[0];
    let split: Vec<String> =
      travel_info.split(" ").map(|x| x.to_string()).collect();
    let travel_rout = split[2].split("-").map(|x| x.to_string()).collect();
    if split[1] == "healthy" {
      travellers.push(create_traveller(
        split[0].clone(),
        split[1].clone(),
        travel_rout,
        false,
      ));
    } else {
      travellers.push(create_traveller(
        split[0].clone(),
        split[1].clone(),
        travel_rout,
        true,
      ));
    }
    create_list(object_info_array[1..].to_vec(), travellers)
  } else {
    return travellers;
  }
}

fn get_sick_recover(
  travellers: Vec<Travellers>,
  mut infected: Vec<(String, i32)>,
  day: i32,
) -> Vec<(String, i32)> {
  if travellers.len() != 0 {
    let traveller = travellers[0].clone();
    if traveller.condition == "recovering" || traveller.condition == "sick" {
      infected.push((traveller.name.clone(), day));
    }
    get_sick_recover(travellers[1..].to_vec(), infected, day)
  } else {
    return infected;
  }
}

fn get_condition(traveller: &Travellers, infect: bool) -> (String, bool) {
  if infect {
    let mut new_condition = "".to_string();
    let mut new_infectious = true;
    if traveller.condition == "healthy" {
      new_condition = "sick".to_string();
      new_infectious = true;
    } else if traveller.condition == "sick" {
      new_condition = "recovering".to_string();
      new_infectious = true;
    } else if traveller.condition == "recovering" {
      new_condition = "healthy".to_string();
      new_infectious = false;
    }
    (new_condition, new_infectious)
  } else {
    (traveller.condition.clone(), traveller.infectious)
  }
}

fn next_city(travel: Vec<String>) -> Vec<String> {
  let current = travel[0].clone();
  let mut next_travel: Vec<String> = travel[1..].to_vec();
  next_travel.push(current);
  next_travel
}

fn next_day(
  travellers_list: Vec<Travellers>,
  mut new_travellers: Vec<Travellers>,
  get_infected: Vec<String>,
) -> Vec<Travellers> {
  if travellers_list.len() != 0 {
    let traveller = travellers_list[0].clone();
    let new_travel = next_city(traveller.travel.clone());
    let mut new_infec = traveller.infectious.clone();
    if get_infected.iter().any(|x| *x == traveller.name) {
      new_infec = true;
    }
    let (new_condition, new_infec) = get_condition(&traveller, new_infec);
    let traveller_aux = create_traveller(
      traveller.name.clone(),
      new_condition,
      new_travel,
      new_infec,
    );
    new_travellers.push(traveller_aux.clone());
    next_day(
      travellers_list[1..].to_vec().clone(),
      new_travellers,
      get_infected,
    )
  } else {
    return new_travellers;
  }
}

fn compare_cities(
  traveller: Travellers,
  travellers_list: Vec<Travellers>,
  mut get_infected: Vec<String>,
) -> Vec<String> {
  if travellers_list.len() != 0 {
    if traveller.travel[0] == travellers_list[0].travel[0] {
      if traveller.infectious || travellers_list[0].infectious {
        if traveller.condition == "sick" || traveller.condition == "recovering"
        {
          get_infected.push(travellers_list[0].name.clone());
        } else if travellers_list[0].condition == "sick"
          || travellers_list[0].condition == "recovering"
        {
          get_infected.push(traveller.name.clone());
        }
      }
    }
    compare_cities(traveller, travellers_list[1..].to_vec(), get_infected)
  } else {
    get_infected
  }
}

fn run_day(
  travellers_list: Vec<Travellers>,
  mut aux_travellers: Vec<Travellers>,
  mut get_infected: Vec<String>,
) -> Vec<Travellers> {
  if travellers_list.len() != 0 {
    let traveller = travellers_list[0].clone();
    get_infected = compare_cities(
      traveller.clone(),
      travellers_list[1..].to_vec(),
      get_infected,
    );
    aux_travellers.push(traveller.clone());
    run_day(travellers_list[1..].to_vec(), aux_travellers, get_infected)
  } else {
    next_day(aux_travellers, Vec::<Travellers>::new(), get_infected)
  }
}

fn start_simulation(
  travellers: Vec<Travellers>,
  mut infected: Vec<(String, i32)>,
  days: Vec<i32>,
) -> Vec<(String, i32)> {
  if days.len() != 0 {
    let new_travellers =
      run_day(travellers, Vec::<Travellers>::new(), Vec::<String>::new());
    infected = get_sick_recover(new_travellers.clone(), infected, days[0]);
    start_simulation(new_travellers.clone(), infected, days[1..].to_vec())
  } else {
    return infected;
  }
}

fn get_answers(
  recovering: Vec<(String, i32)>,
  old_day: i32,
  len: usize,
  mut last_recovers: Vec<String>,
) -> (Vec<String>, i32) {
  if len != 0 {
    let (name, day) = &recovering[len - 1];
    if day.clone() == old_day {
      last_recovers.push(name.to_string());
      get_answers(recovering.clone(), *day, len - 1, last_recovers)
    } else {
      return (last_recovers, old_day);
    }
  } else {
    return (last_recovers, old_day);
  }
}

fn main() -> io::Result<()> {
  let mut buffer = String::new();
  io::stdin().read_to_string(&mut buffer)?;
  let mut object_info_array: Vec<&str> = buffer.split("\n").collect();
  object_info_array.pop();
  object_info_array.remove(0);

  let travellers_list =
    create_list(object_info_array, Vec::<Travellers>::new());

  let recovering = start_simulation(
    travellers_list.clone(),
    Vec::<(String, i32)>::new(),
    (0..100).collect::<Vec<i32>>().to_vec(),
  );
  let (mut names_answer, day) = get_answers(
    recovering.clone(),
    recovering[recovering.len() - 1].1,
    recovering.len(),
    Vec::<String>::new(),
  );
  names_answer.sort();
  println!("{:?}, {}", names_answer, day);

  Ok(())
}
/*
$ cat DATA.lst | ./rechavar
["Abe", "Dave", "Judd", "Lee", "Lori", "Mark", "Nat", "Sid", "Wes"], 99
*/
