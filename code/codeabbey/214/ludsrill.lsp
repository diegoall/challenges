#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun split-by-hyphen (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\- item :start i)
    collect (subseq item i j)
    while j))

(defun extract-differents (item)
  (let ((diff ()))
    (loop for i in item do
      (cond ((string/= (find i diff :test #'equal) i)
              (setq diff (append diff (list i))))))
    (return-from extract-differents diff)))

(defun organize-data ()
  (let ((state ())
        (state-aux ())
        (route ())
        (name ())
        (route-aux ())
        (data))

    (setq data (cdr (read-data)))

    (dolist (item data)
      (setq state-aux (append state-aux (list (subseq item 0 2))))
      (setq route-aux (append route-aux (list (subseq item 2)))))

    (dolist (item route-aux)
      (setq route (append route
                    (list (split-by-hyphen (string (first item)))))))

    (dolist (item state-aux)
      (setq state (append state (list (second item))))
      (setq name (append name (list (first item)))))

    (return-from organize-data (list state name route))))


(defun sick-travellers ()
  (let ((city)
        (data-from-organize-data)
        (diff)
        (equal-pos)
        (general-status)
        (healthy-pos)
        (len)
        (name)
        (number-day)
        (pos)
        (pos-last)
        (recovering-position)
        (route)
        (sick-position)
        (state))

    (setq number-day -1)
    (setq city ())

    (setq data-from-organize-data (organize-data))
    (setq state (first data-from-organize-data))
    (setq name (second data-from-organize-data))
    (setq route (third data-from-organize-data))

    (loop while (< number-day 101) do
      (setq number-day (+ number-day 1))
      (setq city ())
      (setq recovering-position ())
      (setq sick-position ())

      (dolist (item route)
        (setq len (length item))
        (setq pos (mod number-day len))
        (setq city (append city (list (elt item pos)))))

      (setq diff (extract-differents city))

      (loop for i from 0 to (- (length state) 1) do
        (when (equal (elt state i) 'SICK)
          (setq sick-position (append sick-position (list i))))
        (when (equal (elt state i) 'RECOVERING)
          (setq recovering-position (append recovering-position (list i)))))

      (setq healthy-pos ())

      (loop for i in diff do
        (setq equal-pos ())
        (setq general-status ())

        (loop for j = 0 then (1+ k)
          as k = (position i city :test #'equal :start j)
          while k do
            (setq equal-pos (append equal-pos (list k))))

        (when (> (length equal-pos) 1)
          (dolist (item equal-pos)
            (setq general-status (append general-status
                                  (list (elt state item)))))

          (when (and (equal (find 'HEALTHY general-status) 'HEALTHY)
                (or (equal (find 'SICK general-status) 'SICK)
                    (equal (find 'RECOVERING general-status) 'RECOVERING)))

            (loop for i from 0 to (- (length general-status) 1) do
              (when (equal (elt general-status i) 'HEALTHY)
                (setq healthy-pos (append healthy-pos
                                    (list (elt equal-pos i)))))))))

      (setq pos-last ())

      (loop for i from 0 to (- (length state) 1) do
        (when (or (equal (elt state i) 'RECOVERING)
                  (equal (elt state i) 'SICK))
          (setq pos-last (sort (append pos-last
                                (list (elt name i))) #'string-lessp))))

      (dolist (item recovering-position)
        (setf (elt state item) 'HEALTHY))

      (dolist (item sick-position)
        (setf (elt state item) 'RECOVERING))

      (dolist (item healthy-pos)
        (setf (elt state item) 'SICK))

      (setq healthy-pos ())

      (when (and (equal (find 'SICK state) NIL)
                 (equal (find 'RECOVERING state) NIL)
                 (equal (find 'HEALTHY state) 'HEALTHY))
        (dolist (item pos-last)
          (format t "~s " item))

        (format t "~s " number-day)
        (return))

    (when (= number-day 100)
      (dolist (item pos-last)
        (format t "~s " item))

      (format t "~s " 99)))))

(sick-travellers)

#|
cat DATA.lst | clisp ludsrill.lsp
ABE DORY IAN JEFF KARA MARY PAM PHIL SID VAL WES 99
|#
