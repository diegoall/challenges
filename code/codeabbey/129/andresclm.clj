;; $ clj-kondo --lint andresclm.clj
;; linting took 18ms, errors: 0, warnings: 0

(ns andresclm
  (:require [clojure.string :as str])
  (:require [clojure.math.combinatorics :as combo]))

(def symbols ["0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "A" "B" "C" "D" "E" "F"
              "G" "H" "I" "J" "K" "L" "M" "N" "O" "P" "Q" "R" "S" "T" "U" "V"
              "W" "X" "Y" "Z"])

(defn get-data-from [file]
  (let [data (slurp file)
        data' (str/split data #"\n")
        data'' (vec (rest data'))]
    (vec (map (fn [line] (str/split line #" ")) data''))))

(defn -main "Entry point" []
  (let [data (get-data-from "DATA.lst")
        result (map (fn [triplet] (let [[N K I] triplet
                                        symbols (subvec symbols
                                                        0
                                                        (Integer. N))
                                        result (combo/nth-combination
                                                symbols
                                                (Integer. K)
                                                (Long. I))]
                                    (str/join "" result)))
                    data)]
    (apply print result)))

;; $ clj -m andresclm
;; 135ACEFHJKLMNOPQRV 369AEKOSTUVXZ 123457ABCDFGHJKLMN 034567ABCDE 1345689ACE
;; 356 34678BCHPQRUVY 123456789ABDE 15789 245679HILNSVWXY 13479DEGHLMNQS 5
;; 25CFJMORUVWYZ 13456789BCFGHKPQTUVXY 1349AFGLNRVWXY 2469ACDEHJLNO
;; 367AEGHLMNOX 13567ACDEFGHIJMNOQ 0145689ACEFGHIJ 19ADFGHIJLOQRUV
