# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
use Readonly;
use POSIX;
our ($VERSION) = 1;

my $EMPTY = q{};
my $SPACE = q{ };

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data;
}

sub calc {
  my @data = @_;
  my $val1 = $data[0];
  my $val2 = $data[1];
  my $val3 = $data[2];
  return $val2, $val1 - $val3 * $val2;
}

sub euclidean {
  my @args = @_;
  my $a    = $args[0];
  my $m    = $args[1];
  my $s    = 1;
  my $t    = 0;
  my $x    = $a;
  my $y    = $m;
  while ( $y != 0 ) {
    my $q      = floor $x / $y;
    my $r_temp = $x % $y;
    $x = $y;
    $y = $r_temp;
    ( $s, $t ) = calc $s, $t, $q;
    return $x, $s if $x == 1;
  }
  return ( 0 - 1 ), 0;
}

sub mod_inverse {
  my @data = @_;
  my $s    = $data[0];
  my $m    = $data[1];
  if ( $s < 0 ) {
    return $s + $m;
  }
  return $s;
}

sub find_x {
  my @data  = @_;
  my $m     = $data[0];
  my $a     = $data[1];
  my $coe_b = $data[2];
  my ( $egcd_a, $egcd_b ) = euclidean $a, $m;
  if ( $egcd_a == 1 ) {
    my $inv = mod_inverse $egcd_b, $m;
    return ( -$coe_b * $inv ) % $m;
  }
  return ( 0 - 1 );
}

sub solution {
  my @args = @_;
  my @vals = split $SPACE, $args[0];
  my $val1 = $vals[0];
  my $val2 = $vals[1];
  my $val3 = $vals[2];
  my $res  = find_x $val1, $val2, $val3;
  exit 1 if !print "$res ";
  return 'Void that prints the modular inverse';
}

sub main {
  my @vals = data_in();
  for ( 1 .. $#vals ) {
    solution $vals[$_];
  }
  exit 1 if !print "\n";
  return 'Void that solves the challenge';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# -1 -1 -1 -1 626135039 13969163 -1 25161550 15743 -1 324770 692787
# 26903215 -1 24233 -1 3595709 4607 -1 1041 386903 35608272 18954474
# -1 53871 38286
