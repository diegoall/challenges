#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.79 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

require "big"

class Fibo
  @@list = [BigInt.new(0), BigInt.new(1)]

  def self.growup(n : Int)
    if n > @@list.size - 1
      (0..(n - @@list.size)).step {@@list << @@list[-1] + @@list[-2]}
    end
  end

  def self.[](index : Int)
    self.growup(index)
    @@list[index]
  end

  def self.[](range : Range)
    self.growup(range.end) if range.end
    @@list[range]
  end
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i64}

args.each do |x|
  index = 1

  while Fibo[index] % x != 0
    index += 1
  end

  print "#{index} "
end

puts

# $ ./richardalmanza.cr
# 102 396 7728 1914 990 840 750 900 2700 8700 1592 72 552 807 1000
# 2688 270 2100 3738 300
