#|
  $ sblint -v bridamo98.lsp
  $ [INFO] Lint file bridamo98.lsp
|#

(defun degrees_to_radians (degrees)
  (* pi (/ degrees 180.0))
)

(defun calc_height(distance angle height)
  (setq height (round (* distance (tan (- (degrees_to_radians angle)
    (degrees_to_radians 90))))))
  (format t "~a " height)
)

(defun solve(size_input distances angles height)
  (loop for x from 0 to (- size_input 1)
    do(calc_height (aref distances x) (aref angles x) height)
  )
)

(defun get_data(x number distances angles)
  (setq number (read))
  (setf (aref distances x) number)
  (setq number (read))
  (setf (aref angles x) number)
)

(defvar size_input (read))
(defparameter distances (make-array size_input))
(defparameter angles (make-array size_input))
(defvar number 0)
(defvar height 0)

(loop for x from 0 to (- size_input 1)
  do(get_data x number distances angles)
)

(solve size_input distances angles height)

#|
  cat DATA.lst | clisp bridamo98.lsp
  36 50 22 32 27 43 18 49 23 70 56 61 18 34 38 27
|#
