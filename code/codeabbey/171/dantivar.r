# $ lintr::lint('dantivar.r')

input <- readLines("stdin")
i <- 1
result <- c()

objects <- as.integer(input[1])

while (i <= objects) {
    values <- unlist(strsplit(input[i + 1], " "))
    d <- as.integer(values[1])
    a <- as.numeric(values[2]) - 90

    h <- round(tan(a * (pi / 180)) * d)

    result <- append(result, h)
    i <- i + 1
}

cat(paste(result, sep = " "))
# $ cat DATA.lst | Rscript dantivar.r
# 36 50 22 32 27 43 18 49 23 70 56 61 18 34 38 27
