#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 5.98 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Int32)
  data.each_line do |x|
    inter = [] of Int32
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_i : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)

  sum = 0

  array.each do |x|
    val = x.is_a?(Int32) ? x.try &.to_s : x
    split = val.split("")
    counter = 0
    while counter < split.size
      sum += Int32.new(split[counter])*(counter+1)
      counter += 1
    end
    print "#{sum} "
    sum = 0
  end
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 139 66 75 102 279 157 18 138 2 3 340 268 146 2 36 127 36
# 69 57 12 27 243 28 10 26 62 93 11 46 201 240 58 2 57 175
