
let verificar_sumatoria cuenta =
  let sum = ref 0 in
  let i = ref 16 in
  let segundo_valor num =
    let valor =
      (int_of_string (Char.escaped  num))*2 
    in
    if valor > 9 then
      valor-9
    else
      valor
    in    
    while !i>0 do
      let aux =
        int_of_string(Char.escaped (cuenta.[!i-1])) + (segundo_valor cuenta.[!i-2])
      in
        sum := !sum + aux;
      i:=!i-2;
    done;
    if(!sum mod 10) = 0 then
      true
    else
      false
;;

let reordenar pos cuenta =
  let concatenar posicion cadena =
    cadena^(Char.escaped cuenta.[posicion])
  in
  let cuenta_result = ref "" in
  let i = ref 0 in
  while !i<=15 do
    if !i = pos then
      (cuenta_result := concatenar (!i+1) !cuenta_result;
      cuenta_result := concatenar !i !cuenta_result;
      i:=!i+1)
    else
      cuenta_result := concatenar !i !cuenta_result;
        i := !i + 1;();
    done;
    !cuenta_result;
;;

let corregir_numero cuenta pos n= 
  let i = ref 0 in
  let aux2 = ref "" in
  let aux x = 
    if x = pos then 
      !aux2 ^ n
    else
      !aux2 ^ (Char.escaped cuenta.[x])
    in
    while !i < 16 do
      aux2 := aux !i;
      i := !i+1;
    done;
    !aux2
;;

let buscar_numero cuenta pos =
  let cuenta = ref (corregir_numero cuenta pos "0") in
  let i = ref 1 in
  let n = ref "" in
  while (verificar_sumatoria !cuenta) = false  do
    n := string_of_int !i;
    cuenta := corregir_numero !cuenta pos !n;
    i:=!i+1;
  done;
    !cuenta
;;
let verificar_validez cuenta = 
  let pos = try String.index cuenta '?' with Not_found -> -1 in
  if pos = -1 then
    let i = ref 0 in
    let cuenta_copia = ref (reordenar !i cuenta) in
    i:=!i+1;
      while verificar_sumatoria (!cuenta_copia) = false do
        cuenta_copia := (reordenar !i cuenta);
        i:=!i+1;
      done;
      !cuenta_copia
  else
    buscar_numero cuenta pos
;;
let cantCuentas = read_int ();;
let x = Array.make cantCuentas "";;
for i=0 to cantCuentas-1 do 
  x.(i) <- read_line ()
done;;
print_string (verificar_validez x.(0));;
for i=1 to cantCuentas-1 do 
  print_string (" " ^ verificar_validez x.(i))
done;;
