# $ mix credo --strict
#Checking 1 source file ...

#Please report incorrect results: https://github.com/rrrene/credo/issues

#Analysis took 0.2 seconds (0.01s to load, 0.2s running 54 checks on 1 file)
#10 mods/funs, found no issues.

# iex(1)> c("rechavar.exs")

defmodule SafeLanding do
@moduledoc "
This module solves Codeabbey exercise number 165 (Safe Landing)
"

  def main do
    data = IO.read(:stdio, :all)
    [str_intial, str_dm, _] = String.split(data, "\n")
    [m_craft, m_fuel, h, v] = Enum.map(String.split(str_intial, " "),
                                &String.to_integer/1)

    int_dm = Enum.map(String.split(str_dm, " "), &String.to_integer/1)
    v_final = simulation(m_craft, m_fuel, h, v, int_dm)

    IO.puts v_final
  end

  def simulation(m_craft, m_fuel0, h0, v0, [dm|t]) do
    i = Enum.to_list 1..100
    [m_total_1, m_fuel1, h1, v1] = iteration(m_craft,
                              m_fuel0, h0, v0, dm / 10, i)

    simulation(m_total_1, m_fuel1, h1, v1, t)
  end

  def simulation(m_craft, m_fuel0, h0, v0, []) do
    if h0 > 0 do
      i = Enum.to_list 1..100
      [m_total_1, m_fuel1, h1, v1] = iteration(m_craft, m_fuel0, h0, v0, 0, i)
      simulation(m_total_1, m_fuel1, h1, v1, [])
    else
      v0
    end
  end

  def iteration(m_craft, m_fuel0, h0, v0, dm, [_|t]) do
    h1 = SafeLanding.get_new_h(h0, v0)
    if h1 > 0 do
      m_total_0 = m_craft + m_fuel0
      dv = SafeLanding.get_delta_v(dm, m_total_0)
      m_fuel1 = m_fuel0 - dm
      g1 = SafeLanding.get_new_g(h1)
      v1 = SafeLanding.get_new_v(v0, g1, dv)
      iteration(m_craft, m_fuel1, h1, v1, dm, t)
    else
      iteration(m_craft, m_fuel0, h1, v0, dm, [])
    end
  end

  def iteration(m_total_0, m_fuel0, h0, v0, _, []) do
    [m_total_0, m_fuel0, h0, v0]
  end

  def get_delta_v(dm, m) do
    vex = 2_800
    vex * (dm / m)
  end

  def get_new_h(h0, v) do
    h0 - (v * 0.1)
  end

  def get_new_g(h) do
    r = 1_737_100
    rh = r + h
    g0 = 1.622
    g0 * (:math.pow(r, 2) / :math.pow(rh, 2))
  end

  def get_new_v(v0, g, dv) do
    v0 + (g * 0.1) - dv
  end

end

# $ cat DATA.lst | elixir -r rechavar.exs -e SafeLanding.main
# 1189.6706275864562
