/*
  $ rustfmt vmelendez.rs
  $ rustc vmelendez.rs
  $
*/

fn partition(array: &mut Vec<i32>, left: usize, right: usize) -> usize {
  let mut lt = left;
  let mut rt = right;
  let mut dir = 1;
  let pivot = array[left];
  while lt < rt {
    if dir == 1 {
      if array[rt] > pivot {
        rt -= 1;
      } else {
        array[lt] = array[rt];
        lt += 1;
        dir = 0;
      }
    } else {
      if array[lt] < pivot {
        lt += 1;
      } else {
        array[rt] = array[lt];
        rt -= 1;
        dir = 1;
      }
    }
  }
  array[lt] = pivot;
  return lt;
}

fn quicksort(array: &mut Vec<i32>, left: usize, right: usize) {
  print!("{}-{} ", left, right);
  let pivot_pos = partition(array, left, right);
  if pivot_pos - left > 1 {
    quicksort(array, left, pivot_pos - 1);
  }

  if right - pivot_pos > 1 {
    quicksort(array, pivot_pos + 1, right);
  }
}

fn main() -> std::io::Result<()> {
  let mut n = String::new();
  std::io::stdin().read_line(&mut n).unwrap();

  let mut array: Vec<i32> = {
    let mut a = String::new();
    std::io::stdin().read_line(&mut a).unwrap();
    a.split_whitespace()
      .map(|x| x.trim().parse::<i32>().unwrap())
      .collect()
  };

  let len = array.len();
  quicksort(&mut array, 0, len - 1);
  Ok(())
}

/*
  $ cat DATA.lst | .\vmelendez.exe
  105 91-99 91-97 93-97 93-96 95-96 101-105 103-105 103-104 107-116
  107-108 110-116 111-116 111-112 114-116
*/
