%>> mlint('paolagiraldo.m')
%>>

data = textscan (stdin, '%s');
data = str2double([data{1}(2:3:end), data{1}(3:3:end), data{1}(4:3:end)]);

for i=1:size(data,1)
  subs = data(i,:);
  savings = subs(1);
  goal = subs(2);
  rate = subs(3)/100;
  years = 0;

  while savings < goal
    years = years+1;
    savings = savings + savings*rate;
  end

final(1,i) = years;
end
%  cat DATA.lst | octave paolagiraldo.m
%  final =
%    109   109   273   302   163   137   163    21    92   324   163
%    163   221   163     7
%
%
