--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local result = {}
local case
local d = -1
for line in data do
    if d == -1 then
        case = tonumber(line)
    else
        local money
        local pay
        local interest
        local years = 0
        local a = 0
        for token in string.gmatch(line, "[^%s]+") do
            if a == 0 then
                money = tonumber(token)
            elseif a == 1 then
                pay = tonumber(token)
            else
                interest = tonumber(token)
            end
            a = a +1
        end
        while money<pay do
            years = years + 1
            money = money + money*(interest/100)
        end
        result[d] = years
    end
    d = d+1
end
for i = 0, case-1 do
    print(result[i])
end

--[[
$ lua mrsossa.lua
9
31
8
8
10
26
7
43
8
66
7
100
37
48
6
17
10
8
]]
