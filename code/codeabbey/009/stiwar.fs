open System

[<EntryPoint>]
let main argv =
    let operations a b c =
          if ((a+b)>c) then (
            if ((a+c)>b) then (
                if((b+c)>a) then 1 else 0
                )else 0
            )else
                0
    printfn "ingrese la cantidad de pruebas:"
    let c = stdin.ReadLine() |> int
    let mutable out = Array.zeroCreate c
    for i in 0..(c-1) do
        printfn "(%i) ingrese las medidas de los lados separados por un espacio" (i+1)
        let arrayData = stdin.ReadLine().Split ' '
        let a = int arrayData.[0]
        let b = int arrayData.[1]
        let c = int arrayData.[2]
        out.[i] <- (operations a b c)
    for i in 0..(out.Length-1) do
        printf "%i " out.[i]
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
