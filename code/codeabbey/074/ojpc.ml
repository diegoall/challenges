let rec coorx angx man =
        let pi = 4.0 *. atan 1.0 in
        if  angx>0.0 && angx < 90.0 
            then  10.0 +. man*.cos((90.0*.(pi/.180.0))-.(angx*.(pi/.180.0)))
            else coorx2 angx man
and coorx2 angx man=
        let pi = 4.0 *. atan 1.0 in
            if 90.0 <= angx && angx < 180.0 
            then 10.0 +. man*.cos((angx*.(pi/.180.0))-. (90.0*.(pi/.180.0)))
            else coorx3 angx man
and coorx3 angx man=
        let pi = 4.0 *. atan 1.0 in
        if 180.0 <= angx && angx < 270.0 
            then 10.0 -. man*.sin((angx*.(pi/.180.0)) -. (180.0*.(pi/.180.0)))
            else 10.0 -. man*.cos((angx*.(pi/.180.0)) -. (270.0*.(pi/.180.0)))

let rec coory angy man =
        let pi = 4.0 *. atan 1.0 in
        if  angy>0.0 && angy < 90.0 
            then  10.0 +. man*.sin((90.0*.(pi/.180.0))-.(angy*.(pi/.180.0)))
            else coory2 angy man
and coory2 angy man=
        let pi = 4.0 *. atan 1.0 in
            if 90.0 <= angy && angy < 180.0 
            then 10.0 -. man*.sin((angy*.(pi/.180.0))-. (90.0*.(pi/.180.0)))
            else coory3 angy man
and coory3 angy man=
        let pi = 4.0 *. atan 1.0 in
        if 180.0 <= angy && angy < 270.0 
            then 10.0 -. man*.cos((angy*.(pi/.180.0)) -. (180.0*.(pi/.180.0)))
            else 10.0 +. man*.sin((angy*.(pi/.180.0)) -. (270.0*.(pi/.180.0)));;
            
let h=2 in
let m =14 in
let angh h m = float_of_int((h*(30)))+.((float_of_int m)*.0.5) in
let angm m =float_of_int(m*(360/60)) in
let fh=angh h m in
let fm=angm  m in
let hx=coorx fh 6.0 in
let hy=coory fh 6.0 in
let mx=coorx fm 9.0 in
let my=coory fm 9.0 in
print_float hx; 
print_string " ";
print_float hy;
print_string " ";
print_float mx;
print_string " ";
print_float my;
print_string " ";
