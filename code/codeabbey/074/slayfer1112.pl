# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
use Readonly;
use POSIX;
our ($VERSION) = 1;

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data;
}

sub dicc_h {
  Readonly my $pi   => 3.14159265358979323846;
  Readonly my $n    => 180;
  Readonly my $cond => 359.5;
  my $counter = $n;
  my $degrees = 0.0;
  my %dicc_hour;
  while ( $degrees <= $cond ) {
    $dicc_hour{"$counter"} = $degrees * $pi / $n;
    if ( $counter <= 1 ) {
      Readonly my $n_n => 721;
      $counter = $n_n;
    }
    $counter = $counter - 1;
    $degrees = $degrees + ( 1 / 2 );
  }
  return %dicc_hour;
}

sub dicc_m {
  Readonly my $pi   => 3.14159265358979323846;
  Readonly my $n    => 180;
  Readonly my $m    => 15;
  Readonly my $cond => 360;
  my $counter = $m;
  my $degrees = 0.0;
  my %dicc_min;
  while ( $degrees != $cond ) {
    $dicc_min{"$counter"} = $degrees * $pi / $n;
    if ( $counter <= 0 ) {
      Readonly my $n_n => 60;
      $counter = $n_n;
    }
    $counter = $counter - 1;
    $degrees = $degrees + ( 2 * 2 + 2 );
  }
  return %dicc_min;
}

sub solution {
  my @args   = @_;
  my @data   = @args;
  my @hours  = ();
  my @mins   = ();
  my %d_hour = dicc_h();
  my %d_min  = dicc_m();
  Readonly my $min_hand  => 9.0;
  Readonly my $hour_hand => 6.0;

  for ( 0 .. $#data ) {
    Readonly my $n   => 720;
    Readonly my $sep => qw(:);
    my @x = split $sep, $data[$_];
    my $y = $x[0] * ( $n / ( 2 * 2 * ( 2 + 1 ) ) ) + $x[1];
    if ( $y > $n ) {
      $y = $y - $n;
    }
    push @hours, $y;
    push @mins,  $x[1];
  }
  Readonly my $ten => 10.0;
  for ( 0 .. $#data ) {
    my $x_hour = ( $ten + cos( $d_hour{"$hours[$_]"} ) * $hour_hand );
    my $y_hour = ( $ten + sin( $d_hour{"$hours[$_]"} ) * $hour_hand );
    my $x_min  = ( $ten + cos( $d_min{"$mins[$_]"} ) * $min_hand );
    my $y_min  = ( $ten + sin( $d_min{"$mins[$_]"} ) * $min_hand );
    exit 1 if !print "$x_hour $y_hour $x_min $y_min ";
  }
  return 'Tic Tac';
}

sub main {
  my @vals  = data_in();
  my $SPACE = q{ };
  my @body  = split $SPACE, $vals[1];
  solution @body;
  exit 1 if !print "\n";
  return 'The clock is running';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 4.58257569 2.0 1.73205081 4.3588989400000004 7.0 1.0 3.0 5.0 8.18535277
# 3.60555128 4.3588989400000004 7.0 1.0 1.0 1.0 4.3588989400000004 7.0
# 4.58257569 6.0 1.73205081 2.64575131
