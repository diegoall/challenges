#!/usr/bin/env ruby
# frozen_string_literal: true

# $ rubocop danimelo.rb
# Inspecting 1 file
# .
# 1 file inspected, no offenses detected

def triangle
  # Read triangle data
  triangles = []
  STDIN.each_line do |line|
    sides = line.split(' ')
    triangles.append(sides)
  end
  triangles.shift
  triangles
end

def defining_triangle(one, two, three)
  # is right , acute, obtuse?
  if one**2 + two**2 == three**2
    'R'
  elsif one**2 + two**2 > three**2
    'A'
  else
    'O'
  end
end

def sides
  # send each leg side and h data to save the result
  type_of = []
  triangle.each do |legs|
    one = legs[0].to_i
    two = legs[1].to_i
    three = legs[2].to_i
    result = defining_triangle(one, two, three)
    type_of.append(result)
  end
  print type_of
end

sides
__END__

$ cat DATA.lst | ./danimelo.rb
["R", "R", "A", "O", "O", "R", "O", "R", "O",
"R", "R", "O", "O", "A", "A", "A", "A", "R",
"R", "R", "O", "A", "O", "R", "R", "O", "R",
"A", "O", "O"]
