#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(defvar *aux*)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(setq *data* (read-data))

(loop for i from 0 to (floor (- (length (first *data*)) 1) 2)
  do(setq *aux* (elt (first *data*) i))
  (setf (char (first *data*) i) (elt (first *data*)
         (- (- (length (first *data*)) 1) i)))
  (setf (char (first *data*) (- (- (length (first *data*)) 1) i)) *aux*))

(print (first *data*))

#|
cat DATA.lst | clisp ludsrill.lsp
nrut evitagorretni tes boj erehw elpmis llit dne
|#
