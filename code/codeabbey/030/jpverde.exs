# mix credo
# Analysis took 0.1 seconds (0.04s to load, 0.09s running checks)
# 3 mods/funs, found no issues.
# $ iex
# iex(1)> c("jpverde.ex")

defmodule Words do
  def phrase do
    {:ok, phrase} = File.read("DATA.lst")
    IO.puts(String.reverse(phrase))
  end
end

#iex(2)> Words.phrase
# sutcac tuoba ydrapoej flehs ffo reppus eraf no
# :ok
