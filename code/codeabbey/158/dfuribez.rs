/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};
use std::str;

fn str2bin(string: String, index: usize) -> u32 {
  let bin: u32 = (string.as_bytes()[index] - 48) as u32;
  bin
}

fn xor(index: u8, string: String) -> u8 {
  let start = 2u8.pow(index as u32) - 1u8;
  let step = 2u8.pow(index as u32);
  let mut xored = 0;

  let mut skip = 0;
  for i in start as usize..(&string).len() {
    if skip < step {
      xored ^= str2bin(string.clone(), i as usize);
    }
    skip = (skip + 1) % (step * 2);
  }
  xored as u8
}

fn encode(string: String) {
  let mut encoded = string.clone().into_bytes();
  let string_len = (&string).len();
  let max = (string_len as f64).ln() / 2f64.ln();
  let max = max.floor() + 1f64;

  for i in 0..max as usize {
    let bit = xor(i as u8, string.clone());
    let index = 2u8.pow(i as u32) as usize - 1usize;
    encoded[index] = bit + 48;
  }
  println!("{}", str::from_utf8(&encoded).unwrap());
}

fn generate_string(line: String) -> String {
  let mut new_string: String = "00".to_string();
  let mut used: u32 = 0;
  for index in 1..(&line).len() {
    let mut end = (2u8.pow(index as u32) - 1u8) as usize;
    let start = used as usize;
    if used + (end as u32) >= (&line).len() as u32 {
      end = (&line).len() - start;
      for i in start..start + end {
        new_string += &(line.as_bytes()[i] as char).to_string();
      }
      break;
    }

    for i in start..start + end {
      new_string += &(line.as_bytes()[i] as char).to_string();
    }
    new_string += "0";
    used += end as u32;
  }
  new_string
}

fn detach(string: String) -> (Vec<u8>, Vec<u8>) {
  let mut data = Vec::new();
  let mut parity = Vec::new();

  for i in 1..(&string).len() + 1 {
    let is_parity = (i as f64).ln() / (2f64.ln());
    if is_parity.fract() == 0.0 {
      parity.push(str2bin(string.clone(), i - 1) as u8);
    } else {
      data.push(str2bin(string.clone(), i - 1) as u8);
    }
  }
  (data, parity)
}

fn find_error(parity1: Vec<u8>, parity2: Vec<u8>) -> u8 {
  let len = parity1.len();
  let mut bit = 0u8;
  let mut xored;
  let mut index;
  for i in 0..len {
    index = len - i - 1;
    xored = parity1[index] ^ parity2[index];
    bit += 2u8.pow(index as u32) * xored;
  }
  bit
}

fn bin2ascii(data: Vec<u8>) -> Vec<u8> {
  let mut ascii = Vec::new();

  for i in data {
    ascii.push(i + 48);
  }
  ascii
}

fn fix_error(string: String, error: usize) {
  let mut fixed = string.into_bytes();
  if fixed[error - 1] == 48 {
    fixed[error - 1] = 49;
  } else {
    fixed[error - 1] = 48;
  }
  let (data, _) = detach(str::from_utf8(&fixed).unwrap().to_string());

  let data = bin2ascii(data);

  println!("{}", str::from_utf8(&data).unwrap());
}

fn decode(string: String) {
  let mut parity_now = Vec::new();

  let (data, parity) = detach(string.clone());

  let data = bin2ascii(data);

  let string_now = str::from_utf8(&data).unwrap().to_string();
  let string_now = generate_string(string_now);

  let string_len = (&string_now).len();
  let max = (string_len as f64).ln() / 2f64.ln();
  let max = (max.floor() + 1f64) as usize;
  for i in 0..max as usize {
    let bit = xor(i as u8, string_now.clone());
    parity_now.push(bit);
  }
  let error = find_error(parity, parity_now);
  if error != 0 {
    fix_error(string, error as usize);
  } else {
    println!("{}", str::from_utf8(&data).unwrap());
  }
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();
  let mut to_encode = true;
  let mut encode_lines = 0;

  for (index, line) in lines.enumerate() {
    let l = line.unwrap();
    if encode_lines == 0 && to_encode {
      encode_lines = l.parse().unwrap_or(0);
    } else if index <= encode_lines {
      encode(generate_string(l));
    } else if index == encode_lines + 1 {
      to_encode = false;
    } else if !to_encode {
      decode(l);
    }
  }
}

/*
$ cat DATA.lst | ./dfuribez
01100111100111111
00110100000110
11011110110000000
011011000110110
110011010001001101111001011101
101011000110000110011
00011010011000000011
101011110011011001010101000
0111110111000010111100000101
1010011011
010011111011
111010100110000100100
0011111011
0111110
1100000010
0110011001111010100001111
11111001010100
1101101
01011011101
00110001110100010111
110000
11111001110110011
01001110000100001101
0111001100101010010000000
010101011100101
0101101101
*/
