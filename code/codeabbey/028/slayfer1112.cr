#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 79.96 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Float64)
  data.each_line do |x|
    inter = [] of Float64
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_f : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  weight = array[0]
  height = array[1]
  bmi = weight / (height*height)
  sol = ""
  if bmi < 18.5
    sol = "under"
  elsif 18.5 <= bmi && bmi < 25
    sol = "normal"
  elsif 25 <= bmi && bmi < 30
    sol = "over"
  else
    sol = "obese"
  end
  print "#{sol} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# obese under obese normal under under normal under under over under under
# under obese obese normal under under under under over normal under obese
# under over under normal normal over under under normal
