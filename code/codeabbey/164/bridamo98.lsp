#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defstruct
  (node (:conc-name dr-))
  entry-value
  current-value
  neighbors
)

(defun load-edge(nodes)
  (setq fth (read))
  (setq sth (read))
  (if (not (aref nodes fth))
    (progn
      (setf (aref nodes fth)
        (make-node
          :entry-value 0.0
          :current-value 50.0
          :neighbors (list)
        )
      )
    )
  )
  (if (not (aref nodes sth))
    (progn
      (setf (aref nodes sth)
        (make-node
          :entry-value 0.0
          :current-value 50.0
          :neighbors (list)
        )
      )
    )
  )
  (setf (dr-neighbors (aref nodes fth))
    (append (dr-neighbors (aref nodes fth)) (list sth)))
)

(defun load-input-data (amount-edges nodes)
  (loop for i from 0 to (- amount-edges 1)
    do(load-edge nodes)
  )
)

(defun distribute-value (i)
  (loop for j from 0 to (- (length (dr-neighbors (aref nodes i))) 1)
    do(setf
      (dr-entry-value
        (aref nodes (nth j (dr-neighbors (aref nodes i)))))
        (+
          (dr-entry-value (aref nodes
          (nth j (dr-neighbors (aref nodes i)))))
          (/ (dr-current-value (aref nodes i))
          (length (dr-neighbors (aref nodes i))))
        )
      )
  )
)

(defun update-value (i)
  (setf (dr-current-value (aref nodes i))
    (dr-entry-value (aref nodes i)))
  (setf (dr-entry-value (aref nodes i)) 0)
)

(defun run-step()
  (loop for i from 0 to (- amount-nodes 1)
    do(distribute-value i)
  )
  (loop for i from 0 to (- amount-nodes 1)
    do(update-value i)
  )
)

(defun calc-page-rank(steps)
  (loop for i from 0 to (- steps 1)
    do(run-step)
  )
)

(defun print-solution(nodes)
  (loop for i from 0 to (- amount-nodes 1)
    do(format t "~a " (round (dr-current-value (aref nodes i))))
  )
)

(defvar amount-nodes (read))
(defvar amount-edges (read))
(defvar nodes (make-array amount-nodes))
(load-input-data amount-edges nodes)
(defvar steps 1000)
(calc-page-rank steps)
(print-solution nodes)

#|
  cat DATA.lst | clisp bridamo98.lsp
  20 34 79 35 113 28 58 74 43 23 38 70 38
|#
