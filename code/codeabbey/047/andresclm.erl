% $ erlc -Werror andresclm.erl
% andresclm.beam

-module(andresclm).

-export([start/0]).

-compile(andresclm).

start() ->
  {Key, Data} = read_file("DATA.lst"),
  Matrix = [string:lexemes(Line," ") || Line<-Data],
  Result = [shift_row(Row,Key)||Row<-Matrix],
  [io:format("~s ", [Sentence]) || Sentence <- Result].

shift_row(Row,Key) ->
  string:join([shift_string(Word,Key)||Word <-Row], " ").

shift_string(String,Key) ->
  lists:flatmap(fun(Letter) -> apply_shift(Letter,Key) end, String).

apply_shift(LetterNumber,Key) ->
  if LetterNumber=:=46 ->
    [LetterNumber];
  LetterNumber-Key<65 ->
    [LetterNumber-Key+26];
  LetterNumber-Key>=65 ->
    [LetterNumber-Key] end.

read_file(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  [A|Data] = string:lexemes(erlang:binary_to_list(Binary), "\n"),
  {Key, []}= string:to_integer(tl(string:lexemes(A," "))),
  {Key,Data}.

% $ erl -noshell -s andresclm -s init stop
% CALLED IT THE RISING SUN FOUR SCORE AND SEVEN YEARS AGO THE
% ONCE AND FUTURE KING. A DAY AT THE RACES. MET A WOMAN AT THE
% WELL. THE DEAD BURY THEIR OWN DEAD THAT ALL MEN ARE CREATED EQUAL.
% AS EASY AS LYING. AND SO YOU TOO BRUTUS. THE SECRET OF HEATHER ALE
% WHO WANTS TO LIVE FOREVER.
