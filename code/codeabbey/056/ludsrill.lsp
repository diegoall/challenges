#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defparameter *next-round-by-row* ())
(defparameter *next-round-all* ())
(defparameter *count-row-dead* -1)
(defparameter *count-row-life* -1)
(defparameter *count-row* -1)
(defparameter *count-col* -1)

(defvar *count-next-round*)
(defvar *position-organism*)
(defvar *pos-empty-cell*)
(defvar *count-to-die*)
(defvar *count-to-born*)
(defvar *data-next-round*)
(defvar *data*)

(defun join-string-list (string-list)
  (let ((join ()))
    (loop for i in string-list
      do(setq join (format nil "~{~A~^~}" i))
      collect (list join))))

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun dead (aux-item)
  (let ((aux-elt)
        (str))
    (setq *count-row-dead* (+ *count-row-dead* 1))
    (setq str (copy-seq aux-item))
    (loop for i from 0 to (- (length aux-item) 1)
      do(setq aux-elt (elt str i))
      when (equal aux-elt #\X)
        collect (list *count-row-dead* i))))

(defun life (aux-item)
  (let ((aux-elt)
        (str))
    (setq *count-row-life* (+ *count-row-life* 1))
    (setq str (copy-seq aux-item))
    (loop for i from 0 to (- (length aux-item) 1)
      do(setq aux-elt (elt str i))
      when (equal aux-elt #\-)
        collect (list *count-row-life* i))))

(defun organize-list(h)
  (let ((nested-list))
    (loop for x from 0 to (- (length h) 1)
      do(setq nested-list (list (elt h x) (elt h (+ x 1))))
      (setq x (+ 1 x))
      collect nested-list)))

(defun next-round(fin *position-organism*)
  (let ((top_left)
        (top_cen)
        (top_right)
        (bot_cen)
        (bot_left)
        (bot_right)
        (med_left)
        (med_right))
    (loop for i in fin
      do(setq *count-next-round* 0)
      (setq top_left (list (- (first i) 1) (- (second i) 1)))
      (setq med_left (list (first i) (- (second i) 1)))
      (setq bot_left (list (+ (first i) 1) (- (second i) 1)))
      (setq top_cen (list (- (first i) 1) (second i)))
      (setq bot_cen (list (+ (first i) 1) (second i)))
      (setq top_right (list (- (first i) 1) (+ (second i) 1)))
      (setq med_right (list (first i) (+ (second i) 1)))
      (setq bot_right (list (+ (first i) 1) (+ (second i) 1)))

      (cond ((equal (find top_left *position-organism* :test #'equal)
            top_left) (setq *count-next-round* (+ *count-next-round* 1))))
      (cond ((equal (find top_cen *position-organism* :test #'equal)
              top_cen) (setq *count-next-round* (+ *count-next-round* 1))))
      (cond ((equal (find top_right *position-organism* :test #'equal)
            top_right) (setq *count-next-round* (+ *count-next-round* 1))))
      (cond ((equal (find med_left *position-organism* :test #'equal)
              med_left) (setq *count-next-round* (+ *count-next-round* 1))))
      (cond ((equal (find med_right *position-organism* :test #'equal)
            med_right) (setq *count-next-round* (+ *count-next-round* 1))))
      (cond ((equal (find bot_left *position-organism* :test #'equal)
              bot_left) (setq *count-next-round* (+ *count-next-round* 1))))
      (cond ((equal (find bot_cen *position-organism* :test #'equal)
              bot_cen) (setq *count-next-round* (+ *count-next-round* 1))))
      (cond ((equal (find bot_right *position-organism* :test #'equal)
            bot_right) (setq *count-next-round* (+ *count-next-round* 1))))
      collect *count-next-round*)))

(defun calc(data)
  (let ((pos-organism-raw ())
        (pos-empty-raw ())
        (all-pos-organism-raw)
        (all-pos-empty-raw))

    (dolist (item data)
      (loop for aux-item in item
        do(setq all-pos-organism-raw (dead (string aux-item)))
        (dolist (item2 all-pos-organism-raw)
        (setq pos-organism-raw (append pos-organism-raw item2)))))

    (dolist (item data)
      (loop for aux-item in item
        do(setq all-pos-empty-raw (life (string aux-item)))
        (dolist (item3 all-pos-empty-raw)
        (setq pos-empty-raw (append pos-empty-raw item3)))))

    (setq *position-organism* (organize-list pos-organism-raw))
    (setq *count-to-die* (next-round *position-organism*
                                      *position-organism*))
    (setq *pos-empty-cell* (organize-list pos-empty-raw))
    (setq *count-to-born* (next-round *pos-empty-cell*
                                      *position-organism*))))

(defun update(data)
  (calc data)
  (let ((live-organisms 0)
        (aux_pos))
    (dolist (item_4 data)
      (setq *count-row* (+ *count-row* 1))
      (dolist (i item_4)
        (loop for j across (string i)
          do(setq *count-col* (+ *count-col* 1))
          (setq aux_pos (list *count-row* *count-col*))
          (cond ((equal (find aux_pos *position-organism* :test #'equal)
                  aux_pos)
                  (cond ((or (< (elt *count-to-die*
                                 (position aux_pos *position-organism*
                                  :test #'equal)) 2)
                          (> (elt *count-to-die*
                              (position aux_pos *position-organism*
                               :test #'equal)) 3))
                         (setq j #\-))))

                ((equal (find aux_pos *pos-empty-cell* :test #'equal)
                  aux_pos)
                  (cond ((= (elt *count-to-born*
                            (position aux_pos *pos-empty-cell*
                              :test #'equal)) 3) (setq j #\X)))))

          (when (equal j #\X)
            (setq live-organisms (+ live-organisms 1)))
          (setq *next-round-by-row* (append *next-round-by-row*
                                      (list (string j)))))

        (setq *count-col* -1)
        (setq *next-round-all* (append (list *next-round-by-row*)
                                        *next-round-all*))
        (setq *next-round-by-row* ())))

    (setq *data-next-round* (join-string-list *next-round-all*))
    (format t "~s " live-organisms)
    (setq *next-round-all* ())))

(setq *data* (read-data))
(update *data*)
(loop for i from 0 to 3
  do(update *data-next-round*))

#|
cat DATA.lst | clisp ludsrill.lsp
7 2 0 0 0
|#
