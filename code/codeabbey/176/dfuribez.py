#!/usr/bin/env python

"""
$ pylint dfuribez.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""

import sys
import requests


def main() -> None:
    """
    Main function.
    """
    token = sys.stdin.readlines()[0]
    token = token.strip()

    response = requests.post(
        "http://open-abbey.appspot.com/interactive/say-100",
        data={"token": token}
    )
    number = response.content.decode().split(":")[1].strip()
    answer = 100 - int(number)

    response = requests.post(
        "http://open-abbey.appspot.com/interactive/say-100",
        data={"token": token, "answer": answer}
    )
    print(response.content.decode().strip())


main()

# cat DATA.lst | python dfuribez.py
# end: M/h6miuljtizM0AYP+L+nTEy
