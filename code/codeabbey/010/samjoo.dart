/*
$ dartanalyzer --fatal-infos --fatal-warnings --no-declaration-casts \
 --no-implicit-casts --no-implicit-dynamic --lints samjoo.dart
Analyzing samjoo.dart...
No issues found!
*/

import 'dart:io';

/// Reads and parse [linesNum] lines from the STDIN.
List<List<int>> readData(int linesNum) {
  List<List<int>> lines = List<List<int>>(linesNum);

  for(int i = 0; i < linesNum; i++) {
    lines[i] = [];
    stdin.readLineSync().split(' ').forEach((String elem) {
      lines[i].add(int.parse(elem));
    });
  }

  return lines;
}

double getSlope(List<int> points) {
  return ((points[3] - points[1]) / (points[2] - points[0]));
}

double getBParameter(List<int> points) {
  double b = (getSlope(points)*(points[0]*-1)) + points[1];
  return b;
}

void printAnswerFormat(double slope, double b) {
    stdout.write('(${slope.toStringAsFixed(0)} ${b.toStringAsFixed(0)}) ');
}

void main(List<String> args) {
  List<List<int>> data = readData(int.parse(stdin.readLineSync()));

  // List structure: [0: x1, 1: y1, 2: x2, 3: y2]
  data.forEach((List<int> points) {
    double slope = getSlope(points);
    double b = getBParameter(points);
    printAnswerFormat(slope, b);
  });
}

/*
$ cat DATA.lst |dart samjoo.dart
(-24 -183) (-58 -958) (4 982) (-64 916) (4 -488) (-77 -113)
(-45 340) (56 -459) (36 295) (34 691) (67 961) (87 39)
*/
