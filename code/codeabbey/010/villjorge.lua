--[[
$ luacheck villjorge.lua #linting
Checking villjorge.lua                              OK

Total: 0 warnings / 0 errors in 1 file
$ luac5.3 -o villjorge.luac villjorge.lua #compilation
--]]

local x1, x2, y1, y2, j, sel
j = 0
local function readall(filename)
  local fh = assert(io.open(filename, "rb"))
  local contents = assert(fh:read(_VERSION <= "Lua 5.2" and "*a" or "a"))
  fh:close()
  return contents
end

for i in string.gmatch(readall("DATA.lst"), "%S+") do
  if (j ~= 0) then
    sel = (j-1) % 4
    if (sel == 0) then
      x1 = i
    elseif (sel == 1) then
      y1 = i
    elseif (sel == 2) then
      x2 = i
    elseif (sel == 3) then
      y2 = i
      local a = (y2 - y1) / (x2 - x1)
      local b = y1 - (x1 * a)
      print("("..a.." "..b..")\b")
    end
  end
  j = j + 1
end

--[[
$ lua5.3 villjorge.lua
(-78.0 -834.0) (-48.0 -353.0) (95.0 621.0) (58.0 -495.0) (91.0 -681.0)
(-60.0 254.0) (-10.0 430.0) (-32.0 -645.0) (-48.0 -855.0) (-21.0 -279.0)
(-66.0 520.0) (-97.0 -212.0) (11.0 268.0) (-48.0 -699.0) (73.0 -977.0)
--]]
