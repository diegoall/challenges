/*
$ rustfmt rechavar.rs
$ rustc rechavar.rs
*/
use std::io::{self, Read};

fn get_max_value(values_array: Vec<i32>, max: i32) -> i32 {
  if values_array.len() != 0 {
    if values_array[0] > max {
      get_max_value(values_array[1..].to_vec().clone(), values_array[0])
    } else {
      get_max_value(values_array[1..].to_vec().clone(), max)
    }
  } else {
    return max;
  }
}

fn swap_values(mut values_array: Vec<i32>, idx: usize) -> Vec<i32> {
  let value = values_array[values_array.len() - 1];
  values_array.remove(idx);
  values_array.insert(idx, value);
  values_array.pop();
  values_array
}

fn run_challenge(values_arr: Vec<i32>, mut answer: Vec<usize>) -> Vec<usize> {
  if values_arr.len() != 1 {
    let max_value = get_max_value(values_arr.clone(), 0);
    let idx = values_arr.iter().position(|&r| r == max_value).unwrap();
    let new_array = swap_values(values_arr, idx);
    answer.push(idx);
    run_challenge(new_array, answer)
  } else {
    return answer;
  }
}

fn main() -> io::Result<()> {
  let mut buffer = String::new();
  io::stdin().read_to_string(&mut buffer)?;
  let mut in_array: Vec<&str> = buffer.split("\n").collect();
  in_array.pop();
  let values_array: Vec<i32> =
    in_array[1].split(" ").map(|x| x.parse().unwrap()).collect();
  let answer = run_challenge(values_array, Vec::new());
  for i in answer {
    print!("{} ", i);
  }

  Ok(())
}
/*
$ cat DATA.lst | ./rechavar
*/
