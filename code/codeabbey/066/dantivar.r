# $ lintr::lint('dantivar.r')

library(caesar)

en_letter_freq <- c(8.1, 1.5, 2.8, 4.3, 13.0, 2.2, 2.0, 6.1, 7.0, 0.15, 0.77,
                    7.0, 2.4, 6.8, 7.5, 1.9, 0.095, 6.0, 6.3, 9.1, 2.8, 0.98,
                    2.4, 0.15, 2.0, 0.074)

alphabet <- c("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
              "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")

input <- readLines("stdin")
i <- 1
result <- ""

objects <- as.integer(input[1])

while (i <= objects) {
    min_freq <- 10000000
    key <- 1
    values <- input[i + 1]
    j <- 1
    decode <- unlist(strsplit(values, " "))[1:3]
    values <- gsub(" ", "", values, fixed = TRUE)
    string <- unlist(strsplit(values, ""))
    current_freq <- c()
    while (j <= length(alphabet)) {
        x <- sum(string == alphabet[j])
        current_freq <- append(current_freq, x)
        j <- j + 1
    }
    current_freq <- (current_freq * 100) / length(string)
    k <- 1
    len <- length(alphabet)
    while (k <= len) {
        sqr_diff <- sum((current_freq - en_letter_freq) ** 2)

        if (sqr_diff < min_freq) {
            key <- k
            min_freq <- sqr_diff
        }
        temp <- current_freq[len]
        current_freq <- append(current_freq[-len], temp, after = 0)
        k <- k + 1
    }
    shift <- 27 - key
    decoded <- c()
    for (x in decode) {
        decoded <- append(decoded,
                          toupper(caesar(x, shift = shift, decrypt = TRUE)))
    }
    result <- paste(result,
                    paste(decoded, collapse = " "), shift, collapse = " ")
    i <- i + 1
}

cat(trimws(result, "l"))

# $ cat DATA.lst | Rscript dantivar.r
# POETRY IS WHAT 5 CARTHAGE MUST BE 22 NO SOONER SPOKEN 21 IT IS BLACK 6
# THAT ALL MEN 18
