#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 6.32 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Float64)
  data.each_line do |x|
    inter = [] of Float64
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_f : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)

  alan = array[0]
  bob = array[1]

  chance_alan = alan
  chance_bob = (100 - chance_alan)*(bob/100)

  chance_total = chance_alan + chance_bob

  alan_win = Int32.new((alan * 100 / chance_total).round(0))

  print "#{alan_win} "

end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 96 34 29 24 96 91 35 36 13 46 54 96 24 95
# 80 88 85 59 72 55 91 76 33 92 81 88 92 69
