# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
use List::Util qw( min max );
our ($VERSION) = 1;

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data;
}

sub solution {
  my @data    = @_;
  my $SPACE   = q{ };
  my $maximum = max @data;
  my $minimum = min @data;
  exit 1 if !print $maximum. $SPACE . $minimum . "\n";
  return 'The functions works, that is really true?, what is the real truth';
}

sub main {
  my @vals  = data_in();
  my $SPACE = q{ };
  my @body  = split $SPACE, $vals[0];
  solution(@body);
  return 'Ok, if this code works that means that I can make some code in Perl';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 79968 -79467
