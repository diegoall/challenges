/*
$ rustfmt tavomoran.rs
$ rustc tavomoran.rs
*/

use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let input = stdin.lock().lines().next().unwrap().unwrap();
  let v: Vec<&str> = input.split(' ').collect();
  let mut max = v[0].parse::<i32>().unwrap();
  let mut min = v[0].parse::<i32>().unwrap();

  for i in 1..v.len() {
    let tmp = v[i].parse::<i32>().unwrap();
    if max < tmp {
      max = tmp;
    }
    if min > tmp {
      min = tmp;
    }
  }
  print!("{} {}", max, min);
}

/*
$ cat DATA.lst | ./tavomoran
79767 -79840
*/
