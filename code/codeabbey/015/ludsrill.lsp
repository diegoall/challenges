#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(defvar *item*)
(defvar *maximum*)
(defvar *minimum*)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(setq *data* (read-data))

(setq *maximum* (first (first *data*)))
(setq *minimum* (first (first *data*)))

(dolist (*item*  (first *data*))
  (cond ((> *item* *maximum*) (setq *maximum* *item*)))
  (cond ((< *item* *minimum*) (setq *minimum* *item*))))

(format t "~s " *maximum*)
(format t "~s " *minimum*)

#|
cat DATA.lst | clisp ludsrill.lsp
79767 -79840
|#
