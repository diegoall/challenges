;; $ clj-kondo --lint slayfer1112.clj
;; linting took 15ms, errors: 0, warnings: 0

(ns slayfer1112-038
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data [file]
  (let [dat (slurp file)
        datv (str/split-lines dat)
        head (str/split (datv 0) #" ")
        body (subvec datv 1 (alength (to-array-2d datv)))]
    [head
     body]))

(defn same-pos [hint guess counter start]
  (if (< start 4)
    (if (= (hint start) (guess start))
      (same-pos hint guess (inc counter) (inc start))
      (same-pos hint guess counter (inc start)))
    counter))

(defn any-pos [hint guess counter start]
  (if (< start 4)
    (if (= (.indexOf hint (guess start)) -1)
      (any-pos hint guess counter (inc start))
      (any-pos hint guess (inc counter) (inc start)))
    counter))

(defn solution [head body]
  (let [hint (str/split (head 0) #"")
        guesses (str/split (body 0) #" ")]
    (doseq [x guesses]
      (let [guess (str/split x #"")
            correct-pos (same-pos hint guess 0 0)
            diff-pos (- (any-pos hint guess 0 0) correct-pos)
            ]
        (print (str correct-pos "-" diff-pos " "))
        ))))

(defn main []
  (let [[head body] (get-data "DATA.lst")]
    (solution head body)))

(main)

;; 0-1 0-1 0-0 0-3 1-1 0-2 0-2 0-3 0-2 0-1 0-2
