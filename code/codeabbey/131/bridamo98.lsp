#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun delimiterp (c)
  (char= c #\Space)
)

(defun split (string &key (delimiterp #'delimiterp))
  (loop
    :for beg = (position-if-not delimiterp string)
    :then (position-if-not delimiterp string :start (1+ end))
    :for end = (and beg (position-if delimiterp string :start beg))
    :when beg :collect (subseq string beg end)
    :while end
  )
)

(defun order-array(array)
  (sort array #'string<)
)

(defun add-word(word words-db)
  (setf (aref words-db incremental-index) word)
  (setq incremental-index (+ incremental-index 1))
)

(defun load-db(words-db filename)
   (let ((in (open filename :if-does-not-exist nil)))
    (when in
      (loop for line = (read-line in nil)
        while line do (add-word (order-array line) words-db)
      )
      (close in)
    )
  )
)

(defun match-word (word posible-word)
  (let ((i 0) (j 0))
    (loop
      (if (string= (aref posible-word j) (nth i word))
        (progn
          (setq j (+ j 1))
          (setq i (+ i 1))
          (if (> j (- (length posible-word) 1))
            (return-from match-word 1)
          )
        )
        (setq i (+ i 1))
      )
      (when (> i (- (length word) 1)) (return-from match-word 0))
    )
  )
)

(defun solve-problem (word words-db)
  (let ((unknown-size-word (parse-integer (subseq word 0 1)))
    (i 0) (result 0))
    (loop
      (if (= (length (aref words-db i)) unknown-size-word)
        (progn
          (if (= (match-word (order-array
            (split (subseq word 2 (length word)))) (aref words-db i)) 1)
            (setq result (+ result 1))
          )
        )
      )
      (setq i (+ i 1))
      (when (> i (- (length words-db) 1)) (return NIL))
    )
    (format t "~a " result)
  )
)

(defun solve-problem-words(words-db)
  (let ((size-input (read)))
    (loop for i from 0 to (- size-input 1)
      do(solve-problem (read-line) words-db)
    )
  )
)

(defparameter words-db (make-array 63998))
(defvar incremental-index 0)
(load-db words-db "words.txt")
(solve-problem-words words-db)

#|
  cat DATA.lst | clisp bridamo98.lsp
  119 1 7 17 11 1 8 9 19 59 33 2 6 6 1
|#
