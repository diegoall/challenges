#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun calc-point(alpha pi pf)
  (+ pi (* alpha (- pf pi)))
)

(defun make-copy-array (arr size)
  (setq array-copy (make-array size-input))
  (setq cont 0)
  (loop
    (setf (aref array-copy cont) (aref arr cont))
    (setq cont (+ cont 1))
    (when (= cont size)(return array-copy))
  )
)

(defun recursive-calc(current-alpha n-points x-input-copy y-input-copy)
  (if (= n-points 2)
    (progn
        (format t "~a ~a "
          (round (calc-point current-alpha
            (aref x-input-copy 0) (aref x-input-copy 1)))
          (round (calc-point current-alpha
            (aref y-input-copy 0) (aref y-input-copy 1)))
        )
    )
    (progn
      (setq cont 1)
      (loop
        (setf (aref x-input-copy (- cont 1)) (calc-point current-alpha
          (aref x-input-copy (- cont 1)) (aref x-input-copy cont)))
        (setf (aref y-input-copy (- cont 1)) (calc-point current-alpha
          (aref y-input-copy (- cont 1)) (aref y-input-copy cont)))
        (setq cont (+ cont 1))
        (when (> cont (- n-points 1))(return NIL))
      )
      (recursive-calc current-alpha
        (- n-points 1) x-input-copy y-input-copy
      )
    )
  )
)

(defun obtain-points (size-input alpha-amount x-input y-input)
  (format t "~a ~a " (aref x-input 0) (aref y-input 0))
  (setq alpha-count 1)
  (setq current-alpha (* (/ 1.0 (- alpha-amount 1)) alpha-count))
  (loop
    (recursive-calc current-alpha size-input
      (make-copy-array x-input size-input)
      (make-copy-array y-input size-input)
    )
    (setq alpha-count (+ alpha-count 1))
    (setq current-alpha (* (/ 1.0 (- alpha-amount 1)) alpha-count))
    (when (> alpha-count (- alpha-amount 2))(return NIL))
  )
  (format t "~a ~a "
    (aref x-input (- size-input 1))
    (aref y-input (- size-input 1))
  )
)

(defun get-input-line(x number x-input y-input)
  (setq number (read))
  (setf (aref x-input x) number)
  (setq number (read))
  (setf (aref y-input x) number)
)

(defvar size-input (read))
(defvar alpha-amount (read))
(defparameter x-input (make-array size-input))
(defparameter y-input (make-array size-input))
(defvar number 0)

(loop for x from 0 to (- size-input 1)
  do(get-input-line x number x-input y-input)
)

(obtain-points size-input alpha-amount x-input y-input)

#|
  cat DATA.lst | clisp bridamo98.lsp
  494 492 458 504 429 515 407 525 391 534 382 542 377 548
  377 554 382 559 390 564 402 567 417 570 434 573 452 574
  473 575 494 576 515 575 537 574 558 573 577 570 596 567
  612 562 625 556 636 549 643 541 646 532 644 520 638 507
  626 492 607 475 583 455 551 433
|#
