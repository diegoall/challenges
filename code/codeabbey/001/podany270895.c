#include <stdio.h>

static int sum(int first, int second) /*@*/ {
    return first + second;
}

int main(int argc, char *argv[]) {
  if ( argc == 2) {
    int first;
    int second;
    if (sscanf(argv[0], "%i", &first) != 1) {
      return(1);
    }
    if (sscanf(argv[1], "%i", &second) != 1) {
      return(1);
    }
    printf("%5d\n",sum(first, second));
    return(0);
  }
  return(1);
}
