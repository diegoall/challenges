(defun sum (n1 n2)
  (+ n1 n2)
)

(print (sum (parse-integer (nth 0 *args*)) (parse-integer (nth 1 *args*))))
