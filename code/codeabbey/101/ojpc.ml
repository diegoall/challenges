let cal x y a b c =    ((x-.a)**2.0)+.((y-.b)**2.0)+.c*.exp((-.((x+.a)**2.0)-.((y+.b)**2.0)));;
let printArray arr = Array.iter (Printf.printf "%i ") arr;;  

let out  =
    let a= -0.4 in
  let b= 0.5 in
  let c= 7.0 in
    let g= 1e-9 in
    let inp=[[-0.5;0.4];[-0.5;0.1];[0.7;0.5];[0.1;0.1];[0.7;0.1];[-0.7;-0.4];[-0.6;0.6];[-0.7;-0.3];[-0.3;-0.3];[-0.8;0.5];[-0.6;-0.9];[0.0;-1.0];[-0.5;-0.2];[-0.5;-0.1];[-0.6;0.7]] in
    for i=0 to (List.length inp) -1     do    
        let x=(List.nth (List.nth inp (i)) 0) in
        let y=(List.nth (List.nth inp (i)) 1) in
        let f= cal x y a b c in
      let fp=cal (x+.g) y a b c in
      let fpp=cal x (y+.g) a b c in
      let res=  [|0|]    in    
      let xg=(fp-.f)/.(g) in
        let yg=(fpp-.f)/.(g) in
        let ang= atan2 yg xg in
      let pi = 4.0 *. atan 1.0 in 
        let f=int_of_float (180.0+.(floor(ang*.(180.0/.pi) +. 0.5))) in
        res.(0) <- f;
        printArray res
    done
