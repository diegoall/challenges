# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
use POSIX;
use Math::Round;
our ($VERSION) = 1;

my $EMPTY = q{};
my $SPACE = q{ };

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data[ 1 .. $#data ];
}

sub isComplex {
  my @data  = @_;
  my $valBS = $data[0];
  my $valAC = $data[1];
  if ( $valBS < $valAC ) {
    return 1;
  }
  return 0;
}

sub result {
  my @data    = @_;
  my $valB    = $data[0];
  my $valBS   = $data[1];
  my $valAC   = $data[2];
  my $valA    = $data[ 2 + 1 ];
  my $complex = $data[ 2 + 2 ];
  if ($complex) {
    my $val1 = round( -$valB / $valA );
    my $val2 = round( ( $valAC - $valBS )**( 1 / 2 ) / $valA );
    exit 1 if !print "$val1+$val2" . 'i ';
    return "$val1-$val2" . 'i; ';
  }
  my $val1 = round( -$valB + ( $valBS - $valAC )**( 1 / 2 ) / $valA );
  my $val2 = round( -$valB - ( $valBS - $valAC )**( 1 / 2 ) / $valA );
  return "$val1 $val1; ";
}

sub solution {
  my @data    = @_;
  my $vals    = $data[0];
  my @dat     = split $SPACE, $vals;
  my $valA    = $dat[0];
  my $valB    = $dat[1];
  my $valC    = $dat[2];
  my $valBS   = $valB * $valB;
  my $valAC   = 2 * 2 * $valA * $valC;
  my $valAS   = 2 * $valA;
  my $complex = isComplex( $valBS, $valAC );
  exit 1 if !print result( $valB, $valBS, $valAC, $valAS, $complex );
  return 'This is time of probability!';
}

sub main {
  my @vals = data_in();
  for my $test (@vals) {
    solution($test);
  }
  exit 1 if !print "\n";
  return 'You make it in the first attemp?';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# -16 -16; -41 -41; 34 34; 4 4; 5+4i 5-4i; 1+6i 1-6i;
# -9+3i -9-3i; -9 -9; 66 66; 10+1i 10-1i; 73 73; 10+2i 10-2i;
