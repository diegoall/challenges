;; $ clj-kondo --lint slayfer1112.clj
;; linting took 16ms, errors: 0, warnings: 0

(ns slayfer1112-038
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [dat (slurp *in*)
        datv (str/split-lines dat)
        head (str/split (datv 0) #" ")
        body (subvec datv 1 (alength (to-array-2d datv)))]
    [head
     body]))

(defn abc [line]
  (let [x (str/split line #" ")
        a (x 0)
        b (x 1)
        c (x 2)]
    [(read-string a)
     (read-string b)
     (read-string c)]))

(defn is-complex [b2 ac4]
  (if (< b2 ac4)
    true
    false))

(defn result [b b2 ac4 a2 complex]
  (if complex
    (do (let [val1 (int (/ (* b -1) a2))
              val2 (int (/ (Math/pow (- ac4 b2) 0.5) a2))]
          (print (str val1 "+" val2 "i" " "))
          (print (str val1 "-" val2 "i")))
        (print "; "))
    (do (let [val1 (int (/ (+ (* b -1) (Math/pow (- b2 ac4) 0.5)) a2))
              val2 (int (/ (- (* b -1) (Math/pow (- b2 ac4) 0.5)) a2))]
          (print (str val1 " " val2)))
        (print "; "))))

(defn root [a b c]
  (let [b2 (* b b)
        ac4 (* 4 a c)
        a2 (* 2 a)
        complex (is-complex b2 ac4)]
    (result b b2 ac4 a2 complex)))

(defn solution [_ body]
  (doseq [x body]
    (let [[a b c] (abc x)]
      (root a b c))))

(defn main []
  (let [[head body] (get-data)]
    (solution head body)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; -2 -7; 2 -7; 9 1; 5 -8; 5+4i 5-4i; 1+6i 1-6i; -9+3i
;; -9-3i; -1 -2; 6 2; 10+1i 10-1i; 8 2; 10+2i 10-2i
