/* $ npx eslint paolagiraldo.ts
$ tsc paolagiraldo.ts
*/

function calculateModExp(numData: number[]): number {
  const [base, exponent, module] = [numData[0], numData[1], numData[2]];

  if (exponent === 0) {
    const result = 1;
    console.log(result);
  } else if (exponent % 2 === 0) {
    const res = Math.pow(base, exponent / 2) % module;
    const result = (res * res) % module;
    console.log(result);
  } else {
    const result =
      (((base % module) * Math.pow(base, exponent - 1)) % module) % module;
    console.log(result);
  }
  return 0;
}

function dataProcessing(inputData: string): number {
  const rowsData = inputData.split('\n').slice(1);
  rowsData.forEach((row: string) => {
    const numData = row.split(' ').map((numStr: string) => Number(numStr));
    calculateModExp(numData);
    return 'Data Processed';
  });
  return 0;
}

function main(): number {
  process.stdin.setEncoding('utf8');
  process.stdin.on('readable', () => {
    const inputData = process.stdin.read();
    if (inputData !== null) {
      dataProcessing(inputData);
    }
    return 'Data Loaded';
  });
  return 0;
}

main();

/*
$ cat DATA.lst | node paolagiraldo.js
10594023 211939547 55545729 2832713 56271383 178407083 58669467 22271111
82003049 47973142 109455420 230060344 163697559 200585365 160550707
293986818 159584647 202895490 215033073 154766909 26071892 135793813
66955167
 */
