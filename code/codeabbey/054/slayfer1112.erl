% $ erl -compile slayfer1112.erl
% $ erl -noshell -s erlint slayfer1112 -s init stop
% ok

-module(slayfer1112).

-import(lists, [nth/2]).

-export([start/0]).

-compile(slayfer1112).

% Read file and convert in binary list
readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").

% Take the number of inputs
inputs(Array) ->
  [H | T] = Array,
  {Inputs, _} = string:to_integer(H),
  Values = for(1, Inputs,
   fun (X) -> {A, _} = string:to_integer(nth(X, T)), [A]
   end),
  {Inputs, Values}.

% For loop with I<N
for(N, N, F) -> F(N);
for(I, N, F) -> F(I) ++ for(I + 1, N, F).

% Print the result
result(F, List) -> [F(X) || X <- List].

% Test if sum the number
triplet(A, D) ->
  Condition = case abs of
    _ when D * D - 2 * D * A >= 0 ->
      math:fmod(D * D - 2 * D * A, 2 * (D - A));
    _ -> []
  end,
  case triplets of
    _ when Condition == 0 ->
  B = (D * D - 2 * D * A) / (2 * (D - A)),
  C = D - B - A,
  R = trunc(C * C),
  [R];
    _ -> []
  end.

% Count the number of triplets that sum the number
count(Value) ->
  Counter = for(1, Value,
    fun (X) -> triplet(X, Value) end),
  Counter.

start() ->
  Data = readfile("DATA.lst"),
  {N, Values} = inputs(Data),
  Counter = for(1, N,
    fun (X) -> [nth(1, count(nth(X, Values)))] end),
  Print = fun (X) -> io:fwrite("~w ", [X]) end,
  result(Print, Counter),
  io:fwrite("~n").

% erl -noshell -s slayfer1112 -s init stop
% 74769519481969 86897912829025 84579014989225 60020114980225
% 56010510456289 43231953160201 76400623155025 37827580070569
