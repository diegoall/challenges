#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(defun split-by-one-space (item i split)
  (let ((j))
    (setq j (position #\Space item :start i))
    (if (equal j NIL)
      (progn
        (setq split (append split (list (subseq item i (length item)))))
        (return-from split-by-one-space split))
      (progn
        (setq split (append split (list (subseq item i j))))
        (setq i (1+ j))
        (split-by-one-space item i split)))))

(defun organize-data (data data-pointer all-different with-coincidence)
  (let ((counter 0)
        (void-list ())
        (item))

    (if (<= data-pointer (- (length data) 1))
      (progn
        (setq item (elt data data-pointer))
        (if (> (length item) 4)
          (setq item (split-by-one-space item counter void-list)))

        (if (equal (second item) "0")
          (setq all-different (append all-different (list item)))
          (setq with-coincidence (append with-coincidence (list item))))

        (setq data-pointer (+ data-pointer 1))
        (organize-data data data-pointer all-different with-coincidence))

      (return-from organize-data (list all-different with-coincidence)))))

(defun without-coincidence (all-different with-coincidence
                            filtered all-different-pointer
                            with-coincidence-pointer i)

  (let ((guess)
        (bad-guess))
    (if (<= with-coincidence-pointer (- (length with-coincidence) 1))
      (progn
        (setq guess (elt with-coincidence with-coincidence-pointer))
        (setq bad-guess (elt all-different all-different-pointer))
        (if (< i 4)
          (progn
            (when (equal (char (first guess) i) (char (first bad-guess) i))
              (setf (char (first guess) i) #\.))
            (setq i (1+ i)))

          (progn
            (setq i 0)
            (if (< all-different-pointer (- (length all-different) 1))
              (setq all-different-pointer (1+ all-different-pointer))

              (progn
                (setq filtered (append filtered (list guess)))
                (setq with-coincidence-pointer (1+ with-coincidence-pointer))
                (setq all-different-pointer 0)))))

        (without-coincidence all-different with-coincidence filtered
         all-different-pointer with-coincidence-pointer i))

      (return-from without-coincidence filtered))))

(defun one-number-in-row (filtered filtered-pointer i result)
  (let ((aux-filtered))
    (if (<= filtered-pointer (- (length filtered) 1))
      (progn
        (setq aux-filtered (first (elt filtered filtered-pointer)))
        (if (and (= (count #\. aux-filtered :test #'equal) 3) (< i 4))
          (if (equal (char aux-filtered i) #\.)
            (setq i (1+ i))
            (progn
              (setf (char result i) (char aux-filtered i))
              (setq i (1+ i))))

          (progn
            (setq filtered-pointer (1+ filtered-pointer))
            (setq i 0)))
        (one-number-in-row filtered filtered-pointer i result))

      (return-from one-number-in-row result))))

(defun all-equal-one-column (result filtered filtered-pointer i column)
  (let ((aux-filtered))
    (if (<= i (- (length result) 1))
      (progn
        (if (<= filtered-pointer (- (length filtered) 1))
          (progn
            (when (= filtered-pointer 0)
              (setq column ()))
            (setq aux-filtered (first (elt filtered filtered-pointer)))
            (setq filtered-pointer (1+ filtered-pointer))
            (if (equal (char aux-filtered i) #\.)
              NIL
              (setq column (append column (list (char aux-filtered i))))))

          (progn
            (setq filtered-pointer 0)
            (if (and
                (= (length (remove-duplicates column :test #'char-equal)) 1)
                (equal (char result i) #\.))
              (setf (char result i) (first column)))
            (setq i (1+ i))))

        (all-equal-one-column result filtered filtered-pointer i column))

      (return-from all-equal-one-column result))))

(defun update-filtered (filtered filtered-pointer result i)
  (let ((aux-filtered))
    (if (<= filtered-pointer (- (length filtered) 1))
      (progn
        (setq aux-filtered (elt filtered filtered-pointer))
        (if (and (equal (second aux-filtered) "1")
                (= (count #\. (first aux-filtered) :test #'equal) 3))
          (progn
            (if (< i 4)
              (if (and (string/= (string (char result i)) ".")
                      (equal (char (first aux-filtered) i) (char result i)))
                (progn
                  (setf filtered (delete aux-filtered filtered :test #'equal))
                  (setq i 0))

                (setq i (1+ i)))

              (progn
                (setq i 0)
                (setq filtered-pointer (1+ filtered-pointer)))))

          (if (< i 4)
            (progn
              (when (string/= (string (char result i)) ".")
                (if (and (equal (char (first aux-filtered) i) (char result i)))
                  (progn
                    (setf (second aux-filtered)
                          (write-to-string
                            (- (parse-integer (second aux-filtered)) 1)))
                    (setf (char (first aux-filtered) i) #\.))
                  (setf (char (first aux-filtered) i) #\.)))
              (setq i (1+ i)))
            (progn
              (setq i 0)
              (setq filtered-pointer (1+ filtered-pointer)))))

        (update-filtered filtered filtered-pointer result i))
      (return-from update-filtered filtered))))

(defun code-guesser (data result)
  (let ((counter 0)
        (filtered ())
        (void-list ())
        (group)
        (all-different)
        (with-coincidence))

    (setq group (organize-data data counter void-list void-list))
    (setq all-different (first group))
    (setq with-coincidence (second group))

    (setq filtered (without-coincidence all-different with-coincidence
      filtered counter counter counter))
    (setq result (one-number-in-row filtered counter counter result))
    (setq result (all-equal-one-column result filtered counter counter
      void-list))
    (setq filtered (update-filtered filtered counter result counter))

    (if (= (count #\. result :test #'equal) 0)
      (print result)
      (code-guesser filtered result))))

(defun main ()
  (let ((data)
        (result))
    (setq data (cdr (read-data)))
    (setq result (copy-seq "...."))
    (code-guesser data result)))

(main)

#|
cat DATA.lst | clisp ludsrill.lsp
6776
|#
