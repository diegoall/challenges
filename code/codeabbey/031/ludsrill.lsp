#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun rotate-string ()
  (let ((data)
        (str)
        (value)
        (first-part)
        (second-part))

    (setq data (cdr (read-data)))
    (dolist (item data)
      (setq str (copy-seq (string (second item))))

      (if (> (first item) 0)
        (setq value (first item))
        (setq value (+ (length str) (first item))))

      (setq second-part (subseq str 0 value))
      (setq first-part (subseq str value))

      (format t (concatenate 'string first-part second-part))
      (format t " "))))

(rotate-string)

#|
cat DATA.lst | clisp ludsrill.lsp
ORYNEPXVIEOJANSUKAYLRFXEU QRFODLTAHPRYUVGQF TITZEXRGASUSGRRNYB
LHIKEIZYJRJQROEECWVPOXW XMOYXYIRYHCBXUOAAU DFEOYYSNPBESYPPUPQIHUDHT
JUGOMVLAFOJMYMUMQEOAATZSB FNIFYJJTDKHEEJGRPCE FYRLXAVLEYTYBUMOE
LRRKUARUUGALLGKURCWGBNMD KUAOGEYBWNBPBERP
|#
