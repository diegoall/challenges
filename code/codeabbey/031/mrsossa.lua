--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local d = -1
for line in data do
    if d ~= -1 then
        local num
        local cadena
        local a = 0
        for token in string.gmatch(line, "[^%s]+") do
            if a == 0 then
                num = tonumber(token)
            else
                cadena = token
            end
            a = a + 1
        end
        local sub1
        local sub2
        if num>0 then
            sub1 = cadena:sub(1,num)
            sub2 = cadena:sub(num+1)
        else
            sub2 = cadena:sub((cadena:len()+num)+1)
            sub1 = cadena:sub(1,cadena:len()+num)
        end
        print(sub2 .. sub1)
    end
    d = d +1
end

--[[
$ lua mrsossa.lua
whomthebelltollsfor numberverycomplex
]]
