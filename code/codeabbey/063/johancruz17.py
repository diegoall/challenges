#!/usr/bin/env python3
# $ mypy johan17.py
# Success: no issues found in 1 source file

from typing import List


def data() -> List[int]:

    input_0: int = int(input())
    inputs: List[int] = []
    count: int = 0

    while count < input_0:
        inputs = inputs + [int(input())]
        count += 1

    return inputs


def calculate(inputs: List[int]) -> None:
    multiplications: int = 1
    answers = ""
    answers_list: List[str] = []
    for number in inputs:
        answer = ""
        factor = 2
        while factor <= number and (multiplications != number):
            while number % factor == 0:
                answer += str(factor) + "*"
                multiplications = multiplications * factor
                number = int(number / factor)
            if number == 1:
                break
            factor += 1
        answers += answer + " "
    answers_list = answers.split("* ")
    definitive = ""
    for answer in answers_list:
        definitive += answer + " "
    print(definitive)


if __name__ == "__main__":
    calculate(data())


# $ cat DATA.lst | python3 johan17.py
# 89*193*223*353*367 137*193*241*313*587 53*167*271*433*523
# 191*223*251*349*547 149*283*389*433*587 113*479*509*571*577
# 107*211*359*587*599 67*113*197*563*571 89*107*349*401*541
# 89*109*193*223*449 199*229*263*293*379 59*157*251*409*457
# 53*241*269*379*409 107*199*283*317*547 79*347*379*431*439
# 389*439*467*509 59*89*193*233*263 107*181*379*463*467
# 317*443*461*541 223*233*293*439*571 83*107*317*443*463
# 79*97*101*397*433 307*349*359*457*509 61*79*389*461*503
# 53*239*277*523*577 61*97*157*193*599 71*227*251*283*503
