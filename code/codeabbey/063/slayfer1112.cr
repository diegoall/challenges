#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 8.48 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Float64)
  data.each_line do |x|
    inter = [] of Float64
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_f : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)

  num = array[0]
  sol = [] of String

  counter = 2

  while num != 1
    a = num.divmod(counter)
    if a[1] == 0
      num = a[0]
      sol << "#{counter}"
      counter = 1
    end
    counter += 1
  end

  sols = sol.join("*")

  print "#{sols} "

end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 89*193*223*353*367 137*193*241*313*587 53*167*271*433*523
# 191*223*251*349*547 149*283*389*433*587 113*479*509*571*577
# 107*211*359*587*599 67*113*197*563*571 89*107*349*401*541
# 89*109*193*223*449 199*229*263*293*379 59*157*251*409*457
# 53*241*269*379*409 107*199*283*317*547 79*347*379*431*439
# 389*439*467*509 59*89*193*233*263 107*181*379*463*467
# 317*443*461*541 223*233*293*439*571 83*107*317*443*463
# 79*97*101*397*433 307*349*359*457*509 61*79*389*461*503
# 53*239*277*523*577 61*97*157*193*599 71*227*251*283*503
