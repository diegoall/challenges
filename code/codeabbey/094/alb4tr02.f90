! fortran-linter --syntax-only --linelength = 80 --verbose alb4tr02.f90 #linting
! Checking alb4tr02.f90
! gfortran -Wall -pedantic-errors -Werror alb4tr02.f90 -o alb4tr02 #compilation

PROGRAM products

  IMPLICIT NONE

  integer n
  integer stat
  integer i
  integer rlt
  integer aux
  integer asc
  character(1) c
  logical flag

  n = 0
  flag = .TRUE.

  DO WHILE(flag)
   CALL fget(c, stat)
   IF (stat /= 0) exit
   asc = IACHAR(c)
   IF (asc == 10) then
      flag = .FALSE.
   ELSE
      n = n *10
      n = n + asc - 48
   END IF
  END DO

  rlt = 0
  aux = 0

  DO i = 1, n, 1
    flag = .TRUE.
    DO WHILE(flag)
       CALL fget(c, stat)
       IF (stat /= 0) exit
       asc = IACHAR(c)
       IF (asc == 10) flag = .FALSE.
       IF ((asc == 32) .OR. (asc == 10)) then
          rlt = rlt + aux**2
          aux = 0
       ELSE
          aux = aux * 10
          aux = aux + asc - 48
       END IF
    END DO
    write (*, fmt="(1x,a,i0)", advance='no') '', rlt
    rlt = 0
  END DO
  print *, ''
END PROGRAM products

! cat DATA.lst | ./alb4tr02
! 93 224 1760 219 1791 179 973 583 311 568 26 276 37 924 322 1090 1192
