# frozen_string_literal: true

# $ rubocop aurajimenez.rb
# Inspecting 1 file
# .
# 1 file inspected, no offenses detected

def fool_day(numbers_array)
  sum = 0
  numbers_array.each do |number|
    number = number.to_i
    sum += number * number
  end
  sum
end

File.readlines('DATA.lst').drop(1).each do |line|
  data = line.split
  result = fool_day(data)
  print result.to_s + ' '
end

# ruby aurajimenez.rb
# 93 224 1760 219 1791 179 973 583 311 568 26 276 37 924 322 1090 1192
