# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
our ($VERSION) = 1;

use Readonly;
use POSIX;

my $EMPTY = q{};
my $SPACE = q{ };

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data;
}

sub visitor_position {
  my @args  = @_;
  my @vals  = @args;
  my $index = 0;
  my @a     = ( 0 .. $#vals );
  for my $i (@a) {
    if ( $vals[$i] != 0 && $vals[$index] == 0 ) {
      $index = $i;
    }
    if ( $vals[$i] != 0 && $vals[$index] != 0 ) {
      if ( $vals[$i] > $vals[$index] ) {
        $index = $i;
      }
    }
  }
  return $index;
}

sub solution {
  my @args           = @_;
  my @vals           = split $SPACE, $args[0];
  my $visitors       = $vals[0];
  my $seed           = $vals[1];
  my $discomfort     = 0;
  my $N              = $visitors;
  my $T              = 0;
  my @visitors_queue = ();
  Readonly my $A => 445;
  Readonly my $C => 700_001;
  Readonly my $M => 2_097_152;
  Readonly my $Z => 999;

  while ( $visitors > 0 ) {
    if ( $T % 2 == 0 && $T > 0 ) {
      my $vis_pos = visitor_position @visitors_queue;
      $discomfort += $visitors_queue[$vis_pos] * ( $T - $vis_pos );
      $visitors_queue[$vis_pos] = 0;
      $visitors--;
    }
    if ( $T < $N ) {
      my $linear     = ( $A * $seed + $C ) % $M;
      my $starvation = ( $linear % $Z ) + 1;
      $seed = $linear;
      push @visitors_queue, $starvation;
    }
    $T++;
  }

  exit 1 if !print "$discomfort ";
  return 'When I was trying with functional programming i get a stackoverflow';
}

sub main {
  my @vals = data_in();
  for ( 0 .. $#vals ) {
    solution $vals[$_];
  }
  exit 1 if !print "\n";
  return 'This one was a hard challenge, solved in the date 24/07/2020';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 12583026226
