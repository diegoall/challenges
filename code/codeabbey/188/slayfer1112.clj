;; $ clj-kondo --lint slayfer1112.clj
;; linting took 16ms, errors: 0, warnings: 0

(ns slayfer1112-038
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.shell :as shell]))

(defn get-data []
  (let [dat (slurp *in*)
        datv (str/split-lines dat)
        head (str/split (datv 0) #" ")]
    [head]))

(defn solution [head]
  (let [web (str
             "<html>\n"
              "<head>\n"
              "<title>Basics of HTML demo</title>\n"
              "</head>\n"
              "<body>\n"
              "<p>Secret value is <b>" head "</b>.</p>\n"
              "<p>Read more at\n"
              "<a href="
              "'http://www.codeabbey.com/index/task_view/basics-of-html'"
              ">this task</a>.\n"
              "</p>\n"
              "</body>\n"
              "</html>")
        folder "/Codeabbey"
        challenge "/188"
        filename "/basics-of-HTML.html"
        path (str folder challenge filename)
        git (str "cd ./Codeabbey; git ")]
    (spit (str "." path) web)
    (println (:out (shell/sh "sh" "-c" (str git "add ."))))
    (println (:out (shell/sh "sh" "-c" (str git "commit -m 'Secret'"))))
    (println (:out (shell/sh "sh" "-c" (str git "push"))))
    (println (str "https://pastelito02.github.io" path)))
  (Thread/sleep 5000)
  (System/exit 0))

(defn main []
  (let [[head] (get-data)]
    (solution (head 0))))

(main)

;; cat DATA.lst | clj slayfer1112.clj
;; http://pastelito02.github.io/Codeabbey/188/basics-of-HTML.html
