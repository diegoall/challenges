/*
$ clippy-driver villjorge.rs -D warnings
$ rustc villjorge.rs
*/

fn io2vec() -> Vec<u32> {
  let data: Vec<u32> = { strdata() };
  data
}

fn strdata() -> Vec<u32> {
  use std::io::{self, BufRead};
  let stdin = io::stdin();
  let strsizevalue = stdin.lock().lines().next().unwrap().unwrap();
  let numvect = strsizevalue
    .split_whitespace()
    .map(|num| num.trim().parse::<u32>().unwrap())
    .collect();
  numvect
}

fn gpath(isles: &[u32], posr: isize) -> u32 {
  let isles = isles;
  use std::cmp;
  use std::cmp::Ordering;
  match posr.cmp(&0) {
    Ordering::Less => 0,
    Ordering::Greater => {
      let path_1 = gpath(&isles, posr - 2);
      let path_2 = gpath(&isles, posr - 3);
      let posr = posr as usize;
      let comp = cmp::max(path_1, path_2) as u32;
      isles[posr] + comp
    }
    Ordering::Equal => {
      let posr = posr as usize;
      isles[posr]
    }
  }
}

fn main() {
  use std::cmp;
  let nworlds = io2vec();

  for _i in 0..nworlds[0] {
    let data = io2vec();
    let isles: Vec<u32> = data.iter().copied().rev().collect();
    let posr = isles.len() as isize;
    let gpath_a = gpath(&isles, posr - 1);
    let (_, isles_redux) = isles.split_at(1);
    let gpath_b = gpath(&isles_redux, posr - 2);
    print!("{} ", cmp::max(gpath_a, gpath_b));
  }
  println!();
}

/*
$ cat DATA.lst | ./villjorge
213 195 112 214 170 170 188 225 160 167 186 126 115 147 140 155 200 156
*/
