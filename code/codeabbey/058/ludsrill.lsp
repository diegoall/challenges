#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun card-names ()
  (let ((data)
        (suit)
        (suits)
        (rank)
        (ranks))
    (setq data (first (cdr (read-data))))
    (setq suits '(Clubs Spades Diamonds Hearts))
    (setq ranks '(2 3 4 5 6 7 8 9 10 Jack Queen King Ace))

    (dolist (item data)
      (setq suit (elt suits (floor item 13)))
      (setq rank (elt ranks (mod item 13)))
      (format t "~:(~a~)-of-~:(~a~) " rank suit))))

(card-names)

#|
cat DATA.lst | clisp ludsrill.lsp
6-of-Clubs 9-of-Diamonds 9-of-Spades King-of-Diamonds 2-of-Diamonds
Queen-of-Clubs 3-of-Diamonds 10-of-Clubs Ace-of-Hearts King-of-Hearts
Jack-of-Hearts Queen-of-Hearts King-of-Spades 5-of-Diamonds 10-of-Hearts
Jack-of-Spades 5-of-Clubs 10-of-Spades 9-of-Hearts Ace-of-Diamonds 2-of-Hearts
6-of-Spades 5-of-Hearts Ace-of-Spades 7-of-Clubs Jack-of-Clubs
|#
