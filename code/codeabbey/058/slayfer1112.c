/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#define MAX_SIZE 1024

static int get_suits (/*@reldef@*/ int suit)
  /*@modifies nothing@*/ {
  if (suit == 0) { printf("Clubs "); }
  else if (suit == 1) { printf("Spades "); }
  else if (suit == 2) { printf("Diamonds "); }
  else if (suit == 3) { printf("Hearts "); }
  else { return 0; }
  return 1;
}

static int get_ranks (/*@reldef@*/ int rank)
  /*@modifies nothing@*/ {
  if (rank < 8) { printf("%d", rank + 2); }
  else if (rank == 8) { printf("10"); }
  else if (rank == 9) { printf("Jack"); }
  else if (rank == 10) { printf("Queen"); }
  else if (rank == 11) { printf("King"); }
  else if (rank == 12) { printf("Ace"); }
  else { return 0; }
  return 1;
}

static int get_vals (/*@in@*/ char cards_arr[],/*@reldef@*/ int z)
  /*@modifies nothing@*/ {
  char temp[MAX_SIZE];
  static int card_seq[MAX_SIZE];
  int i, j;
  int k = 0;
  j = 0, i = 0;
  while (true) {
    if (cards_arr[i] != '\n') {
      if (cards_arr[i] != ' ') { temp[k] = cards_arr[i]; }
      else { temp[k] = '\0', card_seq[j] = atoi(temp), j++, k = -1; }
      k++, i++;
    } else { temp[k] = '\0', card_seq[j] = atoi(temp); break; }
  }
  return card_seq[z];
}

int main(void) {
  char temp[MAX_SIZE];
  char cards_arr[MAX_SIZE];
  int cards;
  int card[2] = {0, 0};
  int i = 0;
  if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
  cards = atoi(temp);
  if (fgets(cards_arr, MAX_SIZE, stdin) == 0) { return 0; }
  for (i = 0; i < cards; i++) {
    card[0] = (int) get_vals(cards_arr, i) / 13;
    card[1] = get_vals(cards_arr, i) % 13;
    if (get_ranks(card[1]) == 0) {return 0;}
    printf("-of-");
    if (get_suits(card[0]) == 0) {return 0;}
  }
  printf("\n");
  return 1;
}

/*
$ cat DATA.lst | ./slayfer1112
King-of-Spades Queen-of-Hearts Jack-of-Spades Ace-of-Hearts 3-of-Clubs
Ace-of-Clubs Queen-of-Clubs 4-of-Diamonds 6-of-Clubs King-of-Clubs
4-of-Spades Ace-of-Diamonds 3-of-Diamonds Ace-of-Spades Jack-of-Diamonds
2-of-Diamonds 9-of-Spades Jack-of-Clubs 7-of-Clubs 2-of-Clubs Queen-of-Spades
2-of-Spades 4-of-Clubs 10-of-Spades 7-of-Spades King-of-Diamonds
10-of-Diamonds 10-of-Hearts 8-of-Spades
*/
