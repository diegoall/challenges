% erlc -W john2104.erl
% erl -compile john2104.erl
% erlint:lint("john2104.erl").
% {ok,[]}

-module(john2104).
-export([start/0]).
-export([readfile/1]).
-export([domath/1]).
-export([tofloat/1]).
-export([loop/1]).
-export([digest/1]).
-import(lists,[nth/2]).


loop(A) ->
  lists:foreach(fun(X) -> digest(X) end, A).


digest(A) ->
  Z = element(1, tofloat(A)),
  domath(Z).


domath(A) when A > 1 ->
  ok;


domath(A) when A < 1 ->
  K = float(A),
  Z = K * 6,
  R = erlang:float_to_list(Z, [{decimals, 4}]),
  Resp = element(1,string:to_integer(R)) + 1,
  io:fwrite("~w ", [Resp]).


tofloat(S) ->
  Z = string:to_float(S),
  Y = element(1, Z),
  if Y == error ->
    string:to_integer(S);
  true -> Z
  end.


readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").


start() ->
  Arr = readfile("DATA.lst"),
  loop(Arr),
  io:fwrite("~n").


% erl -noshell -s john2104 start -s init stop
% 4 6 3 2 5 1
