#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defparameter *gansters* ())
(defparameter *weapon-pistol* ())
(defparameter *weapon-grenades* ())
(defvar *parameters*)
(defvar *data*)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun combinations (s n)
  (cond ((zerop n) (list nil))
        ((null s) nil)
        (T (nconc (mapcar #'(lambda (c) (cons (car s) c))
                   (combinations (cdr s) (1- n))) (combinations (cdr s) n)))))

(defun show (pistol grenades)
  (let ((aux-pistols 0)
        (aux-grenades 0))
    (loop for i from 0 to (- (length pistol) 1)
      do(loop for j from 0 to (- (length (elt pistol i)) 1)
        do(setq aux-pistols (+ aux-pistols (elt (elt pistol i) j)))
        (setq aux-grenades (+ aux-grenades (elt (elt grenades i) j))))

      (when (and (= aux-pistols (second *parameters*)) (= aux-grenades
                                                        (third *parameters*)))
        (setq *gansters* (elt pistol i)))

      (setq aux-pistols 0)
      (setq aux-grenades 0))))

(defun organize-data (data)
  (let ((filtered-data (cdr data))
        (counter -1)
        (aux-list ())
        (aux-list-chunks ())
        (aux-list-pistol ())
        (aux-list-grenades ()))

    (dolist (item filtered-data)
    (if (= (length item) 2)
      (setq aux-list (append aux-list (list item)))
      (setq aux-list-chunks (append aux-list-chunks item))))

    (dolist (item aux-list-chunks)
      (loop for i from 0 to (- item 1)
        do(setq counter (+ 1 counter))
        (setq aux-list-pistol (append aux-list-pistol
        (list (first (elt aux-list counter)))))
        (setq aux-list-grenades (append aux-list-grenades
                                  (list (second (elt aux-list counter))))))

      (setq *weapon-pistol* (append *weapon-pistol* (list aux-list-pistol)))
      (setq *weapon-grenades* (append *weapon-grenades*
                                (list aux-list-grenades)))
      (setq aux-list-pistol ())
      (setq aux-list-grenades ()))))

(setq *data* (read-data))
(setq *parameters* (car *data*))
(organize-data *data*)

(loop for i from 0 to (- (length *weapon-pistol*) 1)
  do(loop for j from 2 to (length (elt *weapon-pistol* i))
    do(setq combination-pistol (combinations (elt *weapon-pistol* i) j))
    (setq combination-grenades (combinations (elt *weapon-grenades* i) j))
    (show combination-pistol combination-grenades))
  (format t "~s " (length *gansters*)))

#|
cat DATA.lst | clisp ludsrill.lsp
6 8 11 4 11 6 8 5 8 5 6 9
|#
