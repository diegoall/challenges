## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://cryptosploit/robots.txt
  CWE:
    CWE-288: Authentication Bypass Using an Alternate Path or Channel
  Rule:
    REQ.R122: R122. Validate credential ownership
  Goal:
    Gain root access
  Recommendation:
    Keep kernel up to date

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Debian          |   stretch   |
    | Chrome          |80.0.3987.122|
    | netdiscover     |  0.3-beta7  |
    | nmap            |    7.40     |
    | gobuster        |    3.0.1    |
    | base64          |    8.26     |
  TOE information:
    Given I am accessing the site 192.168.1.15
    And I added "cryptosploit" to my "hosts" file
    """
    192.168.1.15    cryptosploit    cryptosploit
    """
    And is running on Ubuntu 12.0.4 and kernel 3.13.0
    And is on VirtualBox


  Scenario: Normal use case
    Given I access the site it is a simple page
    And reproduces a gif

  Scenario: Static detection
    Given I just have access to the html code
    And there is just a username commented
    Then I can't do static detection

  Scenario: Dynamic detection
    Given that I wasn't able to access the source code
    And that the site tells me that is vulnerable
    Then I proceed to execute the following command:
    """
    $ gobuster dir -u gobuster dir -u http://cryptosploit -w common.txt
    """
    Then I get the output:
    """
    ...
    /index (Status: 200)
    /hacker (Status: 200)
    /robots (Status: 200)
    ...
    """
    Then I check all three address
    Then I found that "/robots" has some random text
    Then I can conclude that such text is encrypted

  Scenario: Exploitation
    Given that I know it is encrypted
    And that I suspect that it is encoded on base64
    When I execute the following line in the shell:
    """
    $ curl -s http://cryptosploit/robots | base64 -d
    """
    Then I get the output:
    """
    Good Work !
    Flag1: cybersploit{youtube.com/c/cybersploit}base64: invalid input
    """
    Then I can conclude that this is the password of the username found
    Then I execute an ssh connection with the username
    And the password
    Then I see that there is a file called "flag2.txt"
    Then I checked it and it was filled with binary information
    Then I decode such information
    Then I have another flag
    Then I execute a script:
    """
    $ uname -a
    """
    Then I get the output:
    """
    Linux cybersploit-CTF 3.13.0-32-generic #57~precise1-Ubuntu SMP ...
    """
    Then I search about this kernel
    And I find that it can be exploited
    Then I compile and execute an script
    """
    https://www.exploit-db.com/exploits/37292
    """
    And I have gained root access

  Scenario: Remediation
    Given don't store passwords on public places even if encrypted
    And keep the kernel up to date

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.8/10 (High) - AV:L/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.2/10 (High) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date {2020-08-11}
