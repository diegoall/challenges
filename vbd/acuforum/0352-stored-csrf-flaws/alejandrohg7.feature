## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuforum
  Category:
    Cross site request forgery (CSRF)
  Location:
    https://testasp.vulnweb.com/showforum.asp?id=1 - title (field)
  CWE:
    CWE-352: cross site request forgery (CSRF)
    https://cwe.mitre.org/data/definitions/352.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit Cross site request forgery in the forum form
  Recommendation:
    Always validate and sanitize the inputs fields and use anti CSRF tokens

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 18.04.3 LTS     |
    | Firefox           | 69.0            |
    | OWASP ZAP         | 2.8.0           |
  TOE information:
    Given I am accessing the site https://testasp.vulnweb.com
    And I enter an "ASP.NET" site that allows me to login
    And I can submit a post through a form
    And the server is running Microsoft SQL server version 8.5
    And "ASP.NET"

  Scenario: Normal use case
    When I login into the website
    Then I can submit a post in the page form
    """
    https://testasp.vulnweb.com/showforum.asp?id=1
    """
    And I can post

  Scenario: Static detection
    When I don't have access to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    When I login into the website
    Then I can submit a post in the page form
    """
    https://testasp.vulnweb.com/showforum.asp?id=1
    """
    And I can input HTML tags in the title field
    """
    <h1>This is before CSRF</h1>
    """
    And I click on "post it" button
    And I can see my post in "h1" tag [evidence](img1.png)
    And I can see this flag "Absence of anti-CSRF tokens" in OWASP ZAP
    And I can conclude that it can be affected by cross site request forgery

  Scenario: Exploitation
    When I login into the website
    Then I can submit a post in the page form
    """
    https://testasp.vulnweb.com/showforum.asp?id=1
    """
    And I can use HTML tags in the title field to log out everyone in the page
    """
    <img src="https://testasp.vulnweb.com/Logout.asp?
    RetURL=%2Fshowforum%2Easp%3Fid%3D0" width="0" height="0" border="0">
    """
    And I click on "post it" button
    And I get log out when I click any link [evidence](img2.png)
    And I can conclude that it can be affected by cross site request forgery

  Scenario: Remediation
    Given the site uses an insecure form against cross site request forgery
    When I input a value it must be inserted only if it is trusted Data
    And It must use anti forgery tokens
    And It must sent the cookies with the same site cookie attribute
    And It must include addition authentication for sensitive actions

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.1/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.7/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.7/10 (High) - CR:M/IR:M

  Scenario: Correlations
    vbd/Acuforum/0079-xss-forum
      Given I create an HTML tag to log out everyone in the forum page
      And I can create Javascript injection provided by 0079-xss-forum
      When someone visit the forum page
      Then I can get them to download a malware
      And I get information from their computers
