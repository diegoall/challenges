## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
    http://testphp.vulnweb.com - cookie (mycookie:3)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure input fields

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  TOE information:
    Given I am accessing the site testphp.vulnweb.com
    And Entered a php site that allows me to access a search bar

  Scenario: Normal use case
    Given I access testphp.vulnweb.com
    And write "Anything" on the search bar
    Then I can see the normal results page

  Scenario: Static detection:
    When I access testphp.vulnweb.com/search.php
    Then I can see the vulnerability is being caused by <form>
    And another function I'm not able to watch
    """
    55  <form action="search.php?test=query" method="post">
    """
    Then I conclude that the declaration does not validate input data

  Scenario: Dynamic detection:

    Given I access testphp.vulnweb.com
    Then I can write the following script in the search bar
    """
    <script>alert("Really")</script>
    """
    Then I get the output:
    And [evidence](evidence.png)
    Then I can conclude that code does not validate the input data

  Scenario: Exploitation:
    Then I can write the following script in the search bar
    """
    <script>alert("Really")</script>
    """
    Then I get the output:
    And [evidence](evidence.png)
    Then I can conclude that malicious code can be executed in the users side

  Scenario: Remediation:
    When the method called by
    """
    55  <form action="search.php?test=query" method="post">
    """
    And should validate the data made from the input
    And avoid malicious code to be incrusted

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.1 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.4 (Medium) - CR:L/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-04-29
