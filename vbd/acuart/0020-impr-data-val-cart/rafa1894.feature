## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Category:
    Improper Input Validation
  Location:
    http://testphp.vulnweb.com/cart.php - price (field)
  CWE:
    CWE-0020: Improper Input Validation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Change the number in the price field to purchase at a cheaper price
  Recommendation:
    Retrieve the price information from the server

  Background:
  Hacker's software:
    | Kali            | 4.19.0      |
    | Mozilla Firefox | 60.4.0      |
    | Burp Suite      | 1.7.36      |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com/cart.php
    And I was redirected to this page after adding an item to the cart
    And the site is built using PHP

  Scenario: Normal use case
    When I access http://testphp.vulnweb.com/product.php?pic=1
    Then I can click on "add this picture to cart"
    And that way I am redirected to http://testphp.vulnweb.com/cart.php
    And in this page I can see the item listed with the correct price

  Scenario: Static detection
    When I inspect the page looking for the code source
    Then I recognize the html code that handles addition of items to the cart
    """
    62    input type='hidden' name='price' value='500'><input type='hidden'
          name='addcart' value='1'><input type='submit' value='add this
          picture to cart'></form></div></div>
    """
    And I check for the backend code but it isn't accesible
    Then I conclude that the item price is present as an input but hidden

  Scenario: Dynamic detection
    When I access http://testphp.vulnweb.com/product.php?pic=1
    Then I click on "add this picture to cart"
    And I intercept the request of this action
    Then I see that the price is added as data to the request
    And I conclude that you can change the price easily from here

  Scenario: Exploitation
    When I access http://testphp.vulnweb.com/product.php?pic=1
    Then I click on "add this picture to cart"
    And I intercept the request of this action
    Then I change the value of the "price" object from 500 to 0
    And in the cart page I can see the price changed
    And this can be seen in [evidence](img1.png)
    And I successfully exploit the vulnerability

  Scenario: Remediation
    When the website is in its development stage
    Then developers should make it so the price data is not present as input
    And it should instead be retrieved directly from the server
    And this can be done by writing in the PHP code something like:
    """
    $stmt = $mysqli->prepare("SELECT * FROM priceTable WHERE id = ?");
    $stmt->bind_param("i", $pictureid);
    """
    And this way the exploit can be prevented

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.1/10 (Medium) - IR:L/MAV:N/MAC:L/MUI:N/MI:L

  Scenario: Correlations
    No correlations have been found to this date 2019/07/19
