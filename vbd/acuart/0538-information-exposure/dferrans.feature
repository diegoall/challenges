## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    Information exposure
  Location:
    http://testphp.vulnweb.com/admin/
  CWE:
    CWE-538: File and Directory Information Exposure
  Rule:
    REQ.006: https://fluidattacks.com/web/rules/006/
  Goal:
    Get unprotected information in the server protected area
  Recommendation:
    Force nginx server NOT to display folders if there is no default index

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | macos           | 10.15.5     |
    | chrome          | 74.0.379    |
    | dirbuster       | 0.9.12      |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    And the server is running Ngxix version 1.4.1
    And PHP version 5.3.10

  Scenario: Normal use case
    When I open http://testphp.vulnweb.com/
    Then I can see the page

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection

  Scenario: Dynamic detection
    When I use dirbuster to try detect unprotected directories
    Then I get the  result url
    Then I get the list of all files in folder
    Then I can see file[evidence](adminfolder.png)
    Then I can conclude that NGINX server is not configured properly
    And I can conclude that confidential information is being disclosed

  Scenario: Exploitation
    When I open  the url
    Then I can see files in the folder
    Then I can conclude that the nginx is displaying confidential information

  Scenario Outline: Extraction
  Exposed files
    When I open URL files are displayed
    Then I can access table schema file[evidence](create-sql.png)
    """
    create database waspart;
    use waspart;

    CREATE TABLE IF NOT EXISTS forum(
    sender    CHAR(150),
    mesaj     TEXT,
    senttime  INTEGER(32));

    CREATE TABLE IF NOT EXISTS artists(
    artist_id INTEGER(5) PRIMARY KEY AUTO_INCREMENT,
    aname   CHAR(50),
    adesc   BLOB);

    CREATE TABLE IF NOT EXISTS categ(
    cat_id    INTEGER(5) PRIMARY KEY AUTO_INCREMENT,
    cname   CHAR(50),
    cdesc   BLOB);

    CREATE TABLE IF NOT EXISTS pictures(
    pic_id    INTEGER(5) PRIMARY KEY AUTO_INCREMENT,
    pshort    BLOB,
    plong   TEXT,
    price   INTEGER,
    img     CHAR(50));
    """
    Then I can conclude that that file can be extracted

  Scenario: Remediation
    When the autoindex directive is set to off
    Then files are no longer displayed.
    """
    location /somedirectory/ {
    autoindex off;
    }
    """
    Then If I re-open the URL:
    """
    http://testphp.vulnweb.com/admin/
    """
    Then I get blank page withou listing files.
    Then I can conclude that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/RL:O/RC:R/MAV:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.0/10 (Medium) - E:X/RL:O/RC:R
  Environmental: Unique and relevant attributes to a specific user environment
    4.0/10 (Medium) - MAV:N

  Scenario: Correlations
    No correlations have been found to this date {2019-06-04}
