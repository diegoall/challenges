## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
    http://testphp.vulnweb.com - cookie (mycookie:3)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure comments section
  Recommendation:
    Escape external input before executing it

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  TOE information:
    Given I am accessing the site testphp.vulnweb.com
    And Entered to site .../signup.php
    Then I can see a form tu submit data

  Scenario: Normal use case
    Given I access testphp.vulnweb.com/signup
    And fill the fields with standard data
    Then I can see the info I entered rendered in the screen

  Scenario: Static detection:
    Given I do not have access to the source coude
    Then I can not make static detection

  Scenario: Dynamic detection:
    Given I access testphp.vulnweb.com/signup.php
    Then I can write the following script in any of the inputs
    """
    <script>alert("hacked")</script>
    """
    Then I get an alert window as output
    And I notice that my data is stored in the page
    Then I conclude that it is possible to store malicious code on the page

  Scenario: Exploitation:
  Given I access testphp.vulnweb.com/signup.php
    Then I can write the following script in any of the inputs
    """
    <script>window.onload(alert("this is a stored xss"))</script>
    """
    And I get redirected to the new Users welcome page
    Then I get an alert window with the message "this is a stored xss"
    And The script executes everytime I open that particular section
    Then I can conclude that is possible to store scripts at the server

  Scenario: Remediation:
    Given the user data is registered
    Then it should be validated before being saved

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.1 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.4 (Medium) - CR:L/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-05-07
