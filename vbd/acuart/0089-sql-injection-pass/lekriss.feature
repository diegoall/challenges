## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
    http://testphp.vulnweb.com - cookie (mycookie:3)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure SQL query input

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
    |sqlmap                | 1.3.4-37       |
  TOE information:
    Given I am accessing the site testphp.vulnweb.com
    And Entered /search.php

  Scenario: Normal use case
    Given I access http://testphp.vulnweb.com/search.php
    And I click on "search"
    Then I can see this http://testphp.vulnweb.com/searh.php?test=query

  Scenario: Static detection:
    Given I open the source code of the page
    Then it is not possible to access to the source code

  Scenario: Dynamic detection:
    Given I open sqlmap with parameter "-u" as
    """
    http://testphp.vulnweb.com/seacrh.php?test=query
    """
    Then the software shows that a blind SQLi is possible

  Scenario: Exploitation:
      Given I open sqlmap with parameter "-u" as
    """
    http://testphp.vulnweb.com/seacrh.php?test=query
    """
    Then it throws the name of the database which is "acuart"
    Then I can add parameter "-D" as "acuart"
    And I can see the tables of that database
    And I see the interesting table "users"
    Then I execute sqlmap adding "-T users" parameters
    And "-C uname,pass" and "--dump" parameters
    And I can see the only entry on the table
    """"
    +-------+--------+
    | uname | pass   |
    +-------+--------+
    |test   |test    |
    +-------+--------+
    """"
    Then I conclude that is possible to access sensible information


  Scenario: Remediation:
    When the query is programmed it should use parametrized queries
    And an PHP extension for DBManagement like PDO
    Then the program can succesfully avoid most of the known SQLi

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.2/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.9 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.2 (Medium) - CR:M/IR:L/MC:L/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-04-29
