## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Category:
    Improper Input Validation
  Location:
    http://testphp.vulnweb.com/userinfo.php - Phone number (field)
  CWE:
    CWE-0020: Improper Input Validation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Enter invalid data in the Phone number field
  Recommendation:
    Validate all data inputs depending on the context

  Background:
  Hacker's software:
    | Kali            | 4.19.0      |
    | Mozilla Firefox | 60.4.0      |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com/userinfo.php
    And entered the page where I can modify user information
    And the site is built using PHP

  Scenario: Normal use case
    When I access http://testphp.vulnweb.com/userinfo.php
    Then I can see the fields where I can modify info
    And I can enter a number in the Phone number field
    And I click on the "update" button
    And the information is updated normally

  Scenario: Static detection
    When I inspect the page looking for the code source
    Then I recognize the html code that handles the Phone number input
    """
    60    <tr><td valign="top">Phone number:</td><td><input type="text"
          value="1112221111" name="uphone" style="width:200px"></td></tr>
    """
    And I check for the backend code but it isn't accesible
    Then I can see that at least the client side needs input validation

  Scenario: Dynamic detection
    When I access http://testphp.vulnweb.com/userinfo.php
    Then I can enter some input that should be invalid like any letters
    And I see that the page accepts letters normally in a field for numbers

  Scenario: Exploitation
    When I access http://testphp.vulnweb.com/userinfo.php
    Then I enter the text '"invalid input" </a>' in the Phone number field
    And I click on the "update" button
    Then I can see that an error that shows part of the script occurs
    And this can be seen in [evidence](img1.png)
    And I successfully exploit the poor input validation vulnerability

  Scenario: Remediation
    When the website is in its development stage
    Then the user input should be validated so it accepts only numbers
    And this can be done by writing in the PHP code something like:
    """
    if(is_numeric(userInput) == false) {
        $errorMsg=  "error : Not a number";
    }
    else{
    echo "No errors.";
    }
    """
    Then input should be validated server side similarly depending on the lang
    And this way the exploit can be prevented

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.1/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - IR:H/MAV:N/MAC:L/MUI:N/MI:L

  Scenario: Correlations
    No correlations have been found to this date 2019/07/15
