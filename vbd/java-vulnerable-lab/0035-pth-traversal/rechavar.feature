## Version 1.4.1
## language: en

Feature:

  TOE:
    JavaVulnerableLab
  Location:
    http://http://127.0.0.1:8080/JavaVulnerableLab/
  CWE:
    CWE-0035: Path traversal
  Rule:
    Rule.037 Parameters without sensitive data
  Goal:
    download passwd file
  Recommendation:
    Use indirect references for file downloading

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Ubuntu          |     20.4    |
    | Mozilla firefox |     80.0    |
  TOE information:
    Given I am accessing locally to the site 127.0.0.1:8080/JavaVulnerableLab
    And I enter the site
    When it shows me a message
    """
    Welcome to Java Vulnerable Lab!

    A Deliberately vulnerable Web Application built on JAVA designed to
    teach Web Application Security.
    """
    And the copyrights belong to Cyber Security & Privacy Foundation
    Then I can see that uses MySQL
    And I can conclude that query chaining is not possible


  Scenario: Normal use case
    Given I access to the site
    And I see a buttons bar with six different options
    """
    Home, Vulnerability, Forum, Login, Register, Contact
    """
    Then I place the cursor on Vulnerability
    And a dropdown list is displayed
    When I navigate through the options to end up in Stored XSS
    """
    Vulnerability > A4 Insecure Direct Object References > Download document
    """
    Then I can see [evidence](image1.png)
    """
    Download Files:

    Doc1.pdf
    ExampleDoc.pdf
    """
    When I click in "Doc1.pdf" a file is downloaded [evidence](image2.png)

  Scenario: Static detection
    Given 2 possible download files
    Then I right-click on one of them
    And I copy the link location
    When I paste it on another tap
    And I can see
    """
    /JavaVulnerableLab/vulnerability/idor/download.jsp?file=doc1.pdf
    """
    Then I conclude that doc1.pdf is download by download.jsp
    When I inspect that file
    And I can conclude that the vulnerability goes from line 11 to 13
    """
    filePath = request.getParameter("file");
    File file = new File(getServletContext().getRealPath(context));
    file = new File(file.getParent()+"/docs/"+filePath);
    """
    Then I conclude that there's any type of validation in filePath variable
    And it allows path traversal.

  Scenario: Dynamic detection
    Given I access the site
    And I right-click on "doc1.pdf"
    Then I copy the URL and paste it in another tap [evidence](image3.png)
    When I change the file value to ExampleDoc.pdf
    And it starts to download the file [evidence](image4.png)
    Then I change the file value
    """
    ?file = ../etc/shadow
    """
    And I can see that the page doesn't hold this type of situation
    Then I continue adding ../ to the file value
    When I type
    """
    ?file=../../../../../../../etc/shadow
    """
    Then the shadow file starts to download [evidence](image5.png)
    And I conclude that the site is vulnerable to path traversal

  Scenario: Exploitation
    Given The download page
    Then I copy the link location of doc1.pdf
    And I paste it in another tap
    When I change the value
    """
    ?file=../etc/passwd
    """
    And nothing happens
    Then I add ../ until I reach passwd file
    When I type
    """
    ?file=../../../../../../../etc/passwd
    """
    Then a file starts to download [evidence](image6.png)
    When I open it I can see sensitive information [evidence](image7.png)
    And I try the same with the shadow file
    """
    ?file=../../../../../../../etc/shadow
    """
    Then the shadow file starts to download [evidence](image5.png)
    When I open it I can see sensitive information [evidence](image8.png)

  Scenario: Remediation
    Given The OWASP ESAPI library
    Then I create a new controller which will give me a random 6 char
    And they will refer to the document name [evidence](image9.png)
    """
    public class FileMap {
    public static AccessReferenceMap getFilesMap(){
            Set fileSet = new HashSet();
            fileSet.add("doc1.pdf");
            fileSet.add("exampledoc.pdf");
            return new RandomAccessReferenceMap(fileSet);
        }
    }
    """
    When I create an "Access Reference Map" named map in download.jsp
    And it was equal to null
    Then I create a statement to know if there's an attribute named "filesMap"
    And if it doesn't exist it will set with FileMap.getFilesMap() method
    """
    AccessReferenceMap map = null;
        if(session.getAttribute("filesMap")==null){
            map = FileMap.getFilesMap();
            session.setAttribute("filesMap", map);
        }
        else{
            map= (AccessReferenceMap)session.getAttribute("filesMap");
        }
    """
    Then Using map.getIndirectReference I change the URL displayed values
    """
    <li><a href="download.jsp?file=
    <%=map.getIndirectReference("doc1.pdf")%>"> Doc1.pdf </a>  </li>
    <li><a href="download.jsp?file=
    <%=map.getIndirectReference("exampledoc.pdf")%>"> ExampleDoc.pdf </a></li>
    """
    When Using map.getDirectReferences I get the real filePath name
    """
    filePath = request.getParameter("file");
    filePath = (String)map.getDirectReference(filePath);
    """
    And the final code [evidence](image10.png)
    Then I run the project
    And I go to the download page
    When I copy and paste the document URL I can see [evidence](image11.png)
    Then I try
    """
    ?file=../etc/passwd
    """
    And I can see [evidence](image12.png)
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.7/10 (High) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (High) - E:H/RL:W/RC:X/
  Environmental: Unique and relevant attributes to a specific user environment
    9.6/10 (High) - CR:H/IR:X/AR:X/

  Scenario: Correlations
    No correlations have been found to this date {2020-09-18}
