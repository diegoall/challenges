## Version 1.4.1
## language: en

Feature:

  TOE:
    JavaVulnerableLab
  Location:
    http://http://127.0.0.1:8080/JavaVulnerableLab/
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
    CWE-0080: Improper Neutralization of Script-Related HTML Tags in a Web Page
  Rule:
    Rule.173 Discard unsafe inputs
  Goal:
    Execute java scripts
  Recommendation:
    Encode all variable string to an HTML entity

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Ubuntu          |     20.4    |
    | Mozilla firefox |     80.0    |
    | ZAP             |     2.8.0   |
  TOE information:
    Given I am accessing locally to the site 127.0.0.1:8080/JavaVulnerableLab
    And I enter the site
    When it shows me a message
    """
    Welcome to Java Vulnerable Lab!

    A Deliberately vulnerable Web Application built on JAVA designed to
    teach Web Application Security.
    """
    And the copyrights belong to Cyber Security & Privacy Foundation
    Then I can see that uses MySQL
    And I can conclude that query chaining is not possible


  Scenario: Normal use case
    Given I access to the site
    And I see a buttons bar with six different options
    """
    Home, Vulnerability, Forum, Login, Register, Contact
    """
    Then I place the cursor on Vulnerability and a dropdown list is displayed
    Then I navigate through the options to end up in Stored XSS
    """
    Vulnerability > A3 XSS > Stored XSS
    """
    Then I can see [evidence](image1.PNG)
    And I conclude that the vulnerability is in the forum post page
    Then I log in with my user
    And I create a test post [evidence](image2.PNG)
    Then I open ZAP to see how the post was sent [evidence](image3.PNG)

  Scenario: Static detection
    Given I see the URL
    And I conclude that forum page loads from forum.jsp
    """
    http://127.0.0.1:8080/JavaVulnerableLab/vulnerability/forum.jsp
    """
    Then I open the file
    And I can see that the vulnerability is caused by line 39 to line 51
    """
    if(request.getParameter("post")!=null)
    {
        String user=request.getParameter("user");
        String content=request.getParameter("content");
        String title=request.getParameter("title");
        if(con!=null && !con.isClosed())
            {
              Statement stmt = con.createStatement();
              //Posting Content
              stmt.executeUpdate("INSERT into posts(content,title,user)
                              values ('"+content+"','"+title+"','"+user+"')");
                out.print("Successfully posted");
            }
        }
    """
    Then I conclude that neither title nor content is validated
    And it allows to store XSS

  Scenario: Dynamic detection
    Given I access the site
    Then I create post with a HTML tag
    """
    Title: <h1> mega title </h1>
    Message: hi buddies!
    """
    And its vulnerable to HTML injection and XSS [evidence](image4.PNG)

  Scenario: Exploitation
    Given the forum page
    Then I create a post with an alert script at the end of the title
    """
    Title: Can u help me?  <script>alert(document.cookie)</script>
    Message: I cant understand this page? would you help me?
    """
    And I post it
    Then I open the post
    And I can see an alert message with user cookie [evidence](image5.PNG)

  Scenario: Remediation
    Given I add the necessary libraries
    """
    <%@page import="java.lang.StringBuilder"%>
    <%@page import="java.util.HashMap"%>
    """
    Then I create a HashMap variable to hold HTML entities
      """
    public static final HashMap<Integer, String> charMap =
        new HashMap<Integer, String>();
    static {
        charMap.put(34, "&quot;");    // double quote
        charMap.put(38, "&amp;");     // ampersand
        charMap.put(39, "&apos;");    // single quote
        charMap.put(60, "&lt;");      // less than
        charMap.put(62, "&gt;");      // greater than
      }
    }
    """
    And I add a function to compare chars of a string and encode it
    """
      public static String encodeString(String str) {
        StringBuilder builder = new StringBuilder();
        char[] charArray = str.toCharArray();
        for (char nextChar: charArray) {
          String entityName = charMap.get((int) nextChar);
          if (entityName == null) {
            if (nextChar > 0x7F)
              builder.append("&#")
                .append(Integer.toString(nextChar, 10))
                .append(";");
            else
              builder.append(nextChar);
          }
          else
            builder.append(entityName);
        }
        return builder.toString();
      }
    """
    Then I encode the title and content inputs
    And I fix the code
    """
    if(request.getParameter("post")!=null)
    {
      String user=request.getParameter("user");
      String content=request.getParameter("content");
      String title=request.getParameter("title");
      String fixTitle = encodeString(title);
      String fixContent = encodeString(content);
      if(con!=null && !con.isClosed())
          {
            Statement stmt = con.createStatement();
            //Posting Content
            stmt.executeUpdate("INSERT into posts(content,title,user)
                    values ('"+fixContent+"','"+fixTitle+"','"+user+"')");
              out.print("Successfully posted");
          }
      }
    """
    Then I apply the same payload as in the Exploitation section
    And I can confirm that the vulnerability was patched [evidence](image6.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.4/10 (High) - AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.3/10 (High) - CR:H/IR:X/AR:X/

  Scenario: Correlations
    No correlations have been found to this date {2020-09-02}
