## Version 1.4.1
## language: en

Feature:
  TOE:
    Altoro Mutual
  Location:
    https://demo.testfire.net/login.jsp
  CWE:
    CWE-521: Weak Password Requirements
  Rule:
    REQ.133: R133. Passwords with at least 20 characters
  Goal:
    Change users passwords
  Recommendation:
    Modify the policy and set password length and complexity

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Debian          |   stretch   |
    | Chrome          |     84      |
  TOE information:
    Given I am accessing the TOE

  Scenario: Normal use case
    Given I access the site login page
    And I have my credentials
    Then I use them to login to my account

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I have valid admin credentials through 0307-brute-force-attack
    Then I can access to "Administration" options
    And there I see that I can change user's password

  Scenario: Exploitation
    Given I have access to the "Edit Users" view
    Then I try to change a user's passwords
    And I input as password "123"
    Then It allows me to change the password

  Scenario: Remediation
    Given the following regular expression:
    """
    ^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&.])[A-Za-z\d@$!%*#?&.]{20,}$
    """
    Then it can be used to assure that the password has at least 20 characters
    And it contains at least one lower case, one upper case, a number
    And a special character

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.3/10 (Medium) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.1/10 (Medium) - CR:H/IR:H/AR:H

  Scenario: Correlations
    vbd/altoro-mutual/0307-brute-force-attack
      Given that passwords can be simpler
      When a brute force attack is performed it would take less time
      And a much simpler dictionary
