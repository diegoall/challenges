## Version 1.4.1
## language: en

Feature:
  TOE:
    Altoro Mutual
  Location:
    https://demo.testfire.net/login.jsp
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.R237: R237. Ascertain human interaction
  Goal:
    Gain admin access
  Recommendation:
    Implement Captcha

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Debian          |   stretch   |
    | Chrome          |80.0.3987.122|
    | Python          |   3.5.3     |
    | BurpSuite       |   2020.8    |
  TOE information:
    Given I am accessing the TOE

  Scenario: Normal use case
    Given I access the site login page
    And I have my credentials
    Then I use them to login to my account

  Scenario: Static detection
    Given I don't have access to the source code

  Scenario: Dynamic detection
    Given that I don't have any valid credentials
    Then I try to login using random passwords and the user "admin"
    And I don't get restricted in the amount of attempts I can make
    Then I can conclude that the login can be attack with a brute force attack

  Scenario: Exploitation
    Given that I know I can make countless login attempts
    Then I use "Burp Suite" to capture the POST request for login
    When I do it I see that it uses an structure of three values
    And those are "uid" "passw" and "btnSubmit"
    Then I execute a Python script [evidence](script.py)
    And it prints the following:
    """
    admin password is: admin
    """

  Scenario: Remediation
    Given Implement a Captcha solution

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.8/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.4/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.4/10 (High) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date {2020-08-13}
