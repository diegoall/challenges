## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Category:
    Path traversal
  Location:
    http://172.17.0.2/download.php - file (field)
  CWE:
    CWE-22: Improper Limitation of a Pathname to a
    Restricted Directory ('Path Traversal')
  Rule:
    REQ.037: https://fluidattacks.com/web/rules/037/
  Goal:
    Download any file outside files folder
  Recommendation:
    Sanitize input data as well as implementing a white list

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ArchLinux       | 2020.07.01  |
    | Brave           | 1.10.97     |
    | Doccker         | 19.03.12-ce |
    | OWASP ZAP       | 2.9.0       |
  TOE information:
    Given I am accessing the site on 172.17.0.2
    And it runs on a Docker container

  Scenario: Normal use case
    Given I access http://172.17.0.2/?page=downloads
    When I see two buttons allowing me to download two files
    Then I click an a buttons and a text file gets downloaded
    And [evidence](downloads.png)

  Scenario: Static detection
    Given the application's source code
    When I look at the code at download.php
    """
    03  $file = __DIR__ . '/files/' . $_GET['file'];
    94  header('Content-Type: ' . $mimeType);
    95  header('Content-Disposition: attachment; filename="' \
        . basename($file) . '"');
    96  readfile($file);
    """
    Then in line 3 a path to files/ is created
    And without any validation of the parameter file
    When in line 96 file gets downloaded as is
    Then I can conclude the code is vulnerable to path traversal

  Scenario: Dynamic detection
    Given the downloads section
    When I try to download the download.php page with the following link
    """
    http://172.17.0.2/download.php?file=../download.php
    """
    Then I can see it actually downloads the php file [evidence](downphp.png)
    And I can conclude that the site it is vulnerable to path traversal

  Scenario: Exploitation
    Given The downloads section
    And knowing the file I want to download
    When I go to
    """
    http://172.17.0.2/download.php?file=../file/to/download
    """
    Then I can download the file, for example /etc/passwd [evidence](pass.png)
    And I can conclude I can download any file

  Scenario: Remediation
    Given I have patched the code by adding the following  lines
    """
    04  if (!(basename(dirname($file)) === "files")) {
    05    header('HTTP/1.1 500  Internal Server Error');
    06    exit;
    07  }
    """
    When line 04 checks that the parent folder of the requested file is files
    And if the parent dir is not files returns a 500 code
    And If I go back to
    """
    http://172.17.0.2/download.php?file=../download.php
    """
    Then I get a 500 code
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.4 (High) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.4 (High) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date {2020-07-06}
