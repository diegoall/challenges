## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Cross Site Scripting
  Location:
    WebGoat/start.mvc#lesson/Challenge7.lesson - password reset (Email)
  CWE:
    CWE-319: Cleartext Transmission of Sensitive Information
  Rule:
    REQ.185: https://fluidattacks.com/web/en/rules/185/
  Goal:
    Get another user's reset token
  Recommendation:
    Encrypt your connections

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/start.mvc#lesson/Challenge7.lesson"
    And I get a page where I can submit an email to reset password

  Scenario: Static detection
  No origin validation
    Given I check the code at
    """
    webgoat-lessons/challenge/src/main/java/org/owasp/webgoat/plugin/challenge7/
    Assignment7.java
    """
    And I see
    """
    61  @RequestMapping(method = POST)
    62  @ResponseBody
    63  public AttackResult sendPasswordResetLink(@RequestParam String email, Ht
    tpServletRequest request) throws URISyntaxException {
    64      if (StringUtils.hasText(email)) {
    65          String username = email.substring(0, email.indexOf("@"));
    66          if (StringUtils.hasText(username)) {
    67              URI uri = new URI(request.getRequestURL().toString());
    68              Email mail = Email.builder()
    69                      .title("Your password reset link for challenge 7")
    70                      .contents(String.format(TEMPLATE, uri.getScheme() +
    "://" + uri.getHost(), new PasswordResetLink().createPasswordReset(username,
    "webgoat")))
    71                      .sender("password-reset@webgoat-cloud.net")
    72                      .recipient(username)
    73                      .time(LocalDateTime.now()).build();
    74              restTemplate.postForEntity(webWolfMailURL, mail,
    Object.class);
    75          }
    76      }
    77      return success().feedback("email.send").feedbackArgs(email).build();
    78  }
    """
    Then I notice it's sending the reset email in plain text
    Then it could potentially be sniffed and misused

  Scenario: Dynamic detection
  Detecting plain text in the network
    Given I'm on the same network as the server
    Given I am running wireshark
    And send myself a password reset email
    Then I sift through the TCP traffic captured in Wireshark
    And I filter it to find a "reset" string
    Then I find an occurence with the reset email content (evidence)[wrshrk.png]
    Then I know I could do this with any user's email

  Scenario: Exploitation
  Get admin's reset token/link
    Given I know the target sends the reset link in plaintext
    Then I run wireshark to capture the network traffic
    Then I attempt to reset the password for "admin@webgoat.com"
    Then I capture the email in wireshark and get the admin reset email
    Then I go to the reset link
    And reset admin's password, granting myself access to their account

  Scenario: Remediation
  Check the origin corresponds to the site
    Given I patch the code to use SSL like this
    """
    61  @RequestMapping(method = POST)
    62  @ResponseBody
    63  public AttackResult sendPasswordResetLink(@RequestParam String email, Ht
    tpServletRequest request) throws URISyntaxException {
    64      if (StringUtils.hasText(email)) {
    65          String username = email.substring(0, email.indexOf("@"));
    66          if (StringUtils.hasText(username)) {
    67              URI uri = new URI(request.getRequestURL().toString());
    68              Properties props = new Properties();
    69              props.put("mail.smtp.host", "smtp.gmail.com");
    70              properties.put("mail.smtp.port", "465");
    71              properties.put("mail.smtp.ssl.enable", "true");
    72              Session session = Session.getDefaultInstance(props,
    73              new javax.mail.Authenticator() {
    74                                  @Override
    75                  protected PasswordAuthentication getPasswordAuthenticati
    on() {
    76                      return new PasswordAuthentication("username@gmail.co
    m","password");
    77                  }
    78              });
    79
    80          try {
    81
    82              Message message = new MimeMessage(session);
    83              message.setFrom(new InternetAddress("password-reset@webgoat-
    cloud.net"));
    84              message.setRecipients(Message.RecipientType.TO,
    85                      InternetAddress.parse(username));
    86              message.setSubject("Your password reset link for challenge 7
    ");
    87              message.setText(String.format(TEMPLATE, uri.getScheme() +
    "://" + uri.getHost(), new PasswordResetLink().createPasswordReset(username,
    "webgoat")));

    88              Transport.send(message);
    89          } catch (MessagingException e) {
    90              throw new RuntimeException(e);
    91          }
    92          }
    93      }
    94      return success().feedback("email.send").feedbackArgs(email).build();
    95  }
    """
    Then when the email is sent it travels encrypted
    Then it's unsniffable

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.1/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.4/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/webgoat/79-stored-xss
      Given I reset a privileged users password
      Then I have privileged access to the platform
      Then I can leave a malicious comment as a privileged user
      And make sure every other user sees it
      Then I can reach a lot more users than I would otherwise with my attack
