## Version 1.4.2
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Bad Coding Practices
  Location:
    WebGoat/ - code
  CWE:
    CWE-1006: Bad Coding Practices
  Rule:
    REQ.161: https://fluidattacks.com/web/en/rules/161/
  Goal:
    Better code quality
  Recommendation:
    Write an else for every if

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/search.jsp
    Then I can search for products

  Scenario: Static detection
  No precedent password validation
    Given I see the code at "/owasp/webwolf/user/RegistrationController.java"
    """
    ...
    39  if (bindingResult.hasErrors()) {
    40          return "registration";
    41      }
    42      userService.addUser(userForm.getUsername(), userForm.getPassword());
    43      request.login(userForm.getUsername(), userForm.getPassword());
    ...
    """
    Then I see it doesn't have an else for the if in line 39

  Scenario: Dynamic detection
  No dynamic detection

  Scenario: Exploitation
  No exploitation

  Scenario: Remediation
  Else for the if
    Given I write an else for the if
    """
    ...
    39  if (bindingResult.hasErrors()) {
    40          return "registration";
    41  } else { Logging.log("Registration Error") }
    42  userService.addUser(userForm.getUsername(), userForm.getPassword());
    43  request.login(userForm.getUsername(), userForm.getPassword());
    ...
    """
    Then the code is rule-compliant

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    0.0/10 (None) - AV:P/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    0.0/10 (None) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    0.0/10 (None) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-23
