## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Session Management
  Location:
    /WebGoat - JSESSIONID (Cookie)
  CWE:
    CWE-384: Session Fixation
  Rule:
    REQ.029: https://fluidattacks.com/web/en/rules/029/
  Goal:
    Steal a user's session and navigate as them while they are on
  Recommendation:
    Forbid concurrent sessions

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/" and navigate the site
    Then I can login from another location and still be logged in in the first

  Scenario: Static detection
  No session invalidation
    Given I check the code at
    """
    webgoat-container/src/main/java/org/owasp/webgoat/WebSecurityConfig.java
    """
    And I see
    """
    56  @Override
    57  protected void configure(HttpSecurity http) throws Exception {
    58      ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterce
    ptUrlRegistry security = http
    59              .authorizeRequests()
    60              .antMatchers("/css/**", "/images/**", "/js/**", "fonts/**",
    "/plugins/**", "/registration", "/register.mvc").permitAll()
    61              .antMatchers("/servlet/AdminServlet/**").hasAnyRole("WEBGOAT
    _ADMIN", "SERVER_ADMIN") //
    62              .antMatchers("/JavaSource/**").hasRole("SERVER_ADMIN") //
    63              .anyRequest().authenticated();
    64      security.and()
    65              .formLogin()
    66              .loginPage("/login")
    67              .defaultSuccessUrl("/welcome.mvc", true)
    68              .usernameParameter("username")
    69              .passwordParameter("password")
    70              .permitAll();
    71      security.and()
    72              .logout().deleteCookies("JSESSIONID").invalidateHttpSession(
      true);
    73      security.and().csrf().disable();
    74
    75      http.headers().cacheControl().disable();
    76      http.exceptionHandling().authenticationEntryPoint(new AjaxAuthentica
    tionEntryPoint("/login"));
    77  }
    """
    Then I notice it has no session invalidation configuration

  Scenario: Dynamic detection
  Logging in twice
    Given I log into the site with my account
    And log in again from a second device
    Then I'm logged in in both devices and can perform actions no problem
    Then I know there's no session concurrency control

  Scenario: Exploitation
  Login as another user while they are still working
    Given I know the victim is on the site
    Given I have obtained the victims credentials or have another way to login
    Then I log in as the victim
    Then I can use the site as the victim at the same time as them
    Then my malicious actions are going to blend with their legitimate ones

  Scenario: Remediation
  Forbid concurrent sessions
    Given I patch the code like this
    """
    56  @Override
    57  protected void configure(HttpSecurity http) throws Exception {
    58      ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterce
    ptUrlRegistry security = http
    59              .authorizeRequests()
    60              .antMatchers("/css/**", "/images/**", "/js/**", "fonts/**",
    "/plugins/**", "/registration", "/register.mvc").permitAll()
    61              .antMatchers("/servlet/AdminServlet/**").hasAnyRole("WEBGOAT
    _ADMIN", "SERVER_ADMIN") //
    62              .antMatchers("/JavaSource/**").hasRole("SERVER_ADMIN") //
    63              .anyRequest().authenticated();
    64      security.and()
    65              .formLogin()
    66              .loginPage("/login")
    67              .defaultSuccessUrl("/welcome.mvc", true)
    68              .usernameParameter("username")
    69              .passwordParameter("password")
    70              .permitAll();
    71      security.and()
    72              .logout().deleteCookies("JSESSIONID").invalidateHttpSession(
      true);
    73      security.and().csrf().disable();
    74
    75      http.headers().cacheControl().disable();
    76      http.exceptionHandling().authenticationEntryPoint(new AjaxAuthentica
    tionEntryPoint("/login"));
    77      http.sessionManagement().maximumSessions(1)
    78  }
    """
    Then when the user logs in while I'm logged in as them
    Then my session will be closed and i'll have to relogin

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/webgoat/89-without-password
      Given I can login as a user via SQLi
      Then I can do so and perform actions on the site while the user is also on
      Then my actions are harder to trace and the user is none the wiser