## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Injection Flaws
  Location:
    /rest/user/login - email (Parameter)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
    ('SQL Injection')
  Rule:
    REQ.173 Descartar entradas inseguras
  Goal:
    Login to the admin account
  Recommendation:
    Sanitize user input and use prepared statements

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  User input to build SQL
    Given I see the code at "login.js"
    """
    ...
    44  models.sequelize.query('SELECT * FROM Users WHERE email = \'' + (req.bod
    y.email || '') + '\' AND password = \'' + insecurity.hash(req.body.password
    || '') + '\'', { model: models.User, plain: true })
    45    .then((authenticatedUser) => {
    46      let user = utils.queryResultToJson(authenticatedUser)
    47
    48      const rememberedEmail = insecurity.userEmailFrom(req)
    49      if (rememberedEmail && req.body.oauth) {
    50        models.User.find({ where: { email: rememberedEmail } }).then(remem
    beredUser => {
    51          user = utils.queryResultToJson(rememberedUser)
    52          if (utils.notSolved(challenges.loginCisoChallenge) && user.data.
    id === users.ciso.id) {
    53            utils.solve(challenges.loginCisoChallenge)
    54          }
    55          afterLogin(user, res, next)
    56        })
    57      } else if (user.data && user.data.id) {
    58        afterLogin(user, res, next)
    59      } else {
    60        res.status(401).send('Invalid email or password.')
    61      }
    62    }).catch(error => {
    63      next(error)
    64    })
    ...
    """
    Then I see it uses user input to build the sql query to log in

  Scenario: Dynamic detection
  Causing a system error
    Given I try to login with my user using known SQLi methods
    """
    email: a@b.com'; --
    password: gibberish
    """
    Then I get logged in to my account
    Then I know the application is vulnerable to SQLi

  Scenario: Exploitation
  Using leaked info
    Given I know there is a SQL injection vuln in the login system
    Given I have access to the unprotected administration dashboard
    Then I see the admin account email is "admin@juice-sh.op"
    Then I use SQLi to login with that email
    Then I have access to the admin account

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    ...
    44  models.sequelize.query('SELECT * FROM Users WHERE email = ? + '\' AND pa
    ssword = \'' + insecurity.hash(req.body.password || '') + '\'', { model: mod
    els.User, plain: true, replacements: { email: req.body.email } })
    45    .then((authenticatedUser) => {
    46      let user = utils.queryResultToJson(authenticatedUser)
    47
    48      const rememberedEmail = insecurity.userEmailFrom(req)
    49      if (rememberedEmail && req.body.oauth) {
    50        models.User.find({ where: { email: rememberedEmail } }).then(remem
    beredUser => {
    51          user = utils.queryResultToJson(rememberedUser)
    52          if (utils.notSolved(challenges.loginCisoChallenge) && user.data.
    id === users.ciso.id) {
    53            utils.solve(challenges.loginCisoChallenge)
    54          }
    55          afterLogin(user, res, next)
    56        })
    57      } else if (user.data && user.data.id) {
    58        afterLogin(user, res, next)
    59      } else {
    60        res.status(401).send('Invalid email or password.')
    61      }
    62    }).catch(error => {
    63      next(error)
    64    })
    ...
    """
    Then the SQL injection is fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.0/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.0/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-02-01
