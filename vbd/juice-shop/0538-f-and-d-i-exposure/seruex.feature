## Version 1.4.1
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Information Exposure
  Location:
    /ftp
  CWE:
    CWE-538: File and Directory Information Exposure
  Rule:
    REQ.185: https://fluidattacks.com/web/es/rules/185/
  Goal:
    Obtaining sensitive information in the "ftp" folder
  Recommendation:
    Remove lines from the server.js file

  Background:
  Hacker's software:
    | <Software name>     | <Version>   |
    | Parrot Security OS  |  4.2.2x64   |
    | Firefox             |   52.9.0    |
    | Owasp               |    2.6.0    |

  TOE information:
    Given I enter the site "http://localhost:3000/" (Juice-Shop "npm")
    And I can see that it is a web page of juices and drinks
    And the server is runing SQLite
    And Node.js Version 8.11.2
    And is running on Parrot OS 4.2.2x64

  Scenario: Normal use case
  See items in the menu of Juice-shop
    Given I access the site /#/
    And I see drinks of different flavors
    And I see the price of the drinks
    And I also see the description and the quantity of the product
    And I also see other products such as: sweaters and shirts

  Scenario: Static detection
  The file server.js in the main directory of Juice-shop. It has lines that
  #provide access to files with sensitive information
    When I open the file server.js
    Then I see the resource content extra lines  115-117
    """
    115  /* /ftp directory browsing and file download */
    116  app.use('/ftp', serveIndex('ftp', { 'icons': true }))
    117  app.use('/ftp/:file', fileServer())
    """
    Then I can conclude that the folder and the subfolders and files
    #are displayed, because the server.js file is configured to display them.

  Scenario: Dynamic detection
  Scan the web page (Juice-shop) whit owasp
    Given I access the site http://localhost:3000
    Then I open the owasp
    And I add the url, and click on #atacar
    Then owasp show me the vulnerabilities
    And I see the hide folder #ftp (see imagen.png)
    Then I go to the url http://localhost:3000/ftp
    And I see the hidden files and the sensitive information
    Then I conclude that the tool Owasp provee directories that can be used to
    #exploit vulnerabilities not finding in the scan

  Scenario: Exploitation
  gettin random file whit the null byte "%00=%2500"(eastere.gg)
    Given I need geet and see sensitive information
    Then I can see the #md files, but not other files extensions
    Then I try to put the null byte and file extension at the end of the URL
    #%2500.md
    And I see that the file starts downloading
    And I open the file
    Then I see the message
    #"Congratulations, you found the easter egg!"
    #  - The incredibly funny developers

    Then I conclude that the get of others files is posible trough
    #null byte(%2500)

  Scenario: Remediation
  The server Juice-Shop show sensitive information in the dir /ftp/
    #http://localhost:3000/ftp
    Given this I advise to repair the vulnerability remove the lines 115-117
    #Remove (see imagen1.png)
    """
    115  /* /ftp directory browsing and file download */
    116  app.use('/ftp', serveIndex('ftp', { 'icons': true }))
    117  app.use('/ftp/:file', fileServer())
    """

    And I keep

    #  114  /* /encryptionkeys directory browsing */
    #  115  app.use('/encryptionkeys', serveIndex('encryptionkeys',
    #        { 'icons': true, 'view': 'details' }))
    #  116  app.use('/encryptionkeys/:file', keyServer())

    Then I can confirm that the vulnerability can be successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.2/10 (Medium) - AV:L/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.8/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.1/10 (Medium) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2019-01-25
