## Version 1.4.1
## language: en

Feature:
  TOE:
    Zero Bank
  Category:
   Cleartext Storage
  Location:
    http://zero.webappsecurity.com/admin/index.html
  CWE:
    CWE-312: Cleartext Storage of Sensitive Information
  Rule:
    REQ.127 Store hashed passwords
  Goal:
    Capture credentials on shared network
  Recommendation:
    Store passwords using hash algorithms.

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | NixOS                | 74.0 (64-bit)  |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Wireshark            | 3.0.3          |
  TOE information:
    Given I am accessing the site http://zero.webappsecurity.com

  Scenario: Normal use case:
    Given I access http://zero.webappsecurity.com/
    Then I obtain the normal result from the page

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I search the url ../bank/pay-bills-get-payee-details.html
    And exploit the vulnerability 0089 SQL Injection
    Then I have full access to the database
    And there are usernames and passwords

  Scenario: Exploitation
    Given I have access to the database via SQL Injection
    Then I use the following query to search for users
    """
    ' union select concat(username,':',password,':',enabled) FROM users --
    """
    And see clear text credentials
    Then I can access to the user interface as another user

  Scenario: Remediation
    Store passwords using hash algorithms.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) -AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.5 (Medium) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.5 (Medium) - CR:M/IR:M/AR:M/MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-03-30
