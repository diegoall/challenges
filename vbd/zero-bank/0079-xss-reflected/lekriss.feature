## Version 1.4.1
## language: en

Feature:
  TOE:
    Zero Bank
  Location:
    http://zero.webappsecurity.com/bank/transfer-funds-verify.html
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure transactions zone
  Recommendation:
    Escape external input before executing it

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  TOE information:
    Given I am accessing the site http://zero.webappsecurity.com
    And Entered to site .../bank/transfer-funds-verify.html
    Then I can see there is section for making transactions
    And allows me to comment the transaction

  Scenario: Normal use case
    Given I access zero.webappsecurity.com/bank/transfer-funds-verify.html
    And I write a numeric value in the value section
    And A random word in the description section
    Then I can see a confirmation page with all the information I entered

  Scenario: Static detection
    Given I do not have access to the source coude
    Then I can not make static detection

  Scenario: Dynamic detection
    Given I access zero.webappsecurity.com/bank/transfer-funds-verify.html
    Then I can write the following script in the description box
    """
    <script>alert(Document.cookie)</script>
    """
    Then I see that the script is not executed
    Then I try closing a previous HTML tag
    And rewrite the tag like the following:
    """
    "> <script>alert("xss")</script>
    """
    Then I can see the alert showing in the screen

  Scenario: Exploitation
    Given I access zero.webappsecurity.com/bank/transfer-funds-verify.html
    Then I can write the following script in the search bar
    """
    <script>alert(Document.cookie)</script>
    """
    Then I get a white alert as result because the cookie is blank
    And I conclude that it is possible to steal a session in this way

  Scenario: Remediation
    Given the input form which receives the decription of the transaction
    Then the ">", "<" and """ should be escaped before doing the query

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.6 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.6 (Medium) - CR:L/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-05-07
