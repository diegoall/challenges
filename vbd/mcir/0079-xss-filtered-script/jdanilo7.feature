## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR: Magical Code Injection Rainbow
  Category:
    Cross-Site Scripting
  Location:
    MCIR/xssmh/challenges/challeng4.php - Injection String (field)
  CWE:
    CWE-79: Cross-Site Scripting
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Disclose the contents of the site cookies using stored xss
  Recommendation:
    Escape the input

  Background:
  Hacker's software:
    |     <Software name>      |    <Version>    |
    |          OWASP           |       1.6       |
    |   VMWare Workstation 15  |     15.0.2      |
    |     Firefox Quantum      |     60.0.1      |
  TOE information:
    Given I'm running OWASP on VMWare
    And I'm accessing the MCIR pages through my Firefox browser
    And I enter a php page that takes a string, blacklist filters it
    And includes it in the content of the website

  Scenario: Normal use case
  The site allows the user to enter a string that can be rendered as content
    Given I access the page MCIR/xssmh/challenges/challenge4.php
    And I enter the string "hello"
    And I click the Inject! button
    And I click the "See the output" link
    Then the string is shown as part of the site's content
    And I can see [evidence](normal-use-case.png)

  Scenario: Static Detection
  The input is sanitized but only for the "script" word
    When I look at the source code
    And I check the code that prepares the input
    Then I notice there are two files that process it
    And the first one is sanitize.inc.php and it has these lines
    """
    24  if(isset($_REQUEST['sanitization_level']) and isset($_REQUEST['
        sanitization_type']) and $_REQUEST['sanitization_type']=='keyword'){
    25    switch($_REQUEST['sanitization_level']){
    ...
    67      case 'high':
    68        do{
    69          $keyword_found = 0;
    70          foreach($params as $keyword){
    71            $_REQUEST['inject_string'] = str_ireplace($keyword, '',
                    $_REQUEST['inject_string'], $count);
    72            $keyword_found += $count;
    73          }
    74        }while ($keyword_found);
    75        break;
    """
    And the second one is xss.php and it has these lines
    """
    43  $base_output = 'Foo! <img src="baz.jpg"><input type="text"
                             value="bar!"><script>a="javascript";</script>';
    ...
    54  switch ($_REQUEST['location']){
    55    case 'body':
    56      $output = str_replace('Foo!', $_REQUEST['inject_string'],
                     $base_output);
    57      break;
    ...
    99  echo '<b>Output:</b><br>' . $output;
    """
    Then I notice only some keywords are validated

  Scenario: Dynamic detection
  The input of the Inject! field is rendered as part of the site's content
    Given I enter "<img src="/" onerror=alert("hi")></img>" in the text field
    And I click the Inject! button
    Then I get an alert window that can be seen in [evidence](alert.png)
    And I conclude that my input was treated as code by the server
    And the application is vulnerable to cross-site scripting

  Scenario: Exploitation
  Retrieving all cookies
    Given I enter the following code in the input field
    """
    1  <img src="/" onerror="document.cookie = 'username=John Doe';
       var x = document.cookie;alert(x);"></img>
    """
    And I click the Inject! button
    Then I get an alert window showing all the cookies
    And it is exemplified by the cookie I inserted
    And it can be seen in [evidence](cookies.png)

  Scenario: Remediation
  The php code can be fixed by escaping the input before writing it
    Given I have patched the code by escaping the user input
    """
    99  echo '<b>Output:</b><br>' . htmlspecialchars($output, ENT_QUOTES);
    """
    Then I can prevent the injection of the code shown above
    Given I re-run my exploit
    Then I get no alert window and my input is shown without getting executed
    And it can be seen in [evidence](escaped-output.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.2/10 (High) - CR:M/IR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-03-03
