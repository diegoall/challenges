## Version 1.4.1
## language: en

Feature:
  TOE:
    bts lab
  Category:
  Location:
    http://localhost:8056/btslab/vulnerability/clickjacking/
    cj.php - csrf (field)
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
  Rule:
    REQ.231 Define biometric verification component
  Goal:
    Delete user account without his consent
  Recommendation:
    Verify token validation mechanism bypasses.
    Include checks to undefined variables.

  Background:
  Hacker's software:
    | <Software name>      | <Version>         |
    | Windows              | 10                |
    | Firefox              | 68.0.1            |
  TOE information:
    Given I am accessing the site "http://localhost:8056/btslab/"
    And it is built with php
    And uses sql database

  Scenario: Normal use case
    Given I am logged as 'test0' user
    And I access
    """
    http://localhost:8056/btslab/
      vulnerability/clickjacking/cj.php
    """
    And the page title is 'Delete Your account'
    And a button with a 'Delete' label is show
    When I click the button
    Then I see a message
    """
    Successfully You have deleted your account. Now Go to Home Page
    """

  Scenario: Static detection
    Given ToE source code
    When I inspect 'cj.php'
    Then I notice that token is determined by lines 7 to 9
    """
    7   if(!isset($_POST['delete']))
    8   {
    9       $_SESSION['csrf']=md5(rand());
    """
    And the csfr-token validation mechanism at line 21
    """
    21  if(isset($_POST['delete'])&&($_POST['csrf']==$_SESSION['csrf']))
    """
    Then I notice that if post and session csrf are not set
    Then condition is true
    And bypass is done without knowing token

  Scenario: Dynamic detection
    Given I can access with some user through registration
    When sending requests to 'cj.php'
    And trying not setting token field
    Then I get my account deleted
    And csrf is feasible for user deletion

  Scenario: Exploitation
    Given the token validation mechanism vulnerability
    And the attacker page: 'hacker-page.html'
    And I am logged as user 'test1' in the app
    When I visit the attacker page 'file:///D:/.../hacker-page.html'
    And try to navigate through the app
    Then I get logged out
    When trying to log in again
    Then error appears, confirming deletion of the user

  Scenario: Remediation
    Given a modification of token validation mechanism
    """
    21  if(isset($_POST['delete'])&&($_POST['csrf']==$_SESSION['csrf'])&&
            isset($_POST['csrf'])&&!empty($_POST['csrf']))
    """
    When I visit the attacker page 'file:///D:/.../hacker-page.html'
    And try to navigate through the app
    Then nothing strange occurs
    When re log in
    Then I get access
    And confirm that user has not been deleted

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:N/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2/10 (medium) - E:F/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.5/10 (medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-07-26
