## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Insecure Interaction Between Components
  Location:
    /login - Form
  CWE:
    CWE-0352: Cross-Site Request Forgery (CSRF) -compound-
      https://cwe.mitre.org/data/definitions/352.html
    CWE-0346: Origin Validation Error -base-
      https://cwe.mitre.org/data/definitions/346.html
    CWE-0345: Insufficient Verification of Data Authenticity -class-
      https://cwe.mitre.org/data/definitions/345.html
    CWE-0949: SFP Secondary Cluster: Faulty Endpoint Authentication -category-
      https://cwe.mitre.org/data/definitions/949.html
  CAPEC:
    CAPEC-467: Cross Site Identification -detailed-
      http://capec.mitre.org/data/definitions/467.html
    CAPEC-062: Cross Site Request Forgery -standard-
      http://capec.mitre.org/data/definitions/62.html
    CAPEC-021: Exploitation of Trusted Credentials -meta-
      http://capec.mitre.org/data/definitions/21.html
  Rule:
    REQ.263: https://fluidattacks.com/web/es/rules/263/
  Goal:
    Send a request on behalf of the logged in user without their action
  Recommendation:
    Implement an anti-CSRF token

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/core/passport.js"
    """
    29  passport.use('login', new LocalStrategy({
    30          passReqToCallback: true
    31      },
    32      function (req, username, password, done) {
    33          db.User.findOne({
    34              where: {
    35                  'login': username
    36              }
    37          }).then(function (user) {
    38              if (!user) {
    39                  return done(null, false, req.flash('danger', 'Invalid Cr
    edentials'))
    40              }
    41              if (!isValidPassword(user, password)) {
    42                  return done(null, false, req.flash('danger', 'Invalid Cr
    edentials'))
    43              }
    44              return done(null, user);
    45          });
    46      }))
    """
    Then I see it executes the transaction with no regard for req origin at all

  Scenario: Dynamic detection
  Checking default routes
    Given I go to "http://localhost:8000/app/login"
    Then I intercept a request to create a new product
    """
    POST /login HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/login
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 19
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=qo61W2w2ZW6a3BeyxzDmjrPKl
    Y5bAyD0LO4XpRqkJQ8VE7g1ovNMn9kZ9wrE; connect.sid=s%3AbkE-ktw_GSO1pdPj7wsbVgd
    dUZKH5tf_.4hjFmNywNQEjr1mNN8MrylaQ52JWXOliji4kssQpxrs
    Upgrade-Insecure-Requests: 1

    username=&password=
    """
    Then I notice it doesn't implement an ant-csrf token
    Then I try changing the host header to see if it accepts external requests
    """
    POST /login HTTP/1.1
    Host: test
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    ...
    """
    And it logs me in normally

  Scenario: Exploitation
  Stealing credentials
    Given I craft phishing page to steal credentials
    Then I can make it POST to the real application after storing the data
    Then the user is redirected to the real site and logged in
    And have no reason to suspect anything

  Scenario: Remediation
  Anti-csrf token
    Given I generate an anti-csrf token everytime the user edits a product
    And validate the token when recieving the POST req before saving changes
    Then CSRF isn't posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.7/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0601-unvalidated-redirects
      Given I use the unvalidated redirect to take users to my phishing page
      Then they see the URL is from the real application
      Then they are much more likely to click the link and fall for my attack
