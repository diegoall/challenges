## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Information Leakage
  Location:
    / - Server Misconfiguration
  CWE:
    CWE-0209: Information Exposure Through an Error Message -base-
      https://cwe.mitre.org/data/definitions/209.html
    CWE-0200: Information Exposure -class-
      https://cwe.mitre.org/data/definitions/200.html
    CWE-0963: SFP Secondary Cluster: Exposed Data -category-
      https://cwe.mitre.org/data/definitions/963.html
  CAPEC:
    CAPEC-215: Fuzzing and observing application log data/errors for application
    mapping -detailed-
      http://capec.mitre.org/data/definitions/215.html
    CAPEC-054: Query System for Information -standard-
      http://capec.mitre.org/data/definitions/54.html
    CAPEC-116: Excavation -meta-
      http://capec.mitre.org/data/definitions/116.html
  Rule:
    REQ.156: https://fluidattacks.com/web/es/rules/156/
  Goal:
    Get error messages from the server
  Recommendation:
    Don't expose system errors to the user, set up production environment

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the configuration at "dvna/package.json"
    """
    "scripts": {
      "test": "echo \"Error: no test specified\" && exit 1",
      "start": "node server.js"
    },
    """
    Then I see it doesn't define "NODE_ENV" so it defaults to "development"

  Scenario: Dynamic detection
  Causing a system error
    Given I go to "http://localhost:8000/app/calc"
    Then I submit "3;console.log(1)"
    And get an error message with full stacktrace
    """
    Error: Undefined symbol console
        at undef (/app/node_modules/mathjs/lib/expression/node/SymbolNode.js:92:
        11)
        at eval (eval at Node.compile (/app/node_modules/mathjs/lib/expression/n
        ode/Node.js:71:19), <anonymous>:3:523)
        at eval (eval at Node.compile (/app/node_modules/mathjs/lib/expression/n
        ode/Node.js:71:19), <anonymous>:3:646)
        at Object.eval (eval at Node.compile (/app/node_modules/mathjs/lib/expre
        ssion/node/Node.js:71:19), <anonymous>:3:682)
        at string (/app/node_modules/mathjs/lib/expression/function/eval.js:40:3
        6)
        at Object.compile (eval at _typed (/app/node_modules/typed-function/type
        d-function.js:1115:22), <anonymous>:22:14)
        at module.exports.calc (/app/core/appHandler.js:197:19)
        ...
    """
    Then I know the server is configured to display system errors

  Scenario: Exploitation
  Using leaked info
    Given I can see full stack traces on error
    Then I can use this information to plan my attacks

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    "scripts": {
      "test": "echo \"Error: no test specified\" && exit 1",
      "start": "NODE_ENV=production node server.js"
    },
    """
    Then the NODE_ENV is set to production and stacktraces aren't displayed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0915-vulnerable-components
      Given I can see the stacktrace of the error caused by invalid input
      Then I know what library is being used
      And I can search for known vulns
