## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Access Control
  Location:
    /app/admin/usersapi - Access Control
  CWE:
    CWE-0648: Incorrect Use of Privileged APIs -base-
      https://cwe.mitre.org/data/definitions/648.html
    CWE-0269: Improper Privilege Management -class-
      https://cwe.mitre.org/data/definitions/269.html
    CWE-0901: SFP Primary Cluster: Privilege -category-
      https://cwe.mitre.org/data/definitions/901.html
  CAPEC:
    CAPEC-0234: Hijacking a privileged process -standard-
      http://capec.mitre.org/data/definitions/234.html
    CAPEC-0233: Privilege Escalation -meta-
      http://capec.mitre.org/data/definitions/233.html
  Rule:
    REQ.096: https://fluidattacks.com/web/es/rules/096/
  Goal:
    Use an api endpoint I shouldn't be able to access
  Recommendation:
    Implement proper access control

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/routes/app.js"
    """
    43  router.get('/admin/usersapi', authHandler.isAuthenticated, appHandler.
    listUsersAPI)
    """
    Then I see it allows access just by being logged in

  Scenario: Dynamic detection
  Accessing unprotected endpoint
    Given I send a GET request to "http://localhost:8000/app/admin/usersapi"
    Then I get returned a JSON with user's names, roles, emails, password hashes
    """
    {
      "success": true,
      "users": [
        {
          "id": 1,
          "name": "simo",
          "login": "username",
          "email": "a@b.com",
          "password": "$2a$10$HyfNES9e3vTmAeHIUrjvCexTtwKYzJCtSlWLx1CIQHsnbK5/4F
          cVi",
          "role": null,
          "createdAt": "2019-01-25T12:31:38.823Z",
          "updatedAt": "2019-01-25T12:33:44.313Z"
        },
        {
          "id": 2,
          "name": "ggg",
          "login": "admin",
          "email": "a@b.com",
          "password": "$2a$10$4rPKh5kRZgtS4eUWbeHQqeXa8sJwnLKrEvURhgmBcSTKc83.Hj
          pv2",
          "role": null,
          "createdAt": "2019-01-25T12:33:10.535Z",
          "updatedAt": "2019-01-25T12:33:27.251Z"
        }
      ]
    }
    """

  Scenario: Exploitation
  Using leaked data
    Given I can see a list of users with their personal data and password hashes
    Then I can try to crack their passwords
    And I can target them with social engineering using their personal data

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    43  router.get('/admin/usersapi', authHandler.isAuthenticated, authHandler.i
    sAdmin,
    appHandler.listUsersAPI)
    """
    Then I can't access the endpoint

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0639-idor
      Given I can see a list of users including their ids
      Then I can use the IDOR vuln to change a specific user's pass
