# language: en

Feature: Tag Based XSS - Pure Tags IMG
  From the Firing Range system
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/tags/img?q=a
    Message: Allows any img tag
    Details:
      - img tag Based Page
      - Message on the top of the page with content 'Invalid input, no tags'
    Objective: Perform a redirect of the page with a tag based xss
    """

    Scenario: img XSS first attempt
    Given the description said we need to do a redirect with the img tag
    And I found that on the source code on the body there is no img tag
    When I replace the url 'q' parameter by the following using an URL encoder
    """
    %3Cimg+src%3D%22http%3A%2F%2Fwww.google.com%22+onclick%3D%22document.
    location.href%3D%27http%3A%2F%2Fwww.google.com%27%22%3E
    """
    And using the 'src' and 'onclick' attributes passed a img tag
    Then the page was reloaded with a not found question mark image
    And once I clicked on it the page was redirected to google home page
