# language: en

Feature: Reflected XSS - Escaping and filtering - Attribute unquoted
  From the Firing Range system
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/reflected/filteredcharsets/attribute_unquoted/
    DoubleQuoteSinglequote?q=a
    Message: The parameter is echoed inside an unquoted HTML attribute of a
    tag. Payloads containing double quotes and single quotes are blocked
    Details:
      - Reflected XSS Escaping and filtering Blocks DoubleQuoteSinglequote page
      - The page is in blank
    Objective: Perform an reflected xss with the tag attribute page
    """
    Scenario: direct script injection attempt
    Given the following HTML code for the page
    """
    <html>
      <body>
        <tag attribute=a>
      </body>
    </html>
    """
    When I see description and noticed the quotes won't work
    And  I fill the 'q' parameter with the following
    """
    <script>alert(window.location.pathname)</script>
    """
    Then the page was reloaded and a text on the top appeared
    """
    alert(window.location.pathname)>
    """
    But there was no popup showing the path

    Scenario: second XSS attempt
    When I realize I had to close the tag without quotes
    And  I fill the 'q' parameter with the following
    """
    ><script>alert(window.location.pathname)</script>
    """
    Then the page was reloaded and the popup appeared with the pathname
    """
    /reflected/filteredcharsets/attribute_unquoted/DoubleQuoteSinglequote
    """
    And after the popup was closed the page was loaded with '>' text on the top
