## Version 1.4.1
## language: en

Feature:
  TOE:
    rails-goat
  Category:
    Basic XSS
  Location:
    http://http://localhost:3000/signup
  CWE:
    CWE-080: Improper Neutralization of Script-Related HTML Tags in a Web Page.
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject executable HTML code to the server.
  Recommendation:
    Render plain text or sanitize the user input.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | firefox         | 52.9.0      |
  TOE information:
    Given I am accessing the site http://localhost:3000
    And enter a Ruby on Rails site that allows me to create a new user
    And the server is running Ruby version 2.3.5
    And Rails version 5.1.4
    And MariaDB version 10.1.29
    And docker-compose version 1.22.0

  Scenario: Normal use case
    Given I access http://localhost:3000
    When I click the signup button
    Then I can create a new user by filling out the form.

  Scenario: Static detection
    When I look at the files in the views folder
    Then I can see that in "layout/_header.html.erb" the first name is rendered
    And it can be found in this file in line 31.
    """
    30 <li style="color: #FFFFFF">
    31   Welcome, <%= current_user.first_name.html_safe %>
    32 </li>
    """
    Then I can conclude that the webpage is vulnerable to XSS.

  Scenario: Dynamic detection
    Given I access http://localhost:3000
    Then I can access the signup page
    And fill out the form
    When I type in the first name
    Then I notice that there are no input restrictions
    And I am able to enter special characters like "<>"
    Then I can conclude that the webpage is vulnerable to XSS.

  Scenario: Exploitation
    Given I access http://localhost:3000
    Then I can create a new user
    And add the HTML code:
    """
    <script>alert(document.cookie)</script>
    """
    When I click on the submit button
    Then I can see a pop-up with the info entered [evidence](signup-popup.png)
    Then I can conclude that HTML code can be injected to de server.

  Scenario: Remediation
    Given I have patched the code by sanitizing the input
    And removing the "html_safe" attribute.
    """
    30 <li style="color: #FFFFFF">
    31   Welcome, <%= sanitize(current_user.first_name) %>
    32 </li>
    """
    Then If I try to create another user
    Then no HTML code renders [evidence](no-alert.png)
    And now I can see the user's first name in the navbar.
    Then I can confirm that the vulnerability was successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:F/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.4/10 (Low) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:N/MI:N/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-07-11
