## Version 1.4.1
## language: en

Feature:
  TOE:
    OWASP Security Shepherd
  Category:
    Failure to restrict URL
  Location:
    https://localhost/index.jsp - POST (header)
  CWE:
    CWE-0425: Direct Request ('Forced Browsing')
  Rule:
    REQ.033: Restric administrative access
  Goal:
    Get administrator rights and access the hidden information
  Recommendation:
    Implement proper url access control mechanisms

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.2 LTS |
    | VirtualBox      | 6.0.8       |
    | Firefox         | 67.0.1      |
    | Burp Suite      | 1.7.36      |
  TOE information:
    Given I am accessing the site https://localhost/index.jsp
    And the site's database environment is MySQL
    And the overall website is built with JSP technology
    And I entered one of its pages where there is a server status button

  Scenario: Normal use case
    When I access https://localhost/index.jsp
    Then I can see the server status button
    And I click on the button
    And the page shows me the following message
    """
    The server status is normal. Nothing to see here.
    Move along.
    """
    And I can't access the restricted information

  Scenario: Static detection
    When I inspect the page looking for the source code
    Then I recognize the JavaScript code that handles the server status button
    """
    40 $("#leForm").submit(function(){
    41   $("#submitButton").hide("fast");
    42   $("#loadingSign").show("slow");
    43   $("#resultsDiv").hide("slow", function(){
    44     var ajaxCall = $.ajax({
    45       type: "POST",
    46       url: "4a1bc73dd68f64107db3bbc7ee74e3f1
                   336d350c4e1e51d4eda5b52dddf86c99",
    47       data: {
    48         userData: "4816283",
    49       },
    """
    And I see another identical block of code that seems to handle a button
    And I notice it has the following couple differences
    """
    68    $("#leAdminForm").submit(function(){
    74       url: "4a1bc73dd68f64107db3bbc7ee74e3f1
                   336d350c4e1e51d4eda5b52dddf86c992",
    """
    Then I infer that the second form that does the ajax call is for admins
    And that the slightly different url can take me to the hidden info
    And I also check for the server side code but it isn't accesible

  Scenario: Dynamic detection
    When I access https://localhost/index.jsp
    Then I use Burp Suite to intercept the http traffic
    And I check the POST value in the http request header which is an url
    And I conclude I can change the url to the admin url from here
    And that way I can access the hidden information

  Scenario: Exploitation
    When I access https://localhost/index.jsp
    Then I click on the "Get Server Status" button
    And I use Burp Suite to intercept the http traffic
    And I can see the url for the ajax call mentioned in the source code
    Then I change the normal url for the admin url I discovered previously
    And this takes me to the url even though it should be just for admins
    And this time I can see the hidden information

  Scenario: Remediation
    When the web site is in development
    Then developers should plan to prevent users from accessing restricted urls
    And the web site should also check if the user is normal or admin
    And this can be done from the backend validating the user login
    And the validation can be done for example using PHP code this way
    """
    if($_SESSION[AdminLogIn]){
        //display page and info for admin
    }
    elseif($_SESSION[NormalLogIn]){
        header('Location: /outtahere.html');
        //redirect elsewhere
        //or if not an admin page display info for user
    }
    """
    And this way the admin page info will be safe

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.1/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.1/10 (Medium) - CR:M/IR:L/MAC:L/MUI:N/MC:L

  Scenario: Correlations
    No correlations have been found to this date 2019/06/18
