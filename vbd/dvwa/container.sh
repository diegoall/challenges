#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "Usage: container.sh port"
  else
    docker run --rm -d --name dvwa -p "$1:80" -t vulnerables/web-dvwa
fi
