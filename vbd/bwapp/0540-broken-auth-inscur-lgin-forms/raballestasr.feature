# language: en

Feature: Detect and exploit vuln Insecure Login Forms
  From the bWAPP application
  From the A2 - Broken auth & Sessiong mgmt
  With Low security level
  Rule:
    REQ.156: https://fluidattacks.com/web/es/rules/156/

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    Given I am browsing in Firefox 57.0.4
    Given a PHP site with inputs for user and password
    And the URL is bwapp/ba_insecure_login_1.php
    Given the goal is to login

  Scenario: Static detection
    When I look in the code
    Then I see the form has labels that say "Login:" and "Password"
    But they also have labels within labels
    And those labels are colored white:
    """
    <label for="login">Login:</label><font color="white">tonystark</font>
    """

  Scenario: Dynamic exploitation
    When I select everything in the page or
    When I change the value of color to anything other than white:
    """
    <label for="login">Login:</label><font color="black">tonystark</font>
    """
    Then the user and password are visible right on top of the inputs
