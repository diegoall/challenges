# language: en

Feature: XSS Reflected - (Back button)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_back_button.php
  Message: XSS Reflected - (Back Button)
  Details:
        - Click the button to go back to the previous page
        - Submit button
  Objective: Inject javascript code into the site
  """

Scenario: XSS Reflected - (Back button)
  Given the vulnerable form
  """
  # xss_back_button.php
  51  <div id="main">
  52    <h1>XSS - Reflected (Back Button)</h1>
  53    <p>Click the button to go to back to the previous page:
  54    <input type=button value="Go back" onClick="document.location.href=''">
  55    </p>
  56  </div>
  """
  When I hit the button, I get back to where I was
  Then I use Burp to check the request
  """
  # Burp Request
  GET /bWAPP/portal.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/xss_back_button.php
  Cookie: security_level=0; PHPSESSID=e8a4a078592f5211c4c48d4d4d922bda
  ...
  ...
  """
  And I modify it to redirect it to another website
  """
  # Burp modified request
  GET /bWAPP/xss_post.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/xss_back_button.php
  Cookie: security_level=0; PHPSESSID=e8a4a078592f5211c4c48d4d4d922bda
  ...
  ...
  """
  And I see it redirected me to xss_post.php
  And not to the initial page I was
  Then I see this can lead to take the user into some evil url inside the app
