## Version 1.4.1
## language: en

Feature: Buffer Overflow (Remote)
  TOE:
    bWAPP
  Category:
    Buffer Overflow
  Location:
    192.168.1.119:666 TCP
  CWE:
    CWE-121: Stack-based Buffer Overflow
  Rule:
    REQ.157: https://fluidattacks.com/web/rules/157/
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Execute commands in the system
  Recommendation:
    Use protection mechanism against buffer overflow attacks

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Python          | 2.7         |
    | Msfconsole      | 5.0.52-dev  |
    | Netcat          | v1.10-41.1  |
    | Ufw             | 0.16.2      |
  TOE information:
    Given The service running at port 666
    When I enter into it
    Then I can search for a movie by its name

  Scenario: Normal use case
    Given The service
    When A user wants to search a movie
    Then He sends the movie's name back to the server
    And The service searches for that movie

  Scenario: Static detection
    Given That I can access the source code of the service
    Then I can't make static detection

  Scenario: Dynamic detection
    Given The service
    When I send a big request
    Then The service crash
    When I run the command to analyse the binary of the service
    """
    $ ./checksec.sh --file=movie_search
    """
    Then I get the file security information
    """
    RELRO           STACK CANARY      NX            PIE
    No RELRO        No canary found   NX disabled   No PIE

    RPATH      RUNPATH      Symbols       FORTIFY Fortified  Fortifiable
    No RPATH   No RUNPATH   102 Symbols   No      0          0
    """
    And It is vulnerable to buffer overflow attacks

  Scenario: Exploitation
    Given The vulnerability
    When I create a Python script "exploit.py"
    Then I use "msfconsole" to generate my shellcode [evidence](image1.png)
    When I create a socket connection
    Then I can send data to the service
    When I bruteforce the buffer size of the input
    Then I can inject my shellcode in memory
    And I have remote code execution on the server [evidence](image2.png)

  Scenario: Remediation
    Given The service with the vulnerability
    When I block the port using "ufw"
    """
    $ sudo ufw deny 666
    """
    Then I only can connect to it locally
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.1/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.3/10 (High) - E:F/RL:T/RC:R/CR:H/IR:H/AR:L
    Environmental: Unique and relevant attributes to a specific user environment
      7.9/10 (High) - MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:H/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-10-10
