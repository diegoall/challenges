## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Location:
    http://bwapp/cs_validation.php - new_password (Field)
  CWE:
    CWE-602: Client-Side Enforcement of Server-Side Security
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Change password to something super insecure
  Recommendation:
    Validate in the server as well as in the client

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/cs_validation.php
    And click on "reset your secret to any bugs?"
    Then my secret gets changed to "Any bugs?"

  Scenario: Static detection
  External Entities are not disabled
    When I look at the code in bwapp/cs_validation.php
    """
    179 <script type="text/javascript">
    180
    181 function check_password(string)
    182 {
    183
    184     var pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
    185
    186     return pattern.test(string);
    187
    188 }
    189
    190 function check_form(form)
    191 {
    192
    193     if(form.password_new.value == "")
    194     {
    195
    196         form.password_new.focus();
    197         document.getElementById("message").innerHTML = "<font color=\"re
    d\">Please enter a new password...</font>";
    198
    199         return false;
    200
    201     }
    202
    203     if(form.password_new.value != form.password_conf.value)
    204     {
    205
    206         form.password_new.focus();
    207         document.getElementById("message").innerHTML = "<font color=\"re
    d\">The passwords don't match!</font>";
    208
    209         return false;
    210
    211     }
    212
    213     if(!check_password(form.password_new.value))
    214     {
    215
    216         form.password_new.focus();
    217         document.getElementById("message").innerHTML = "<font color=\"re
    d\">The new password is not valid!<br />Password policy: minimum 6 character
    s containing at least one uppercase letter, lowercase letter and number.</fo
    nt>";
    218
    219         return false;
    220
    221     }
    222
    223    return true;
    224
    225 }
    226
    227 </script>
    """
    Then I see it's checking password matching and properness in javascript
    And there's no validation to be found server side

  Scenario: Dynamic detection
  View page source
    Given I go to "http://bwapp/bWAPP/cs_validation.php"
    And open the page's source in my browser
    Then I see a validation script
    """
    <script type="text/javascript">

    function check_password(string)
    {

        var pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;

        return pattern.test(string);

    }

    function check_form(form)
    {

        if(form.password_new.value == "")
        {

            form.password_new.focus();
            document.getElementById("message").innerHTML = "<font color=\"red\">
            Please enter a new password...</font>";

            return false;

        }

        if(form.password_new.value != form.password_conf.value)
        {

            form.password_new.focus();
            document.getElementById("message").innerHTML = "<font color=\"red\">
            The passwords don't match!</font>";

            return false;

        }

        if(!check_password(form.password_new.value))
        {

            form.password_new.focus();
            document.getElementById("message").innerHTML = "<font color=\"red\">
            The new password is not valid!<br />Password policy: minimum 6 chara
            cters containing at least one uppercase letter, lowercase letter and
            number.</font>";

            return false;

        }

        return true;

    }

    </script>
    """
    Then I know it's probably not being validated in the server too

  Scenario: Exploitation
  Setting an insecure password
    Given I open my browser console
    And replace the "check_form" function so it always returns true
    """
    check_form=function(form) {return true;}
    """
    Then I change my password to "a"
    And get the response
    """
    The password has been changed!
    """
    Then I sign out and then in with my new "a" password
    Then I get logged in

  Scenario: Remediation
  Check password validity server-side
    Given I patch bwapp/cs_validation.php as follows
    """
    43  function check_input($password_new,$password_conf)
    44  {
    45  $error = "";
    46      if($password_new == "")
    47      {
    48          $error = "<font color=\"red\">Please enter a new password...</fo
    nt>";
    49          return $error;
    50      }
    51      if($password_new != $password_conf)
    52      {
    53          $error = "<font color=\"red\">The passwords don't match!</font>"
    ;
    54          return $error;
    55      }
    56      if(preg_match("/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/",$password_new)
    )
    57      {
    58          $error = "<font color=\"red\">The new password is not valid!<br
    />Password policy: minimum 6 characters containing at least one uppercase
    letter, lowercase letter and number.";
    59          return $error;
    60      }
    61  return $error;
    62  }
    """
    Then it fully validates the new password in the server

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.9/10 (Critical) - AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.2/10 (Critical) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.2/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-11
