#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "Usage: container.sh port"
  else
    docker run --rm -d --name bwapp -p "$1:80" -t raesene/bwapp
    sleep 10
    firefox "http://127.0.0.1:$1/install.php"
    echo "if the install page doesn't open automatically, please go to http://i\
p:$1/install.php"
fi

