## Version 1.4.1
## language: en

Feature:
  """
  Wackopicko is a site to upload ,  buy and comment images of other users.
  In this ToE I describe how to attack the Sql vulnerability in the users login.
  using the vega tool and sqlmap I can access the database.
  Finally I repair the vulnerability with a character filter
  """
  TOE:
    WackoPicko
  Category:
    SQL Injection
  Location:
    users/login.php - username, (field)
  CWE:
    CWE-0089: SQL Injection -base-
      https://cwe.mitre.org/data/definitions/89.html
    CWE-0943: Improper Neutralization of Special Elements in
    Data Query Logic -class-
      https://cwe.mitre.org/data/definitions/943.html
    CWE-1019: Validate Inputs -category-
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-0007: Blind SQL Injection -detailed-
      https://capec.mitre.org/data/definitions/7.html
    CAPEC-0066: SQL Injection -standard-
      https://capec.mitre.org/data/definitions/66.html
    CAPEC-0048: Command Injection -meta-
      https://capec.mitre.org/data/definitions/248.html
  Rule:
    REQ.173: Discard unsafe inputs
      https://fluidattacks.com/web/en/rules/173/
  Goal:
    Access the database through SQL injection
  Recommendation:
    Implement restrictions for Post parameter username

  Background:
  Hacker's software:
    | <Software name>     | <Version>   |
    | Parrot Security OS  | 4.2.2x64    |
    | Firefox             | 52.9.0      |
    | Wackopicko          | 1.4.0       |
    | Vega scanner        | 1.0         |
    | SQLmap              | 1.2.12      |

  TOE information:
    Given I enter the site http://localhost:8080
    And I can buy, upload and share images on the site
    And the server is runing MySQL version >=5
    And PHP version 5.5.9
    And apache version 2.4.7
    And is running on Docker image (adamdoupe/wackopicko) on parrot OS 4.2.2x64

  Scenario: Normal use case
  Making a update of my basket
    Given I access the site http://localhost:8080
    And I can log whit my username and my password
    And click on login
    And I can upload and shared images and shared comment

  Scenario: Static detection
  The input provided (post parameter) does not have the respective precaution
    Then I access to login.php on var/www/html/users
    And I see the code requiere others pages
    #"require_once("../include/users.php");"
    Then I access to users.php on var/www/html/include/users.php
    And I see that the query no is protect in line 90
    """
    85 function check_login($username, $pass, $vuln = False)
    86 {
    87   if ($vuln)
    88   {
    89  $query = sprintf("SELECT * from `users` where `login` like '%s' and
        `password` = SHA1( CONCAT('%s', `salt`)) limit 1;",
    90                    $username,
    91                    mysql_real_escape_string($pass));
    92   }
    93   else
    94   {
    95  $query = sprintf("SELECT * from `users` where `login` like '%s' and
        `password` = SHA1( CONCAT('%s', `salt`)) limit 1;",
    96                   mysql_real_escape_string($username),
    97                   mysql_real_escape_string($pass));
    """

  Scenario: Dynamic detection
  searching vulnerabilities with vega
    Given I scann whit vega subgraph the site
    And I see a vulnerability SQL injection in the query:
    #[username=joey]
    Then I can conclude that the querys of the |username|
    #No have restriction of the parameter post

  Scenario: Exploitation
  http://localhost:8080/users/login.php [username=joey]
    Given I need add the querys [username=joey] to the
    #command SQLmap
    Then I use the next command in the console SQLmap
    #$sqlmap -u http://localhost:8080/users/login.php --data
    #"username=joey"
    And I see that the SQlmap find a method Injectable (MySQL)
    Then I use the command:
    #$sqlmap -u http://localhost:8080/users/login.php --data
    #"username=joey" --dbs
    Then I see the dbs of the server wackopicko
    Then I can conclude that SQL injection with the tool SQLmap work
    And this shows me that the server is exploitable by SQL injection.
    #see imagen.png

  Scenario: Remediation
  The server wackopicko is vulnerable in the direction
    #http://localhost:8080/users/login.php whit the
    #query [username]
    Given this I advise to repair the vulnerability add the line 90
    """
    85 function check_login($username, $pass, $vuln = False)
    86 {
    87   if ($vuln)
    88   {
    89  $query = sprintf("SELECT * from `users` where `login` like '%s' and
        `password` = SHA1( CONCAT('%s', `salt`)) limit 1;",
    90                    mysql_real_escape_string($username),
    91                    mysql_real_escape_string($pass));
    92   }
    93   else
    94   {
    95  $query = sprintf("SELECT * from `users` where `login` like '%s' and
        `password` = SHA1( CONCAT('%s', `salt`)) limit 1;",
    96                   mysql_real_escape_string($username),
    97                   mysql_real_escape_string($pass));
    """
    Then I can confirm that the vulnerability can be successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:L/AC:L/PR:L/UI:N/S:C/C:N/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.3/10 (Medium) - E:H/RL:T/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.2/10 (Medium) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2018-12-23
