## Version 1.4.1
## language: en

Feature: Connect to database
  TOE:
    node-goat
  Category:
    Exposure of Sensitive Information to an Unauthorized Actor
  Location:
    http://localhost:4000/
  CWE:
    CWE-15: External Control of System or Configuration Setting
    https://cwe.mitre.org/data/definitions/15.html
    CWE-200: Exposure of Sensitive Information to an Unauthorized Actor
    https://cwe.mitre.org/data/definitions/200.html
    CWE-306: Missing Authentication for Critical Function
    https://cwe.mitre.org/data/definitions/306.html
  Rule:
    REQ.173: Discard unsafe inputs
    https://fluidattacks.com/web/rules/173/
    REQ.228: Authenticate Using Standard Protocols
    https://fluidattacks.com/web/rules/228/
    REQ.234: Protect authentication credentials
    https://fluidattacks.com/web/rules/234/
  Goal:
    Connect to the databsae
  Recommendation:
    Sanitize all user inputs and add authentication to database.

  Background:
  Hacker's software:
    | <Software name> |    <Version>    |
    | Windows         |       10        |
    | Chrome          |  80.0.3987.122  |
    | NoSQL Booster   |      5.2.10     |
  TOE information:
    Given I am accessing the site http://localhost:4000
    And the server is running MongoDB version 2.1.18
    And Nodejs version 10.15.3

  Scenario: Normal use case
    Given I access to "http://localhost:4000/"
    And login in with a new user
    When I was redirect to dashboard
    And I can manage my account

  Scenario: Static detection
    Given The source code
    When I see the code in "/config/env/all.js"
    Then I see this line
    """
    db:  process.env.MONGOLAB_URI || process.env.MONGODB_URI ||
    "mongodb://nodegoat:owasp@ds159217.mlab.com:59217/nodegoat",
    """
    And I found the way to connect with the database
    When I see the code in "/app/routes/contributions.js"
    Then I see the following lines
    """
    var preTax = eval(req.body.preTax);
    var afterTax = eval(req.body.afterTax);
    var roth = eval(req.body.roth);
    """
    And I found that function eval is vulnerable to injections
    When I think a way to use this 3 things
    Then I think that I can use injections to take the database info
    And I found a vulnerability

  Scenario: Dynamic detection
    Given I'm logged in the page
    And I'm in the contributions tab
    When I use the input fields to update the contributions
    Then I see that the contirbutions update the values
    And I think if this input is vulnerable
    When I try to see if the input is vulnerable
    Then I try to inject something like scripts
    And I see that the scripts didn't work
    When I try to use a command injection like "process.kill(process.pid) "
    Then I found that the server drops [evidence](img1)
    And I make a DoS
    When I see that the server is vulnerable to command injection
    Then If the server is runing in Nodejs I can use fs function
    And I found a vulnerability

  Scenario: Exploitation
    Given The system is vulnerable to command injections
    And The server is running in Nodejs
    Then I want to dump the database
    When I go to the page to attack
    Then I see a login form
    And I go to singup link
    When I register with fake credentials
    Then I try to login
    And I'm redirecting to dashboard tab
    When I see if i can find some vulnerabilities
    Then I found an interesting tab "contributions"
    And I found 3 fields to input data and update
    When I try to use this field to injections
    Then I found that this fields are vulnerable to command injection
    And I try to use fs library to search in it
    When I type the following command
    """
    res.end(require('fs').readdirSync('.').toString())
    """
    Then I see the response with the files in the server
    """
    .gitignore,.jshintrc,.nodemonignore,.vscode,app,app.json,artifacts,
    config,docker-compose.yml,Dockerfile,Gruntfile.js,LICENSE,node_modules,
    package-lock.json,package.json,Procfile,README.md,server.js,test
    """
    And I try to found a way to use that
    When I read every route and file with the commands
    """
    readdirSync for directories
    res.end(require('fs').readdirSync('<route>').toString())

    readFileSync for files
    res.end(require('fs').readFileSync('<route>/<file>'))
    """
    Then I use the command
    """
    res.end(require('fs').readFileSync('/config/env/all.js'))
    """
    And I can read the content of this config file
    When I see the content and I found an interesting thing
    """
    // default app configuration
    module.exports = {
      port: process.env.PORT || 4000,
      db:  process.env.MONGOLAB_URI || process.env.MONGODB_URI ||
      "<URI to connect with MongoDB>",
      cookieSecret: "session_cookie_secret_key_here",
      cryptoKey: "a_secure_key_for_crypto_here",
      cryptoAlgo: "aes256",
      hostName: "192.168.1.184"
    };
    """
    Then I found the connection to the database
    And I try to connect with this
    When I want to connect to the database that is in MongoDB
    Then I use NoSQLBooster for MongoDB
    And I try to connect [evidence](img2)
    And I see that the database don't have authentication restrictions
    When I see that the connection is successful
    Then I see all tables [evidence](img3)
    And I can see all data for this webapp
    When I try to to modify data [evidence](img3)
    Then I see that the data is updated
    And I add the fiedl "isAdmin" in my evil user
    Then I have admin in my evil user [evidence](img4)
    And I exploit the vulnerability

  Scenario: Remediation
    Given The source code
    When I go to the route "/app/routes/contributions.js"
    Then I see the following code
    """
    var preTax = eval(req.body.preTax);
    var afterTax = eval(req.body.afterTax);
    var roth = eval(req.body.roth);
    """
    And I want to change eval() because is vulnerable
    When I found a way to validate the input with a safe function
    Then I change those lines with
    """
    var preTax = parseInt(req.body.preTax);
    var afterTax = parseInt(req.body.afterTax);
    var roth = parseInt(req.body.roth);
    """
    And I try to simulate the attack again
    When I login and go to "contributions" tab
    Then I try to inject the same command in the inputs
    """
    res.end(require('fs').readdirSync('.').toString())
    """
    And I see the response "Invalid contribution percentages"
    When I see that message I know that the injection fail
    Then I can't see the database connection
    When I try to fix the database connection to avoid of any user connect
    Then I use the following query
    """
    db.updateUser(
      "nodegoat",
      {
        customData: {},

        roles: [{ "role": "dbOwner", "db": "nodegoat" }],

        pwd: passwordPrompt(),

        authenticationRestrictions: [
          {
            clientSource: ["<IP>" | "<CIDR range>", ...],
            serverAddress: ["<IP>", | "<CIDR range>", ...]
          },
        ],
      }
    )
    """
    And I add the "pwd" and the "authenticationRestrictions"
    When I try to connect with another machine the connect fail
    Then If I try to connect without know the "pwd" the connection fail
    And Only the owner can connect with the database
    And I fix the vulnerabilities

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
      8.8/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
      8.4/10 (High) - E:H/RL:O/RC:C/CR:H/IR:H/AR:H
  Environmental: Unique and relevant attributes to a specific user environment
      8.4/10 (High) - MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2020-03-31
