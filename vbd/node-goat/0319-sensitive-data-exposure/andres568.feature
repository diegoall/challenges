## Version 2.0
## language: en

Feature: Sensitive Data Exposure
  TOE:
    Node-goat
  Category:
    Sensitive Data
  Location:
    http://localhost:4000/dashboard
  CWE:
    CWE-319: Cleartext Transmission of Sensitive Information
      https://cwe.mitre.org/data/definitions/319.html
  Rule:
    REQ.029 Cookies with security attribute
      https://fluidattacks.com/web/rules/029/
  Goal:
   Steal user's session
  Recommendation:
   Use the HTTPS protocol

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
    | Wireshark       | 3.2.2         |
  TOE information:
    Given I am accessing the site http://localhost:4000
    And the server is running MongoDB version 2.1.18
    And Nodejs version 10.15.3
    And Express version 4.13.4

  Scenario: Normal use case
    Given I am on the login page
    And I can see a form with "Username" parameter and "Password" parameter
    When I login with "newuser" as "Username" and "1234" as the "Password"
    And I press the "Submit" button
    Then Appears the dashboard page
    And The login is successfully

  Scenario: Static detection
    Given I access to the source code
    When I check the source code
    Then I can see the related code
    """
    var http = require("http");
    http.createServer(app).listen(config.port, function() {
        console.log("Express http server listening on port " + config.port);
    });
    """
    When I analyze the code
    Then I realize the website uses the HTTP protocol
    And The user's session is exposed to be stolen

  Scenario: Dynamic detection
    Given I am on the login page
    When I use the Wireshark tool to monitor the network traffic
    And I log in with "newuser" as "Username" and "1234" as the "Password"
    Then The login is successfully
    And I can capture the login request
    When I inspect the request
    Then I realize that the request is an HTTP request
    And The user's session is exposed to be stolen

  Scenario: Exploitation
    Given I can access to HTTP request
    When I look for the user's session
    Then I can see the variables related to the session [evidence](evidence.png)
    """
    Cookie pair: PHPSESSID=lvt41apitlks8lab26lc380122
    Cookie pair: connect.sid=s%3Afwfgd0frQDnBPZbjnQPA3CT4aIfZ6K5D.oHeY5O51R
    aq8hg75EDXFbruLUYevcJuhqx2FaZhBtVU
    """
    When I access the website within the incognito mode
    Then Appears the login page
    When I set "PHPSESSID" and "connect.sid" into the cookies
    And I refresh the website
    Then Appears the dashboard page

  Scenario: Remediation
    Given The web site is using the HTTP protocol
    When The website needs to communicate with the server
    Then The the website have to use the HTTPS protocol
    """
    var fs = require("fs");
    var https = require("https");
    var path = require("path");
    var httpsOptions = {
        key: fs.readFileSync(path.resolve(__dirname, "./app/cert/key.pem")),
        cert: fs.readFileSync(path.resolve(__dirname, "./app/cert/cert.pem"))
    };
    https.createServer(httpsOptions, app).listen(config.port, function() {
    console.log("Express https server listening on port " + config.port);
    });
    """
    And The website can establish secure communication with the server

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      5.3/10 (Medium) - AV:A/AC:H/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.1/10 (Medium) - E:H/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      3.4/10 (Medium) - CR:L/IR:L/AR:L/MAV:A/MAC:H/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-03-03
