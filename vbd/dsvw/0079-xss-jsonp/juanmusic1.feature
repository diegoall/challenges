## Version 1.4.1
## language: en

Feature:
  TOE:
    dsvw
  Category:
    Cross Site Scripting (jsonp)
  Location:
    http://127.0.0.1:65412/ - users.json (field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject a persistent script
  Recommendation:
    Encode the text of the input to escape the special characters

  Background:
  Hacker's software:
    | <Software name>   | <Version>   |
    | Ubuntu            | 18.04.3     |
    | Mozilla Firefox   | 72.0.1      |
  TOE information:
    Given The site
    And The server is running BaseHTTP version 0.3
    And Python version 2.7.16
    And SQLite version 3

  Scenario: Normal use case
    When I click for read a json file
    Then A message in a alert show with the users

  Scenario: Static detection
    When I look at the code of file dsvw.py
    """
    content = "%s%s%s" % ("" if not "callback" in params
            else "%s(" % params["callback"],
            json.dumps(dict((_.findtext("username"),
            _.findtext("surname"))
            for _ in xml.etree.ElementTree.fromstring(USERS_XML).findall("user"))),
            "" if not "callback" in params else ")")
    """
    Then I can see this callback can execute some JavaScript code
    And I can conclude that there are no safe callback

  Scenario: Dynamic detection
    When I try to add bad JavaScript code on the 'a' tag
    """
    href="/users.json?callback=var x =%3Bprocess"
    """
    Then This send me an error
    And I can conclude that there are no safe callback

  Scenario: Exploitation
    When I make the next request
    """
    http://127.0.0.1:65412/users.json?callback=alert(%22HACKED%22)%3Bprocess
    """
    Then I see that an alert comes up on the screen with my message
    And I concluded that the site allows to insert other JavaScript code

  Scenario: Remediation
    When I add the next code
    """
    content = "%s%s%s" % ("Error to load" if preg_match('/\W/', params["callback"])
            else "%s(" % params["callback"],
            json.dumps(dict((_.findtext("username"),
            _.findtext("surname"))
            for _ in xml.etree.ElementTree.fromstring(USERS_XML).findall("user"))),
            "" if preg_match('/\W/', params["callback"] else ")")
    """
    Then I make the next request:
    """
    http://127.0.0.1:65412/users.json?callback=alert(%22HACKED%22)%3Bprocess
    """
    And the comment look like this:
    """
    Error to load
    """
    When The parameter contains a non-word character
    Then The content will return 'Error on load'
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (Medium) - CR:L/IR:L/AR:L/MAV:X/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-01-29
