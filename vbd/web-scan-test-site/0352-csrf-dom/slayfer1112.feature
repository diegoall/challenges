## Version 1.4.1
## language: en

Feature: CSRF dom
  TOE:
  Web Scanner Test Site
  Category:
  XSS
  Location:
  http://www.webscantest.com/crosstraining/reservation.php
  CWE:
  CWE-79: Improper Neutralization of Input During Web Page Generation
  ('Cross-site Scripting')
  https://cwe.mitre.org/data/definitions/79.html
  CWE-352: Improper Neutralization of Script-Related HTML Tags in a Web Page
  ('Cross-Site Request Forgery (CSRF)')
  https://cwe.mitre.org/data/definitions/352.html
  Rule:
  REQ.173: Discard unsafe inputs
  https://fluidattacks.com/web/rules/173/
  Goal:
  Get the cookie of any user.
  Recommendation:
  Sanitize and validate inputs.

  Background:
      Hacker's software:
      | <Software name> | <Version>     |
      | Windows         | 10            |
      | Google Chrome   | 80.0.3987.132 |
    TOE information:
    Given I am accessing the site http://www.webscantest.com/
    Then I can see there is a web page with challenges

  Scenario: Normal use case:
    Given I access http://www.webscantest.com/
    And I select a challenge
    Then I can try to hack it

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection
    Given http://www.webscantest.com/crosstraining/dom.php
    And I see a form that ask me my name
    Then I type my name and send the form
    And I see the response with my name
    When I see another button that say clear the cookies
    Then I was curious and read the cookies
    And I found a cookie "firtname" with my name
    When I see that I clear the cookies and try inject something
    Then I use "<script>alert('XSS')</script>"
    And I send the form
    When I see that the alert works I think that I can inject any HTML
    Then I found a vulnerability

  Scenario: Exploitation
    Given The system is vulnerable to HTML injection
    And I want to make CSRF
    When I think a way to make the CSRF
    Then I think that I can use a XSS-reflected with a fake web
    And I perform a fake website with "webhook.site"
    When I try to make fill the form with an iframe
    Then I send the form
    And I get the Iframe with the CSRF site [evidence 1](evidence1.png)
    When I see that load successful I try to sen the form with a new name
    Then I see that the name is changed successfuly [evidence 2](evidence2.png)
    And I explot the vulnerability

  Scenario: Remediation
    Given I didn't have access to the source code
    And I can give a way to solve this vulnerability
    When I think that is probably that the site is written in php
    Then I think that is possile that the site use the following code
    """
    <?php
    $fname = $_COOKIE["firstname"];

    if ($fname) {
      echo '<p>Hello ' , $fname , '!</p>';
    }
    else {
      echo "<form method='post' action='request.php'>First Name:
      <input type='text' name='fname' value=''><input type='submit'
      value='Submit'></form><br/><br/>";
    }

    echo "<br/><form method='post' action='request.php'>
    <input type='hidden' name='clear' value='all'>Clear all cookies
    <input type='submit' value='Submit'></form><br/><br/>";

    echo "<a href='index.php'>Go Back</a>";
    ?>
    """
    And I think that one way to solve that vulnerability
    When I think that I can scape the injections with "htmlentities()"
    Then I would use this code
    """
    <?php
    $fname = htmlentities($_COOKIE["firstname"]);

    if ($fname) {
      echo '<p>Hello ' , $fname , '!</p>';
    }
    else {
      echo "<form method='post' action='request.php'>First Name:
      <input type='text' name='fname' value=''><input type='submit'
      value='Submit'></form><br/><br/>";
    }

    echo "<br/><form method='post' action='request.php'>
    <input type='hidden' name='clear' value='all'>Clear all cookies
    <input type='submit' value='Submit'></form><br/><br/>";

    echo "<a href='index.php'>Go Back</a>";
    ?>
    """
    And I think that with that the vulnerability can be fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
  5.7/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
  5.5/10 (Medium) - E:H/RL:O/RC:C/CR:H
  Environmental: Unique and relevant attributes to a specific user environment
  8.4/10 (High) - MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-07-10
