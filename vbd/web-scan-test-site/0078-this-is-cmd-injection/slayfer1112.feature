## Version 1.4.1
## language: en

Feature: This is vulnerable to cmd injection
   TOE:
    Web Scanner Test Site
  Category:
    Exposure of Resource to Wrong Sphere
  Location:
    http://www.webscantest.com/osrun/whois.php
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS Command
    ('OS Command Injection')
    https://cwe.mitre.org/data/definitions/78.html
  Rule:
    REQ.173: Discard unsafe inputs
    https://fluidattacks.com/web/rules/173/
    REQ.142: Change system default credentials
    https://fluidattacks.com/web/rules/142/
  Goal:
    Get the users of the machine.
  Recommendation:
    Sanitize and validate inputs.

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Windows         | 10                |
    | Google Chrome   | 80.0.3987.132     |
  TOE information:
    Given I am accessing the site http://www.webscantest.com/
    Then I can see there is a web page with challenges

  Scenario: Normal use case:
    Given I access http://www.webscantest.com/
    And I select a challenge
    Then I can try to hack it

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection
    Given I acces to http://www.webscantest.com/osrun/whois.php
    And The website have a field to make whois in CMD
    When I use google.com and I receive the response [evidence](img1)
    Then I try to use a command, like "id"
    And I see that I receive a error message
    When I think that probably scape directly the command
    Then I try to run another command with ";" to make a new line
    And I type ";id"
    When I receive the response I can see the id [evidence](img2)
    Then I can conclude that the system is vulnerable to command injection
    And I found a vulnerability

  Scenario: Exploitation
    Given The system is vulnerable to command injection
    And The system can execute more than 1 command
    When I think a way to exploit this vulnerability
    Then I think I can see the users list
    And I type ";cat /etc/passwd"
    When I submit this line
    Then I see in the response the users list [evidence](img3)
    And I exploit the vulnerability

  Scenario: Remediation
    Given I didn't have access to the source code
    And I can give a way to solve this vulnerability
    When I think a way to solve the vulnerability
    Then I can use "escapeshellcmd" and "escapeshellarg" in "whois.php"
    """
    exec(escapeshellcmd("whois " .
          escapeshellarg($_GET['<parameter>'])), $output);
    """
    And I can fix the vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.8/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.4/10 (High) - E:H/RL:O/RC:C/CR:H/IR:H/AR:H
  Environmental: Unique and relevant attributes to a specific user environment
    8.4/10 (High) - MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2020-04-15
