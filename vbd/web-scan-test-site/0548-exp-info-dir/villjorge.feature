## Version 1.4.1
## language: en
Feature:
  TOE:
    Web Scanner Test Site
  Category:
    Directory Indexing
  Location:
    http://webscantest.com/angular/angular1/sub/
  CWE:
    CWE-548: Exposure of Information Through Directory Listing
  Rule:
    REQ.176: Restrict system objects
  Goal:
    Get access to hidden files
  Recommendation:
    Disable directory listing

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Ubuntu          | 18.04.5 LTS |
    | Mozilla Firefox | 80.0.1      |
  TOE information:
    Given I am accessing http://webscantest.com/angular/angular1/index.html
    Then I see various hyperlinks in the top of the webpage
    And the server is running Apache version 2.4.7

  Scenario: Normal use case
    Given I access http://webscantest.com/angular/angular1/index.html
    Then I move the cursor in the option "Features-keydown"
    But the hyperlink does not seems to work

  Scenario: Static detection
    Given I access the web site
    Then I notice that the source code is inaccesible

  Scenario: Dynamic detection
    Given I access http://webscantest.com/angular/angular1/index.html
    Then I access to any hyperlink holded in this site, like "Docs-mousedown"
    """
    http://webscantest.com/angular/angular1/sub/docs-mousedown.html
    """
    But I notice a strange "/sub/" folder
    Then I access the folder by using the following link
    """
    http://webscantest.com/angular/angular1/sub/
    """
    And I get access to the directory shown in [evidence] (dir.png)

  Scenario: Exploitation
    Given I access to the directory shown in [evidence] (dir.png)
    And I notice the hidden page "Features-keydown"
    Then I access in the hidden website as show in [evidence] (features.png)
    """
    http://webscantest.com/angular/angular1/sub/features-keydown.html
    """
    Then I can conclude that I can access the index dir of "/sub/" folder

  Scenario: Remediation
    Given the site runing Apache 2.4.7
    Then directory listing must be disabled by setting the following directive
    """
    <Directory /your/website/directory>Options -Indexes</Directory>
    """
    And that directive is added in the Apache "httpd.conf"
    Then I can confirm that the patch avoids to show the index directory

  Scenario: Scoring

  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - /AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - /E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - /MAV:N/MAC:L/MPR:N/MUI:R/MS:U/MC:L/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date {2020-10-01}
