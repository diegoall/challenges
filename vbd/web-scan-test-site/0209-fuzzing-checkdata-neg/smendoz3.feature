## Version 1.4.1
## language: en

Feature:
  TOE:
    web-scanner-test-site
  Category:
    Fuzzing
  Location:
    http://webscantest.com/payment_analysis/checkdata.php - Number (field)
  CWE:
    CWE-209: Generation of Error Message Containing Sensitive Information
      https://cwe.mitre.org/data/definitions/209.html
    CWE-134: Use of Externally-Controlled Format String
      https://cwe.mitre.org/data/definitions/134.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Indicate that the application has not fully validated user-supplied input
  Recommendation:
    Sanitize inputs
    Use of Prepared Statements
    Use of Stored Procedures
    Escaping all User Supplied Input
    Normalize all user-supplied data before applying filters or regexp

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 16.04         |
    | Firefox       | 72.0.02       |
  TOE information:
    Given I am accesing the webpage
    """
    http://webscantest.com/payment_analysis/checkdata.php
    """
    When I get site IP address using command "nslookup webscantest.com"
    """
    Server:     127.0.1.1
    Address:    127.0.1.1#53

    Non-authoritative answer:
    Name:    webscantest.com
    Address: 69.164.223.208
    """
    Then I can see the IP Address information
    When I run "nmap -sC -sV -oA nmap 69.164.223.208"
    Then I can see the webpage information
    And is runing on Ubuntu 14.04.2 LTS
    And is running Apache/2.4.7 (Ubuntu)
    And is powered by PHP/5.5.9

  Scenario: Normal use case
  Users can use the webapp to submit orders
    When I fill a form
    Then I get a message:
    """
    Thank you for your submission. Your order will be processed immediately.
    """

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    When I access "http://webscantest.com/payment_analysis/checkdata.php"
    Then I input "Number" field with "-10" value
    And I see that the webpage return an error:
    """
    Error Occurred While Processing Request
    ...
    """
    And Based on this result I conclude there exists an input error
    And I can conclude that the service does not sanitize inputs

  Scenario: Exploitation
    When I access to Normal use case
    Then I test the number field with "-10" value
    """
    Error Diagnostic Information

    Processing Error Code = 22005
    Processing Error Text = Cannot convert -10 to number

    Date/Time: 04/21/2020 10:19:56
    Browser: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0)
    Gecko/20100101 Firefox/75.0
    Remote Address: 181.62.28.167
    Query String:
    CFTOKEN=4022d2b%2D448c7e09%2D35cf%2D4c28%2Da42c%2D41674b83c311&
    CFID=18446744073709551617&return_page=%2Fhome%2Ecfm
    """
    And the query generate an error
    When I look at the error information
    Then I notice it get processing error code 22005
    When I look to this error
    Then I notice is a DB2 SQL error
    And also gives CFTOKEN and CFID
    And I can search to elaborated attacks for the identified targets
    And I can conclude the website is vulnerable to Fuzzing

  Scenario: Remediation
    Given The application has not fully validated user-supplied input
    And is powered by PHP/5.5.9
    When I make use of Prepared Statements or use PHP Sanitize filters
    Then the webpage can prevent giving vulnerable information

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
  No correlations have been found to this date 2020-04-21
