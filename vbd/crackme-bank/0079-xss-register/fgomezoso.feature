## Version 1.4.1
## language: en

Feature:
  TOE:
    Crack Me Bank
  Category:
    Cross Site Scripting
  Location:
    http://crackme.trustwave.com/kelev/register/register.php - FirstName(Field)
    http://crackme.trustwave.com/kelev/php/login.php
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
      https://cwe.mitre.org/data/definitions/79.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit cross site scripting
  Recommendation:
    Escape input data from users

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 10              |
    | Chrome            | 78.0.3904.70    |
  TOE information:
    Given I access the main page
    And the page is made with PHP
    And any user must register in order to create a new account
    And any user doesn't need to validate the new account by email

  Scenario: Normal use case
    When I click on the register link from the side bar
    Then a register form is displayed
    And almost every field is required in order the create the new account

  Scenario: Static detection
    Given I do not have access to the PHP server code
    When the HTML is the only code available
    Then I cannot perform static detection

  Scenario: Dynamic detection
    When I access the register form
    Then I write an alert javascript code on the First Name field
    """
    <script>alert(1);</script>
    """
    And I submit the form using simple names for UserID and password:
      | <UserID>  | <Password>  |
      |  test1    |   asdf      |
    And the new user "test1" is created
    When I go to the login link located on the side bar
    Then I enter my userID and Password
    And a new page is generated with an alert message [evidence](img.png)
    And the site is not validating user input

  Scenario: Exploitation
    When I access the Register form
    Then I write a javascript code on the First Name field
    """
    <script>alert(document.cookie);</script>
    """
    And I create a new account by using "test2" as UserID
    When I go to the login form
    Then I login as "test2"
    And a new page is generated with an alert message [evidence](img2.png)
    And the message shows me sensitive data
    When I login again using the same UserID and Password for "test2"
    Then the alert is shown again
    And the bank's website is vulnerable to persistent XSS

  Scenario: Remediation
    When a form is handling user input with sensitive data
    Then malicious characters like '<' and '>' must be escaped
    And the XSS attacks can be prevented

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.0/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.0/10 (Medium) - CR:M/IR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-11-07
