## Version 1.4.1
## language: en

Feature:
  TOE:
    Kioptrix4
  Category:
   Unprotected Storage
  Location:
    192.168.60.130:22
  CWE:
    CWE-256: Unprotected Storage of Credentials
  Rule:
    REQ.035 Manage privilege modifications
  Goal:
    Get clear text credentials
  Recommendation:
    Avoid storing passwords in easily accessible locations.

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | OpenSSH              | 7.7p1          |
  TOE information:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials

  Scenario: Normal use case
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I get the LigGoat Shell
    And bypass it with the vulnerability 0269

  Scenario: Static detection
    Given I am accessing the server 192.168.60.130 through SSH
    And can bypass the restricted shell
    Then I can check the file /var/www/checklogin.php
    And I can see credentials on lines 4 and 5
    """
    $host="localhost"; // Host name
    $username="root"; // Mysql username
    $password=""; // Mysql password
    """

  Scenario: Dynamic detection
    Given I am accessing the server 192.168.60.130 through SSH
    And can bypass the restricted shell
    Then I can use the following command
    """
    grep -nr "password=" .
    """
    And find the file /var/www/checklogin.php with the credentials

  Scenario: Exploitation
    Given I am accessing the server 192.168.60.130 through SSH
    And that I have credentials to the MySQL database
    Then I can view all databases like members
    And get all of it's data
    """
    mysql> select * from members;
    +----+----------+-----------------------+
    | id | username | password              |
    +----+----------+-----------------------+
    |  1 | john     | MyNameIsJohn          |
    |  2 | robert   | ADGAdsafdfwt4gadfga== |
    +----+----------+-----------------------+
    2 rows in set (0.00 sec)
    """
    Then I can also get the MySQL database version
    """
    mysql> select @@version;
    +--------------------+
    | @@version          |
    +--------------------+
    | 5.0.51a-3ubuntu5.4 |
    +--------------------+
    1 row in set (0.01 sec)
    """

  Scenario: Remediation
    Avoid storing passwords in easily accessible locations.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.9/10 (High) - AV:L/AC:L/PR:L/UI:N/S:C/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.6 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.4 (High) - CR:H/IR:H/AR:H/MAV:L/MAC:L/MPR:L/MUI:N/MS:C/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-04-03
