## Version 1.4.1
## language: en

Feature:
  TOE:
    Kioptrix4
  Category:
   XSS Reflected
  Location:
    http://192.168.60.130/member.php?username=something
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure transactions zone
  Recommendation:
    Escape external input before executing it

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Windows              | 10 Pro         |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Burpsuite            | 2.1.04         |
  TOE information:
    Given I am accessing the site http://192.168.60.130/
    And Entered to site .../member.php
    Then I can see there is section for members

  Scenario: Normal use case
    Given I access 192.168.60.130/member.php
    Then I can see there is section for members
    And it shows the username of the current user

  Scenario: Static detection
    Given I do not have access to the source coude
    Then I can not make static detection

  Scenario: Dynamic detection
    Given I access to 192.168.60.130/member.php
    Then I can write the following script in the username HTML query
    """
    "<script>alert("xss")</script>
    """
    Then I can see the alert showing in the screen

  Scenario: Exploitation
    Given I access to 192.168.60.130/member.php
    Then I can write the following script in the search bar
    """
    <script>alert(Document.cookie)</script>
    """
    Then I get the cookie in the alert as result
    And I conclude that it is possible to steal a session in this way

  Scenario: Remediation
    Escape external input before executing it

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.6 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.6 (Medium) - CR:L/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-04-01
