## Version 1.4.1
## language: en

Feature:
  TOE:
    Kioptrix4
  Category:
   Bruteforce attack
  Location:
    192.168.60.130:22
  CWE:
    CWE-269: Improper Privilege Management
  Rule:
    REQ.035 Manage privilege modifications
  Goal:
    Execute commands outside the restricted shell
  Recommendation:
    Restrict all unnecessary commands

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | OpenSSH              | 7.7p1          |
  TOE information:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials

  Scenario: Normal use case
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I get the LigGoat Shell

  Scenario: Static detection
    Given I do not have access to the source code
    Then I can not make static detection

  Scenario: Dynamic detection
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I get the LigGoat Shell
    And I can use the command echo
    Then I try to bypass the restricted shell

  Scenario: Exploitation
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I get the LigGoat Shell
    And I can use the command echo
    Then I use the following command
    """
    echo os.system('/bin/bash')
    """
    Then I get the bash shell
    And I can execute any linux command

  Scenario: Remediation
    Restrict all unnecessary commands

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.3 (Medium) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.4 (High) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:L/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-04-02
