## Version 1.4.1
## language: en

Feature:
  TOE:
    Kioptrix 4
  Category:
   Information Exposure
  Location:
    Kioptrix4
  CWE:
    CWE-200: Information Exposure
  Rule:
  REQ.0176. The system must restrict access to system objects that have
  sensitive content. It will only allow access to authorized users.
  Goal:
    View the server's SSH version
  Recommendation:
    Don't display information that is not necessary for the end user

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | OpenSSH              | 7.7p1          |
  TOE information:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials

  Scenario: Normal use case:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I execute commands as a low privilege user
    Then I get the LigGoat Shell

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I execute the following command
    """
    nc -v 192.168.60.130 22
    """
    Then I see that the SSH version is "SSH-2.0-OpenSSH_4.7p1"

  Scenario: Exploitation
    Given I know the SSH version
    Then I get reduced my vulnerabilities search space
    When I look at the current open vulnerabilities for OpenSSH_4.7p1
    Then I see a list of highly and moderately critical vulnerabilities
    And I could exploit the open ones

  Scenario: Remediation
    Update the server to the most recent version

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.8/10 (Low) - E:U/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.2/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-04-06
