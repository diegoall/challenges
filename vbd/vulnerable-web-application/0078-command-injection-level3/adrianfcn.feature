## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    OS Command Injection
  Location:
    http://localhost:9991/www/CommandExecution/CommandExec-3.php
    typeBox (field)
  CWE:
    CWE 78: Improper Neutralization of Special Elements used in an OS Command
    ('OS Command Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Get the flag in "comex3/log3.txt"
  Recommendation:
    Pass each entry through a filter in search of malicious characters

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | Buster      |
    | Firefox         | 68.9.0      |
  TOE information:
    Given I am accesing the site
    And a text box
    And The app is running on XAMPP 5.6.21

  Scenario: Normal use case
    Given a common textbox asking for a keyword
    When I submit the textbox with my name
    Then the app doesn't give any response

  Scenario: Static detection
    When I watch the source code
    Then I can see a function inside the PHP code
    """
    $target = $_GET["typeBox"];
    $substitutions = array(
        '&&' => '',
        '& ' => '',
       '&& ' => '',
        ';'  => '',
        '|'  => '',
        '-'  => '',
        '$'  => '',
        '('  => '',
        ')'  => '',
        '`'  => '',
        '||' => '',
        '/'  => '',
        '\\' => '',
    );
    $target = str_replace(array_keys($substitutions),$substitutions,$target);
    echo shell_exec($target);
    """
    And As seen, some characters are deleted
    """
    '&&','& ','&& ',';','|','-','$','(',')','`','||','/','\\'
    """
    And execute the given command without the previous characters

  Scenario: Dynamic detection
    When I test if the commands operate "id"
    Then the program shows the command output
    And I can see the command in the URL
    Then I can conclude that I can execute commands
    And the app use the method of the protocol HTTP GET for sending the message
    And [evidence](image1.png)

  Scenario: Exploitation
    Given I can execute commands
    And that the characters are removed
    """
    '&&','& ','&& ',';','|','-','$','(',')','`','||','/','\\'
    """
    Then I get the complete list of files and directories
    """
    CommandExec-1.php CommandExec-2.php CommandExec-3.php CommandExec-4.php
    commandexec.html
    comex1: log1.txt
    comex2: log2.txt
    comex3: log3.txt
    """
    When I use "cd comex3 &;&; grep 'password' log3.txt"
    Then I obtain a password hashed with MD5
    """
    password:327a6c4304ad5938eaf0efb6cc3e53dc
    """
    When I use "https://www.md5online.org/md5-decrypt.html"
    Then I obtain a password in plain text "flag"
    And I can conclude that the application has a weak filter

  Scenario: Remediation
    Given I have patched the code by doing
    """
    $string = $_GET["typeBox"];
    $target = preg_replace("/[^A-Za-z0-9 ]/", '', $string);
    echo shell_exec($target);
    """
    Then the filter accepts only "[A-Za-z0-9 ]"
    When I use "cd .. &;&;cd ..&;&; cd etc &;&; cat passwd"
    And the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constant over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.2/10 (Medium) - E:H/RL:W
  Environmental: Unique and relevant Attributes to a specific user environment
    5.2/10 (Medium) - CR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MC:L

  Scenario: Correlations
    No correlations have been found to this date 2019-09-03
