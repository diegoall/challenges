## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Vulnerable Web Services - dvws
  Category:
    XSS - Cross Site Scripting
  Location:
    http://dwvs/admin.html
  CWE:
    CWE-79: Improper Neutralization of Input During
    Web Page Generation ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Execute JS code performing an XSS attack
  Recommendation:
    Encode user input

  Background:
  Machine information:
    | <Software name>   |  <Version> |
    | VMWare ESX        |     6.5    |
    | Windows           |      10    |
    | Debian VM         |   10.4.6   |
  Hacker Software:
    | Firefox           |   78.0.1   |

  TOE information:
    Given I am accessing the site "http://dvws/"
    And is installed in a Debian VM
    And it is built with php and mysql

  Scenario: Normal use case
    Given I login into dvws with a user with admin credentials
    And I access "http://dvws/admin.html"
    Then a form in which you can search for usernames
    And receive a response confirming whether the user exists or not

  Scenario: Static detection
    When I look at the code in the "/soapserver/dvwsuserservice.js" file
    Then I can see where the result is displayed
    """
    router.post('/', function (req, res, next) {
        var options = {
            noent: true,
            dtdload: true
        }
        var xmlDoc = libxml.parseXml(req.body, options);
        var xmlchild = xmlDoc.get('//username');
        var username = xmlchild.text()
        mongoose.connect(connUri, { useNewUrlParser: true, useUnifiedTopology:
        true }, (err) => {
            User.findOne({ username }, function (err, obj) {
                if (obj != null) {
                    result = "User Exists:" + xmlchild.text()
    ...
                } else {
                    result = "User Not Found:" + xmlchild.text()
    """
    And I can conclude that no sanitizing is being done
    And I can perform XSS attacks

  Scenario: Dynamic detection
    Given I login into dvws with a user with admin credentials
    And I access "http://dvws/admin.html"
    And I encode the following string
    """
    <h1>Test</h1>
    """
    And I send the following encoded string via dvws form
    """
    &lt;h1&gt;Test&lt;/h1&gt;
    """
    Then I get the html sent code parsed
    And I can conclude that the application is vulnerable to XSS

  Scenario: Exploitation
    Given I login into dvws with a user with admin credentials
    And I access "http://dvws/admin.html"
    And I encode the following string
    """
    <script>alert('Test')</script>
    """
    And I send the following encoded string via dvws form
    """
    &lt;script&gt;alert('Test')&lt;/script&gt;
    """
    Then I get a popup showing the word 'Test'
    And I can conclude that the js code has been executed

  Scenario: Remediation
    Given the application does not sanitize correctly the input
    Then I edit the code in the "/soapserver/dvwsuserservice.js" file
    And I add the encodeURIComponent in the following lines
    """
    result = "User Exists:" + encodeURIComponent(xmlchild.text())
    result = "User Not Found:" + encodeURIComponent(xmlchild.text())
    """
    Then the input is not parsed and neither the code executed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.6/10 (High) - AV:A/AC:L/PR:H/UI:R/S:C/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.4/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    None
