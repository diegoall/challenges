## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Vulnerable Web Services - dvws
  Category:
    SFP Secondary Cluster: Exposed Data
  Location:
    http://dvws/
  CWE:
    CWE-210: Information Exposure Through Self-generated Error Message
  Rule:
    REQ.105 Avoid assets leakage
  Goal:
    Enumerate valid usernames
  Recommendation:
    Use generic error message for failed authentication attempts

  Background:
  Machine information:
    | <Software name>   |  <Version> |
    | VMWare ESX        |     6.5    |
    | Windows           |      10    |
    | Debian VM         |   10.4.6   |
  Hacker Software:
    | Firefox           |   78.0.1   |
    | curl              |   7.64.0   |

  TOE information:
    Given I am accessing the site "http://dvws"
    And is installed in a Debian VM
    And it is built with php and mysql

  Scenario: Normal use case
    Given I access "http://dvws"
    And a user authentication form is showed
    When I insert a user
    And a password
    Then the application gives access if data is correct
    But if not, the application shows one of these messages
    """
    Login Failed! User xxxxxxx not found!
    Authentication error
    """

  Scenario: Static detection
    When I look at the code in "controllers/users.js"
    Then I see the conditionals determining if the username is an existing one
    And the variable that is determining the state of the error message
    """
       User.findOne({username}, (err, user) => {
         if (!err && user) {
    """
    Then in case the condition is not true I can see the error message
    """
       result.error = 'Login Failed! User ' + username + ' not found!';
    """
    Then I can see the password comparison condition in the following lines
    """
       // We could compare passwords in our model instead of below as well
       bcrypt.compare(password, user.password).then(match => {
         if (match) {
    """
    Then in case the condition is not true I can see the error message
    """
       result.error = `Authentication error`;
    """

  Scenario: Dynamic detection
    Given a registered user
    When testing with a registered-user/random-password
    Then I can see "Authentication error" message is displayed
    When testing with a  random-user/random password
    Then I can see "Login Failed! User random user not found!" msg is shown
    When testing with a registered-user/correct-password
    Then the user logs in succesfully

  Scenario: Exploitation
    Given the information error msg provides to know if a username is valid
    When I develop and run a bash script that checks for valid users
    """
    See 'enumerate-users.sh' file
    """
    And use curl to send post request to http://dvws/api/v2/login
    And a sample dictionary of common users
    """
    See 'usernames.txt" file
    """
    Then I can determine valid user names in the valid.txt output file

  Scenario: Remediation
    Given I have changed the error message
    And in both cases with the generic message
    """
    Authentication error
    """
    Then the attacker cannot distinguish between incorrect user or password

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      4.3/10 (medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      4.0/10 (medium) - E:F/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      3.4/10 (low)- CR:L/IR:L/AR:L

  Scenario: Correlations
    None
