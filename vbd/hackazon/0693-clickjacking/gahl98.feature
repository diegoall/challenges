## Version 1.4.1
## language: en

Feature: Detect vulnerabilities in Hackazon application
  TOE:
    Hackazon
  Category:
    0693
  Location
    http://hackazon.lc/ - X-Frame-Options (header)
  CWE:
    CWE-693: Protection Mechanism Failure
  Rule:
    REQ.175: https://fluidattacks.com/web/en/rules/175/
  Goal:
    Exploit vulnerability of application
  Recommendation:
    Implement X-Frame Header

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Zorin OS              | 12.4      |
    | Docker                | 18.09.2   |
    | OWASP ZAP             | 2.7.0     |
    | Postman               | 6.4.4     |
  TOE information:
    Given The site is running from docker
    And I am accessing the site http://hackazon.lc/
    And having created a user for testing


  Scenario: Normal use case
    Given I access http://hackazon.lc/
    And I click "Sign In" link
    And it loads a login form box
    Then I write username and password of test user
    And I hit "Sign In" Button
    Then I access http://hackazon.lc/account page

  Scenario: Static detection
    Given I access the site http://hackazon.lc/ in Postman
    When I inspect the headers of the page
    And I don't find "X-Frame-Options" header
    Then I open apache2/conf-available/security.conf
    And I read the code and I find
    """
    70 #Header set X-Frame-Options: "sameorigin"
    """

  Scenario: Dynamic detection
    Given I'm running OWASP ZAP 2.7.0
    And I scan for vulnerabilities
    And the scanner found "X-Frame-Options Header Not Set"
    Then I search how to test clickjacking vulnerabilities
    And I find a test at
    """
    https://www.owasp.org/index.php/Testing_for_Clickjacking_(OTG-CLIENT-009)
    """
    Then I write an HTML for test loading into an iframe
    """
    5 <body>
    6   <p>Website is vulnerable to clickjacking!</p>
    7   <iframe src="http://hackazon.lc/" width="500" height="500"></iframe>
    8 </body>
    """
    And I see the page loads into the iframe

  Scenario: Exploitation
    Given No X-Frame-Options are configured
    And I have tested the load into an iframe
    Then I add the login form fields of the login box into the test page
    """
    8   <form role="form" method="post" class="signin bv-form"
    action="http://hackazon.lc/user/login" id="loginForm"
    novalidate="novalidate" target="iframeb">
    9   <input type="text" name="username" id="username"
    class="form-control input-lg"
     placeholder="Username or Email" data-bv-field="username">
    10  <input type="password" name="password" id="password"
    class="form-control input-lg" placeholder="Password"
    data-bv-field="password">
    11  <button id="loginbtn" type="submit"
    class="btn btn-success btn-block btn-lg">
    Sign In</button>
    12 </form>
    13 <iframe name="iframeb" src="http://hackazon.lc/"
    width="1200" height="500"></iframe>
    """
    Then I write the username and password of test user outside the iframe
    And I click the "Sign In" button
    And the iframe loads the account page for the test user

  Scenario: Remediation
    Given that server option that enables X-Frame-Options is commented
    Then I access the config file an remove the comment status in line 70
    """
    70 Header set X-Frame-Options: "sameorigin"
    """
    Then I save the file
    And I enable the header module
    And I restart the apache server
    Then I tried to access again
    And nothing load on the iframe
    And it displays a message
    """
    hackazon.lc refused to connect.
    """

  Scenario: Scoring
    Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
    5/10 (Medium) E:F/RL:O/RC:C/
    Environmental: Unique and relevant attributes to a specific user environment
    5/10 (Medium) MAV:X/MAC:L/MPR:N/MUI:R/MS:U/MC:L/MI:L/MA:N
