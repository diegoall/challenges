#!/usr/bin/env bash

docker run --rm -d --name badstore -p "80:80" -t  jvhoof/badstore-docker
sleep 10
firefox "http://127.0.0.1:80/"

