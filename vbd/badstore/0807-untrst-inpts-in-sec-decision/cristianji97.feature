## Version 1.4.1
## language: en

Feature:
  TOE:
    Badstore
  Category:
    Reliance on Untrusted Inputs in a Security Decision
  Location:
    http://192.168.56.101/cgi-bin/badstore.cgi?action=loginregister
  CWE:
    CWE-807: Reliance on Untrusted Inputs in a Security Decision
  Rule:
    REQ.241: https://fluidattacks.com/web/rules/241
    REQ.240: https://fluidattacks.com/web/rules/240
  Goal:
    Modify role hidden field while user registration in order to get admn access
  Recommendation:
    Avoid manage user permissions from input fields than can be modified

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | Mozilla Firefox | 67.0.2      |
    | Burp suite      | 1.7.36      |
  TOE information:
    Given I am accessing to the site http://192.168.56.101/cgi-bin/badstore.cgi
    And Enter to a web site that allows me register new users
    And The server is running MySql version 4.1.7-standard
    And Apache version 1.3.28
    And Is running on Linux 2.4.18

  Scenario: Normal use case
    Given Access http://192.168.56.101/cgi-bin/badstore.cgi?action=loginregister
    And I fill the new account information
    When I send the account info
    And Complete the registration process
    And The web site automatically log me in with normal user role
    Then I can't see any report available only for admins [evidence](img1.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I cant make a static detection

  Scenario: Dynamic detection
    Given I send new account info in order to create it
    When I intercept HTTP request from Burp
    Then I can see a hidden role parameter that is sent as 'U' by default
    And I assume that 'U' value is equals to normal user role
    Then I change this value to 'A' assuming that it's equals to Admin user role
    And Registration process finishes
    And I was logged in as an admin
    Then I can have admin access modifying role parameter [evidence](img.png)

  Scenario: Exploitation
    Given Being intercepted the following POST HTTP request with Burp
    """
    POST /cgi-bin/badstore.cgi?action=register HTTP/1.1
    Referer: http://192.168.56.101/cgi-bin/badstore.cgi?action=loginregister
    Cookie: SSOid=cHJ1ZWJhMkBwcnVlYmEuY29tOjgyN2NjYjBlZWE4YTcwNmM0YzM0YTE2OD...

    fllnme=Prueba&email=prueba%40prueba.com&passwd=12345&pwdhint=green&role=U...
    """
    And Parameters about account info are in clear text
    When I change the role parameter to 'A'
    """
    POST /cgi-bin/badstore.cgi?action=register HTTP/1.1
    Referer: http://192.168.56.101/cgi-bin/badstore.cgi?action=loginregister
    Cookie: SSOid=cHJ1ZWJhMkBwcnVlYmEuY29tOjgyN2NjYjBlZWE4YTcwNmM0YzM0YTE2OD...

    fllnme=Prueba&email=prueba%40prueba.com&passwd=12345&pwdhint=green&role=A...
    """
    And Finish the registration process
    Then I can access to http://192.168.56.101/cgi-bin/badstore.cgi?action=admin
    And I can see any report such as current users [evidence](img2.png)

  Scenario: Remediation
    When A sign-up form or any form is being developed
    Then It should never has input fields related with user permissions
    But If it's absolutely necessary to use them
    Then They must be correctly checked on the server side
    And This vulnerability won't exist anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.6/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.2/10 (High) - CR:H/IR:H/AR:L/MAC:H/MC:H

  Scenario: Correlations
    No correlations have been found to this date 2019-06-26
