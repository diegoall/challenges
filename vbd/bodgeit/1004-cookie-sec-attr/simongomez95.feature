## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Cookie Management
  Location:
    bodgeit/ - JSESSIONID (Cookies)
  CWE:
    CWE-1004: Sensitive Cookie Without 'HttpOnly' Flag -variant-
      https://cwe.mitre.org/data/definitions/1004.html
    CWE-0732: Incorrect Permission Assignment for Critical Resource -class-
      https://cwe.mitre.org/data/definitions/732.html
    CWE-1011: Authorize Actors -category-
      https://cwe.mitre.org/data/definitions/1011.html
  CAPEC:
    CAPEC-031: Accessing/Intercepting/Modifying HTTP Cookies -detailed-
      https://capec.mitre.org/data/definitions/31.html
    CAPEC-157: Sniffing Attacks -standard-
      https://capec.mitre.org/data/definitions/157.html
    CAPEC-117: Interception -meta-
      https://capec.mitre.org/data/definitions/117.html
  Rule:
    REQ.029: https://fluidattacks.com/web/es/rules/029/
  Goal:
    Steal cookies
  Recommendation:
    Set 'HttpOnly' flag in sensitive cookies

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
  TOE information:
    Given I am running bwapp on http://bwapp

  Scenario: Normal use case
    Given I go to "http://bwapp/"
    Then I can log in and navigate the site

  Scenario: Static detection
  Given I check the code at "WEB-ING/web.xml"
    """
    ...
    03  <web-app xmlns="http://java.sun.com/xml/ns/j2ee"
    04  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    05  xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/
    xml/ns/j2ee/web-app_2_4.xsd"
    06  version="2.4">
    07
    08  <servlet>
    09  <servlet-name>InitServlet</servlet-name>
    10  <display-name>The BodgeIt Store Init Servlet</display-name>
    11  <!-- servlet-class>org.zaproxy.bodgit.servlet.InitServlet</servlet-class
    -->
    12  <jsp-file>/init.jsp</jsp-file>
    13  <load-on-startup>1</load-on-startup>
    14  </servlet>
    15  <servlet-mapping>
    16  <servlet-name>InitServlet</servlet-name>
    17  <url-pattern>/servlet/InitServlet</url-pattern>
    18  </servlet-mapping>
    19
    20  <welcome-file-list>
    21  <welcome-file>home.jsp</welcome-file>
    22  <welcome-file>home.html</welcome-file>
    23  </welcome-file-list>
    24
    25  <!-- error-page>
    26  <error-code>404</error-code>
    27  <location>/axis2-web/Error/error404.jsp</location>
    28  </error-page-->
    29
    30  <!-- error-page>
    31  <error-code>500</error-code>
    32  <location>/axis2-web/Error/error500.jsp</location>
    33  </error-page-->
    34
    35  </web-app>
    ...
    """
    Then I see session cookies aren't being flagged as HttpOnly

  Scenario: Dynamic detection
  Checking HttpOnly flag in cookies
    Given I log in to the site
    And open Firefox Developer tools
    Then I check the cookies
    And find that none of them have HttpOnly set
    Then I know they are accessible via JavaScript

  Scenario: Exploitation
  Steal non-HttpOnly cookies
    Given I know the cookies on the site aren't HttpOnly
    Given I figure out a way to inject an XSS
    Then I can steal user cookies and hijack their sessions

  Scenario: Remediation
  Check the origin corresponds to the site
    Given I add this to "web.xml"
    """
    <session-config>
    <cookie-config>
    <http-only>true</http-only>
    <secure>true</secure>
    </cookie-config>
    </session-config>
    """
    Then the session cookies are flagged as HttpOnly
    Then they can't be stolen anymore via XSS

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/bodgeit/0079-*
      Given I launch a xss attack that steals the user cookies
      And a user falls for it
      Then I have their session cookies
      And I can hijack their session to impersonate them
