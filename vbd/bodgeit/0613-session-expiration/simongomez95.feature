## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Session Management
  Location:
    bodgeit/ - JSESSIONID (Cookie)
  CWE:
    CWE-0613: Insufficient Session Expiration -base-
      https://cwe.mitre.org/data/definitions/613.html
    CWE-0287: Improper Authentication -class-
      https://cwe.mitre.org/data/definitions/287.html
    CWE-1018: Manage User Sessions -category-
      https://cwe.mitre.org/data/definitions/1018.html
  CAPEC:
    CAPEC-060: Reusing Session IDs (aka Session Replay) -detailed-
      http://capec.mitre.org/data/definitions/60.html
    CAPEC-593: Session Hijacking -standard-
      http://capec.mitre.org/data/definitions/593.html
    CAPEC-021: Exploitation of Trusted Credentials -meta-
      http://capec.mitre.org/data/definitions/21.html
  Rule:
    REQ.023: https://fluidattacks.com/web/es/rules/023/
  Goal:
    Hijack another user's session while they're gone
  Recommendation:
    Expire inactive sessions after a prudential time

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/
    Then I can navigate the site and add products to my basket

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the configuration at "/bodgeit/root/WEB-INF/web.xml"
    """
    01  <?xml version="1.0" encoding="ISO-8859-1"?>
    02
    03  <web-app xmlns="http://java.sun.com/xml/ns/j2ee"
    04      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    05      xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.
    com/xml/ns/j2ee/web-app_2_4.xsd"
    06      version="2.4">
    07
    08      <servlet>
    09          <servlet-name>InitServlet</servlet-name>
    10          <display-name>The BodgeIt Store Init Servlet</display-name>
    11          <!-- servlet-class>org.zaproxy.bodgit.servlet.InitServlet</servl
    et-class-->
    12          <jsp-file>/init.jsp</jsp-file>
    13          <load-on-startup>1</load-on-startup>
    14      </servlet>
    15      <servlet-mapping>
    16          <servlet-name>InitServlet</servlet-name>
    17          <url-pattern>/servlet/InitServlet</url-pattern>
    18      </servlet-mapping>
    19
    20    <welcome-file-list>
    21        <welcome-file>home.jsp</welcome-file>
    22        <welcome-file>home.html</welcome-file>
    23      </welcome-file-list>
    24
    25      <!-- error-page>
    26        <error-code>404</error-code>
    27        <location>/axis2-web/Error/error404.jsp</location>
    28      </error-page-->
    29
    30      <!-- error-page>
    31          <error-code>500</error-code>
    32          <location>/axis2-web/Error/error500.jsp</location>
    33    </error-page-->
    34
    35  </web-app>
    """
    Then I see no settings for sesion expiration

  Scenario: Dynamic detection
  Wait and see
    Given I login to the platform with my own account
    And I idle for a long time not interacting with the site
    Then I refresh the site
    And I'm still logged in
    Then I know sessions are not timing out

  Scenario: Exploitation
  Hijack another user's session
    Given obtain another user's session cookies by whatever means
    And use them to access the site
    Then I have control of their account until they log out manually

  Scenario: Remediation
  Expire sessions after a set time
    Given I add this to "web.xml"
    """
    <session-config>
      <session-timeout>5</session-timeout>
    </session-config>
    """
    Then when a user idles for more than 5 minutes
    Then their session expires
    And can't be abused later

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.5/10 (Low) - AV:P/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.3/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.3/10 (Low) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-17

