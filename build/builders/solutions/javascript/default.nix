{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  builders.nodePackage = import ../../../builders/nodejs-module pkgs;
  builders.pythonPackage = import ../../../builders/python-package pkgs;
  inputs = [
    pkgs.nodejs
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            srcEslintConfig = ../../../configs/eslint-javascript.json;
            srcPrettierConfig = ../../../configs/prettier-generic.yaml;

            builder = ./builder.sh;

            nodeJsModuleYargs = builders.nodePackage "yargs@15.3.1";

            nodeJsModulePrettier = builders.nodePackage "prettier@2.0.5";
            nodeJsModuleEslint = builders.nodePackage "eslint@7.1.0";
            nodeJsModuleEslintstrict = builders.nodePackage "eslint-config-strict@14.0.1";
            nodeJsModuleEslintpluginfs = builders.nodePackage "eslint-plugin-fp@2.3.0";

            pyPkgLizard = builders.pythonPackage "lizard==1.17.3";
          })
    )
