source "${srcGeneric}"

function helper_lint_prettier {
  local solution="${1}"

  prettier \
    --check \
    --config "${srcPrettierConfig}" \
    "${solution}"
}

function lint {
  local solution="${1}"
  local lizard_max_warns='0'
  local lizard_max_func_length='30'
  local lizard_max_ccn='10'

      eslint \
        -c "${srcEslintConfig}" \
        "${solution}" \
  &&  if helper_lint_prettier "${solution}"
      then
        continue
      else
            echo '[ERROR] Please run prettier on your solution with config:' \
        &&  echo "${srcPrettierConfig}" \
        &&  return 1
      fi \
  &&  lizard \
        --ignore_warnings "${lizard_max_warns}" \
        --length "${lizard_max_func_length}" \
        --CCN "${lizard_max_ccn}" \
        "${solution}"
}

function build {
      env_prepare_ephemeral_vars \
  &&  env_prepare_node_modules \
  &&  env_prepare_python_packages \
  &&  generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
