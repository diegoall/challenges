{ solutionPath }:

let
  pkgs = import ../../../pkgs/old.nix;
  inputs = [
    pkgs.clojure
    pkgs.clj-kondo
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            builder = ./builder.sh;
          })
    )
