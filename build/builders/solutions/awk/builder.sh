source "${srcGeneric}"

function lint {
  local solution="${1}"

  gawk \
    --source 'BEGIN { exit(0) } END { exit(0) }' \
    --lint=fatal \
    --lint=invalid \
    --posix \
    -f "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
