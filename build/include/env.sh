# shellcheck shell=bash

function list_vars_with_regex {
  local regex="${1}"
  printenv | grep -oP "${regex}" | sort
}

function env_prepare_environment_variables {
  export IS_LOCAL_BUILD
  export ENVIRONMENT_NAME
  export STARTDIR="${PWD}"
  export REPO_NAME
  export REPO_PATH
  export WORKDIR

      echo '[INFO] Sourcing .envrc.public' \
  &&  source './.envrc.public' \
  &&  REPO_NAME="$(basename "${STARTDIR}")" \
  &&  REPO_PATH="$(dirname "${STARTDIR}")" \
  &&  WORKDIR="${REPO_PATH}/.ephemeral/${REPO_NAME}" \
  &&  if test -n "${CI:-}"
      then
            echo '[INFO] In remote build system' \
        && IS_LOCAL_BUILD="${FALSE}"
      else
            echo '[INFO] In local build system' \
        && IS_LOCAL_BUILD="${TRUE}"
      fi \
  &&  if test "${CI_COMMIT_REF_NAME}" = 'master'
      then
            echo '[INFO] In productive environment' \
        &&  ENVIRONMENT_NAME='prod'
      else
            echo '[INFO] In development environment' \
        &&  ENVIRONMENT_NAME='dev'
      fi
}

function env_prepare_ephemeral_vars {
  export MYPY_CACHE_DIR
  export TEMP_FD
  export TEMP_FILE1
  export TEMP_FILE2

  MYPY_CACHE_DIR=$(mktemp)
  exec {TEMP_FD}>TEMP_FD
  TEMP_FILE1=$(mktemp)
  TEMP_FILE2=$(mktemp)
}

function env_prepare_python_packages {
  export PATH
  export PYTHONPATH
  local pkg

  echo '[INFO] Preparing python packages'

  list_vars_with_regex 'pyPkg[a-zA-Z0-9]+' > "${TEMP_FILE1}"

  while read -r pkg
  do
    echo "  [${pkg}] ${!pkg}"
    PATH="${PATH}:${!pkg}/site-packages/bin"
    PYTHONPATH="${!pkg}/site-packages:${PYTHONPATH:-}"
  done < "${TEMP_FILE1}"
}

function env_prepare_ruby_modules {
  export PATH
  export GEM_PATH=''
  local gem

  echo '[INFO] Preparing ruby gems'

  list_vars_with_regex 'rubyGem[a-zA-Z0-9]+' > "${TEMP_FILE1}"

  while read -r gem
  do
    echo "  [${gem}] ${!gem}"
    PATH="${PATH}:${!gem}/bin"
    GEM_PATH="${GEM_PATH}:${!gem}/"
  done < "${TEMP_FILE1}"
}

function env_prepare_node_modules {
  export PATH
  export NODE_PATH
  local module

  echo '[INFO] Preparing node modules'

  list_vars_with_regex 'nodeJsModule[a-zA-Z0-9]+' > "${TEMP_FILE1}"

  while read -r module
  do
    echo "  [${module}] ${!module}"
    PATH="${PATH}:${!module}/node_modules/.bin"
    NODE_PATH="${NODE_PATH}:${!module}/node_modules"
  done < "${TEMP_FILE1}"
}

function env_prepare_cargo_libs {
  local lib

      echo '[INFO] Preparing cargo libs' \
  &&  list_vars_with_regex 'cargoLib[a-zA-Z0-9]+' > "${TEMP_FILE1}" \
  &&  while read -r lib
      do
            echo "  [${lib}] ${!lib}" \
        &&  cp -R ${!lib}/project/ project/ \
        &&  chmod -R 756 project/ \
        ||  return 1
      done < "${TEMP_FILE1}"
}

function env_prepare_composer_modules {
  export PATH
  local module

  echo '[INFO] Preparing composer modules'

  list_vars_with_regex 'composerModule[a-zA-Z0-9]+' > "${TEMP_FILE1}"

  while read -r module
  do
    echo "  [${module}] ${!module}"
    PATH="${PATH}:${!module}/vendor/bin"
  done < "${TEMP_FILE1}"
}

function env_set_utf8_encoding {
  export LC_ALL='en_US.UTF-8'
  export PYTHONIOENCODING='utf-8'
}
