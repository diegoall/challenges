#! /usr/bin/env bash

function generate_and_execute_tests {
  local commit_msg
  local ephemeral_test_script
  local parser_script
  local test_name

  ephemeral_test_script=$(mktemp)
  parser_script=$(readlink -f '../commit_msg_parser.py')

  echo '#! /usr/bin/env bats' > "${ephemeral_test_script}"

  # BATS syntax is something like this:
  #   @test "addition using bc" {
  #     result="$(echo 2+2 | bc)"
  #     [ "$result" -eq 4 ]
  #   }
  for test_file in success/*;
  do
    commit_msg=$(cat "${test_file}")
    test_name=$(basename "${test_file}")

    {
      echo "@test \"${test_name}\" {"
      echo "  run '${parser_script}' '${commit_msg}'"
      # shellcheck disable=SC2016
      echo '  test "${status}" -eq 0 '
      echo '}'
      echo
    } >> "${ephemeral_test_script}"
  done

  for test_file in fail/*;
  do
    commit_msg=$(cat "${test_file}")
    test_name=$(basename "${test_file}")

    {
      echo "@test \"${test_name}\" {"
      echo "  run '${parser_script}' '${commit_msg}'"
      # shellcheck disable=SC2016
      echo '  test "${status}" -ne 0 '
      echo '}'
      echo
    } >> "${ephemeral_test_script}"
  done

  bats "${ephemeral_test_script}"
}

generate_and_execute_tests
