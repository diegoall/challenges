let
  pkgs = import ../pkgs/stable.nix;
  builders.nodePackage = import ../builders/nodejs-module pkgs;
  builders.pythonPackage = import ../builders/python-package pkgs;
in
  pkgs.stdenv.mkDerivation (
        (import ../src/basic.nix)
    //  (import ../src/external.nix pkgs)
    //  (rec {
          name = "builder";

          buildInputs = [
            pkgs.git
            pkgs.curl
            pkgs.cacert
            pkgs.nodejs
            pkgs.bats
            pkgs.python37
          ];

          nodeJsModuleCommitlint = builders.nodePackage "@commitlint/cli@9.0.1";
          nodeJsModuleCommitlintConfigConventional =
            builders.nodePackage "@commitlint/config-conventional@9.0.1";

          pyPkgPyparsing = builders.pythonPackage "pyparsing==2.2.2";
        })
  )
