let
  pkgs = import ../pkgs/stable.nix;
  builders.rubyGem = import ../builders/ruby-gem pkgs;
  builders.pythonPackage = import ../builders/python-package pkgs;
in
  pkgs.stdenv.mkDerivation (
        (import ../src/basic.nix)
    //  (import ../src/external.nix pkgs)
    //  (rec {
          name = "builder";

          buildInputs = [
            pkgs.git
            pkgs.cacert
            pkgs.file
            pkgs.ruby
            pkgs.yq
            pkgs.pre-commit
            pkgs.python37Packages.yamllint
          ];

          rubyGemRubocop = builders.rubyGem "rubocop:0.85.1";
          rubyGemSlimlint = builders.rubyGem "slim_lint:0.20.1";

          pyPkgProspector = builders.pythonPackage "prospector[with_everything]==1.2.0";
          pyPkgMypy = builders.pythonPackage "mypy==0.770";
          pyPkgLizard = builders.pythonPackage "lizard==1.17.3";
          pyPkgRuamelyaml = builders.pythonPackage "ruamel.yaml==0.16.10";
        })
  )
