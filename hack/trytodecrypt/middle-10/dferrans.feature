## Version 2.0
## language: en

Feature: middle-10-decrypt-trytodecrypt.com
  Code:
    middle-10
  Site:
    trytodecrypt.com
  Category:
    decrypt
  User:
    dferrans
  Goal:
    decrypt secret string

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | macos           | 10.14.5     |
    | chrome          | 74.0.3729   |

  Machine information:
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=10#headline
    """
    And the string to decrypt:
    """
    261129152E152B
    """

  Scenario: Success:build-dictionary-to-decrypt-secret-word
    When I am access the challenge through a webbrowser
    Then I am able to try multiple characters to encrypt
    Then I can get each all values to build dictionary
    Then I try to decrypt the string by creating a script
    Then I get the string that solves the problem.
