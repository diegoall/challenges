## Version 2.0
## language: en

Feature: 005 - Cryptography - trythisone
  Site:
    http://www.trythis0ne.com
  Category:
    Web-challenges
  User:
    michael682
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Mozilla Firefox   |   72.0.2   |
  Machine information:
    Given the challenge URL
    """
    http://www.trythis0ne.com/levels/cryptography/ReverseMe/Enc.php
    """
    When I access the URL
    Then I can see a message
    """
    What is the number that give you the code: 133701337 ?
    """

  Scenario: Fail: Decrypt the number using binary reverse order
    Given The challenge URL
    When I use BINARY REVERSE ORDER script in the number (michael682.py)
    Then I get a new number
    """
    81510655
    """
    And I put the number
    But I do not get a correct answer

  Scenario: Fail: Decrypt the number using byte reverse order
    Given The challenge URL
    When I use BYTE REVERSE ORDER script in the number (michael682.py)
    Then I get a new number
    """
    733107331
    """
    And I put the number
    But I do not get a correct answer

  Scenario: Success: Use Brute Force to identify the algorithm
    Given The challenge URL
    When I try to get the code using brute force
    Then I get arbitrary numbers
    When I sort the inputs and the outputs
    Then I realize a symmetry in the codes
    """
         Input      Output
    | 999999999 | 907254361 |
    | 000000000 | 018365472 |
    | 111111111 | 129476583 |
    | 222222222 | 230587694 |
    | 333333333 | 341698705 |
    | 444444444 | 452709816 |
    | 555555555 | 563810927 |
    """
    When I create the combination using the BRUTE FORCE script (michael682.py)
    Then I get the flag
    And I put the password on the site
    And I solve the challenge
