## Version 2.0
## language: en

Feature: cracking-forensic-active-directory
  Site:
    root-me.org
  User:
    ununicornio
  Goal:
    Get the active directory admin password

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | pcredz                | 1.0.0     |

  Scenario: Success:extraction
    Given I have a pcap file with the network traffic during AD boot sequence
    And I run it through pcredz to get any LDAP data in the pcap
    And get credentials for Admin and Helpdesk
    """
    ➜  PCredz git:(master) ✗ python Pcredz -f ~/Downloads/prueba/ch12.pcap
    ...
    Using TCPDump format

    439 protocol: tcp 10.0.2.15:445 > 10.0.2.30:49181
    Found a username in an SMB read operation:
    user:
    ['clsid="{DF5F1855-51E5-4d24-8B1A-D9BDE98BA1D1}" name="Helpdesk" image="2"
    changed="2015-05-06 05:50:08" uid="{43F9FF29-C120-48B6-8333-9402C927BE09}">
    <Properties action="U" newName="" fullName="" description=""
    cpassword="PsmtscOuXqUMW6KQzJR8RWxCuVNmBvRaDElCKH+FU+w" changeLogon="1"
    noChange="0" neverExpires="0" acctDisabled="0" userName="Helpdesk"/></User>
    <User clsid="{DF5F1855-51E5-4d24-8B1A-D9BDE98BA1D1}" name="Administrateur"
    image="2" changed="2015-05-05 14:19:53"
    uid="{5E34317F-8726-4F7C-BF8B-91B2E52FB3F7}"
    userContext="0" removePolicy="0"><Properties action="U"
    newName="" fullName="Admin Local" description=""
    cpassword="LjFWQMzS3GWDeav7+0Q0oSoOM43VwD30YZDVaItj8e0"
    changeLogon="0" noChange="0" neverExpires="1" acctDisabled="0"
    subAuthority="" userName="Administrateur"/></User>']

    /root/Downloads/prueba/ch12.pcap parsed in: 0.279 seconds
    (File size 0.164 Mo).
    """
    Given I know AD uses AES-256 encryption
    And microsoft published the private key in their website
    Then I use an AES decryption script to decrypt the admin password
    Then I pass the challenge
