## Version 2.0
## language: en

Feature: Web server-rootme
  Site:
    rootme
  Category:
    Web server
  User:
    dianaosorio97
  Goal:
    Get access to index.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
    | Mozilla Firefox | 65.0.1-2    |
    | Burp Suite      | 1.7.36      |
  Machine information:
    Given I am accessing the following url
    """
    https://www.root-me.org/es/Challenges/Web-Servidor/Improper-redirect
    """
    And I click in the button start
    And I am redirect to the following link
    """
    http://challenge01.root-me.org/web-serveur/ch32/login.php?redirect
    """
    And I see a login form

  Scenario: Success: Using burp suite to intercept request
    Given the server's response contains all the content of the html page
    Then I start the challenge
    And I intercept the request using burp suite
    Then I can see the full html in the answer
    And I see the following message
    """
    Yeah ! The redirection is OK, but without exit() after the header
    ('Location: ...'), PHP just continue the execution and send
    the page content !...
    The flag is : ExecutionAfterRedirectIsBad
    """
    Then I put the password in the answer
    """
    ExecutionAfterRedirectIsBad
    """
    Then I solve the challenge
