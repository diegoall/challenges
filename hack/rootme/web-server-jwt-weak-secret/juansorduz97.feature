## Version 2.0
## language: en

Feature: Web server-rootme
  Site:
    rootme
  Category:
    Web server
  User:
    juansorduz97
  Goal:
    Connect to admin section

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.3     |
    | Mozilla         | 72.0.1      |
    | Burp Suite      | 2.1.07      |
  Machine information:
    Given a link in the challenge page
    When I access it
    Then I can see the following message
    """
    Statement

    This API with its /hello endpoint (accessible with GET) seems rather
    welcoming at first glance but is actually trying to play a trick on you.

    Manage to recover its most valuable secrets!
    """

  Scenario:  Fail: use none algorithm
    Given The challenge page show a message
    """
    Let's play a small game, I bet you cannot access to my super secret admin
    section. Make a GET request to /token and use the token you'll get to try
    to access /admin with a POST request.
    """
    When I insert go to the following URL
    """
    http://challenge01.root-me.org/web-serveur/ch59/token
    """
    Then The challenge page show me a JWT token
    When I decode the token with "https://jwt.io/"
    Then The algorithm used is "HS512"
    When I change the algorithm from "HS512" to "none"
    And I obtain a new JWT token
    Then I use Burp Suite to make a POST request with the JWT token as body
    And I send the request to
    """
    http://challenge01.root-me.org/web-serveur/ch59/admin
    """
    But The server does not allow "none" algorithm
    And I cannot get the flag

  Scenario:  Success: obtain signature key
    Given The challenge page does not accept "none" algorithm
    When I search on web how to obtain "HS512" key
    Then I find that it is necessary to use a brute-force attack
    And I use the following tool
    """
    https://github.com/Sjord/jwtcrack/blob/master/crackjwt.py"
    """
    When I put the JWT token given by the challenge page to the tool
    And I use the following dictionary for the brute-force attack
    """
    https://github.com/duyetdev/bruteforce-database
    """
    Then I obtain the key "lol" used by the challenge page
    When I use the key to generate a JWT token with "https://jwt.io/"
    And I send a POST request changing user from "guest" to "admin"
    Then I obtain the flag [evidence](flag.png)
