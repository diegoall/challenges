## Version 2.0
## language: en

Feature: rootme-app-script-pyjail-2
  Site:
    https://www.root-me.org
  Category:
    app-script
  User:
    andrescl94
  Goal:
    Escape from the python console and get the password

  Background:
  Hacker's software:
  | <Software name>                | <Version>                 |
  | Ubuntu (Bionic)                | 18.04.1                   |

  Machine information:
    Given the challenge SSH credentials
    """
    host: challenge02.root-me.org
    port: 2222
    user:app-script-ch9
    pass: app-script-ch9
    """
    When I connect to the remote server
    Then I enter a Python terminal
    And I see the following message
    """
    Use getout() function if you want to escape from here and get the flag !
    """

  Scenario: Fail: Execute-Function
    Given the challenge instruction
    Then I try to execute the "getout" function
    And I get the following message
    """
    Error: getout()
    """
    When I try using a random string as argument
    Then I get the message
    """
    You're in jail dude ... Did you expect to have the key ?
    """
    When I try using a random number as argument
    Then I get the following message
    """
    Hum ... no.
    """
    And I don't get the flag

  Scenario: Fail: Inspect-Functions
    When I investigate how to get the source code of a function
    Then I come across the "inspect" module and its functions
    And I try to import it but I get an error
    """
    You're in jail dude ... Did you expect to have the key ?
    """
    And I don't get the flag

  Scenario: Success: Builtin-Functions
    Given that I can't import the "inspect" module
    Then I decide to use the builtin "dir" to list the scope names
    And I get the following results
    """
    ['__builtins__', 'command', 'getout']
    """
    When I use the "dir" function again on the "getout" function
    Then I see the following insteresting attributes
    """
    [..., 'func_closure', 'func_code', 'func_defaults', 'func_dict',
    'func_doc', 'func_globals', 'func_name']
    """
    When I try to print one of these attributes
    And I get an error message
    Then I decide to use the builtin "getattr" to access them
    When I access "func_doc" using the following command
    """
    getattr(getout, dir(getout)[-3])
    """
    And I see the following message
    """
    check if arg is equal to the random password
    """
    Then I realize I need to obtain that random password
    When I access the "func_globals" attribute
    Then I see the value of the password in a dictionary
    """
    {..., 'passwd': '**********', ...}
    """
    And since I can't use strings as arguments in the "getout" function
    Then I realize I need an expression to extract the value from the dict
    And I decide to convert the dictionary keys to a list
    And use the index to extract the word "passwd"
    And use it to extract the password value from the dictionary
    Then I use that whole expression as the argument of the "getout" function
    And I solve the challenge
