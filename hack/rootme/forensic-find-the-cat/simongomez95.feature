## Version 2.0
## language: en

Feature: find-the-cat-rootme
  Site:
    root-me.org
  User:
    ununicornio
  Goal:
    Find the kidnapped cat

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | The Sleuth Kit        | 4.6.4     |
    | Autopsy               | 4.9.1     |

  Scenario: Success:data-carving
    Given I start the level
    And it prompts
    """
    President’s cat was kidnapped by separatists. A suspect carrying a USB key
    has been arrested. Berthier, once again, up to you to analyze this key and
    find out in which city the cat is retained!

    The md5sum of the archive is edf2f1aaef605c308561888079e7f7f7. Input the
    city name in lowercase.
    """
    And there's a compressed file to download
    Then I extract it
    And get a file called "ch9" with no extension
    And because it's supposed to be a USB key image, I open it with Autopsy
    Then I see a FAT32 partition with some saved websites, pics and documents
    Then I extract the images to check if there's something interesting there
    And find two interesting photos of places
    Then I use exiftool to see if these photos have any location metadata
    But they don't
    Then I keep looking
    And find a file that looks like a json with location data
    """
    Geo = {"city":"Longlaville",
    "country":"FR",
    "lat":"49.534500",
    "lon":"5.802000",
    "IP":"78.236.229.52",
    "netmask":"24"}
    """
    Then I try to submit "longlaville" as answer to the challenge
    But that's not it
    Then I feed the IP in the file to a geolocation lookup
    And get several potential cities
    Then I try all of them as answers
    But no luck. Keep looking.
    Then I find Autopsy has detected some files with mismatching extensions
    And they all appear to be images. What were the kidnappers trying to hide?
    Then I extract these images, and check them out
    But there doesn't appear to be anything interesting on them
    And they don't have interesting metadata either
    Then I find an ODT document with what looks like a ransom note. I extract it
    Then I open it with libreoffice, and discover it includes a pic of the cat
    Then I do some research and find ODT files are just really zip archives
    Then I extract the document
    And get a lot of files, the cat pic among them
    Then I open it with exiftool, and there's location data in it!
    """
    ...
    GPS Altitude                    : 16.7 m Above Sea Level
    GPS Latitude                    : 47 deg 36' 16.15" N
    GPS Longitude                   : 7 deg 24' 52.48" E
    GPS Position                    : 47 deg 36' 16.15" N, 7 deg 24' 52.48" E
    ...
    """
    Then I open the pic with an online photo location extractor tool
    And I get the location info
    """
    Coordinates: 47.604485, 7.414579
    Location: 1 Rue Principale, 68510 Helfrantzkirch, France
    """
    Then I submit "helfrantzkirch" as answer
    And I pass the challenge.
