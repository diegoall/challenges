# lenguage: en

Feature: Solve Cisco-password
  From root-me site
  Category Network
  With my username synapkg

  Background:
    Given I am running Ubuntu Xenial Xerus 16.04 (amd64)
    And I am using Mozilla Firefox Quantum 63.0 (64-bit)

  Scenario: First attempt fail
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Network/CISCO-password
    """
    Then I opened that URL with Mozilla Firefox
    And I read the problem statement
    """
    Find the "Enable" password.
    """
    And I started the challenge opening the follow link
    """
    http://challenge01.root-me.org/reseau/ch15/ch15.txt
    """
    Then I tried to decrypt the "enable" password which is type 5
    """
    $1$p8Y6$MCdRLBzuGlfOs9S.hXOp0.
    """"
    Then I did not found a way.

  Scenario: Second attempt fail
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Network/CISCO-password
    """
    Then I decrypted the "hub" username password which is type 7
    """
    025017705B3907344E
    """
    And its result was
    """
    6sK0_hub
    """
    Then I put it as the answer but the answer was wrong

  Scenario: Third attempt fail
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Network/CISCO-password
    """
    Then I decrypted the "admin" username password which is type 7
    """
    10181A325528130F010D24
    """
    And its result was
    """
    6sK0_admin
    """
    Then I put it as the answer but the answer was wrong

  Scenario: Successful attempt
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Network/CISCO-password
    """
    Then I decrypted the "console" password which is type 7
    """
    144101205C3B29242A3B3C3927
    """
    And its result was
    """
    6sK0_console
    """
    Then I discovered a pattern
    And I thinked that the answer can be
    """
    6sK0_enable
    """
    Then I put it as the answer and I was right.
