Feature: Solve Twitter authentication
  From the Root Me site
  Category Network
  With my username vanem_cb

  Background:
    Given I am running Windows 10 (64 bits)
    And I am using Google Chrome Version 70.0.3538.77 (Build oficial) (64 bits)

  Scenario: Normal use case
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Network/Twitter-authentication-101
    """
    Then I open the URL with Google Chrome
    And I see the challenge statement
    """
    A twitter authentication session has been captured,
    you have to retrieve the password.
    """
    And I see the input field to submit the answer
    And I see a button
    """
    Start the challenge
    """
    When I click the button
    Then a file is downloaded
    """
    ch3.pcap
    """
    Then I try to open the file
    But I don't find any program to open the file on my PC
    Then I don't write nothing in the input field to submit the answer
    And I don't solve the challenge
    But I conclude that I should look for a tool to open the file

  Scenario: Successful solution
    Given the file by clicking the button
    Then I look for on internet a tool to open the file with extension ".pcap"
    And I find a network protocol analyzer
    And I download the analyser
    """
    Wireshark downloaded of https://www.wireshark.org/#download
    """
    And I open the file with this tool
    But I see many information that I don't understand
    Then I analyse the information meticulously
    And I check and unfold the last tab
    """
    Hypertext Transfer Protocol
    """
    And I see other tab
    """
    Authorization: Basic dXNlcnRlc3Q6cGFzc3dvcmQ=\r\n
    """
    When I unfold this tab
    Then I see the information that I need
    """
    Credentials: usertest:password
    """
    Then I write "password" in the input field to submit the answer
    And I solve the challenge
