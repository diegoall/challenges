## Version 1.0
## language: en

Feature: Root Me - Programming - Go back to college
  Site:
    Root Me
  Category:
    Programming
  Challenge:
    Go back to college
  User:
    bridamo98

  Background:
  Hacker's software:
    | <Software name> | <Version>               |
    | Ubuntu          | 18.04.4 LTS (amd64)     |
    | Google Chrome   | 84.0.4147.105 (64-bit)  |
    | GNU bash        | version 4.4.20 (x86_64) |

  Machine information:
    Given I'm accesing the challenge page
    And the tittle is
    """
    Go back to college
    """
    And the statement is
    """
    To start the challenge using IRC, you must send a private message
    to bot Candy : !ep1. The bot replies with a message in private
    with a string like this:<number1>/<number2>
    """

  Scenario: Success: Write a software that shift it
    When I look at the statement
    Then I identified the challenge connection information
    """
    Host         irc.root-me.org
    Protocol     IRC
    Port         6667
    IRC channel  #root-me_challenge
    Bot          candy
    """
    When I think a little
    Then I write a code called "bridamo98.py"
    Given a modify a little the output to make it more understandable
    # filter the output that contains the "password"
    When I run the code
    Then I can see the password given by the bot [evidence](success.png)
    And I solve the challenge
