## Version 2.0
## language: en

Feature: Web Client - Flash Authentication - Root Me
  Site:
    root-me.org
  User:
    william73
  Goal:
    Find the password in a Flash Movie

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | Python            |    3.7.4   |
  Machine Information:
    Given this is a web-based challenge,
    And the vulnerability lies on the client side
    When I get to the challenge page
    Then I will inspect its source and resources

  Scenario: Fail:load-challenge
    Given I start the challenge
    And it prompts
    """
    Find the validation code.
    """
    And a web page loads, with an embedded SWF.

  Scenario: Fail: inspect-source-code
    Given I get to the web page
    When I see its souce
    Then I find a function which compares a value from the SWF
    And an inline value. So I know which value must be injected there.
    Then I put this value on the root-me challenge.
    But it fails.

  Scenario: Fail: is-this-MD5
    When I find the inline value has a 32-byte length
    Then I think it might be a MD5 digest,
    Then I override the function on the page (l1 from now on)
    And redirect it to console.log
    And try a known pattern (111111) to see if the page's hash
    And a MD5 hash of that string are equal.
    But they are not.

  Scenario: Fail: how-to-open-swf-files
    Given I know ActionScript is an interpreted language
    And all the instructions must lie inside the SWF file,
    When the file gets downloaded
    Then I download also a SWF decompiler executable,
    And using it, I'm able to see some instructions inside the SWF
    But they do load a resource that doesn't appear on those the program lists.

  Scenario: Fail: how-to-open-swf-files-reloaded
    Given I can know the resource name (RootMeAsset),
    And the resource ends with "asset",
    When I open the file with another SWF decompiler (JPEXS FFD),
    Then the resource appears but as a byte string.
    And as I can see the instructions inside,
    Then I see this byte string is being loaded,
    But before it loads, its content is XOR-ed with the key "rootmeifyoucan"
    Then I devise a Python Script (find.py) to simulate conversion.
    But it generates another SWF file.

  Scenario: Fail: how-to-open-swf-files-revolution
    But now I have a new SWF file I could inspect again.
    And I do so.
    And the code for the buttons and their click events are there.
    But the password is not.
    But the algorithm to create the validation code's MD5 hash is there!
    And it is, basically, the concatenation of some constants.
    When we replicate this algorithm effectively on Python (replication.py)
    Then we will have an educated guess of how the Flash movie works.
    And we did replicate it.
    Then we have now a brute-force attack as a possible approach.

  Scenario: Fail: dont-be-that-greedy-its-a-sin
    Given the password could only be 12 or 13 bytes long,
    And those bytes are hex digits (0-F).
    When I write the number of possible outcomes here (16^13)
    Then It will take at least 15 digits.
    And it can only be coded to run in Deep Thought computer
    And it will probably take a billion years
    And the answer could be 42.

  Scenario: Fail: still-a-sin
    But as we inspect the code, we realize not all the digits are used.
    And there is a mapping of 1, 2, 3 to "AB", "14", "59", respectively.
    But 4 is mapped to the removal of the last digit on the string.
    When we realize the possible outcomes (6^13) are still too high
    Then we understand we need to think a lot more.

  Scenario: Fail: nearly-there
    When we examine the problem thoroughly,
    And realize we can only have two successive beginnings of pairs,
    And the beginning of a pair and its ending,
    And the ending of a pair and the beginning of another one,
    Then we are ready to create the possibility tree.
    Given it yields only little less than 2.8 M possibilities,
    Then a brute force attack is possible.
    And we did it (bruteForce.py).
    And we found the hex string which could represent the password.
    And it is AA5915591414. We are almost done.

Scenario: Success: there
    Given we know the restrictions any
    Given pair of digits must have,
    When we get two successive beginnings of pairs,
    Then we know these represent the digit mapping to a pair,
    And that pair must have this beginning,
    And a 4.
    When we get a "perfect" pair,
    Then we just replace it for the digit mapping to that pair.
    And following this logic, we get the password "141432434322".
    And Root-Me says we are right.
