## Version 2.0
## language: en

Feature: p-d-validation -web -ctf365
  Site:
    ctf365.com
  Category:
    Web
  User:
    alejandrohg7
  Goal:
    Get the hidden flag in the website

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Firefox         | 69.0        |
    | Burp Suite      | 2.1.04      |
  Machine information:
    Given I am accessing the web-page using the browser
    And I access to the challenge page

  Scenario: Fail: number form injection using python syntax
    Given there is a number form in the challenge page
    When I try a sample injection "{{1-2}}" to get a negative Outcome
    Then there is no result
    Then I can not get the flag

  Scenario: Success: modifying the request with Burp Suite
    When I catch the request with Burp Suit
    Then I can modify the value of "userdata" field
    And I can enter a negative value
    And I get the flag [evidence](img.png)
