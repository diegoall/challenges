## Version 2.0
## language: en

Feature: sesame-street -web -ctftime
  Site:
    ctflearn.com
  Category:
    Web
  User:
    alejandrohg7
  Goal:
    Get the hidden flag in flag.php through a cookie

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Firefox         | 69.0        |
  Machine information:
    Given I am accessing the web-page using the browser
    And I access to the challenge page

  Scenario: Fail: add a new cookie
    Given I am in the challenge page
    When I inspect the elements of the page
    Then I can see the cookies stored on my browser
    And I make a new cookie
    """
    session=1569509637; path=/
    """
    And I go to "flag.php"
    And I can not get the flag

  Scenario: Success: modify the cookie
    When I see the cookies stored on my browser
    Then I can see the cookie
    """
    session-time=1569509637; path=/countdown.php
    """
    And I change the path value to "flag.php"
    """
    session-time=1569509637; path=/flag.php
    """
    And I go to "flag.php"
    And I get the flag
