## Version 2.0
## language: en

Feature: otpyrC - crypto - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Crypto
  User:
    fgomezoso
  Goal:
    Find the flag

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 7            |
     | Chrome          | 78.0.3904.70 |
     | Python          | 3.8.0        |
     | Powershell      | 5.1          |
  Machine information:
    Given I am accessing the challenge site via browser
    """
    https://ctflearn.com/challenge/249
    """
    And the challenge has an encrypted message
    """
    d733432373937303734373666343730373937323733343b7644534
    """
    And the challenge's title is otpyrC

  Scenario: Fail:Decode the encrypted hex
    When I check the encrypted message
    Then I can see that there are only numbers and two letters
    And I think this is a hexadecimal number
    When I use an online tool to decode hex strings
    """
    http://www.convertstring.com/es/EncodeDecode/HexDecode
    """
    Then I get a weird string
    """
    ×3C#sssCsfcCsss#s3C·dE4
    """
    And I couldn't get the flag

  Scenario: Success:Reverse the hex string and decode
    When I check the challenge's title
    Then I find out that otpyrC means Crypto backwards
    And I think that the encrypted message must be reversed
    When I want to reverse the message
    Then I type a python command with powershell
    """
    >>> "d733432373937303734373666343730373937323733343b7644534" [::-1]
    '4354467b343337323739373037343666373437303739373234337d'
    """
    And I get the reversed hex string
    When I want to decode the new hex string
    Then I use python's binascii library
    """
    >>> binascii.unhexlify("4354467b3433373237393730373436663734373037
    39373234337d")
    b'CTF{43727970746f7470797243}'
    """
    When I check the decoded message
    Then I find out that the flag's format is "CTF{}"
    And I can see a hex number inside the flag
    When I try to decode again the hex string
    """
    >>> binascii.unhexlify("43727970746f7470797243")
    """
    Then I get a human readable message "<FLAG>"
    When I put the new message inside the flag's format
    Then I get the full flag
    """
    CTF{<FLAG>}
    """
    And I solve the challenge
