## Version 2.0
## language: en

Feature: symbolic-decimals - crypto - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Crypto
  User:
    fgomezoso
  Goal:
    Find the flag

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 7            |
     | Chrome          | 78.0.3904.70 |
  Machine information:
    Given I am accessing the challenge site via browser
    """
    https://ctflearn.com/challenge/231
    """
    And the challenge has instructions and an encrypted message
    """
    Did you know that you can hide messages with symbols? For example,
    !@#$%^&*( is 123456789!
    Now Try: ^&,*$,&),!@#,*#,!!^,(&,!!$,(%,$^,(%,*&,(&,!!$,!!%,(%,$^,(%,&)
    ,!!!,!!$,(%,$^,(%,&^,!)%,!)@,!)!,!@%
    However, this isn't as easy as you might think.
    """

  Scenario: Success:Translate message to decimals and decode it
    When I check the challenge's hint
    Then I see that every symbol is associated to a digit
    When I translate the encrypted message to digits
    Then I get a long string of numbers and characters
    """
    67,84,79,123,83,116,97,114,95,46,95,87,97,114,115,95,46,95,7),111,114,
    95,46,95,76,1)5,1)2,1)1,125
    """
    And I can see that the character ")" shows up in some parts
    When I want to get a clean string
    Then I delete the commas (,) from the message
    """
    67 84 79 123 83 116 97 114 95 46 95 87 97 114 115 95 46 95 7) 111 114
    95 46 95 76 1)5 1)2 1)1 125
    """
    When I check the list of numbers from the string
    Then I think that every one of them represents a unicode character
    When I want to decode the previous string to unicode characters
    Then I use an online decoder
    """
    https://cryptii.com/pipes/text-decimal
    """
    When I enter the encrypted number in the unicode decoder
    Then I get a message with some weird characters in between
    """
    CTO{Star_._Wars_._or_._L}
    """
    When I check the character "" from the decoded message
    Then I realize it belongs to character "7)" from string of numbers
    And I think that the character ")" must be associated to a digit
    When I try to figure out the meaning of the string "or"
    Then I think that character "F" is a reasonable option
    When I look for the unicode decimal for capital letter "F"
    Then I find out that its unicode decimal is number "70"
    And I know the value for character ")"
    """
    ')' == 0
    """
    When I replace characters ")" with their new value in the string
    Then I get a whole list of numbers
    """
    67 84 79 123 83 116 97 114 95 46 95 87 97 114 115 95 46 95 70 111 114
    95 46 95 76 105 102 101 125
    """
    When I enter the new string of numbers in the unicode decoder
    Then I get the real flag
    """
    CTO{<FLAG>}
    """
    And I solve the challenge
