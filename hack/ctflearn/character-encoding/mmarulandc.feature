## Version 2.0
## language: en

Feature: Character Encoding - Cryptography - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Cryptography
  User:
    mmarulandc
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A text in the challenge page
    And the text has an hexadecimal code to analyze

  Scenario: Success: Analysing the hexadecimal code
    When I decode the hexadecimal code with the help of this site:
    """
    https://www.rapidtables.com/convert/number/hex-to-ascii.html
    """
    Then I get the flag [evidence](evidence.png)
    When I put the value in flag format
    """
    CTFLearn{<FLAG>}
    """
    Then the challenge is solved
