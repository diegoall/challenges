## Version 2.0
## language: en

Feature: time-to-eat - reverse engineering - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Reverse Engineering
  User:
    fgomezoso
  Goal:
    Find the flag

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 7            |
     | Chrome          | 78.0.3904.70 |
     | Python          | 3.8.0        |
     | Powershell      | 5.1          |
  Machine information:
    Given I am accessing the challenge site via browser
    """
    https://ctflearn.com/challenge/743
    """
    And a message shows up in front of me
    """
    My friend sent me some Python code, but something tells me
    he was hungry when he wrote it. Do you think you can put your
    reverse engineering skills to use and get it to output the flag?
    """
    And the website provides me a file called "eat.py"

  Scenario: Success:Change variable names and understand the output
    When I check the python code "eat.py"
    Then I find out that the whole file is hard to understand
    And there are convoluted names for functions and variables
    And every variable name has the word "Eat" in many forms
    When I check the source code
    Then I can see there is a main function "Eat(eat)"
    And I can see a print function "EaT"
    When I try to figure out what does the code
    Then I change some names one after another
    """
    +--------------------+--------------------+
    |    "Eat" Name      |      New Name      |
    +--------------------+--------------------+
    |eat                 |myinput             |
    |Eat(eat)            |main(myinput)       |
    |EaT                 |Print               |
    |EAT                 |INTEGER             |
    |eAT                 |LENGTH              |
    |Ate                 |STRING              |
    |EATEATEATEATEATEAT  |ISDIGIT             |
    |EATEATEAT           |NumberThree         |
    |EATEATEATEAT        |NumberFour          |
    |EATEATEATEATEAT     |NumberTwo           |
    |EAt                 |important           |
    +--------------------+--------------------+
    """
    When I try to understand the new code
    Then I realize that the program asks for an input
    And the input must have a length of 9 characters
    And the input must have a right format
    And I can see an example of a good format
    """
    hint: 123abc456 is an example of good format
    """
    When I check the main function
    Then I realize how a good format is determined
    And the first 3 and last 2 characters of the input must be digits
    When I test the program with the good format example "123abc456"
    Then I get an error message
    """
    369654cba321 Eat9654
    thats not the answer. you formatted it fine tho, here's what you got
    >> E36a96t549cb6a3521
    """
    And I see that the result mixes the word "Eat" with "369654cba321"
    When I inspect the code after the format condition
    Then I can see that there is a variable "eateat"
    And It is calculated with the function "important"
    """
    eateat = important(eaT(myinput), Ate(aTE(aten(myinput))))
    """
    When I check what is the value returned by "eaT(myinput)"
    Then I can see that it multiplies the first 3 strings by 3
    """
    string: 123abc456
    123*9 = 369
    """
    And it concatenates the result with the reversed string
    """
    369654cba123
    """
    When I check the previous error message
    Then I see that this result was previously printed
    And I conclude that the both arguments are shown in the message
    """
    eaT(myinput):               369654cba321
    Ate(aTE(aten(myinput))):    Eat9654
    """
    When I keep inspecting the source code
    Then I can see a "flag" variable
    And I can get it when "eateat" matches an specific string
    """
    if eateat == "E10a23t9090t9ae0140":
    """
    When I check the previous message error
    Then I compare the result with the string I need to get
    """
    E36a96t549cb6a3521
    E10a23t9090t9ae0140
    """
    And I see that one of them is shorter
    And I need to see If can get a better result
    When I try another input
    And I use the word "eat" at the center of the string
    """
    345eat678
    """
    Then I get something more similar to what I need
    """
    E10a35t8796t8ae7546
    E10a23t9090t9ae0140
    """
    When I analize my result based on what "eaT(myinput)" returns
    Then I realize that the first 4 numbers are related to the input
    And I can get them by multiplying the first 3 chars of "myinput" by 3
    When I divide 1023 by 3 then I get the first 3 digits of "myinput"
    """
    1023/3 = 341
    """
    When I want to get the last 3 strings of the input
    Then I check the error message of the last result
    """
    1035876tae543 Eat9876
    thats not the answer. you formatted it fine tho, here's what you got
    >> E10a35t8796t8ae7546
    """
    And I see that "876" is part of the reversed string "876tae543"
    And I see that "876" is present in the result "E10a35t8796t8ae7546"
    When I check the original input
    """
    345eat678
    """
    Then I find out that the input string "678" can get "876"
    When I try to apply the same principle to the "eateat" string
    Then I understand that "009" probably works with the solution
    When I enter the input I calculated in the program
    """
    341eat009
    """
    Then I get a new message
    """
    1023900tae143 Eat9900
    absolutely EATEN!!! CTFlearn{ <FLAG> }
    """
    And I solve the challenge
