# language: en

Feature: Solve challenge 51
  From site W3Challs
  From Cryptography Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am using python Python 2.7.14+
  And I am using VIM - Vi IMproved 8.0
  And I am running Burp Suite Community edition v1.7.30
  And I am using SageMath 8.2
  Given a web site stating there is a communication between two parties
  And that they are using a custom algorithm of asymmetric encryption
  """
  URL: https://w3challs.com/challs/Crypto/asy/
  Message: Asymmetric Encryption
  Details: Alice and Bob, uses a custom algorithm of
  asymmetric encryption, there are two public values (F, H)
  and a ciphertext given. The implementation of the
  algorithm is given too.
  Objective: Get the plaintext
  Evidence: - PHP site
        - PHP Implementation
            - f, h, ciphertext values
            - Less than 3 seconds to answer
  """

Scenario: Asymmetric Encryption Implementation
Asymmetric encryption is ofently used on public
key cryptography, a key used by one party to
perform encryption is not the same as the key
used by another in decryption.
Summarizing, in this challenge, there are two
public keys (F, H) which are used to encrypt
and decrypt, but not simultaneously.
  Given the algorithm implementation
  """
  # Custom asymmetric encryption algorithm
  https://w3challs.com/challs/Crypto/asy/implementation.php
  """
  Then I look at the code
  And I see the following initial computations:
  """
  Source: /implementation.php
  $ONE = gmp_init(1,10);
  """
  And *gmp_init* is used to initialize *$ONE* with a value
  But most likely to be 1:
  """
  # gmp_init example
  <?php
  $ONE = gmp_init(1,10);
  echo $ONE; // = 1
  """
  Then I see four variables that takes random values
  """
  Source: /implementation.php
  $a = gmp_random(3);
  $b = gmp_random(3);
  $c = gmp_random(3);
  $d = gmp_random(3);
  """
  And *gmp_random(<limit>)* returns random 0 to 1 * bits per limb
  """
  Source: /implementation.php
  # Simple example
  $a = gmp_random(3);
  echo $a; // = 167116360251534188830959043907261401833001127892705930819

  The param of the function, is a multiplier of each limb bits
  where each limb, stands for 16 or 32 bits, not guaranteed
  # http://php.net/manual/en/function.gmp-random.php
  """
  Then I see the following operations:
  """
  Source: /implementation.php
  $e = gmp_sub(gmp_mul($a,$b), $ONE); // a*b - 1
  $f = gmp_add(gmp_mul($c,$e), $a); // c*e + a;
  $g = gmp_add(gmp_mul($d,$e), $b); // d*e + b;
  $h = gmp_add(gmp_add(gmp_add(gmp_mul(
        gmp_mul($c,$d), $e),
        gmp_mul($a,$d)),
        gmp_mul($c,$b)), $ONE); // c*d*e + a*d + c*b + 1;

  gmp_sub, gmp_add, gmp_mul are just
  substraction, addition and multiplication operations
  """
  And I see an array, holder of the public keys:
  """
  Source: /implementation.php
  $public_key = array($h,$f);
  """
  And the private key:
  """
  Source: /implementation.php
  $private_key = $g;
  """
  Then there is the encryption function:
  """
  Source: /implementation.ph
  function encrypt($m, $public_key)
  {
    $c = gmp_mod(gmp_mul($m, $public_key[1]),$public_key[0]);
    return $c;
  }
  Which in plain english is:
  ciphertext = (plaintext * f) % h
  """
  And the decryption function:
  """
  Source: /implementation.ph
  function decrypt($c, $public_key, $private_key)
  {
    $m = gmp_mod(gmp_mul($c, $private_key),$public_key[0]);
    return $m;
  }
  Which in plain english is:
  plaintext = (ciphertext * g) % h
  """
  And a little example to see it in action:
  """
  # Initial values
  ONE: 1

  A: 180
  B: 515
  C: 576
  D: 810
  # Computation of E; E = A * B - ONE
  E = 180*515 - 1
  E = 92699

  # Computation of F; F = C * E + A
  F = 576*92699 + 180
  F = 53394804

  # Computation of G; G = D * E + B
  G = 810*92699 + 515
  G = 75086705

  # Computation of H; (C * D * E) + (A * D) + (C * B)
  H = (576*810*92699) + (515*92699) + (576*515)
  H: 43250087881

  # Public keys:
  F: 53394804
  H: 43250087881

  # Plaintext
  plaintext = 1991

  # Encryption (msg * public_key[1]) % public_key[0]
  ciphertext = (1991 * 53394804) % 43250087881
  ciphertext = 19808879002

  # Decryption (cip * private_key) % public_key[0]
  plaintext = (19808879002 * 75086705) % 43250087881
  plaintext = 1991
  """
  Then I check at the values I have at disposition from the challenge
  """
  Public keys (F, H)
  And the ciphertext sent
  """
  And I need to know how to get the plaintext, by using only those three
  But I already know how the algorithm works
  But I don't how to calculate or derive the initial values from those
  Then I search for modular arithmetic
  And I search for the modular inverse
  And I perform the following tests using SageMath
  """
  # SageMath function: inverse_mod
  inverse_mod(h, f) = 53302105

  Nothing useful

  inverse_mod(f, h) = 75086705
  """
  Then I see the last operation is the decryption key (G)!
  And I'm only using the values provided by the challenge!
  And just to be sure why, here is an explanation:
  """
  # Thanks @abbe for the explanation of why i get that value:

  # Equations for E, F, G and H variables
  e = a*b - 1          [1]
  f = c*e + a          [2]
  g = d*e + b          [3]
  h = c*d*e + a*d + c*b + 1  [4]

  # Join [2] and [3] equations
  f*g = (c*e + a)(d*e + b)  [5]

  # Distribute
  f*g = d*c*e^2 + c*e*b + a*d*e + a*b

  # Isolate a*b from [1]
  a*b = e + 1

  # Replace [1] in [5]
  f*g = d*c*e^2 + c*e*b + a*d*e + e + 1

  # Take the common factor e
  f*g = e * (d*c*e + c*b + a*d + 1) + 1

  # Reorder
  f*g = e * (c*d*e + a*d + c*b + 1) + 1

  # Replace [4] in [5]
  f*g = e*h + 1

  f*g = 0+1 = 1 // Because e*h is multiple of h

  So, f*g = 1 mod(h)

  Which it is:
  g = f^-1 mod(h) or in simple words
  f is the modular inverse of g
  """
  And now, get the flag

Scenario: Exploitation
  Given the libraries *beautifulSoup* and *requests* from python
  And I create a session plus make a get request
  """
  Source: w3challs/challenge51/skhorn.py
  100  session = requests.Session()
  ...
  102  response = session.\
  103      get(URL[0], cookies=COOKIES)
  """
  And I use it, to retrieve data from the web page
  But I need to prepare connection parameters:
  """
  Source: w3challs/challenge51/skhorn.py
  14  URL = [
  15     "https://w3challs.com/challs/Crypto/asy",
  16     "https://w3challs.com/challs/Crypto/asy/index.php?p=solution"
  17  ]
  ...
  19  COOKIES = { cookies } => Dict session cookies
  """

  And I perform the first request
  Then I get as a response, the entire html of the challenge
  And this data contains the initial values, as F, H and the ciphertext
  """
  # Get F,H and ciphertext (C) values
  Source: w3challs/challenge51/skhorn.py
  105  response = response.text
  106
  107  html_search = search_in_html(response, 'textarea')
  108  textarea_1 = html_search[0].text.split('\n')
  109  textarea_2 = html_search[1].text.split('\n')
  ...
  113  public_key_h = textarea_1[3].split(' ')[2]
  115  public_key_f = textarea_1[5].split(' ')[2]
  ...
  118  ciphertext = textarea_2[3]
  120  private_key = modinv(int(public_key_f), int(public_key_h))
  """

  Then by was stated before on the modular inverse I take the following code:
  """
  Source: w3challs/challenge51/skhorn.py
  31 def egcd(aval, bval):
  36     if aval == 0:
  37       return (bval, 0, 1)
  38     else:
  39       gval, yval, xval = egcd(bval % aval, aval)
  40       return (gval, xval - (bval // aval) * yval, yval)
  ...
  43 def modinv(aval, mval):
  47  gval, xval, yval = egcd(aval, mval)
  48    if gval != 1:
  49      raise Exception('modular inverse does not exist')
  50    else:
  51      return xval % mval
  """

  And I will use this to compute the modular inverse, using F and H values
  """
  # Private key generation from modular inverse operation
  Source: w3challs/challenge51/skhorn.py
  120  private_key = modinv(int(public_key_f), int(public_key_h))
  """

  And I use this value, to decrypt the given ciphertext
  """
  # Recovering plaintext, decryption
  Source: w3challs/challenge51/skhorn.py
  127  plaintext = (int(ciphertext) * int(private_key)) % int(public_key_h)
  """

  Then I craft another request, this time a POST request
  And using as payload the plaintext found
  """
  Source: w3challs/challenge51/skhorn.py
  141  response = requests.post(URL[1],
  142                         cookies=COOKIES,
  143                         data={"mess": plaintext})

  """
  Then I pass the challenge
  """
  <td><center><font face=tahoma size=2>Asymmetric encryption</font>
  ...
  <font face=tahoma size=2>
  Congraluations, the password to validate this challenge is...
  </td>
  """
