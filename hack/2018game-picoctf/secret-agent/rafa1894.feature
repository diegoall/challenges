## Version 2.0
## language: en

Feature: secret-agent-web-exploitation-2018game-picoctf
  Code:
    Secret Agent
  Site:
    2018game-picoctf
  Category:
    Web Exploitation
  User:
    rferi1894
  Goal:
    Catch the flag hidden in the website.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.2 LTS |
    | Firefox         | 67.0.1      |
    | Burp Suite      | 1.7.36      |

  Machine information:
    Given the challenge URL
    """
    http://2018shell.picoctf.com:3827/
    """
    And the challenge information
    """
    Here's a little website that hasn't fully been finished.
    But I heard google gets all your info anyway.
    """"
    And the field to submit the flag

  Scenario: Success:user-agent
    When I access the site http://2018shell.picoctf.com:40064
    Then I notice you can click a "flag" button to get the flag
    And I click on it but it doesn't give me the flag
    And instead it gives me a message
    """
    You're not google!
    Mozilla/5.0(compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)
    """
    Then based on this I conclude that I need to visit the page as google
    And I research about changing my browser's identity
    And I conclude I can use Burp Suite to modify it in the browser's request
    Then I change user-agent in the request "Mozilla/5.0" for "Googlebot/2.1"
    And this time the flag is revealed
    And I solve the challenge
