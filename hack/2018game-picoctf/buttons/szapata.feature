## Version 2.0
## language: en

Feature: buttons- web-explotation - 2018game-picoctf
  """
  The challenge gives at web-page direction and the
  objective is to find the flag inside the site.
  """
  Site:
    2018game-picoctf
  Category:
    Forensics
  User:
    chalimbu
  Goal:
    get the flag of the site

  Background:
  Hacker's software:
  | <Software name> | <Version> |
  | Ubuntu | 18.04.1 |
  | Google Chrome | 76.0.3809 |
  | burp suite community edition | 2.1.02 |
  | Firefox Quantum(x64) | 68.0.2 |
  Machine information:
    Given I am accessing the web-site through the browser
    And using burp as proxy

  Scenario: Fail : inspect resources web-page
    Given I am accessing the web through google chrome
    Then I could see the HTML/css of the web-page
    Then I look for the flag/validations on client side
    And I did no found nothing
    And I could not capture the flag

  Scenario: Fail : SQL injection
    Given I am accessing the web through Mozilla Firefox
    And using burp as proxy
    Then I can see a cookie is being send
    Then I try to generate a 500 error with
    """
    ' and "
    """
    And did no get unexpected results
    And I did not capture the flag

  Scenario: Success: change method
    """
    call the second link with a POST method
    """
    Given I read the hint of the challenge
    And it said What's different about the two buttons?
    And i recognize that the difference is the method
    And the second link makes a GET petition
    And the first button makes a POST petition
    Then I put the second URL int the POST button
    And I work, showing me the flag of the challenge
