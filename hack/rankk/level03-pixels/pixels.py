# pylint pixels.py
# No config file found, using default configuration
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
Code to count non-white pixels
"""

from PIL import Image

# open image
IM = Image.open("pixels.png")

# get image size
WIDTH, HEIGHT = IM.size

# var to store current array
CURRENT_ARRAY = []

# dictionary containing unique values
UNIQUE_ARRAY = {}

# iterate over each pixel of the image
for x in range(WIDTH):
    for y in range(HEIGHT):
        # get pixels values
        r, g, b = IM.getpixel((x, y))
        # create array
        CURRENT_ARRAY = [r, g, b]
        # create unique key using rgb values
        key = "%d-%d-%d" % (r, g, b)
        # check not white pixels
        if r != 255 or g != 255 or b != 255:
            # check if key exists or not
            if key in UNIQUE_ARRAY:
                UNIQUE_ARRAY[key] += 1
            else:
                UNIQUE_ARRAY[key] = 1

UNIQUE_ARRAY = sorted(UNIQUE_ARRAY.iteritems(),
                      key=lambda x: x[1])

SOL = ''
for element in UNIQUE_ARRAY:
    SOL += str(element[1])+""

print "the solution is " + SOL
