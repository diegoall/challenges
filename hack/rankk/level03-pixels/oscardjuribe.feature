## Version 2.0
## language: en

Feature: rankk-pixels
  Site:
    https://www.rankk.org/challenges/pixels.py
  User:
    alestorm980 (wechall)
  Goal:
    Count the number of non-white pixels

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
    | Python          | 2.7         |
  Machine information:
    Given A ".png" image
    Then I have to count the different non-white pixels
    Then I have to concatenate the values

  Scenario: Success: Create a Python script
    Given The image
    When I create a script "pixels.py"
    Then I count the different pixels values in a dictionary
    When I run the Python script
    Then I get the output
    """
    the solution is <flag>
    """
    And I solve the challenge
