## Version 2.0
## language: en

Feature: Pretty good - miscellaneous - rankk
  Site:
    Rankk
  User:
    pperez7 (wechall)
  Goal:
    Solve the challenge

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Windows         |     1809      |
    | Google Chrome   | 79.0.3945.130 |
  Machine information:
    Given a link in the challenge's page
    When I access it
    Then I can see the following text
    """
    Passphrase: limestone

    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v1.4.5 (MingW32)

    hQEOA5xTp2/IoMK7EAP/YwK6A9EbfRYkO2C5Aa24z8IpBYSvFyuFv5X8WJ/FPDif
    gZIAP5D2uGMrH1r9xcKolZEdgZnY3QdTvv8Lmkw8FLScYSph+YgVUf3M7+jPtXWO
    3QnWaDVqCXZGlm3+bP33j3Wh/eTGs//sUGQw/2dsuGlAH6dM588Oqaco/MQ9U9AD
    /1u/B9ekBSDBVeWIpo+V2S6f14t+Kg7hgFm0GB9YE3aO801wixWeC2gRkEiJKFmB
    DrW8Y8ev92jn84YfsgRX99483lezujsUsCr8PZ5mhknFniirg5USumQc5N2fQTag
    W5frIZfSi5F/+H77Ul4ieDB91PuSHaVKHXFJUuCJxfB40pUBUw2Ard2ZMI5bL4jK
    DvaLRsFwAw4/p9fgOfl4mKl+Jgh8HwPki5s+ncOneXLszCJbA7iEGhAcr/AcHiJi
    9wjUXK6RHmSM9iGdoP3oWMX5dt37+8e8PYxHm26BbcnUvdu3heD0En0SfgMP3emU
    yifsLNO3HKrDc5cMNugVP7MtSBjhs3yspJucOWQWsCZ6e6eyFpZWVQ==
    =8pit
    -----END PGP MESSAGE-----
    """

  Scenario: Fail: Trying the passphrase as the solution
    Given The challenge's text
    When I put the passphrase in the input text
    Then I press the submit button
    And I get "wrong" for that solution

  Scenario: Decrypting the PGP message
    Given The PGP message
    And the passphrase
    When I go to the source code of the page
    Then I see the next message
    """
    <!-- Btw, if you need MD5, we've got one @ files/md5.exe //-->
    """
    When I access to that URL
    Then I download that "md5.exe" file
    When I use the "strings" command to get the strings of that file
    Then I notice that in there I can see the PGP private key
    Given the PGP message
    And the PGP private key
    And the passphrase
    When I go to the online tool for PGP decrypting at the following Site
    """
    https://www.igolder.com/PGP/decryption/
    """
    Then I enter all the information I need to decrypt the PGP message
    And I get the <FLAG>
    When I enter the <FLAG> at the input text of the challenge
    Then I press the submit button
    And I solve the challenge
