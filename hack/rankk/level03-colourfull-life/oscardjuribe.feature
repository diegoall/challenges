## Version 2.0
## language: en

Feature: level03-colourfull-life
  Site:
    https://www.rankk.org/challenges/colourful-life.py
  User:
    alestorm980 (wechall)
  Goal:
    Get the flag from colored message

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
  Machine information:
    Given the challenge description
    And some colored characters

  Scenario: Fail: Convert all hex to text
    Given some colored characters
    When I inspect the source code
    Then I find the HEX format of the color
    When I copy all the HEX values
    Then I try to convert them into text
    And nothing happens

  Scenario: Success: Code to decode the message
    Given some colored characters
    When I inspect the source code
    Then I see that some HEX values starts with similar colors
    """
    <span style="color: #FFFF63">c</span>
    <span style="color: #FFFF6F">o</span>
    <span style="color: #FFFF6C">d</span>
    <span style="color: #FFFF6F">e</span>
    <span style="color: #FFFF75">b</span>
    <span style="color: #FFFF72">r</span>
    <span style="color: #FFFF63">e</span>
    <span style="color: #FFFF6F">a</span>
    <span style="color: #FFFF64">k</span>
    <span style="color: #FFFF65">e</span>
    <span style="color: #FFFF64">r</span>
    """
    When I copy the source code to a ".txt" file
    Then I create a python script to decode
    When I create a regular expression to match HEX formats
    Then I take only the values starting with "#FFFF"
    """
    #FFFF63 #FFFF6F
    #FFFF6C #FFFF6F
    #FFFF75 #FFFF72
    #FFFF63 #FFFF6F
    #FFFF64 #FFFF65
    #FFFF64
    """
    When I take the last to characters
    Then I convert them into text
    When I finish the script
    Then I run "python oscardjuribe.py"
    """
    The solution is: <flag>
    """
    And I get the password of the challenge

