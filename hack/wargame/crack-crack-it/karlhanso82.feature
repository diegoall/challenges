# Version 2.0
## language: en

Feature: Training programming
  Site:
    http://wargame.kr
  Category:
    cracking
    password recovery
  User:
    karlhanso82
  Goal:
    crack the password given the file and clues

 Background:
 Hacker's software:
   | <Software name> | <Version>    |
   | Windows OS      | 10           |
   | Chromium        | 74.0.3729.131|

 Machine information:
  Given the challenge URL
  """
  http://wargame.kr:8080/crack_crack_crack_it/
  """
  Then I open the url with Chromium
  And I see a description of the challenge
  """
  oops, i forgot my password!! somebody help me T_T
  (i remember that my password begin with 'G4HeulB' and the other chars
  is composed of number, lower alphabet only
  notice: this .htaccess file is different each other IP Address
  .authentication have to be same ip plz,
  """
  Then I see a file to download .htpassword

 Scenario: Success:getting-the-password
  Then I decide to use john the ripper
  And Perform a brute for attack
  Given the htpassword in the file
  """
  blueh4g:$1$A.HY4xd8$3PSdFp9Hws6aPbScLQYQG0
  """
  Then I use python script riper.py to feed jhon
  """
  import itertools as iter
  charac = 'abcdefghijklmnopqrstuvwxyz0123456789'
  min_lg = 2
  max_lg = 5
  hint   = 'G4HeulB'
  for i in range ( min_lg , max_lg + 1 ) :
      for s in iter.product( charac , repeat = i ) :
            print hint+''.join(s)
  """
  And then run along the ripper
  """
  python riper.py | john -stdin htpassword
  """
  Then it takes a while to return the password
  And then it solve the challenge

