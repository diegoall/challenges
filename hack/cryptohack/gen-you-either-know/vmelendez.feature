## Version 2.0
## language: en

Feature: general-you-either-know
  Site:
    Cryptohack
  Category:
    General
  User:
    Mbaku
  Goal:
    Decrypt the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Python          | 3.8.5     |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see a description with the following sentence
    """
    I've encrypted the flag with my secret key, you'll never
    be able to guess it.
    """
    And a hint
    """
    Remember the flag format and how it might help you in this challenge!
    """
    Then a string in hexadecimal is delivered

  Scenario: Sucess:Get xor key
    Given it gives us a string in hexadecimal
    And tells us that it was encrypted (XOR) with a secret key
    Then it's possible to get the xor key knowing something about the plaintext
    And this is what the hint tells us
    """
    The flag format: crypto{
    """
    Then following the maths
    """
    plaintext ^ key        = ciphertext
    plaintext ^ ciphertext = key
    ciphertext ^ key       = plaintext
    """
    And because the inverse of XOR is itself
    Then we can get the xor key
    And I wrote a python script to get it [evidences](xorkey.png)
    """
    string = [i for i in bytes.fromhex("0e0b213f26041e480b26217f27342"
             "e175d0e070a3c5b103e2526217f27342e175d0e077e263451150104")]
    flag_format = "crypto{"

    print (string)
    key = ""
    for i in range(len(flag_format)):
        key += chr(ord(flag_format[i]) ^ string[i])

    print (key)
    """
    Then I got the xor key

  Scenario: Success:Get the flag
    Given that I know what the xor key is
    Then the idea is use it to XOR the ciphertext (hexadecimal string)
    And get the flag
    Then I wrote a python script to get it [evidences](solver.py)
    """
    flag = ""
    for i in range(len(string)):
        flag += chr(ord(key[i % len(key)]) ^ string[i])

    print (flag)
    """
    And this way I got the flag [evidences](flag.png)
