## Version 2.0
## language: en

Feature: Easy Phish - OSINT - Hack the Box
  Site:
    https://www.hackthebox.eu
  Category:
    OSINT
  User:
    arguoK
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A text
    """
    Customers of secure-startup.com have been recieving some very
    convincing phishing emails, can you figure out why?
    """
    And the flag format
    """
    HTB{flag}
    """
    And challenge category OSINT

  Scenario: Fail: Incomplete flag
    Given The information text
    And Category OSINT
    When I google "phishing"
    Then I find an article about SPF "Sender Policy Framework"
    When I analyze the link in "https://www.dmarcanalyzer.com/spf/"
    """
    Problems

    We've noticed the following problems with your SPF records.
    Error : Unknown parts found in your SPF record.
    These may invalidate the record
    Warning : There is data after the closing "all" or "redirect":
    this may be ignored. Extra data: htb{----
    Warning : No valid MX record found for the domain secure-startup.com
    """
    Then I see "htb{" format and a part of a Flag
    And I need to continue searching

  Scenario: Success: Flag split in SPF record and DMARC policies
    Given The information text
    And category OSINT
    And first scenario knowledge
    When I search for another tool to examine the URL
    Then I find the google apps toolbox in
    """
    https://toolbox.googleapps.com/apps/checkmx/
    """
    When I scan the URL "secure-startup.com"
    Then I get a better description and data [evidence](img1.png)
    And I can see the flag format splitted among SPF records and DMARC policies
    And I caught the flag
