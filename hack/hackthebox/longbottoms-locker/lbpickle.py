#!/usr/bin/env python
# Python lbpickle.py
"""
Prints a serialized text banner
"""

import pickle

with open('donotshare', 'rb') as f:
    ARR = pickle.load(f)
    for i in enumerate(len(ARR)):
        OUT = ''
        GRP = ARR[i]
        for j in enumerate(len(GRP)):
            for k in enumerate(GRP[j][1]):
                OUT += GRP[j][0]
        print(OUT)
