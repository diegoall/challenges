## Version 2.0
## language: en

Feature: Impossible Password - Reversing - Hack the Box
  Site:
    https://www.hackthebox.eu
  Category:
    Reversing
  User:
    arguoK
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A ZIP file
    And it contains a "impossible_password.bin" file
    And A text "Are you able to cheat me and get the flag?"
    And the flag format "HTB{flag}"
    And challenge category Reversing

  Scenario: Fail: Strings in the file
    Given The information text
    And Category Reversing
    When I use command "strings imposible_password.bin"
    Then I cant see anything legible
    And I don't find anything useful

  Scenario: Fail: Executable file
    Given The file
    When I use the command "file impossible_password.bin"
    """
    impossible_password.bin: ELF 64-bit LSB executable, x86-64,
    version 1 (SYSV), dynamically linked, interpreter /lib64/l, for
    GNU/Linux 2.6.32, BuildID[sha1]=ba116ba1912a8c3779ddeb579404e2fdf34b1568
    """
    Then I can see it is an executable
    When I execute it
    Then It asks for a parameter
    When I write anything
    Then I get the same string back
    And I don't find the flag

  Scenario: Success: Executable file
    Given The file "impossible_password.bin" inside the ZIP file
    And category Reversing
    When I use an online program disassembler
    Then I see the file function contents in assembler [evidence](img1.png)
    And I know the function have 20 hex numbers getting XOR by "9"
    When I get the result of the XOR
    Then I can see the flag "HTB{flag}" format
    And I caught the flag
