## Version 2.0
## language: en

Feature: dragon-assasson-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Firefox         | 74.0 (64-bit)       |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/dragon_51996aa769df79afbf79eb4d66dbcef6.php
    """
    When I open the url with Firefox
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_dragon where id='guest'# and pw=''
    """

  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
    DRAGON Clear!
    """
    When I try with the well known payload:
    """
    ?pw=' or '1'='1
    """
    Then I get nothing

   Scenario: Success:Sqli-space-technique
    Given I inspect the code one more time
    And I see that the query is using "#" to comment the rest
    Then I use the ASCII new line character "%0A"
    And a well known payload
    """
    ?pw='%0A or id='admin' order by id asc --+
    """
    Then I solve the challenge
