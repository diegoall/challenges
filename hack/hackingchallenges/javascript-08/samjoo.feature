## Version 2.0
## language: en

Feature: javascript-08 - javascript - hacking-challenges
  Site:
    hacking-challenges.de
  Category:
    javascript
  User:
    samjoo
  Goal:
    Find the flag from inspecting javascript code

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows10       | 2004        |
    | Chromium        | 85.0        |
  Machine information:
    Given I am accessing the challenge through a web browser
    And I see the following text
    """
    6 Stellen
    """
    And I know the challenge is releated to javascript code
    Then I start the challenge

  Scenario: Success:Inspect the web page source to find the flag
    Given I am in the challegen page
    And I see a html form
    Then I open the chrome dev tools
    And I inspect the source code
    And I find a javascript function at line 160 with the following code
    """
    function pwd(){
    var login=document.hackit.eingabe.value;
      if (login.length == 6)
      {
      var p1 = login.charAt(0);
      var p2 = login.charAt(1);
      var p3 = login.charAt(2);
      var p4 = login.charAt(3);
      var p5 = login.charAt(4);
      var p6 = login.charAt(5);
      var WebSite=p4+p1+p5+p2+p6+p3;
        if (WebSite==""+135024+"") {
          alert ("Das Passwort ist richtig");
        }
        else {
          alert ("Passwort falsch");
        }
      }
    }
    """
    Then I inspect the code
    And I see It is expecting a number ordered n a specific sequence
    """
    var WebSite=p4+p1+p5+p2+p6+p3; //WebSite = 135024
    """
    Then I then organize the sequence
    """
    p4 = 1; p1 = 3; p5 = 5; p2 = 0; p6 = 2; p3 = 4
    p1 = 3; p2; = 0; p3 = 4; p4 = 1; p5 = 5; p6 = 2
    """
    Then I put the next sequence in the text box to check the answer
    """
    304152
    """
    And I got the following answer
    """
    Das Passwort ist richtig
    """
    And I can conclude I got the flag
