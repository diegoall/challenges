# language: en

Feature: Solve the challenge IRC-Chall I - Query
  From the happy-security.de website
  From the Internet Realy Chat category
  With my username raballestas

  Background:
    Given instructions to request the password

  Scenario: Successful solution
    When I log into the #happy-security IRC channel
    And I send a private message to a user
    Then I get the password as reply
    And I solve the challenge
