#lenguage: en

Feature: Solve Railsbin challenge
  From the WeChall site
  Of the category Exploit
  As the registered user disassembly

  Background:
    Given the source code of the project railsbin
    And a link to the demo site
    Given I have the Safari web browser version 11.0.3

  Scenario Outline: Find the password hash of user solution

  Scenario: Vulnerability recognition
    When I dig the source code, I found the users controller
    """
    railsbin/app/controllers/users_controller.rb
    """
    Then the first two functions caught my attention
    """
    4   # GET /users
    5   # GET /users.json
    6   def index
    7     @users = User.all
    8     @users.map {|u| u.password = u.encrypted_password }
    9   end
    11  # GET /users/1
    12  # GET /users/1.json
    13  def show
    14  end
    """

  Scenario: First attempt to find the solution
    When I access the /users path
    Then I see the list of users with name, email and passwords
    But the password hash is too short and changes every time
    When I look the second path /users/17
    Then I see a different hash that doesn't change but again it's too short

  Scenario: Find the solution
    When I search information for first part of the hash
    """
    $2a$10$...
    """
    Then I found is a bcrypt hash in modular crypt format with 60 characters
    Then I look the json endpoint /users/17.json
    """
    {
      "id":17,
      "name":"solution",
      "email":"solution@wechall.net",
      "password":null,
      "created_at":"2017-12-19T18:08:22.000Z",
      "updated_at":"2017-12-19T18:08:34.000Z"
    }
    """
    But doesn't show me any passwords
    Then I look the other json endpoint /users.json
    """
    [...
    {
      "id":17,"name":"solution",
      "email":"solution@wechall.net",
      "password":"$2a$10$44GwiA6ai0wxjzhFkeyjuO3kdVvmco8ReypH7H1tLsM2OrRFhe4CK",
      "url":"http://railsbin.wechall.net/users/17.json"
    },
    ...]
    """
    Then I check the password have 60 chars, C&P the hash and solve the chall
