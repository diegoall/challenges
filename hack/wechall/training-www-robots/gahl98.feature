## Version 2.0
## language: en

Feature: http training wechall
  Site:
    https://www.wechall.net
  Category:
    http training
  User:
    gahl98
  Goal:
    Learn about the robots exclusion standard failures

  Background:
    Hacker's software:
      | <Software name> | <Version>    |
      | Zorin OS        | 12.4         |
      | Chromium        | 73.0.3683.86 |

  Machine information:
    Given the challenge URL
    """
    https://www.wechall.net/challenge/training/www/robots/index.php
    """
    Then I opened the URL with Chromium
    And I see the challenge description
    """
    In this little training challenge, you are going to learn about the
    Robots_exclusion_standard.
    The robots.txt file is used by web crawlers to check if they are allowed
    to crawl and index your website or only parts of it.
    Sometimes these files reveal the directory structure instead of protecting
    the content from being crawled.

    Enjoy!
    """
    And I follow the information link
    """
    https://en.wikipedia.org/wiki/Robots_exclusion_standard
    """
    And I read the information about Robots_exclusion_standard
    Then I write on the navigation bar the address
    """
    https://www.wechall.net/robots.txt
    """
    And I see that loaded page says
    """
    User-agent: *
    Disallow: /challenge/training/www/robots/T0PS3CR3T
    User-agent: Yandex
    Disallow: *
    """
    Then I go to that adress
    """
    https://www.wechall.net/challenge/training/www/robots/T0PS3CR3T/
    """
    And I see it says
    """
    Your answer is correct
    """
    And I see it ask to evaluate the challenge
