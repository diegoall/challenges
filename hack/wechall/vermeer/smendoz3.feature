## Version 2.0
## language: en

Feature: Vermeer - Stegano - WeChall
  Site:
    https://www.wechall.net/
  Category:
    Stegano
  User:
    smendoz3
  Goal:
    Find secret word

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A message
    """
    S12x12a42x10d15x13l40x10y31x14,40x13 28x10t22x16h28x16i15x16s35x12
    43x10i31x12s25x12 20x14n15x15o12x15t35x13 43x15t25x16h27x11e40x14
    20x13m17x10e21x10s34x11s10x16a10x15g20x12e25x11 25x14y20x16o12x16u15x14
    41x16a20x15r37x13e25x10 34x15l26x12o34x12o36x14k37x14i42x14n37x11g27x15
    37x15f26x13o36x15r25x13 31x13:34x10(31x11 11x13T25x15r12x10y17x13
    31x10f16x10u35x11r34x14t12x11h20x10e37x12r40x16!17x11o22x10o10x12o42x13o
    43x14o40x15o37x16o41x10o10x10o31x15o12x14o15x10o12x13o17x14o10x11o43x13o
    42x16o15x11o10x13o34x16o40x11o15x12o34x13o21x16o17x12o17x16o40x12o26x14o
    10x14o20x11o17x15o31x16o43x16o37x10o36x13o16x13
    """
    And sentence "If Picasso got a computer he would know what to do!"

  Scenario: Fail: Lose the game
    Given The encoded message
    And Category Stegano
    Then I see there is a pattern
    When I get each character multiple of 6
    Then I get the message
    """
    Sadly, this is not the message you are looking for :( try further!
    """
    But I don't solve the challenge

  Scenario: Success: Find the secret
    Given The encoded message
    And Category Stegano
    Then I asume there is useless information in the message
    When I split in 6 characters strings
    Then I see that first character of each string make a sentence
    """
    Sadly, this is not the message you are looking for :( try further!
    """
    When I discard the letters and remain with 5 characters strings
    Then I realize they have a format of NNxNN with N being a number
    When I map those strings as coordinates in a 2-dimension array
    Then I get the secret message written in dots
    And I solve the challenge
