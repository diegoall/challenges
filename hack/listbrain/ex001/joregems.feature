## Version 2.0
## language: en

Feature: 001-EX-listbrain
   Site:
    listbrain.awardspace.biz
  Category:
    EX
  User:
    joregems
  Goal:
    Discover the decrypted text

    Background:
  Hacker's software:
    |<software>            |<version>        |
    | Ubuntu               | 18.10 x64       |
    | Firefox Quantum      | 66.0.2 (64-bit) |
  Machine information:
    Given I access to the challenge through
    """
    http://listbrain.awardspace.biz/index.php?p=EX001
    """

  Scenario: Success: find robot.txt
    Given I access to the challenges page
    """
    http://listbrain.awardspace.biz/index.php?p=challenges
    """
    Then I see a clue
    """
    The G*****bot
    """
    And the clue I got, I open
    """
    http://listbrain.awardspace.biz/robots.txt
    """
    Then I see
    """
    User-agent: *
    Disallow: *
    Disallow: /passwordxyz/
    """

  Scenario: Success: following new the new clues
    Given I see a directory disallowed for robots I go to
    """
    http://listbrain.awardspace.biz/passwordxyz/
    """
    And see
    """
    good job the password for EX001 is "robotica"
    """

  Scenario: Success: solving the challenge
    Given I know the password
    Then I go to
    """
    http://listbrain.awardspace.biz/index.php?p=EX001
    """
    And put "robotica" and I solve the challenge
