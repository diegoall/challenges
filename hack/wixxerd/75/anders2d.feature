## Version 2.0
## language: en

Feature: 75-programming-wixxerd
  Code:
    75
  Site:
    https://www.wixxerd.com
  Category:
    programming
  User:
    anders2d
  Goal:
     Produce a query that generates two columns.
     1. if Contains boolean "true" when the person'sdd
     role is "Janitor", "false" in all other instances.
     2."Person" : Contains the user's name, sorted ascending.

  Background:
  Hacker's software:
    | <Software name>   |  <Version>    |
    | Windows           | 10.0.1809     |
    | Chrome            | 76.0.3809.132 |
  Machine information:
    Given I am accessing to input to run queries
    And PostresSQL version is PostgreSQL 9.3.21
    And is running on Ubuntu 4.8.4

  Scenario: Success:alias-column-order
    Given I know the database schemea
    When I use the next query
    """
    SELECT CASE when roles.id=1 then 'true' else 'false' end as Custodian,
    users.UNAME as Person
    FROM users
        INNER JOIN user_roles on (user_roles.USERID = users.ID)
        INNER JOIN roles on (roles.ID = user_roles.ROLEID)
    ORDER by users.UNAME asc
    """
    Then I solved the challenge
    And I caught the flag
