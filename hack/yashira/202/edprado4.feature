Feature: Solve Adivina el Numero challenge
  from site Yashira
  logged as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: Challenge solved
  Given A bulls and cows game in the last turn
  And bulls and cows data from the previous turns
  When I build a truth table with the requirements of the number
  And I compare my possible solutions with the table
  Then I find a number that matches all the requirements
  And I use that number as password to solve the challenge

