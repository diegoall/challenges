## Version 2.0
## language: en

Feature: 008 Hash-8 yashira
  Site:
    yashira
  User:
    Jdiegoc (yashira)
  Goal:
    Decrypting a RIPEMD-160 hash with hashcat

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali            | 2019.4      |
    | Chrome          | 79.0.3945.88|
    | hashcat         | 4.0.1       |
  Machine information:
    Given a RIPEMD-160 hash
    When I identify that it was a RIPEMD-160 hash
    Then I learn to decipher it
    And I decipher it

  Scenario: Success: hashcat
    Given the RIPEMD-160 hash
    When using hashcat
    Then I saw that it was with RIPEMD-160
    And I run it with """hashcat -a 0 -m 6000 password.hash --show"""
    Then I took the exit
    """
    Found : <flag>
    """
    And I solve the challenge
