## Version 2.0
## language: en

Feature: 136-H4CK1NG-enigmes-a-thematiques
  Code:
    136
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  Machine information:
    Given I am accessing the website

  Scenario: Fail:edit-html-code
    Given I am on the challenge page
    And I can see a form with a field and a "Valider" button
    When I accept the mission
    Then I can see a form with an input and an "Authentification" button
    And I can see the message
    """
    Bonjour, si tu veux pénétrer dans ce domaine, c'est très facile :
    le mot de passe est : foobar.
    """
    When I submit the form without fill the input
    Then Appears a message
    """
    Accès refusé !
    """
    When I inspect the source Code
    Then I see the form
    """
    <form name="frm" action="admin.php" method="GET" onsubmit="return false">
      <input type="text" name="rep">
      <input type="submit" value="Authentification">
    </form>
    """
    When I attempt to set the "foobar" as the input value
    And The "Authentification" button does not work
    Then I can not capture the flag

  Scenario: Success:make-URL
    Given I am on the main page
    When I search for the registration form using the GET method
    Then I find how looks like the submission URL
    And I realized I do not need the "Authentification" button
    When I make the URL with the given information
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/
    ep136.php/admin.php/?rep=foobar
    """
    And I load the URL in the browser
    Then Appears a new message
    """
    Accès autorisé !
    Le mot de passe pour valider l'épreuve est : r4z0r
    """
    Then I can see the "<FLAG>"
    When I inter the "<FLAG>" to pass the challenge
    And I press the "Valider" button
    Then I can see the message "Bravo"
    And I capture the flag
