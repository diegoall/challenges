## Version 2.0
## language: en

Feature: encrypted-zip
  Site:
    https://ringzer0ctf.com
  Category:
    cryptography
  User:
    AndresCL94
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version> |
  | Ubuntu (Bionic) | 18.04.1   |
  | Zip             | 3.0       |
  | Pkcrack         | 1.2.2     |
  | 7z              | 16.02     |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/29
    """
    Then I see a button to download a zip file
    When I extract the files from the archive
    Then I obtain two zip files and a text file

  Scenario: Fail: Pkcrack-Zip
    Given that the two zip files were encrypted using the same password
    And that the text file is the uncompressed version from one of them
    Then I investigate possible ways to approach this issue
    And I find a tool to perform a "Known Plaintext Attack" on Github
    """
    https://github.com/keyunluo/pkcrack
    """
    Given that I need to compress the plaintext file
    Then I decide to use the "zip" program to do this
    When I run the "pkcrack" tool
    Then it cannot find the password
    And I do not get the flag

  Scenario: Success: Pkcrack-7z
    Given that the previous attack did not work
    And that the compression method must be the same for the plaintext
    When I investigate more about how to determine the compression tool
    Then I find some interesting parameters like the size and the CRC
    """
    https://www.elcomsoft.com/help/en/archpr/known_plaintext_attack_(zip).html

    CRC must be the same
    Size of the original zip must be 12 bytes larger
    """
    When I analyze the files I am using
    """
    original-zip
    CRC: A78A01E7
    Size: 97

    plaintext-zip
    CRC: A78A01E7
    Size: 86
    """
    And I notice one condition is not fulfilled
    And I decide to use another compression tool
    When I use "7z" to compress the plaintext
    Then I get a compressed file that fulfills both conditions
    When I execute "pkcrack" with the new files
    Then I get the password
    And I can decrypt and extract the remaining archive
    And I get the flag
