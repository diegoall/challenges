################################################################################
#### Commit template for solutions to challenges in writeups/vbd only ##########
#### Version 1.1.2 #############################################################
################################################################################
#
# Fill the fields in angle brackets <> as indicated in these comments.
# These comments will not appear in the final commit message.
# <system-name> is the name of the system
# <CWE> is the four digit CWE code of the vulnerability
# <page-name> is the place where the vulnerability can be found
# example: sol(vbd): #0 juice-shop, 0640-backup-file
# The line after the commit title and the final line MUST be blank,
# but there must not be a blank line at the beggining.
#
####### begin template #######
sol(vbd): #0 <system-name>, <CWE>-<page-name>

- discovered vulnerabilities: X by me, Y already in repo, X+Y total.
- total estimated vulnerabilities in system:   N
- discovery percentage:                        D%
- effort: X hours during <challenges/immersion>.

######## end template ########
#
# X is the ACCUMULATED number of vulnerabilites in this system discovered by you
# Y is the ACCUMULATED number of vulnerabilites already reported in the repo,
# not including those previously reported by you.
# All of these can be zero if you find a vulnerability in a brand new ToE.
# N is the estimated number of vulnerabilities in the system. This number
# could be in the official documentation of the system or a site like VulnHub.
# D is (X+Y)*100/N, with at most two decimals.
# Effort is the number of hours you worked on the vulnerability,
# not necessarily in one sitting. For example if you dedicated two hours today
# and later 20 minutes, and three more hours the day after, that's
# 5 hours and 20 minutes = 5.3 hours.
# Effort report must cover also the time required
# for searching existing solutions (OTHERS),
# as well as linting and compiling time before sending the MR.
# If the MR gets rejected, additional time used on fixing it has to be added
#
# Full example:
# sol(vbd): #0 juice-shop, 0711-fdback-anthr-usrnm
#
# - discovered vulnerabilities: 15 by me, 2 already in repo, 17 total.
# - total estimated vulnerabilities in system: 72
# - discovery percentage: 23%
# - effort: 1 hours during immersion.
#
# Next time you find a vulnerability in the same system,
# if other people have reported 3 vulnerabilities in that time,
# you will have 16 by me, 5 already in repo, 21 total.
#
# To use this template automatically every time you commit, run
# $ git config --local commit.template templates/commit-msg-vbd.txt
#
# Remember to make ONE commit for each challenge solution.
# This commit must also include the appropriate number of OTHERS solutions,
# which is currently 10.
# Then make ONE Merge Request with that ONE commit for ONE challenge,
# but ONLY after the pipeline has succeeded.
# See https://fluidattacks.com/web/es/empleos/retos-tecnicos/#envio.
