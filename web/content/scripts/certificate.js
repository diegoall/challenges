const Pdf = new jsPDF();
const Qrious = new QRious();
function getQR(username) {
  const qruser = new Qrious({
    background: 'transparent',
    foreground: 'rgb(108, 108, 108)',
    size: 150,
    value: `${window.location.href}users/${username}/`,
  });
  return qruser.toDataURL();
}
function getDataUri(url, callfunc, userNode) {
  const image = document.createElement('img');
  image.setAttribute('crossOrigin', 'anonymous');
  image.addEventListener('load', () => {
    const canvas = document.createElement('canvas');
    canvas.setAttribute('width', image.naturalWidth);
    canvas.setAttribute('height', image.naturalHeight);
    const ctx = canvas.getContext('2d');
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    canvas.getContext('2d').drawImage(image, 0, 0);
    callfunc(canvas.toDataURL('image/jpeg'), userNode);
    return 0;
  });
  image.setAttribute('src', url);
  return 0;
}
const doc = new Pdf('l', 'px', [900, 693]);
function ordinalsuffixof(i) {
  const modA = 10;
  const modB = 100;
  const endC = 3;
  const endD = 11;
  const endE = 12;
  const endF = 13;
  const nToSuffix = i % modA;
  const especialSuffix = i % modB;
  if (nToSuffix === 1 && especialSuffix !== endD) {
    return `${i}st`;
  }
  if (nToSuffix === 2 && especialSuffix !== endE) {
    return `${i}nd`;
  }
  if (nToSuffix === endC && especialSuffix !== endF) {
    return `${i}rd`;
  }
  return `${i}th`;
}
function createPDF(logo, userNode) {
  const rowNode = userNode.parentNode.parentNode.parentNode;
  const mid = rowNode.innerText.split('\n');
  const userData = mid.filter((val) => (mid.indexOf(val) % 2) === 0);
  const position = ordinalsuffixof(userData[0]);
  const username = userData[1 + 0];
  const uniquesols = userData[1 + 1];
  const sols = userData[2 + 1];
  const date = new Date();
  const digitsA = 10;
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const dayN = (day < digitsA) ? `0${day}` : day;
  const monthN = (month < digitsA) ? `0${month}` : month;
  const qrcode = getQR(username);
  const fullDate = [dayN, monthN, year].join('    ');
  doc.addImage(logo, 'jpg', 0, 0);
  const PosQR = [524, 346];
  doc.addImage(qrcode, 'jpg', PosQR[0], PosQR[1]);
  doc.setFont('Courier');
  const RGBH = 108;
  doc.setTextColor(RGBH, RGBH, RGBH);
  const SizeK = 65;
  doc.setFontSize(SizeK);
  doc.setFontType('bold');
  const posSols = [400, 262, 290];
  doc.text(uniquesols, posSols[0], posSols[2], 'center');
  doc.text(sols, posSols[1], posSols[2], 'center');
  const posUser = [340, 200, 49];
  doc.setFontSize(posUser[2]);
  doc.setFontType('');
  doc.text(username, posUser[0], posUser[1], 'center');
  const SizeG = 26;
  doc.setFontSize(SizeG + 2 * 2 * 2);
  const PosPos = [340, 390];
  doc.text(position, PosPos[0], PosPos[1], 'center');
  doc.setFontSize(SizeG);
  const PosDate = [360, 448];
  doc.text(fullDate, PosDate[0], PosDate[1], 'center');
  const HostName = [window.location.href,
    340,
    505,
    20];
  doc.setFontSize(HostName[3]);
  doc.text(HostName[0], HostName[1], HostName[2], 'center');
  doc.save(`${username}.pdf`);
  return 0;
}
function certificate(userN) {
  const userNode = userN;
  getDataUri('./gallery/certificate.png', createPDF, userNode);
  return 0;
}
document.body.addEventListener('click', (event) => {
  if (event.target.nodeName === 'BUTTON') {
    certificate(event.target);
    return 0;
  }
  return 0;
});
document.body.addEventListener('click', (event) => {
  if (event.target.parentNode.nodeName === 'BUTTON') {
    certificate(event.target.parentNode);
    return 0;
  }
  return 0;
});
