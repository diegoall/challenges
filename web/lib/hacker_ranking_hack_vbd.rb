# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength

def user_ranking_hack_vbd(folder)
  users, totals = user_ranking_init_hashes(folder, nil)
  users.each_key do |user|
    sltns = Dir.glob("#{folder}/**/#{user}.feature")
    sltns.each do |sltn|
      users[user]['sltns'] += 1
      totals['sltns'] += 1
      if check_sltn_unique_hack_vbd(sltn)
        users[user]['unique-sltns'] += 1
        totals['unique-sltns'] += 1
      end
      page = sltn.split('/')[1]
      unless users[user]['pages'].include?(page)
        users[user]['pages'].push(page)
        users[user]['npages'] += 1
      end
      unless totals['pages'].include?(page)
        totals['pages'].push(page)
        totals['npages'] += 1
      end
    end
  end
  users = add_percentage(users)
  [users, totals]
end

def user_ranking_hack
  user_ranking_hack_vbd(@config[:const][:hack_dir])
end

def user_ranking_vbd
  user_ranking_hack_vbd(@config[:const][:vbd_dir])
end
