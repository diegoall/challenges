# frozen_string_literal: true

# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/CyclomaticComplexity
# rubocop:disable Metrics/PerceivedComplexity

require 'etc'
require 'yaml'
require 'open3'
require 'parallel'
require 'time'

def git_user_stats_users_statistics
  users = { code_users: {}, hack_users: {}, vbd_users: {} }
  threads = Etc.nprocessors
  code_dir = @config[:const][:code_dir]
  hack_dir = @config[:const][:hack_dir]
  vbd_dir = @config[:const][:vbd_dir]
  yml_path = @config[:const][:lang_data]
  lang_info = YAML.safe_load(File.read(yml_path))
  code_sltns = Dir.glob("#{code_dir}/**/*.*")
  hack_sltns = Dir.glob("#{hack_dir}/**/*.feature")
  vbd_sltns = Dir.glob("#{vbd_dir}/**/*.feature")
  code_sltns = code_sltns.reject { |path| path.end_with? 'DATA.lst' }
  code_sltns = code_sltns.reject { |path| path.end_with? 'LINK.lst' }
  code_sltns = code_sltns.reject { |path| path.end_with? 'OTHERS.lst' }
  Parallel.each(code_sltns,
                in_threads: threads) do |code_sltn|
    git_user_stats_users_time(users[:code_users], code_sltn, lang_info,
                              code_dir)
  end
  Parallel.each(hack_sltns,
                in_threads: threads) do |hack_sltn|
    git_user_stats_users_time(users[:hack_users], hack_sltn, lang_info,
                              hack_dir)
  end
  Parallel.each(vbd_sltns,
                in_threads: threads) do |vbd_sltn|
    git_user_stats_users_time(users[:vbd_users], vbd_sltn, lang_info,
                              vbd_dir)
  end
  git_user_stats_postprocess(users)
  users
end

def git_user_stats_postprocess(users)
  users[:general_users] = add_hashes(users[:code_users], users[:hack_users])
  users[:general_users] = add_hashes(users[:general_users], users[:vbd_users])
  git_user_stats_totals(users)
  users[:general_users].each_key do |user|
    unless users[:general_users][user][:time].zero?
      users[:general_users][user][:avg] =
        users[:general_users][user][:sltns] / users[:general_users][user][:time]
    end
    users[:general_users][user] =
      git_user_stats_postprocess_user(user, 'Total', :general_users, users)
    users[:code_users][user] =
      git_user_stats_postprocess_user(user, 'Code', :code_users, users)
    users[:hack_users][user] =
      git_user_stats_postprocess_user(user, 'Hack', :hack_users, users)
    users[:vbd_users][user] =
      git_user_stats_postprocess_user(user, 'Vbd', :vbd_users, users)
  end
  users[:totals].each_value do |hash|
    hash[:avg] = format('%.2f', hash[:avg])
  end
end

def git_user_stats_totals(users)
  users[:totals] = {
    code_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 },
    hack_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 },
    vbd_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 },
    general_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 }
  }
  users.each_key do |hash|
    next if hash == :totals

    users[hash].each_value do |user|
      users[:totals][hash][:time] += user[:time]
      users[:totals][hash][:sltns] += user[:sltns]
      users[:totals][hash][:score] += user[:score]
    end
    next if users[:totals][hash][:sltns].zero?

    users[:totals][hash][:avg] =
      users[:totals][hash][:sltns] / users[:totals][hash][:time]
    users[:totals][hash][:score] =
      users[:totals][hash][:score] / users[:totals][hash][:sltns]
  end
end

def git_user_stats_postprocess_user(user, hash_name, type, users)
  if users[type][user].nil? || users[type][user][:sltns].zero?
    users[type][user] = {
      time: 0.0, sltns: 0, avg: 0.0, score: 0.0,
      first_sltn_date: 'NA', last_sltn_date: 'NA'
    }
  end
  users[type][user][:name] = hash_name
  users[type][user][:avg] = format('%.2f', users[type][user][:avg])
  users[type][user][:score] = format('%.2f', users[type][user][:score])
  unless users[type][user][:first_sltn_date] == 'NA'
    users[type][user][:first_sltn_date] =
      users[type][user][:first_sltn_date].strftime('%F %T')
    users[type][user][:last_sltn_date] =
      users[type][user][:last_sltn_date].strftime('%F %T')
  end
  users[type][user]
end

def git_user_stats_users_time(users, sltn, lang_info, type)
  score_multiplier = 0.5 if code_dir?(type)
  score_multiplier = 0.7 if hack_dir?(type) || vbd_dir?(type)
  return if code_dir?(type) && !check_valid_sltn_code(sltn, lang_info)

  repo = sltn_repo(sltn)
  sltn = prepare_sltn_git(sltn)
  user = sltn.split('/')[-1].split('.')[0]
  users[user] = { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 } if
    users[user].nil?
  command = "git -C #{repo} log --diff-filter=A --follow #{sltn}"
  stdout, _, status = Open3.capture3(command)
  return unless status.success?

  date = Time.parse(stdout.split("\n")[2].strip)
  effort = stdout.split("\n").last.strip
  if users[user][:first_sltn_date].nil?
    users[user][:first_sltn_date] = date
    users[user][:last_sltn_date] = date
  end
  users[user][:first_sltn_date] = [users[user][:first_sltn_date], date].min
  users[user][:last_sltn_date] = [users[user][:last_sltn_date], date].max
  users[user][:sltns] += 1
  users[user][:time] += git_user_stats_parse_hours(effort)
  users[user][:avg] = users[user][:sltns] / users[user][:time] unless
    users[user][:time].zero?
  users[user][:score] = users[user][:avg] * score_multiplier
end

def git_user_stats_parse_hours(string)
  hours_regex = /^- effort: (\d+)(.\d+)?/
  string = string.strip
  return 0.0 if string.match(hours_regex).nil?

  captures = string.match(hours_regex).captures
  hours = captures[0]
  hours += captures[1] unless captures[1].nil?
  hours.to_f
end
